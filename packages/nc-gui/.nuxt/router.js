import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _f2b57e1c = () => interopDefault(import('../pages/apiClient.vue' /* webpackChunkName: "pages/apiClient" */))
const _66123407 = () => interopDefault(import('../pages/inspire.vue' /* webpackChunkName: "pages/inspire" */))
const _52387d9b = () => interopDefault(import('../pages/nc/index.vue' /* webpackChunkName: "pages/nc/index" */))
const _09343c40 = () => interopDefault(import('../pages/projects/index.vue' /* webpackChunkName: "pages/projects/index" */))
const _40dba171 = () => interopDefault(import('../pages/user/index.vue' /* webpackChunkName: "pages/user/index" */))
const _288e79cc = () => interopDefault(import('../pages/error/400.vue' /* webpackChunkName: "pages/error/400" */))
const _2839ecc6 = () => interopDefault(import('../pages/error/403.vue' /* webpackChunkName: "pages/error/403" */))
const _281dbdc4 = () => interopDefault(import('../pages/error/404.vue' /* webpackChunkName: "pages/error/404" */))
const _fd15113a = () => interopDefault(import('../pages/project/edit.vue' /* webpackChunkName: "pages/project/edit" */))
const _e0a54938 = () => interopDefault(import('../pages/project/name.vue' /* webpackChunkName: "pages/project/name" */))
const _685f18b3 = () => interopDefault(import('../pages/project/templates/index.vue' /* webpackChunkName: "pages/project/templates/index" */))
const _0e80a2fc = () => interopDefault(import('../pages/project/xcdb.vue' /* webpackChunkName: "pages/project/xcdb" */))
const _e94b4400 = () => interopDefault(import('../pages/projects/list.vue' /* webpackChunkName: "pages/projects/list" */))
const _38393171 = () => interopDefault(import('../pages/user/admin/index.vue' /* webpackChunkName: "pages/user/admin/index" */))
const _2f1a74a8 = () => interopDefault(import('../pages/user/authentication/index.vue' /* webpackChunkName: "pages/user/authentication/index" */))
const _32865662 = () => interopDefault(import('../pages/user/password/index.vue' /* webpackChunkName: "pages/user/password/index" */))
const _62b0e752 = () => interopDefault(import('../pages/user/settings/index.vue' /* webpackChunkName: "pages/user/settings/index" */))
const _d97e2a2a = () => interopDefault(import('../pages/user/admin/user-edit.vue' /* webpackChunkName: "pages/user/admin/user-edit" */))
const _9bfeb44a = () => interopDefault(import('../pages/user/authentication/callback.vue' /* webpackChunkName: "pages/user/authentication/callback" */))
const _cc0e1b76 = () => interopDefault(import('../pages/user/authentication/passwordValidateMixin.js' /* webpackChunkName: "pages/user/authentication/passwordValidateMixin" */))
const _6e137390 = () => interopDefault(import('../pages/user/authentication/signin.vue' /* webpackChunkName: "pages/user/authentication/signin" */))
const _4e1f2e9e = () => interopDefault(import('../pages/user/authentication/signup/index.vue' /* webpackChunkName: "pages/user/authentication/signup/index" */))
const _967e3314 = () => interopDefault(import('../pages/user/password/forgot.vue' /* webpackChunkName: "pages/user/password/forgot" */))
const _4ac9804f = () => interopDefault(import('../pages/user/password/reset/index.vue' /* webpackChunkName: "pages/user/password/reset/index" */))
const _65d6831e = () => interopDefault(import('../pages/user/settings/accounts.vue' /* webpackChunkName: "pages/user/settings/accounts" */))
const _a66f4ab4 = () => interopDefault(import('../pages/user/settings/password.vue' /* webpackChunkName: "pages/user/settings/password" */))
const _65fc0343 = () => interopDefault(import('../pages/user/settings/picture.vue' /* webpackChunkName: "pages/user/settings/picture" */))
const _87243b24 = () => interopDefault(import('../pages/user/settings/profile.vue' /* webpackChunkName: "pages/user/settings/profile" */))
const _38e88ed7 = () => interopDefault(import('../pages/user/password/reset/form.vue' /* webpackChunkName: "pages/user/password/reset/form" */))
const _747b49b4 = () => interopDefault(import('../pages/user/password/reset/invalid.vue' /* webpackChunkName: "pages/user/password/reset/invalid" */))
const _61b2b1c0 = () => interopDefault(import('../pages/user/password/reset/success.vue' /* webpackChunkName: "pages/user/password/reset/success" */))
const _4e42de52 = () => interopDefault(import('../pages/user/admin/user/_userId.vue' /* webpackChunkName: "pages/user/admin/user/_userId" */))
const _6a3775aa = () => interopDefault(import('../pages/user/authentication/signup/_token.vue' /* webpackChunkName: "pages/user/authentication/signup/_token" */))
const _07090197 = () => interopDefault(import('../pages/nc/base/_shared_base_id.vue' /* webpackChunkName: "pages/nc/base/_shared_base_id" */))
const _017e0836 = () => interopDefault(import('../pages/nc/form/_id.vue' /* webpackChunkName: "pages/nc/form/_id" */))
const _a7d6902e = () => interopDefault(import('../pages/nc/kanban/_id.vue' /* webpackChunkName: "pages/nc/kanban/_id" */))
const _622dc517 = () => interopDefault(import('../pages/nc/view/_id.vue' /* webpackChunkName: "pages/nc/view/_id" */))
const _039ca84a = () => interopDefault(import('../pages/project/templates/_id.vue' /* webpackChunkName: "pages/project/templates/_id" */))
const _3d804ea9 = () => interopDefault(import('../pages/nc/_project_id.vue' /* webpackChunkName: "pages/nc/_project_id" */))
const _c543611e = () => interopDefault(import('../pages/project/_id.vue' /* webpackChunkName: "pages/project/_id" */))
const _1e082234 = () => interopDefault(import('../pages/project/id.vue' /* webpackChunkName: "pages/project/id" */))
const _6ced0f3f = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'hash',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/apiClient",
    component: _f2b57e1c,
    name: "apiClient"
  }, {
    path: "/inspire",
    component: _66123407,
    name: "inspire"
  }, {
    path: "/nc",
    component: _52387d9b,
    name: "nc"
  }, {
    path: "/projects",
    component: _09343c40,
    name: "projects"
  }, {
    path: "/user",
    component: _40dba171,
    name: "user"
  }, {
    path: "/error/400",
    component: _288e79cc,
    name: "error-400"
  }, {
    path: "/error/403",
    component: _2839ecc6,
    name: "error-403"
  }, {
    path: "/error/404",
    component: _281dbdc4,
    name: "error-404"
  }, {
    path: "/project/edit",
    component: _fd15113a,
    name: "project-edit"
  }, {
    path: "/project/name",
    component: _e0a54938,
    name: "project-name"
  }, {
    path: "/project/templates",
    component: _685f18b3,
    name: "project-templates"
  }, {
    path: "/project/xcdb",
    component: _0e80a2fc,
    name: "project-xcdb"
  }, {
    path: "/projects/list",
    component: _e94b4400,
    name: "projects-list"
  }, {
    path: "/user/admin",
    component: _38393171,
    name: "user-admin"
  }, {
    path: "/user/authentication",
    component: _2f1a74a8,
    name: "user-authentication"
  }, {
    path: "/user/password",
    component: _32865662,
    name: "user-password"
  }, {
    path: "/user/settings",
    component: _62b0e752,
    name: "user-settings"
  }, {
    path: "/user/admin/user-edit",
    component: _d97e2a2a,
    name: "user-admin-user-edit"
  }, {
    path: "/user/authentication/callback",
    component: _9bfeb44a,
    name: "user-authentication-callback"
  }, {
    path: "/user/authentication/passwordValidateMixin",
    component: _cc0e1b76,
    name: "user-authentication-passwordValidateMixin"
  }, {
    path: "/user/authentication/signin",
    component: _6e137390,
    name: "user-authentication-signin"
  }, {
    path: "/user/authentication/signup",
    component: _4e1f2e9e,
    name: "user-authentication-signup"
  }, {
    path: "/user/password/forgot",
    component: _967e3314,
    name: "user-password-forgot"
  }, {
    path: "/user/password/reset",
    component: _4ac9804f,
    name: "user-password-reset"
  }, {
    path: "/user/settings/accounts",
    component: _65d6831e,
    name: "user-settings-accounts"
  }, {
    path: "/user/settings/password",
    component: _a66f4ab4,
    name: "user-settings-password"
  }, {
    path: "/user/settings/picture",
    component: _65fc0343,
    name: "user-settings-picture"
  }, {
    path: "/user/settings/profile",
    component: _87243b24,
    name: "user-settings-profile"
  }, {
    path: "/user/password/reset/form",
    component: _38e88ed7,
    name: "user-password-reset-form"
  }, {
    path: "/user/password/reset/invalid",
    component: _747b49b4,
    name: "user-password-reset-invalid"
  }, {
    path: "/user/password/reset/success",
    component: _61b2b1c0,
    name: "user-password-reset-success"
  }, {
    path: "/user/admin/user/:userId?",
    component: _4e42de52,
    name: "user-admin-user-userId"
  }, {
    path: "/user/authentication/signup/:token",
    component: _6a3775aa,
    name: "user-authentication-signup-token"
  }, {
    path: "/nc/base/:shared_base_id?",
    component: _07090197,
    name: "nc-base-shared_base_id"
  }, {
    path: "/nc/form/:id?",
    component: _017e0836,
    name: "nc-form-id"
  }, {
    path: "/nc/kanban/:id?",
    component: _a7d6902e,
    name: "nc-kanban-id"
  }, {
    path: "/nc/view/:id?",
    component: _622dc517,
    name: "nc-view-id"
  }, {
    path: "/project/templates/:id",
    component: _039ca84a,
    name: "project-templates-id"
  }, {
    path: "/nc/:project_id",
    component: _3d804ea9,
    name: "nc-project_id"
  }, {
    path: "/project/:id?",
    component: _c543611e,
    children: [{
      path: "",
      component: _1e082234,
      name: "project-id"
    }]
  }, {
    path: "/",
    component: _6ced0f3f,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
