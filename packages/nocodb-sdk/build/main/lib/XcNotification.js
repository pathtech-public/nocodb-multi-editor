"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.XcNotificationType = void 0;
var XcNotificationType;
(function (XcNotificationType) {
    XcNotificationType["EMAIL"] = "Email";
    XcNotificationType["URL"] = "URL";
    XcNotificationType["DISCORD"] = "Discord";
    XcNotificationType["TELEGRAM"] = "Telegram";
    XcNotificationType["SLACK"] = "Slack";
    XcNotificationType["WHATSAPP"] = "Whatsapp";
    XcNotificationType["TWILIO"] = "Twilio";
})(XcNotificationType = exports.XcNotificationType || (exports.XcNotificationType = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiWGNOb3RpZmljYXRpb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvbGliL1hjTm90aWZpY2F0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUtBLElBQVksa0JBUVg7QUFSRCxXQUFZLGtCQUFrQjtJQUM1QixxQ0FBZSxDQUFBO0lBQ2YsaUNBQVcsQ0FBQTtJQUNYLHlDQUFtQixDQUFBO0lBQ25CLDJDQUFxQixDQUFBO0lBQ3JCLHFDQUFlLENBQUE7SUFDZiwyQ0FBcUIsQ0FBQTtJQUNyQix1Q0FBaUIsQ0FBQTtBQUNuQixDQUFDLEVBUlcsa0JBQWtCLEdBQWxCLDBCQUFrQixLQUFsQiwwQkFBa0IsUUFRN0IifQ==