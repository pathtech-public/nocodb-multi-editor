"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isVirtualCol = void 0;
var UITypes;
(function (UITypes) {
    UITypes["ID"] = "ID";
    UITypes["LinkToAnotherRecord"] = "LinkToAnotherRecord";
    UITypes["ForeignKey"] = "ForeignKey";
    UITypes["Lookup"] = "Lookup";
    UITypes["SingleLineText"] = "SingleLineText";
    UITypes["LongText"] = "LongText";
    UITypes["Attachment"] = "Attachment";
    UITypes["Checkbox"] = "Checkbox";
    UITypes["MultiSelect"] = "MultiSelect";
    UITypes["SingleSelect"] = "SingleSelect";
    UITypes["Collaborator"] = "Collaborator";
    UITypes["Date"] = "Date";
    UITypes["Year"] = "Year";
    UITypes["Time"] = "Time";
    UITypes["PhoneNumber"] = "PhoneNumber";
    UITypes["Email"] = "Email";
    UITypes["URL"] = "URL";
    UITypes["Number"] = "Number";
    UITypes["Decimal"] = "Decimal";
    UITypes["Currency"] = "Currency";
    UITypes["Percent"] = "Percent";
    UITypes["Duration"] = "Duration";
    UITypes["Rating"] = "Rating";
    UITypes["Formula"] = "Formula";
    UITypes["Rollup"] = "Rollup";
    UITypes["Count"] = "Count";
    UITypes["DateTime"] = "DateTime";
    UITypes["CreateTime"] = "CreateTime";
    UITypes["LastModifiedTime"] = "LastModifiedTime";
    UITypes["AutoNumber"] = "AutoNumber";
    UITypes["Geometry"] = "Geometry";
    UITypes["JSON"] = "JSON";
    UITypes["SpecificDBType"] = "SpecificDBType";
    UITypes["Barcode"] = "Barcode";
    UITypes["Button"] = "Button";
})(UITypes || (UITypes = {}));
function isVirtualCol(col) {
    return [
        UITypes.SpecificDBType,
        UITypes.LinkToAnotherRecord,
        UITypes.Formula,
        UITypes.Rollup,
        UITypes.Lookup,
        // UITypes.Count,
    ].includes((typeof col === 'object' ? col === null || col === void 0 ? void 0 : col.uidt : col));
}
exports.isVirtualCol = isVirtualCol;
exports.default = UITypes;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVUlUeXBlcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvVUlUeXBlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFFQSxJQUFLLE9Bb0NKO0FBcENELFdBQUssT0FBTztJQUNWLG9CQUFTLENBQUE7SUFDVCxzREFBMkMsQ0FBQTtJQUMzQyxvQ0FBeUIsQ0FBQTtJQUN6Qiw0QkFBaUIsQ0FBQTtJQUNqQiw0Q0FBaUMsQ0FBQTtJQUNqQyxnQ0FBcUIsQ0FBQTtJQUNyQixvQ0FBeUIsQ0FBQTtJQUN6QixnQ0FBcUIsQ0FBQTtJQUNyQixzQ0FBMkIsQ0FBQTtJQUMzQix3Q0FBNkIsQ0FBQTtJQUM3Qix3Q0FBNkIsQ0FBQTtJQUM3Qix3QkFBYSxDQUFBO0lBQ2Isd0JBQWEsQ0FBQTtJQUNiLHdCQUFhLENBQUE7SUFDYixzQ0FBMkIsQ0FBQTtJQUMzQiwwQkFBZSxDQUFBO0lBQ2Ysc0JBQVcsQ0FBQTtJQUNYLDRCQUFpQixDQUFBO0lBQ2pCLDhCQUFtQixDQUFBO0lBQ25CLGdDQUFxQixDQUFBO0lBQ3JCLDhCQUFtQixDQUFBO0lBQ25CLGdDQUFxQixDQUFBO0lBQ3JCLDRCQUFpQixDQUFBO0lBQ2pCLDhCQUFtQixDQUFBO0lBQ25CLDRCQUFpQixDQUFBO0lBQ2pCLDBCQUFlLENBQUE7SUFDZixnQ0FBcUIsQ0FBQTtJQUNyQixvQ0FBeUIsQ0FBQTtJQUN6QixnREFBcUMsQ0FBQTtJQUNyQyxvQ0FBeUIsQ0FBQTtJQUN6QixnQ0FBcUIsQ0FBQTtJQUNyQix3QkFBYSxDQUFBO0lBQ2IsNENBQWlDLENBQUE7SUFDakMsOEJBQW1CLENBQUE7SUFDbkIsNEJBQWlCLENBQUE7QUFDbkIsQ0FBQyxFQXBDSSxPQUFPLEtBQVAsT0FBTyxRQW9DWDtBQUVELFNBQWdCLFlBQVksQ0FDMUIsR0FJYztJQUVkLE9BQU87UUFDTCxPQUFPLENBQUMsY0FBYztRQUN0QixPQUFPLENBQUMsbUJBQW1CO1FBQzNCLE9BQU8sQ0FBQyxPQUFPO1FBQ2YsT0FBTyxDQUFDLE1BQU07UUFDZCxPQUFPLENBQUMsTUFBTTtRQUNkLGlCQUFpQjtLQUNsQixDQUFDLFFBQVEsQ0FBVSxDQUFDLE9BQU8sR0FBRyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBRyxhQUFILEdBQUcsdUJBQUgsR0FBRyxDQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztBQUNuRSxDQUFDO0FBZkQsb0NBZUM7QUFFRCxrQkFBZSxPQUFPLENBQUMifQ==