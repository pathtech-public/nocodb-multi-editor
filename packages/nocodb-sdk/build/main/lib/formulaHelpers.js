"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.jsepTreeToFormula = exports.substituteColumnIdWithAliasInFormula = exports.substituteColumnAliasWithIdInFormula = exports.jsepCurlyHook = void 0;
const jsep_1 = __importDefault(require("jsep"));
exports.jsepCurlyHook = {
    name: 'curly',
    init(jsep) {
        jsep.hooks.add('gobble-token', function gobbleCurlyLiteral(env) {
            const OCURLY_CODE = 123; // {
            const CCURLY_CODE = 125; // }
            const { context } = env;
            if (!jsep.isIdentifierStart(context.code) &&
                context.code === OCURLY_CODE) {
                context.index += 1;
                const nodes = context.gobbleExpressions(CCURLY_CODE);
                if (context.code === CCURLY_CODE) {
                    context.index += 1;
                    env.node = {
                        type: jsep.IDENTIFIER,
                        name: nodes.map((node) => node.name).join(' '),
                    };
                    return env.node;
                }
                else {
                    context.throwError('Unclosed }');
                }
            }
        });
    },
};
async function substituteColumnAliasWithIdInFormula(formula, columns) {
    const substituteId = async (pt) => {
        if (pt.type === 'CallExpression') {
            for (const arg of pt.arguments || []) {
                await substituteId(arg);
            }
        }
        else if (pt.type === 'Literal') {
            return;
        }
        else if (pt.type === 'Identifier') {
            const colNameOrId = pt.name;
            const column = columns.find((c) => c.id === colNameOrId ||
                c.column_name === colNameOrId ||
                c.title === colNameOrId);
            pt.name = '{' + column.id + '}';
        }
        else if (pt.type === 'BinaryExpression') {
            await substituteId(pt.left);
            await substituteId(pt.right);
        }
    };
    // register jsep curly hook
    jsep_1.default.plugins.register(exports.jsepCurlyHook);
    const parsedFormula = (0, jsep_1.default)(formula);
    await substituteId(parsedFormula);
    return jsepTreeToFormula(parsedFormula);
}
exports.substituteColumnAliasWithIdInFormula = substituteColumnAliasWithIdInFormula;
function substituteColumnIdWithAliasInFormula(formula, columns, rawFormula) {
    const substituteId = (pt, ptRaw) => {
        var _a;
        if (pt.type === 'CallExpression') {
            let i = 0;
            for (const arg of pt.arguments || []) {
                substituteId(arg, (_a = ptRaw === null || ptRaw === void 0 ? void 0 : ptRaw.arguments) === null || _a === void 0 ? void 0 : _a[i++]);
            }
        }
        else if (pt.type === 'Literal') {
            return;
        }
        else if (pt.type === 'Identifier') {
            const colNameOrId = pt === null || pt === void 0 ? void 0 : pt.name;
            const column = columns.find((c) => c.id === colNameOrId ||
                c.column_name === colNameOrId ||
                c.title === colNameOrId);
            pt.name = (column === null || column === void 0 ? void 0 : column.title) || (ptRaw === null || ptRaw === void 0 ? void 0 : ptRaw.name) || (pt === null || pt === void 0 ? void 0 : pt.name);
        }
        else if (pt.type === 'BinaryExpression') {
            substituteId(pt.left, ptRaw === null || ptRaw === void 0 ? void 0 : ptRaw.left);
            substituteId(pt.right, ptRaw === null || ptRaw === void 0 ? void 0 : ptRaw.right);
        }
    };
    // register jsep curly hook
    jsep_1.default.plugins.register(exports.jsepCurlyHook);
    const parsedFormula = (0, jsep_1.default)(formula);
    const parsedRawFormula = rawFormula && (0, jsep_1.default)(rawFormula);
    substituteId(parsedFormula, parsedRawFormula);
    return jsepTreeToFormula(parsedFormula);
}
exports.substituteColumnIdWithAliasInFormula = substituteColumnIdWithAliasInFormula;
function jsepTreeToFormula(node) {
    if (node.type === 'BinaryExpression' || node.type === 'LogicalExpression') {
        return ('(' +
            jsepTreeToFormula(node.left) +
            ' ' +
            node.operator +
            ' ' +
            jsepTreeToFormula(node.right) +
            ')');
    }
    if (node.type === 'UnaryExpression') {
        return node.operator + jsepTreeToFormula(node.argument);
    }
    if (node.type === 'MemberExpression') {
        return (jsepTreeToFormula(node.object) +
            '[' +
            jsepTreeToFormula(node.property) +
            ']');
    }
    if (node.type === 'Identifier') {
        const formulas = [
            'AVG',
            'ADD',
            'DATEADD',
            'WEEKDAY',
            'AND',
            'OR',
            'CONCAT',
            'TRIM',
            'UPPER',
            'LOWER',
            'LEN',
            'MIN',
            'MAX',
            'CEILING',
            'FLOOR',
            'ROUND',
            'MOD',
            'REPEAT',
            'LOG',
            'EXP',
            'POWER',
            'SQRT',
            'SQRT',
            'ABS',
            'NOW',
            'REPLACE',
            'SEARCH',
            'INT',
            'RIGHT',
            'LEFT',
            'SUBSTR',
            'MID',
            'IF',
            'SWITCH',
            'URL',
        ];
        if (!formulas.includes(node.name))
            return '{' + node.name + '}';
        return node.name;
    }
    if (node.type === 'Literal') {
        if (typeof node.value === 'string') {
            return '"' + node.value + '"';
        }
        return '' + node.value;
    }
    if (node.type === 'CallExpression') {
        return (jsepTreeToFormula(node.callee) +
            '(' +
            node.arguments.map(jsepTreeToFormula).join(', ') +
            ')');
    }
    if (node.type === 'ArrayExpression') {
        return '[' + node.elements.map(jsepTreeToFormula).join(', ') + ']';
    }
    if (node.type === 'Compound') {
        return node.body.map((e) => jsepTreeToFormula(e)).join(' ');
    }
    if (node.type === 'ConditionalExpression') {
        return (jsepTreeToFormula(node.test) +
            ' ? ' +
            jsepTreeToFormula(node.consequent) +
            ' : ' +
            jsepTreeToFormula(node.alternate));
    }
    return '';
}
exports.jsepTreeToFormula = jsepTreeToFormula;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybXVsYUhlbHBlcnMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvbGliL2Zvcm11bGFIZWxwZXJzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLGdEQUF3QjtBQUlYLFFBQUEsYUFBYSxHQUFHO0lBQzNCLElBQUksRUFBRSxPQUFPO0lBQ2IsSUFBSSxDQUFDLElBQUk7UUFDUCxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsU0FBUyxrQkFBa0IsQ0FBQyxHQUFHO1lBQzVELE1BQU0sV0FBVyxHQUFHLEdBQUcsQ0FBQyxDQUFDLElBQUk7WUFDN0IsTUFBTSxXQUFXLEdBQUcsR0FBRyxDQUFDLENBQUMsSUFBSTtZQUM3QixNQUFNLEVBQUUsT0FBTyxFQUFFLEdBQUcsR0FBRyxDQUFDO1lBQ3hCLElBQ0UsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztnQkFDckMsT0FBTyxDQUFDLElBQUksS0FBSyxXQUFXLEVBQzVCO2dCQUNBLE9BQU8sQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDO2dCQUNuQixNQUFNLEtBQUssR0FBRyxPQUFPLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3JELElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxXQUFXLEVBQUU7b0JBQ2hDLE9BQU8sQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDO29CQUNuQixHQUFHLENBQUMsSUFBSSxHQUFHO3dCQUNULElBQUksRUFBRSxJQUFJLENBQUMsVUFBVTt3QkFDckIsSUFBSSxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO3FCQUMvQyxDQUFDO29CQUNGLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQztpQkFDakI7cUJBQU07b0JBQ0wsT0FBTyxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQztpQkFDbEM7YUFDRjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztDQUNjLENBQUM7QUFFWCxLQUFLLFVBQVUsb0NBQW9DLENBQ3hELE9BQU8sRUFDUCxPQUFxQjtJQUVyQixNQUFNLFlBQVksR0FBRyxLQUFLLEVBQUUsRUFBTyxFQUFFLEVBQUU7UUFDckMsSUFBSSxFQUFFLENBQUMsSUFBSSxLQUFLLGdCQUFnQixFQUFFO1lBQ2hDLEtBQUssTUFBTSxHQUFHLElBQUksRUFBRSxDQUFDLFNBQVMsSUFBSSxFQUFFLEVBQUU7Z0JBQ3BDLE1BQU0sWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ3pCO1NBQ0Y7YUFBTSxJQUFJLEVBQUUsQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO1lBQ2hDLE9BQU87U0FDUjthQUFNLElBQUksRUFBRSxDQUFDLElBQUksS0FBSyxZQUFZLEVBQUU7WUFDbkMsTUFBTSxXQUFXLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQztZQUM1QixNQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsSUFBSSxDQUN6QixDQUFDLENBQUMsRUFBRSxFQUFFLENBQ0osQ0FBQyxDQUFDLEVBQUUsS0FBSyxXQUFXO2dCQUNwQixDQUFDLENBQUMsV0FBVyxLQUFLLFdBQVc7Z0JBQzdCLENBQUMsQ0FBQyxLQUFLLEtBQUssV0FBVyxDQUMxQixDQUFDO1lBQ0YsRUFBRSxDQUFDLElBQUksR0FBRyxHQUFHLEdBQUcsTUFBTSxDQUFDLEVBQUUsR0FBRyxHQUFHLENBQUM7U0FDakM7YUFBTSxJQUFJLEVBQUUsQ0FBQyxJQUFJLEtBQUssa0JBQWtCLEVBQUU7WUFDekMsTUFBTSxZQUFZLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzVCLE1BQU0sWUFBWSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM5QjtJQUNILENBQUMsQ0FBQztJQUNGLDJCQUEyQjtJQUMzQixjQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxxQkFBYSxDQUFDLENBQUM7SUFDckMsTUFBTSxhQUFhLEdBQUcsSUFBQSxjQUFJLEVBQUMsT0FBTyxDQUFDLENBQUM7SUFDcEMsTUFBTSxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDbEMsT0FBTyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsQ0FBQztBQUMxQyxDQUFDO0FBOUJELG9GQThCQztBQUVELFNBQWdCLG9DQUFvQyxDQUNsRCxPQUFPLEVBQ1AsT0FBcUIsRUFDckIsVUFBVztJQUVYLE1BQU0sWUFBWSxHQUFHLENBQUMsRUFBTyxFQUFFLEtBQVcsRUFBRSxFQUFFOztRQUM1QyxJQUFJLEVBQUUsQ0FBQyxJQUFJLEtBQUssZ0JBQWdCLEVBQUU7WUFDaEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ1YsS0FBSyxNQUFNLEdBQUcsSUFBSSxFQUFFLENBQUMsU0FBUyxJQUFJLEVBQUUsRUFBRTtnQkFDcEMsWUFBWSxDQUFDLEdBQUcsRUFBRSxNQUFBLEtBQUssYUFBTCxLQUFLLHVCQUFMLEtBQUssQ0FBRSxTQUFTLDBDQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUM1QztTQUNGO2FBQU0sSUFBSSxFQUFFLENBQUMsSUFBSSxLQUFLLFNBQVMsRUFBRTtZQUNoQyxPQUFPO1NBQ1I7YUFBTSxJQUFJLEVBQUUsQ0FBQyxJQUFJLEtBQUssWUFBWSxFQUFFO1lBQ25DLE1BQU0sV0FBVyxHQUFHLEVBQUUsYUFBRixFQUFFLHVCQUFGLEVBQUUsQ0FBRSxJQUFJLENBQUM7WUFDN0IsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FDekIsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUNKLENBQUMsQ0FBQyxFQUFFLEtBQUssV0FBVztnQkFDcEIsQ0FBQyxDQUFDLFdBQVcsS0FBSyxXQUFXO2dCQUM3QixDQUFDLENBQUMsS0FBSyxLQUFLLFdBQVcsQ0FDMUIsQ0FBQztZQUNGLEVBQUUsQ0FBQyxJQUFJLEdBQUcsQ0FBQSxNQUFNLGFBQU4sTUFBTSx1QkFBTixNQUFNLENBQUUsS0FBSyxNQUFJLEtBQUssYUFBTCxLQUFLLHVCQUFMLEtBQUssQ0FBRSxJQUFJLENBQUEsS0FBSSxFQUFFLGFBQUYsRUFBRSx1QkFBRixFQUFFLENBQUUsSUFBSSxDQUFBLENBQUM7U0FDcEQ7YUFBTSxJQUFJLEVBQUUsQ0FBQyxJQUFJLEtBQUssa0JBQWtCLEVBQUU7WUFDekMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsS0FBSyxhQUFMLEtBQUssdUJBQUwsS0FBSyxDQUFFLElBQUksQ0FBQyxDQUFDO1lBQ25DLFlBQVksQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssYUFBTCxLQUFLLHVCQUFMLEtBQUssQ0FBRSxLQUFLLENBQUMsQ0FBQztTQUN0QztJQUNILENBQUMsQ0FBQztJQUVGLDJCQUEyQjtJQUMzQixjQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxxQkFBYSxDQUFDLENBQUM7SUFDckMsTUFBTSxhQUFhLEdBQUcsSUFBQSxjQUFJLEVBQUMsT0FBTyxDQUFDLENBQUM7SUFDcEMsTUFBTSxnQkFBZ0IsR0FBRyxVQUFVLElBQUksSUFBQSxjQUFJLEVBQUMsVUFBVSxDQUFDLENBQUM7SUFDeEQsWUFBWSxDQUFDLGFBQWEsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO0lBQzlDLE9BQU8saUJBQWlCLENBQUMsYUFBYSxDQUFDLENBQUM7QUFDMUMsQ0FBQztBQWxDRCxvRkFrQ0M7QUFFRCxTQUFnQixpQkFBaUIsQ0FBQyxJQUFJO0lBQ3BDLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxrQkFBa0IsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLG1CQUFtQixFQUFFO1FBQ3pFLE9BQU8sQ0FDTCxHQUFHO1lBQ0gsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUM1QixHQUFHO1lBQ0gsSUFBSSxDQUFDLFFBQVE7WUFDYixHQUFHO1lBQ0gsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUM3QixHQUFHLENBQ0osQ0FBQztLQUNIO0lBRUQsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLGlCQUFpQixFQUFFO1FBQ25DLE9BQU8sSUFBSSxDQUFDLFFBQVEsR0FBRyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7S0FDekQ7SUFFRCxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssa0JBQWtCLEVBQUU7UUFDcEMsT0FBTyxDQUNMLGlCQUFpQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDOUIsR0FBRztZQUNILGlCQUFpQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDaEMsR0FBRyxDQUNKLENBQUM7S0FDSDtJQUVELElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxZQUFZLEVBQUU7UUFDOUIsTUFBTSxRQUFRLEdBQUc7WUFDZixLQUFLO1lBQ0wsS0FBSztZQUNMLFNBQVM7WUFDVCxTQUFTO1lBQ1QsS0FBSztZQUNMLElBQUk7WUFDSixRQUFRO1lBQ1IsTUFBTTtZQUNOLE9BQU87WUFDUCxPQUFPO1lBQ1AsS0FBSztZQUNMLEtBQUs7WUFDTCxLQUFLO1lBQ0wsU0FBUztZQUNULE9BQU87WUFDUCxPQUFPO1lBQ1AsS0FBSztZQUNMLFFBQVE7WUFDUixLQUFLO1lBQ0wsS0FBSztZQUNMLE9BQU87WUFDUCxNQUFNO1lBQ04sTUFBTTtZQUNOLEtBQUs7WUFDTCxLQUFLO1lBQ0wsU0FBUztZQUNULFFBQVE7WUFDUixLQUFLO1lBQ0wsT0FBTztZQUNQLE1BQU07WUFDTixRQUFRO1lBQ1IsS0FBSztZQUNMLElBQUk7WUFDSixRQUFRO1lBQ1IsS0FBSztTQUNOLENBQUM7UUFDRixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQUUsT0FBTyxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7UUFDaEUsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO0tBQ2xCO0lBRUQsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLFNBQVMsRUFBRTtRQUMzQixJQUFJLE9BQU8sSUFBSSxDQUFDLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDbEMsT0FBTyxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7U0FDL0I7UUFFRCxPQUFPLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO0tBQ3hCO0lBRUQsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLGdCQUFnQixFQUFFO1FBQ2xDLE9BQU8sQ0FDTCxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQzlCLEdBQUc7WUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDaEQsR0FBRyxDQUNKLENBQUM7S0FDSDtJQUVELElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxpQkFBaUIsRUFBRTtRQUNuQyxPQUFPLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUM7S0FDcEU7SUFFRCxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssVUFBVSxFQUFFO1FBQzVCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0tBQzdEO0lBRUQsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLHVCQUF1QixFQUFFO1FBQ3pDLE9BQU8sQ0FDTCxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzVCLEtBQUs7WUFDTCxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ2xDLEtBQUs7WUFDTCxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQ2xDLENBQUM7S0FDSDtJQUVELE9BQU8sRUFBRSxDQUFDO0FBQ1osQ0FBQztBQXhHRCw4Q0F3R0MifQ==