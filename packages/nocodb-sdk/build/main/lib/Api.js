"use strict";
/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Api = exports.HttpClient = exports.ContentType = void 0;
const axios_1 = __importDefault(require("axios"));
var ContentType;
(function (ContentType) {
    ContentType["Json"] = "application/json";
    ContentType["FormData"] = "multipart/form-data";
    ContentType["UrlEncoded"] = "application/x-www-form-urlencoded";
})(ContentType = exports.ContentType || (exports.ContentType = {}));
class HttpClient {
    constructor(_a = {}) {
        var { securityWorker, secure, format } = _a, axiosConfig = __rest(_a, ["securityWorker", "secure", "format"]);
        this.securityData = null;
        this.setSecurityData = (data) => {
            this.securityData = data;
        };
        this.request = async (_a) => {
            var { secure, path, type, query, format, wrapped, body } = _a, params = __rest(_a, ["secure", "path", "type", "query", "format", "wrapped", "body"]);
            const secureParams = ((typeof secure === 'boolean' ? secure : this.secure) &&
                this.securityWorker &&
                (await this.securityWorker(this.securityData))) ||
                {};
            const requestParams = this.mergeRequestParams(params, secureParams);
            const responseFormat = (format && this.format) || void 0;
            if (type === ContentType.FormData &&
                body &&
                body !== null &&
                typeof body === 'object') {
                requestParams.headers.common = { Accept: '*/*' };
                requestParams.headers.post = {};
                requestParams.headers.put = {};
                body = this.createFormData(body);
            }
            return this.instance
                .request(Object.assign(Object.assign({}, requestParams), { headers: Object.assign(Object.assign({}, (type && type !== ContentType.FormData
                    ? { 'Content-Type': type }
                    : {})), (requestParams.headers || {})), params: query, responseType: responseFormat, data: body, url: path }))
                .then((response) => {
                if (wrapped)
                    return response;
                return response.data;
            });
        };
        this.instance = axios_1.default.create(Object.assign(Object.assign({}, axiosConfig), { baseURL: axiosConfig.baseURL || 'http://localhost:8080' }));
        this.secure = secure;
        this.format = format;
        this.securityWorker = securityWorker;
    }
    mergeRequestParams(params1, params2) {
        return Object.assign(Object.assign(Object.assign(Object.assign({}, this.instance.defaults), params1), (params2 || {})), { headers: Object.assign(Object.assign(Object.assign({}, (this.instance.defaults.headers || {})), (params1.headers || {})), ((params2 && params2.headers) || {})) });
    }
    createFormData(input) {
        if (input instanceof FormData) {
            return input;
        }
        return Object.keys(input || {}).reduce((formData, key) => {
            const property = input[key];
            if (property instanceof Blob) {
                formData.append(key, property);
            }
            else if (typeof property === 'object' && property !== null) {
                if (Array.isArray(property)) {
                    // eslint-disable-next-line functional/no-loop-statement
                    for (const prop of property) {
                        formData.append(`${key}[]`, prop);
                    }
                }
                else {
                    formData.append(key, JSON.stringify(property));
                }
            }
            else {
                formData.append(key, `${property}`);
            }
            return formData;
        }, new FormData());
    }
}
exports.HttpClient = HttpClient;
/**
 * @title nocodb
 * @version 1.0
 * @baseUrl http://localhost:8080
 */
class Api extends HttpClient {
    constructor() {
        super(...arguments);
        this.auth = {
            /**
             * @description Create a new user with provided email and password and first user is marked as super admin.
             *
             * @tags Auth
             * @name Signup
             * @summary Signup
             * @request POST:/api/v1/auth/user/signup
             * @response `200` `{ token?: string }` OK
             * @response `400` `{ msg?: string }` Bad Request
             * @response `401` `void` Unauthorized
             * @response `403` `void` Forbidden
             */
            signup: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/user/signup`, method: 'POST', body: data, format: 'json' }, params)),
            /**
             * @description Authenticate existing user with their email and password. Successful login will return a JWT access-token.
             *
             * @tags Auth
             * @name Signin
             * @summary Signin
             * @request POST:/api/v1/auth/user/signin
             * @response `200` `{ token?: string }` OK
             * @response `400` `{ msg?: string }` Bad Request
             */
            signin: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/user/signin`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * @description Returns authenticated user info
             *
             * @tags Auth
             * @name Me
             * @summary User info
             * @request GET:/api/v1/auth/user/me
             * @response `200` `UserInfoType` OK
             */
            me: (query, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/user/me`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * @description Emails user with a reset url.
             *
             * @tags Auth
             * @name PasswordForgot
             * @summary Password forgot
             * @request POST:/api/v1/auth/password/forgot
             * @response `200` `void` OK
             * @response `401` `void` Unauthorized
             */
            passwordForgot: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/password/forgot`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * @description Change password of authenticated user with a new one.
             *
             * @tags Auth
             * @name PasswordChange
             * @summary Password change
             * @request POST:/api/v1/auth/password/change
             * @response `200` `{ msg?: string }` OK
             * @response `400` `{ msg?: string }` Bad request
             */
            passwordChange: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/password/change`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * @description Validtae password reset url token.
             *
             * @tags Auth
             * @name PasswordResetTokenValidate
             * @summary Reset token verify
             * @request POST:/api/v1/auth/token/validate/{token}
             * @response `200` `void` OK
             */
            passwordResetTokenValidate: (token, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/token/validate/${token}`, method: 'POST' }, params)),
            /**
             * @description Api for verifying email where token need to be passed which is shared to user email.
             *
             * @tags Auth
             * @name EmailValidate
             * @summary Verify email
             * @request POST:/api/v1/auth/email/validate/{token}
             * @response `200` `void` OK
             */
            emailValidate: (token, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/email/validate/${token}`, method: 'POST' }, params)),
            /**
             * @description Update user password to new by using reset token.
             *
             * @tags Auth
             * @name PasswordReset
             * @summary Password reset
             * @request POST:/api/v1/auth/password/reset/{token}
             * @response `200` `void` OK
             */
            passwordReset: (token, data, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/password/reset/${token}`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags Auth
             * @name TokenRefresh
             * @summary Refresh token
             * @request POST:/api/v1/auth/token/refresh
             * @response `200` `void` OK
             */
            tokenRefresh: (params = {}) => this.request(Object.assign({ path: `/api/v1/auth/token/refresh`, method: 'POST' }, params)),
            /**
             * No description
             *
             * @tags Auth
             * @name ProjectUserList
             * @summary Project users
             * @request GET:/api/v1/db/meta/projects/{projectId}/users
             * @response `200` `{ users?: { list: (UserType)[], pageInfo: PaginatedType } }` OK
             */
            projectUserList: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/users`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Auth
             * @name ProjectUserAdd
             * @summary Project User Add
             * @request POST:/api/v1/db/meta/projects/{projectId}/users
             * @response `200` `any` OK
             */
            projectUserAdd: (projectId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/users`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Auth
             * @name ProjectUserUpdate
             * @summary Project user update
             * @request PATCH:/api/v1/db/meta/projects/{projectId}/users/{userId}
             * @response `200` `any` OK
             */
            projectUserUpdate: (projectId, userId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/users/${userId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Auth
             * @name ProjectUserRemove
             * @summary Project user remove
             * @request DELETE:/api/v1/db/meta/projects/{projectId}/users/{userId}
             * @response `200` `any` OK
             */
            projectUserRemove: (projectId, userId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/users/${userId}`, method: 'DELETE', format: 'json' }, params)),
            /**
             * @description Resend Invitation to a specific user
             *
             * @tags Auth
             * @name ProjectUserResendInvite
             * @request POST:/api/v1/db/meta/projects/{projectId}/users/{userId}/resend-invite
             * @response `200` `any` OK
             */
            projectUserResendInvite: (projectId, userId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/users/${userId}/resend-invite`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
        };
        this.project = {
            /**
             * No description
             *
             * @tags Project
             * @name MetaGet
             * @summary Project info
             * @request GET:/api/v1/db/meta/projects/{projectId}/info
             * @response `200` `{ Node?: string, Arch?: string, Platform?: string, Docker?: boolean, Database?: string, ProjectOnRootDB?: string, RootDB?: string, PackageVersion?: string }` OK
             */
            metaGet: (projectId, params = {}, query) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/info`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name ModelVisibilityList
             * @summary UI ACL
             * @request GET:/api/v1/db/meta/projects/{projectId}/visibility-rules
             * @response `200` `(any)[]` OK
             */
            modelVisibilityList: (projectId, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/visibility-rules`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name ModelVisibilitySet
             * @request POST:/api/v1/db/meta/projects/{projectId}/visibility-rules
             * @response `200` `any` OK
             */
            modelVisibilitySet: (projectId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/visibility-rules`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * @description Read project details
             *
             * @tags Project
             * @name List
             * @summary Project list
             * @request GET:/api/v1/db/meta/projects/
             * @response `201` `ProjectListType`
             */
            list: (query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/`, method: 'GET', query: query }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name Create
             * @summary Project create
             * @request POST:/api/v1/db/meta/projects/
             * @response `200` `ProjectType` OK
             */
            create: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * @description Read project details
             *
             * @tags Project
             * @name Read
             * @summary Project read
             * @request GET:/api/v1/db/meta/projects/{projectId}
             * @response `200` `object` OK
             */
            read: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name Delete
             * @summary Project delete
             * @request DELETE:/api/v1/db/meta/projects/{projectId}
             * @response `200` `void` OK
             */
            delete: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}`, method: 'DELETE' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name Update
             * @summary Project update
             * @request PATCH:/api/v1/db/meta/projects/{projectId}
             * @response `200` `void` OK
             */
            update: (projectId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}`, method: 'PATCH', body: data, type: ContentType.Json }, params)),
            /**
             * @description Read project details
             *
             * @tags Project
             * @name SharedBaseGet
             * @request GET:/api/v1/db/meta/projects/{projectId}/shared
             * @response `200` `{ uuid?: string, url?: string, roles?: string }` OK
             */
            sharedBaseGet: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/shared`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name SharedBaseDisable
             * @request DELETE:/api/v1/db/meta/projects/{projectId}/shared
             * @response `200` `void` OK
             */
            sharedBaseDisable: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/shared`, method: 'DELETE' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name SharedBaseCreate
             * @request POST:/api/v1/db/meta/projects/{projectId}/shared
             * @response `200` `{ uuid?: string, url?: string, roles?: string }` OK
             */
            sharedBaseCreate: (projectId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/shared`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name SharedBaseUpdate
             * @request PATCH:/api/v1/db/meta/projects/{projectId}/shared
             * @response `200` `{ uuid?: string, url?: string, roles?: string }` OK
             */
            sharedBaseUpdate: (projectId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/shared`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * @description Project compare cost
             *
             * @tags Project
             * @name Cost
             * @summary Project compare cost
             * @request GET:/api/v1/db/meta/projects/{projectId}/cost
             * @response `200` `object` OK
             */
            cost: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/cost`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name MetaDiffSync
             * @request POST:/api/v1/db/meta/projects/{projectId}/meta-diff
             * @response `200` `any` OK
             */
            metaDiffSync: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/meta-diff`, method: 'POST', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name MetaDiffGet
             * @request GET:/api/v1/db/meta/projects/{projectId}/meta-diff
             * @response `200` `any` OK
             */
            metaDiffGet: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/meta-diff`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name AuditList
             * @request GET:/api/v1/db/meta/projects/{projectId}/audits
             * @response `200` `{ list: (AuditType)[], pageInfo: PaginatedType }` OK
             */
            auditList: (projectId, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/audits`, method: 'GET', query: query, format: 'json' }, params)),
        };
        this.dbTable = {
            /**
             * No description
             *
             * @tags DB table
             * @name Create
             * @request POST:/api/v1/db/meta/projects/{projectId}/tables
             * @response `200` `TableType` OK
             */
            create: (projectId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/tables`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table
             * @name List
             * @request GET:/api/v1/db/meta/projects/{projectId}/tables
             * @response `200` `TableListType`
             */
            list: (projectId, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/tables`, method: 'GET', query: query }, params)),
            /**
             * No description
             *
             * @tags DB table
             * @name Read
             * @request GET:/api/v1/db/meta/tables/{tableId}
             * @response `200` `TableInfoType` OK
             */
            read: (tableId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table
             * @name Update
             * @request PATCH:/api/v1/db/meta/tables/{tableId}
             * @response `200` `any` OK
             */
            update: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table
             * @name Delete
             * @request DELETE:/api/v1/db/meta/tables/{tableId}
             * @response `200` `void` OK
             */
            delete: (tableId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}`, method: 'DELETE' }, params)),
            /**
             * No description
             *
             * @tags DB table
             * @name Reorder
             * @request POST:/api/v1/db/meta/tables/{tableId}/reorder
             * @response `200` `void` OK
             */
            reorder: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/reorder`, method: 'POST', body: data, type: ContentType.Json }, params)),
        };
        this.dbTableColumn = {
            /**
             * No description
             *
             * @tags DB table column
             * @name Create
             * @summary Column create
             * @request POST:/api/v1/db/meta/tables/{tableId}/columns
             * @response `200` `void` OK
             */
            create: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/columns`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB table column
             * @name Update
             * @summary Column Update
             * @request PATCH:/api/v1/db/meta/columns/{columnId}
             * @response `200` `ColumnType` OK
             */
            update: (columnId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/columns/${columnId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table column
             * @name Delete
             * @request DELETE:/api/v1/db/meta/columns/{columnId}
             * @response `200` `void` OK
             */
            delete: (columnId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/columns/${columnId}`, method: 'DELETE' }, params)),
            /**
             * No description
             *
             * @tags DB table column
             * @name PrimaryColumnSet
             * @request POST:/api/v1/db/meta/columns/{columnId}/primary
             * @response `200` `void` OK
             */
            primaryColumnSet: (columnId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/columns/${columnId}/primary`, method: 'POST' }, params)),
        };
        this.dbView = {
            /**
             * No description
             *
             * @tags DB view
             * @name List
             * @request GET:/api/v1/db/meta/tables/{tableId}/views
             * @response `200` `ViewListType`
             */
            list: (tableId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/views`, method: 'GET' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name Update
             * @request PATCH:/api/v1/db/meta/views/{viewId}
             * @response `200` `void` OK
             */
            update: (viewId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}`, method: 'PATCH', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name Delete
             * @request DELETE:/api/v1/db/meta/views/{viewId}
             * @response `200` `void` OK
             */
            delete: (viewId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}`, method: 'DELETE' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name ShowAllColumn
             * @request POST:/api/v1/db/meta/views/{viewId}/show-all
             * @response `200` `void` OK
             */
            showAllColumn: (viewId, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/show-all`, method: 'POST', query: query }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name HideAllColumn
             * @request POST:/api/v1/db/meta/views/{viewId}/hide-all
             * @response `200` `void` OK
             */
            hideAllColumn: (viewId, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/hide-all`, method: 'POST', query: query }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name GridCreate
             * @request POST:/api/v1/db/meta/tables/{tableId}/grids
             * @response `200` `GridType` OK
             */
            gridCreate: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/grids`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name FormCreate
             * @request POST:/api/v1/db/meta/tables/{tableId}/forms
             * @response `200` `FormType` OK
             */
            formCreate: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/forms`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name FormUpdate
             * @request PATCH:/api/v1/db/meta/forms/{formId}
             * @response `200` `void` OK
             */
            formUpdate: (formId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/forms/${formId}`, method: 'PATCH', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name FormRead
             * @request GET:/api/v1/db/meta/forms/{formId}
             * @response `200` `FormType` OK
             */
            formRead: (formId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/forms/${formId}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name FormColumnUpdate
             * @request PATCH:/api/v1/db/meta/form-columns/{formViewColumnId}
             * @response `200` `any` OK
             */
            formColumnUpdate: (formViewColumnId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/form-columns/${formViewColumnId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name GridColumnsList
             * @request GET:/api/v1/db/meta/grids/{gridId}/grid-columns
             * @response `200` `(GridColumnType)[]` OK
             */
            gridColumnsList: (gridId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/grids/${gridId}/grid-columns`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name GridColumnUpdate
             * @request PATCH:/api/v1/db/meta/grid-columns/{columnId}
             * @response `200` `any` OK
             */
            gridColumnUpdate: (columnId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/grid-columns/${columnId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name GalleryCreate
             * @request POST:/api/v1/db/meta/tables/{tableId}/galleries
             * @response `200` `object` OK
             */
            galleryCreate: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/galleries`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name GalleryUpdate
             * @request PATCH:/api/v1/db/meta/galleries/{galleryId}
             * @response `200` `void` OK
             */
            galleryUpdate: (galleryId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/galleries/${galleryId}`, method: 'PATCH', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name GalleryRead
             * @request GET:/api/v1/db/meta/galleries/{galleryId}
             * @response `200` `GalleryType` OK
             */
            galleryRead: (galleryId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/galleries/${galleryId}`, method: 'GET', format: 'json' }, params)),
        };
        this.dbViewShare = {
            /**
             * No description
             *
             * @tags DB view share
             * @name List
             * @summary Shared view list
             * @request GET:/api/v1/db/meta/tables/{tableId}/share
             * @response `200` `(any)[]` OK
             */
            list: (tableId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/share`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view share
             * @name Create
             * @request POST:/api/v1/db/meta/views/{viewId}/share
             * @response `200` `{ uuid?: string }` OK
             */
            create: (viewId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/share`, method: 'POST', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view share
             * @name Update
             * @request PATCH:/api/v1/db/meta/views/{viewId}/share
             * @response `200` `SharedViewType` OK
             */
            update: (viewId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/share`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view share
             * @name Delete
             * @request DELETE:/api/v1/db/meta/views/{viewId}/share
             * @response `200` `void` OK
             */
            delete: (viewId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/share`, method: 'DELETE' }, params)),
        };
        this.dbViewColumn = {
            /**
             * No description
             *
             * @tags DB view column
             * @name List
             * @request GET:/api/v1/db/meta/views/{viewId}/columns
             */
            list: (viewId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/columns`, method: 'GET' }, params)),
            /**
             * No description
             *
             * @tags DB view column
             * @name Create
             * @request POST:/api/v1/db/meta/views/{viewId}/columns
             * @response `200` `void` OK
             */
            create: (viewId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/columns`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB view column
             * @name Update
             * @request PATCH:/api/v1/db/meta/views/{viewId}/columns/{columnId}
             * @response `200` `void` OK
             */
            update: (viewId, columnId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/columns/${columnId}`, method: 'PATCH', body: data, type: ContentType.Json }, params)),
        };
        this.dbTableSort = {
            /**
             * No description
             *
             * @tags DB table sort
             * @name List
             * @request GET:/api/v1/db/meta/views/{viewId}/sorts
             * @response `200` `{ sorts?: { list?: (SortType)[] } }` OK
             */
            list: (viewId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/sorts`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table sort
             * @name Create
             * @request POST:/api/v1/db/meta/views/{viewId}/sorts
             * @response `200` `void` OK
             */
            create: (viewId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/sorts`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB table sort
             * @name Get
             * @request GET:/api/v1/db/meta/sorts/{sortId}
             * @response `200` `SortType` OK
             */
            get: (sortId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/sorts/${sortId}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table sort
             * @name Update
             * @request PATCH:/api/v1/db/meta/sorts/{sortId}
             * @response `200` `void` OK
             */
            update: (sortId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/sorts/${sortId}`, method: 'PATCH', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB table sort
             * @name Delete
             * @request DELETE:/api/v1/db/meta/sorts/{sortId}
             * @response `200` `void` OK
             */
            delete: (sortId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/sorts/${sortId}`, method: 'DELETE' }, params)),
        };
        this.dbTableFilter = {
            /**
             * No description
             *
             * @tags DB table filter
             * @name Read
             * @request GET:/api/v1/db/meta/views/{viewId}/filters
             * @response `200` `(FilterType)[]` OK
             */
            read: (viewId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/filters`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table filter
             * @name Create
             * @request POST:/api/v1/db/meta/views/{viewId}/filters
             * @response `200` `FilterType` OK
             */
            create: (viewId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/filters`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table filter
             * @name Get
             * @request GET:/api/v1/db/meta/filters/{filterId}
             * @response `200` `FilterType` OK
             */
            get: (filterId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/filters/${filterId}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table filter
             * @name Update
             * @request PATCH:/api/v1/db/meta/filters/{filterId}
             * @response `200` `void` OK
             */
            update: (filterId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/filters/${filterId}`, method: 'PATCH', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB table filter
             * @name Delete
             * @request DELETE:/api/v1/db/meta/filters/{filterId}
             * @response `200` `void` OK
             */
            delete: (filterId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/filters/${filterId}`, method: 'DELETE' }, params)),
            /**
             * No description
             *
             * @tags DB table filter
             * @name ChildrenRead
             * @request GET:/api/v1/db/meta/filters/{filterGroupId}/children
             * @response `200` `(FilterType)[]` OK
             */
            childrenRead: (filterGroupId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/filters/${filterGroupId}/children`, method: 'GET', format: 'json' }, params)),
        };
        this.dbTableWebhookFilter = {
            /**
             * No description
             *
             * @tags DB table webhook filter
             * @name Read
             * @request GET:/api/v1/db/meta/hooks/{hookId}/filters
             * @response `200` `FilterListType`
             */
            read: (hookId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/hooks/${hookId}/filters`, method: 'GET' }, params)),
            /**
             * No description
             *
             * @tags DB table webhook filter
             * @name Create
             * @request POST:/api/v1/db/meta/hooks/{hookId}/filters
             * @response `200` `void` OK
             */
            create: (hookId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/hooks/${hookId}/filters`, method: 'POST', body: data, type: ContentType.Json }, params)),
        };
        this.dbTableRow = {
            /**
             * No description
             *
             * @tags DB table row
             * @name List
             * @summary Table row list
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}
             * @response `200` `any` OK
             */
            list: (orgs, projectName, tableName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name Create
             * @summary Table row create
             * @request POST:/api/v1/db/data/{orgs}/{projectName}/{tableName}
             * @response `200` `any` OK
             */
            create: (orgs, projectName, tableName, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name FindOne
             * @summary Table row FindOne
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/find-one
             * @response `200` `any` OK
             */
            findOne: (orgs, projectName, tableName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/find-one`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name GroupBy
             * @summary Table row Group by
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/groupby
             * @response `200` `any` OK
             */
            groupBy: (orgs, projectName, tableName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/groupby`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name Read
             * @summary Table row read
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}
             * @response `201` `any` Created
             */
            read: (orgs, projectName, tableName, rowId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name Update
             * @summary Table row update
             * @request PATCH:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}
             * @response `200` `any` OK
             */
            update: (orgs, projectName, tableName, rowId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name Delete
             * @summary Table row delete
             * @request DELETE:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}
             * @response `200` `void` OK
             */
            delete: (orgs, projectName, tableName, rowId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}`, method: 'DELETE' }, params)),
            /**
             * @description check row with provided primary key exists or not
             *
             * @tags DB table row
             * @name Exist
             * @summary Table row exist
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}/exist
             * @response `201` `any` Created
             */
            exist: (orgs, projectName, tableName, rowId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}/exist`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name BulkCreate
             * @summary Bulk insert table rows
             * @request POST:/api/v1/db/data/bulk/{orgs}/{projectName}/{tableName}
             * @response `200` `any` OK
             */
            bulkCreate: (orgs, projectName, tableName, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/bulk/${orgs}/${projectName}/${tableName}`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name BulkUpdate
             * @summary Bulk update all table rows by IDs
             * @request PATCH:/api/v1/db/data/bulk/{orgs}/{projectName}/{tableName}
             * @response `200` `any` OK
             */
            bulkUpdate: (orgs, projectName, tableName, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/bulk/${orgs}/${projectName}/${tableName}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name BulkDelete
             * @summary Bulk delete all table rows by IDs
             * @request DELETE:/api/v1/db/data/bulk/{orgs}/{projectName}/{tableName}
             * @response `200` `any` OK
             */
            bulkDelete: (orgs, projectName, tableName, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/bulk/${orgs}/${projectName}/${tableName}`, method: 'DELETE', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name BulkUpdateAll
             * @summary Bulk update all table rows with conditions
             * @request PATCH:/api/v1/db/data/bulk/{orgs}/{projectName}/{tableName}/all
             * @response `200` `any` OK
             */
            bulkUpdateAll: (orgs, projectName, tableName, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/bulk/${orgs}/${projectName}/${tableName}/all`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name BulkDeleteAll
             * @summary Bulk delete all table rows with conditions
             * @request DELETE:/api/v1/db/data/bulk/{orgs}/{projectName}/{tableName}/all
             * @response `200` `any` OK
             */
            bulkDeleteAll: (orgs, projectName, tableName, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/bulk/${orgs}/${projectName}/${tableName}/all`, method: 'DELETE', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * @description CSV or Excel export
             *
             * @tags DB table row
             * @name CsvExport
             * @summary Tablerows export
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/export/{type}
             * @response `200` `any` OK
             */
            csvExport: (orgs, projectName, tableName, type, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/export/${type}`, method: 'GET', wrapped: true }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name NestedList
             * @summary Nested relations row list
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}/{relationType}/{columnName}
             * @response `200` `any` OK
             */
            nestedList: (orgs, projectName, tableName, rowId, relationType, columnName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}/${relationType}/${columnName}`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name NestedAdd
             * @summary Nested relations row add
             * @request POST:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}/{relationType}/{columnName}/{refRowId}
             * @response `200` `any` OK
             */
            nestedAdd: (orgs, projectName, tableName, rowId, relationType, columnName, refRowId, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}/${relationType}/${columnName}/${refRowId}`, method: 'POST', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name NestedRemove
             * @summary Nested relations row remove
             * @request DELETE:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}/{relationType}/{columnName}/{refRowId}
             * @response `200` `any` OK
             */
            nestedRemove: (orgs, projectName, tableName, rowId, relationType, columnName, refRowId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}/${relationType}/${columnName}/${refRowId}`, method: 'DELETE', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name NestedChildrenExcludedList
             * @summary Referenced tables rows excluding current records children/parent
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}/{relationType}/{columnName}/exclude
             * @response `200` `any` OK
             */
            nestedChildrenExcludedList: (orgs, projectName, tableName, rowId, relationType, columnName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}/${relationType}/${columnName}/exclude`, method: 'GET', query: query, format: 'json' }, params)),
        };
        this.dbViewRow = {
            /**
             * No description
             *
             * @tags DB view row
             * @name List
             * @summary Table view row list
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}
             * @response `200` `any` OK
             */
            list: (orgs, projectName, tableName, viewName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view row
             * @name Create
             * @summary Table view row create
             * @request POST:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}
             * @response `200` `any` OK
             */
            create: (orgs, projectName, tableName, viewName, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view row
             * @name FindOne
             * @summary Table view row FindOne
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/find-one
             * @response `200` `any` OK
             */
            findOne: (orgs, projectName, tableName, viewName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/find-one`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view row
             * @name GroupBy
             * @summary Table view row Group by
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/groupby
             * @response `200` `any` OK
             */
            groupBy: (orgs, projectName, tableName, viewName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/groupby`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view row
             * @name Count
             * @summary Table view rows count
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/count
             * @response `200` `any` OK
             */
            count: (orgs, projectName, tableName, viewName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/count`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view row
             * @name Read
             * @summary Table view row read
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/{rowId}
             * @response `201` `any` Created
             */
            read: (orgs, projectName, tableName, viewName, rowId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/${rowId}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view row
             * @name Update
             * @summary Table view row update
             * @request PATCH:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/{rowId}
             * @response `200` `any` OK
             */
            update: (orgs, projectName, tableName, viewName, rowId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/${rowId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view row
             * @name Delete
             * @summary Table view row delete
             * @request DELETE:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/{rowId}
             * @response `200` `void` OK
             */
            delete: (orgs, projectName, tableName, viewName, rowId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/${rowId}`, method: 'DELETE' }, params)),
            /**
             * @description check row with provided primary key exists or not
             *
             * @tags DB view row
             * @name Exist
             * @summary Table view row exist
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/{rowId}/exist
             * @response `201` `any` Created
             */
            exist: (orgs, projectName, tableName, viewName, rowId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/${rowId}/exist`, method: 'GET', format: 'json' }, params)),
            /**
             * @description CSV or Excel export
             *
             * @tags DB view row
             * @name Export
             * @summary Table view rows export
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/export/{type}
             * @response `200` `any` OK
             */
            export: (orgs, projectName, tableName, viewName, type, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/export/${type}`, method: 'GET', wrapped: true }, params)),
        };
        this.public = {
            /**
             * No description
             *
             * @tags Public
             * @name DataList
             * @request GET:/api/v1/db/public/shared-view/{sharedViewUuid}/rows
             * @response `200` `any` OK
             */
            dataList: (sharedViewUuid, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/public/shared-view/${sharedViewUuid}/rows`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Public
             * @name DataCreate
             * @request POST:/api/v1/db/public/shared-view/{sharedViewUuid}/rows
             * @response `200` `any` OK
             */
            dataCreate: (sharedViewUuid, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/public/shared-view/${sharedViewUuid}/rows`, method: 'POST', body: data, type: ContentType.FormData, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Public
             * @name DataNestedList
             * @request GET:/api/v1/db/public/shared-view/{sharedViewUuid}/rows/{rowId}/{relationType}/{columnName}
             * @response `200` `any` OK
             */
            dataNestedList: (sharedViewUuid, rowId, relationType, columnName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/public/shared-view/${sharedViewUuid}/rows/${rowId}/${relationType}/${columnName}`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Public
             * @name CsvExport
             * @request GET:/api/v1/db/public/shared-view/{sharedViewUuid}/rows/export/{type}
             * @response `200` `any` OK
             */
            csvExport: (sharedViewUuid, type, params = {}) => this.request(Object.assign({ path: `/api/v1/db/public/shared-view/${sharedViewUuid}/rows/export/${type}`, method: 'GET', wrapped: true }, params)),
            /**
             * No description
             *
             * @tags Public
             * @name DataRelationList
             * @request GET:/api/v1/db/public/shared-view/{sharedViewUuid}/nested/{columnName}
             * @response `200` `any` OK
             */
            dataRelationList: (sharedViewUuid, columnName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/public/shared-view/${sharedViewUuid}/nested/${columnName}`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Public
             * @name SharedViewMetaGet
             * @request GET:/api/v1/db/public/shared-view/{sharedViewUuid}/meta
             * @response `200` `object` OK
             */
            sharedViewMetaGet: (sharedViewUuid, params = {}) => this.request(Object.assign({ path: `/api/v1/db/public/shared-view/${sharedViewUuid}/meta`, method: 'GET', format: 'json' }, params)),
            /**
             * @description Read project details
             *
             * @tags Public
             * @name SharedBaseGet
             * @request GET:/api/v1/db/public/shared-base/{sharedBaseUuid}/meta
             * @response `200` `{ project_id?: string }` OK
             */
            sharedBaseGet: (sharedBaseUuid, params = {}) => this.request(Object.assign({ path: `/api/v1/db/public/shared-base/${sharedBaseUuid}/meta`, method: 'GET', format: 'json' }, params)),
        };
        this.utils = {
            /**
             * No description
             *
             * @tags Utils
             * @name CommentList
             * @request GET:/api/v1/db/meta/audits/comments
             * @response `201` `any` Created
             */
            commentList: (query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/audits/comments`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Utils
             * @name CommentRow
             * @request POST:/api/v1/db/meta/audits/comments
             * @response `200` `void` OK
             */
            commentRow: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/audits/comments`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags Utils
             * @name CommentCount
             * @request GET:/api/v1/db/meta/audits/comments/count
             * @response `201` `any` Created
             */
            commentCount: (query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/audits/comments/count`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Utils
             * @name AuditRowUpdate
             * @request POST:/api/v1/db/meta/audits/rows/{rowId}/update
             * @response `200` `void` OK
             */
            auditRowUpdate: (rowId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/audits/rows/${rowId}/update`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags Utils
             * @name TestConnection
             * @request POST:/api/v1/db/meta/connection/test
             * @response `200` `{ code?: number, message?: string }` OK
             */
            testConnection: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/connection/test`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Utils
             * @name AppInfo
             * @request GET:/api/v1/db/meta/nocodb/info
             * @response `200` `any` OK
             */
            appInfo: (params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/nocodb/info`, method: 'GET', format: 'json' }, params)),
            /**
             * @description Generic Axios Call
             *
             * @tags Utils
             * @name AxiosRequestMake
             * @request POST:/api/v1/db/meta/axiosRequestMake
             * @response `200` `object` OK
             */
            axiosRequestMake: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/axiosRequestMake`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Utils
             * @name AppVersion
             * @request GET:/api/v1/version
             * @response `200` `any` OK
             */
            appVersion: (params = {}) => this.request(Object.assign({ path: `/api/v1/version`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Utils
             * @name AppHealth
             * @request GET:/api/v1/health
             * @response `200` `any` OK
             */
            appHealth: (params = {}) => this.request(Object.assign({ path: `/api/v1/health`, method: 'GET', format: 'json' }, params)),
            /**
             * @description Get All K/V pairs in NocoCache
             *
             * @tags Utils
             * @name CacheGet
             * @summary Your GET endpoint
             * @request GET:/api/v1/db/meta/cache
             */
            cacheGet: (params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/cache`, method: 'GET' }, params)),
            /**
             * @description Delete All K/V pairs in NocoCache
             *
             * @tags Utils
             * @name CacheDelete
             * @request DELETE:/api/v1/db/meta/cache
             * @response `200` `void` OK
             */
            cacheDelete: (params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/cache`, method: 'DELETE' }, params)),
        };
        this.dbTableWebhook = {
            /**
             * No description
             *
             * @tags DB table webhook
             * @name List
             * @request GET:/api/v1/db/meta/tables/{tableId}/hooks
             * @response `200` `{ list: (HookType)[], pageInfo: PaginatedType }` OK
             */
            list: (tableId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/hooks`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table webhook
             * @name Create
             * @request POST:/api/v1/db/meta/tables/{tableId}/hooks
             * @response `200` `AuditType` OK
             */
            create: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/hooks`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table webhook
             * @name Test
             * @request POST:/api/v1/db/meta/tables/{tableId}/hooks/test
             * @response `200` `any` OK
             */
            test: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/hooks/test`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table webhook
             * @name SamplePayloadGet
             * @request GET:/api/v1/db/meta/tables/{tableId}/hooks/samplePayload/{operation}
             * @response `200` `{ plugins?: { list: (PluginType)[], pageInfo: PaginatedType } }` OK
             */
            samplePayloadGet: (tableId, operation, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/hooks/samplePayload/${operation}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table webhook
             * @name Update
             * @request PATCH:/api/v1/db/meta/hooks/{hookId}
             * @response `200` `HookType` OK
             */
            update: (hookId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/hooks/${hookId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table webhook
             * @name Delete
             * @request DELETE:/api/v1/db/meta/hooks/{hookId}
             * @response `200` `void` OK
             */
            delete: (hookId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/hooks/${hookId}`, method: 'DELETE' }, params)),
        };
        this.plugin = {
            /**
             * No description
             *
             * @tags Plugin
             * @name List
             * @request GET:/api/v1/db/meta/plugins
             * @response `200` `{ list?: (PluginType)[], pageInfo?: PaginatedType }` OK
             */
            list: (params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/plugins`, method: 'GET', format: 'json' }, params)),
            /**
             * @description Check plugin is active or not
             *
             * @tags Plugin
             * @name Status
             * @request GET:/api/v1/db/meta/plugins/{pluginTitle}/status
             * @response `200` `boolean` OK
             */
            status: (pluginTitle, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/plugins/${pluginTitle}/status`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Plugin
             * @name Test
             * @request POST:/api/v1/db/meta/plugins/test
             * @response `200` `any` OK
             * @response `400` `void` Bad Request
             * @response `401` `void` Unauthorized
             */
            test: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/plugins/test`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Plugin
             * @name Update
             * @request PATCH:/api/v1/db/meta/plugins/{pluginId}
             * @response `200` `PluginType` OK
             */
            update: (pluginId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/plugins/${pluginId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Plugin
             * @name Read
             * @request GET:/api/v1/db/meta/plugins/{pluginId}
             * @response `200` `PluginType` OK
             */
            read: (pluginId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/plugins/${pluginId}`, method: 'GET', format: 'json' }, params)),
        };
        this.apiToken = {
            /**
             * No description
             *
             * @tags Api token
             * @name List
             * @summary Your GET endpoint
             * @request GET:/api/v1/db/meta/projects/{projectId}/api-tokens
             * @response `200` `(ApiTokenType)[]` OK
             */
            list: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/api-tokens`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Api token
             * @name Create
             * @request POST:/api/v1/db/meta/projects/{projectId}/api-tokens
             * @response `200` `void` OK
             * @response `201` `ApiTokenType` Created
             */
            create: (projectId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/api-tokens`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags Api token
             * @name Delete
             * @request DELETE:/api/v1/db/meta/projects/{projectId}/api-tokens/{token}
             * @response `200` `void` OK
             */
            delete: (projectId, token, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/api-tokens/${token}`, method: 'DELETE' }, params)),
        };
        this.storage = {
            /**
             * No description
             *
             * @tags Storage
             * @name Upload
             * @summary Attachment
             * @request POST:/api/v1/db/storage/upload
             */
            upload: (query, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/storage/upload`, method: 'POST', query: query, body: data, type: ContentType.FormData }, params)),
            /**
             * No description
             *
             * @tags Storage
             * @name UploadByUrl
             * @summary Attachment
             * @request POST:/api/v1/db/storage/upload-by-url
             */
            uploadByUrl: (query, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/storage/upload-by-url`, method: 'POST', query: query, body: data, type: ContentType.Json }, params)),
        };
    }
}
exports.Api = Api;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXBpLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9BcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLG9CQUFvQjtBQUNwQixvQkFBb0I7QUFDcEI7Ozs7Ozs7R0FPRzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUE2bkJILGtEQUErRTtBQWlDL0UsSUFBWSxXQUlYO0FBSkQsV0FBWSxXQUFXO0lBQ3JCLHdDQUF5QixDQUFBO0lBQ3pCLCtDQUFnQyxDQUFBO0lBQ2hDLCtEQUFnRCxDQUFBO0FBQ2xELENBQUMsRUFKVyxXQUFXLEdBQVgsbUJBQVcsS0FBWCxtQkFBVyxRQUl0QjtBQUVELE1BQWEsVUFBVTtJQU9yQixZQUFZLEtBS3FCLEVBQUU7WUFMdkIsRUFDVixjQUFjLEVBQ2QsTUFBTSxFQUNOLE1BQU0sT0FFMkIsRUFEOUIsV0FBVyxjQUpKLHNDQUtYLENBRGU7UUFUUixpQkFBWSxHQUE0QixJQUFJLENBQUM7UUFvQjlDLG9CQUFlLEdBQUcsQ0FBQyxJQUE2QixFQUFFLEVBQUU7WUFDekQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDM0IsQ0FBQyxDQUFDO1FBMkNLLFlBQU8sR0FBRyxLQUFLLEVBQXFCLEVBU3ZCLEVBQWMsRUFBRTtnQkFUTyxFQUN6QyxNQUFNLEVBQ04sSUFBSSxFQUNKLElBQUksRUFDSixLQUFLLEVBQ0wsTUFBTSxFQUNOLE9BQU8sRUFDUCxJQUFJLE9BRWMsRUFEZixNQUFNLGNBUmdDLGdFQVMxQyxDQURVO1lBRVQsTUFBTSxZQUFZLEdBQ2hCLENBQUMsQ0FBQyxPQUFPLE1BQU0sS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQkFDbkQsSUFBSSxDQUFDLGNBQWM7Z0JBQ25CLENBQUMsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUNqRCxFQUFFLENBQUM7WUFDTCxNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxFQUFFLFlBQVksQ0FBQyxDQUFDO1lBQ3BFLE1BQU0sY0FBYyxHQUFHLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQztZQUV6RCxJQUNFLElBQUksS0FBSyxXQUFXLENBQUMsUUFBUTtnQkFDN0IsSUFBSTtnQkFDSixJQUFJLEtBQUssSUFBSTtnQkFDYixPQUFPLElBQUksS0FBSyxRQUFRLEVBQ3hCO2dCQUNBLGFBQWEsQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFDO2dCQUNqRCxhQUFhLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7Z0JBQ2hDLGFBQWEsQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQztnQkFFL0IsSUFBSSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBK0IsQ0FBQyxDQUFDO2FBQzdEO1lBRUQsT0FBTyxJQUFJLENBQUMsUUFBUTtpQkFDakIsT0FBTyxpQ0FDSCxhQUFhLEtBQ2hCLE9BQU8sa0NBQ0YsQ0FBQyxJQUFJLElBQUksSUFBSSxLQUFLLFdBQVcsQ0FBQyxRQUFRO29CQUN2QyxDQUFDLENBQUMsRUFBRSxjQUFjLEVBQUUsSUFBSSxFQUFFO29CQUMxQixDQUFDLENBQUMsRUFBRSxDQUFDLEdBQ0osQ0FBQyxhQUFhLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQyxHQUVsQyxNQUFNLEVBQUUsS0FBSyxFQUNiLFlBQVksRUFBRSxjQUFjLEVBQzVCLElBQUksRUFBRSxJQUFJLEVBQ1YsR0FBRyxFQUFFLElBQUksSUFDVDtpQkFDRCxJQUFJLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtnQkFDakIsSUFBSSxPQUFPO29CQUFFLE9BQU8sUUFBUSxDQUFDO2dCQUM3QixPQUFPLFFBQVEsQ0FBQyxJQUFJLENBQUM7WUFDdkIsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUM7UUF2R0EsSUFBSSxDQUFDLFFBQVEsR0FBRyxlQUFLLENBQUMsTUFBTSxpQ0FDdkIsV0FBVyxLQUNkLE9BQU8sRUFBRSxXQUFXLENBQUMsT0FBTyxJQUFJLHVCQUF1QixJQUN2RCxDQUFDO1FBQ0gsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFDckIsSUFBSSxDQUFDLGNBQWMsR0FBRyxjQUFjLENBQUM7SUFDdkMsQ0FBQztJQU1PLGtCQUFrQixDQUN4QixPQUEyQixFQUMzQixPQUE0QjtRQUU1QixtRUFDSyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsR0FDdEIsT0FBTyxHQUNQLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQyxLQUNsQixPQUFPLGdEQUNGLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQyxHQUN0QyxDQUFDLE9BQU8sQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDLEdBQ3ZCLENBQUMsQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUV6QztJQUNKLENBQUM7SUFFUyxjQUFjLENBQUMsS0FBOEI7UUFDckQsSUFBSSxLQUFLLFlBQVksUUFBUSxFQUFFO1lBQzdCLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFDRCxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsRUFBRSxHQUFHLEVBQUUsRUFBRTtZQUN2RCxNQUFNLFFBQVEsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFNUIsSUFBSSxRQUFRLFlBQVksSUFBSSxFQUFFO2dCQUM1QixRQUFRLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxRQUFRLENBQUMsQ0FBQzthQUNoQztpQkFBTSxJQUFJLE9BQU8sUUFBUSxLQUFLLFFBQVEsSUFBSSxRQUFRLEtBQUssSUFBSSxFQUFFO2dCQUM1RCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQzNCLHdEQUF3RDtvQkFDeEQsS0FBSyxNQUFNLElBQUksSUFBSSxRQUFRLEVBQUU7d0JBQzNCLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztxQkFDbkM7aUJBQ0Y7cUJBQU07b0JBQ0wsUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2lCQUNoRDthQUNGO2lCQUFNO2dCQUNMLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLEdBQUcsUUFBUSxFQUFFLENBQUMsQ0FBQzthQUNyQztZQUNELE9BQU8sUUFBUSxDQUFDO1FBQ2xCLENBQUMsRUFBRSxJQUFJLFFBQVEsRUFBRSxDQUFDLENBQUM7SUFDckIsQ0FBQztDQW9ERjtBQXJIRCxnQ0FxSEM7QUFFRDs7OztHQUlHO0FBQ0gsTUFBYSxHQUVYLFNBQVEsVUFBNEI7SUFGdEM7O1FBR0UsU0FBSSxHQUFHO1lBQ0w7Ozs7Ozs7Ozs7O2VBV0c7WUFDSCxNQUFNLEVBQUUsQ0FDTixJQUEyQyxFQUMzQyxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMEJBQTBCLEVBQ2hDLE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7Ozs7ZUFTRztZQUNILE1BQU0sRUFBRSxDQUNOLElBQXlDLEVBQ3pDLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwwQkFBMEIsRUFDaEMsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsRUFBRSxFQUFFLENBQUMsS0FBK0IsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNsRSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsc0JBQXNCLEVBQzVCLE1BQU0sRUFBRSxLQUFLLEVBQ2IsS0FBSyxFQUFFLEtBQUssRUFDWixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7Ozs7ZUFTRztZQUNILGNBQWMsRUFBRSxDQUFDLElBQXdCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdkUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDhCQUE4QixFQUNwQyxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLElBQ25CLE1BQU0sRUFDVDtZQUVKOzs7Ozs7Ozs7ZUFTRztZQUNILGNBQWMsRUFBRSxDQUNkLElBQXdELEVBQ3hELFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSw4QkFBOEIsRUFDcEMsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsMEJBQTBCLEVBQUUsQ0FBQyxLQUFhLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDeEUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLCtCQUErQixLQUFLLEVBQUUsRUFDNUMsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILGFBQWEsRUFBRSxDQUFDLEtBQWEsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUMzRCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsK0JBQStCLEtBQUssRUFBRSxFQUM1QyxNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsYUFBYSxFQUFFLENBQ2IsS0FBYSxFQUNiLElBQStCLEVBQy9CLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwrQkFBK0IsS0FBSyxFQUFFLEVBQzVDLE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksSUFDbkIsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxZQUFZLEVBQUUsQ0FBQyxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUMzQyxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLEVBQ2xDLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxlQUFlLEVBQUUsQ0FBQyxTQUFpQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ2pFLElBQUksQ0FBQyxPQUFPLGlCQUlWLElBQUksRUFBRSw0QkFBNEIsU0FBUyxRQUFRLEVBQ25ELE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILGNBQWMsRUFBRSxDQUNkLFNBQWlCLEVBQ2pCLElBQVMsRUFDVCxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsUUFBUSxFQUNuRCxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxpQkFBaUIsRUFBRSxDQUNqQixTQUFpQixFQUNqQixNQUFjLEVBQ2QsSUFBUyxFQUNULFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSw0QkFBNEIsU0FBUyxVQUFVLE1BQU0sRUFBRSxFQUM3RCxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxpQkFBaUIsRUFBRSxDQUNqQixTQUFpQixFQUNqQixNQUFjLEVBQ2QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLFVBQVUsTUFBTSxFQUFFLEVBQzdELE1BQU0sRUFBRSxRQUFRLEVBQ2hCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILHVCQUF1QixFQUFFLENBQ3ZCLFNBQWlCLEVBQ2pCLE1BQWMsRUFDZCxJQUFTLEVBQ1QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLFVBQVUsTUFBTSxnQkFBZ0IsRUFDM0UsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtTQUNMLENBQUM7UUFDRixZQUFPLEdBQUc7WUFDUjs7Ozs7Ozs7ZUFRRztZQUNILE9BQU8sRUFBRSxDQUFDLFNBQWlCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEtBQWEsRUFBRSxFQUFFLENBQ3hFLElBQUksQ0FBQyxPQUFPLGlCQWFWLElBQUksRUFBRSw0QkFBNEIsU0FBUyxPQUFPLEVBQ2xELE1BQU0sRUFBRSxLQUFLLEVBQ2IsS0FBSyxFQUFFLEtBQUssRUFDWixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsbUJBQW1CLEVBQUUsQ0FDbkIsU0FBaUIsRUFDakIsS0FBZ0MsRUFDaEMsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLG1CQUFtQixFQUM5RCxNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLEVBQ1osTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsa0JBQWtCLEVBQUUsQ0FDbEIsU0FBaUIsRUFDakIsSUFBUyxFQUNULFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSw0QkFBNEIsU0FBUyxtQkFBbUIsRUFDOUQsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsSUFBSSxFQUFFLENBQ0osS0FBMkQsRUFDM0QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDJCQUEyQixFQUNqQyxNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLElBQ1QsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxNQUFNLEVBQUUsQ0FDTixJQUEwQyxFQUMxQyxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMkJBQTJCLEVBQ2pDLE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILElBQUksRUFBRSxDQUFDLFNBQWlCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdEQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLEVBQUUsRUFDN0MsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsTUFBTSxFQUFFLENBQUMsU0FBaUIsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN4RCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsRUFBRSxFQUM3QyxNQUFNLEVBQUUsUUFBUSxJQUNiLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsTUFBTSxFQUFFLENBQUMsU0FBaUIsRUFBRSxJQUFTLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDbkUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLEVBQUUsRUFDN0MsTUFBTSxFQUFFLE9BQU8sRUFDZixJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxJQUNuQixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsYUFBYSxFQUFFLENBQUMsU0FBaUIsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUMvRCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsU0FBUyxFQUNwRCxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGlCQUFpQixFQUFFLENBQUMsU0FBaUIsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNuRSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsU0FBUyxFQUNwRCxNQUFNLEVBQUUsUUFBUSxJQUNiLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxnQkFBZ0IsRUFBRSxDQUNoQixTQUFpQixFQUNqQixJQUEyQyxFQUMzQyxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsU0FBUyxFQUNwRCxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGdCQUFnQixFQUFFLENBQ2hCLFNBQWlCLEVBQ2pCLElBQTJDLEVBQzNDLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSw0QkFBNEIsU0FBUyxTQUFTLEVBQ3BELE1BQU0sRUFBRSxPQUFPLEVBQ2YsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILElBQUksRUFBRSxDQUFDLFNBQWlCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdEQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLE9BQU8sRUFDbEQsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxZQUFZLEVBQUUsQ0FBQyxTQUFpQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQzlELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSw0QkFBNEIsU0FBUyxZQUFZLEVBQ3ZELE1BQU0sRUFBRSxNQUFNLEVBQ2QsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsV0FBVyxFQUFFLENBQUMsU0FBaUIsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUM3RCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsWUFBWSxFQUN2RCxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILFNBQVMsRUFBRSxDQUNULFNBQWlCLEVBQ2pCLEtBQTJDLEVBQzNDLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSw0QkFBNEIsU0FBUyxTQUFTLEVBQ3BELE1BQU0sRUFBRSxLQUFLLEVBQ2IsS0FBSyxFQUFFLEtBQUssRUFDWixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtTQUNMLENBQUM7UUFDRixZQUFPLEdBQUc7WUFDUjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQ04sU0FBaUIsRUFDakIsSUFBa0IsRUFDbEIsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLFNBQVMsRUFDcEQsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxJQUFJLEVBQUUsQ0FDSixTQUFpQixFQUNqQixLQUtDLEVBQ0QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLFNBQVMsRUFDcEQsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxJQUNULE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxJQUFJLEVBQUUsQ0FBQyxPQUFlLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDcEQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDBCQUEwQixPQUFPLEVBQUUsRUFDekMsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxNQUFNLEVBQUUsQ0FDTixPQUFlLEVBQ2YsSUFBd0IsRUFDeEIsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDBCQUEwQixPQUFPLEVBQUUsRUFDekMsTUFBTSxFQUFFLE9BQU8sRUFDZixJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxNQUFNLEVBQUUsQ0FBQyxPQUFlLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdEQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDBCQUEwQixPQUFPLEVBQUUsRUFDekMsTUFBTSxFQUFFLFFBQVEsSUFDYixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsT0FBTyxFQUFFLENBQ1AsT0FBZSxFQUNmLElBQXdCLEVBQ3hCLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwwQkFBMEIsT0FBTyxVQUFVLEVBQ2pELE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksSUFDbkIsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLGtCQUFhLEdBQUc7WUFDZDs7Ozs7Ozs7ZUFRRztZQUNILE1BQU0sRUFBRSxDQUNOLE9BQWUsRUFDZixJQUFtQixFQUNuQixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMEJBQTBCLE9BQU8sVUFBVSxFQUNqRCxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLElBQ25CLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsTUFBTSxFQUFFLENBQ04sUUFBZ0IsRUFDaEIsSUFBbUIsRUFDbkIsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDJCQUEyQixRQUFRLEVBQUUsRUFDM0MsTUFBTSxFQUFFLE9BQU8sRUFDZixJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxNQUFNLEVBQUUsQ0FBQyxRQUFnQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3ZELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwyQkFBMkIsUUFBUSxFQUFFLEVBQzNDLE1BQU0sRUFBRSxRQUFRLElBQ2IsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGdCQUFnQixFQUFFLENBQUMsUUFBZ0IsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNqRSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMkJBQTJCLFFBQVEsVUFBVSxFQUNuRCxNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtTQUNMLENBQUM7UUFDRixXQUFNLEdBQUc7WUFDUDs7Ozs7OztlQU9HO1lBQ0gsSUFBSSxFQUFFLENBQUMsT0FBZSxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3BELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwwQkFBMEIsT0FBTyxRQUFRLEVBQy9DLE1BQU0sRUFBRSxLQUFLLElBQ1YsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUNOLE1BQWMsRUFDZCxJQUtDLEVBQ0QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLEVBQUUsRUFDdkMsTUFBTSxFQUFFLE9BQU8sRUFDZixJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxJQUNuQixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsTUFBYyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3JELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxFQUFFLEVBQ3ZDLE1BQU0sRUFBRSxRQUFRLElBQ2IsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGFBQWEsRUFBRSxDQUNiLE1BQWMsRUFDZCxLQUE2QixFQUM3QixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLE1BQU0sV0FBVyxFQUNoRCxNQUFNLEVBQUUsTUFBTSxFQUNkLEtBQUssRUFBRSxLQUFLLElBQ1QsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGFBQWEsRUFBRSxDQUNiLE1BQWMsRUFDZCxLQUE2QixFQUM3QixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLE1BQU0sV0FBVyxFQUNoRCxNQUFNLEVBQUUsTUFBTSxFQUNkLEtBQUssRUFBRSxLQUFLLElBQ1QsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILFVBQVUsRUFBRSxDQUFDLE9BQWUsRUFBRSxJQUFjLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDMUUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDBCQUEwQixPQUFPLFFBQVEsRUFDL0MsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxVQUFVLEVBQUUsQ0FBQyxPQUFlLEVBQUUsSUFBYyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQzFFLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwwQkFBMEIsT0FBTyxRQUFRLEVBQy9DLE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsVUFBVSxFQUFFLENBQUMsTUFBYyxFQUFFLElBQWMsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN6RSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLE1BQU0sRUFBRSxFQUN2QyxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLElBQ25CLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxRQUFRLEVBQUUsQ0FBQyxNQUFjLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdkQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLEVBQUUsRUFDdkMsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxnQkFBZ0IsRUFBRSxDQUNoQixnQkFBd0IsRUFDeEIsSUFBb0IsRUFDcEIsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLGdDQUFnQyxnQkFBZ0IsRUFBRSxFQUN4RCxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGVBQWUsRUFBRSxDQUFDLE1BQWMsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUM5RCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLE1BQU0sZUFBZSxFQUNwRCxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGdCQUFnQixFQUFFLENBQ2hCLFFBQWdCLEVBQ2hCLElBQW9CLEVBQ3BCLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxnQ0FBZ0MsUUFBUSxFQUFFLEVBQ2hELE1BQU0sRUFBRSxPQUFPLEVBQ2YsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsYUFBYSxFQUFFLENBQ2IsT0FBZSxFQUNmLElBQWlCLEVBQ2pCLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwwQkFBMEIsT0FBTyxZQUFZLEVBQ25ELE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsYUFBYSxFQUFFLENBQ2IsU0FBaUIsRUFDakIsSUFBaUIsRUFDakIsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDZCQUE2QixTQUFTLEVBQUUsRUFDOUMsTUFBTSxFQUFFLE9BQU8sRUFDZixJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxJQUNuQixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsV0FBVyxFQUFFLENBQUMsU0FBaUIsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUM3RCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNkJBQTZCLFNBQVMsRUFBRSxFQUM5QyxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLGdCQUFXLEdBQUc7WUFDWjs7Ozs7Ozs7ZUFRRztZQUNILElBQUksRUFBRSxDQUFDLE9BQWUsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNwRCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMEJBQTBCLE9BQU8sUUFBUSxFQUMvQyxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUFDLE1BQWMsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNyRCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLE1BQU0sUUFBUSxFQUM3QyxNQUFNLEVBQUUsTUFBTSxFQUNkLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUNOLE1BQWMsRUFDZCxJQUF1QyxFQUN2QyxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLE1BQU0sUUFBUSxFQUM3QyxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUFDLE1BQWMsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNyRCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLE1BQU0sUUFBUSxFQUM3QyxNQUFNLEVBQUUsUUFBUSxJQUNiLE1BQU0sRUFDVDtTQUNMLENBQUM7UUFDRixpQkFBWSxHQUFHO1lBQ2I7Ozs7OztlQU1HO1lBQ0gsSUFBSSxFQUFFLENBQUMsTUFBYyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ25ELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxVQUFVLEVBQy9DLE1BQU0sRUFBRSxLQUFLLElBQ1YsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUFDLE1BQWMsRUFBRSxJQUFTLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDaEUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLFVBQVUsRUFDL0MsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxJQUNuQixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQ04sTUFBYyxFQUNkLFFBQWdCLEVBQ2hCLElBQVMsRUFDVCxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLE1BQU0sWUFBWSxRQUFRLEVBQUUsRUFDM0QsTUFBTSxFQUFFLE9BQU8sRUFDZixJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxJQUNuQixNQUFNLEVBQ1Q7U0FDTCxDQUFDO1FBQ0YsZ0JBQVcsR0FBRztZQUNaOzs7Ozs7O2VBT0c7WUFDSCxJQUFJLEVBQUUsQ0FBQyxNQUFjLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDbkQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLFFBQVEsRUFDN0MsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxNQUFNLEVBQUUsQ0FBQyxNQUFjLEVBQUUsSUFBYyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3JFLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxRQUFRLEVBQzdDLE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksSUFDbkIsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILEdBQUcsRUFBRSxDQUFDLE1BQWMsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNsRCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLE1BQU0sRUFBRSxFQUN2QyxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUFDLE1BQWMsRUFBRSxJQUFjLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDckUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLEVBQUUsRUFDdkMsTUFBTSxFQUFFLE9BQU8sRUFDZixJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxJQUNuQixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsTUFBYyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3JELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxFQUFFLEVBQ3ZDLE1BQU0sRUFBRSxRQUFRLElBQ2IsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLGtCQUFhLEdBQUc7WUFDZDs7Ozs7OztlQU9HO1lBQ0gsSUFBSSxFQUFFLENBQUMsTUFBYyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ25ELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxVQUFVLEVBQy9DLE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsTUFBYyxFQUFFLElBQWdCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdkUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLFVBQVUsRUFDL0MsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxHQUFHLEVBQUUsQ0FBQyxRQUFnQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3BELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwyQkFBMkIsUUFBUSxFQUFFLEVBQzNDLE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsUUFBZ0IsRUFBRSxJQUFnQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3pFLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwyQkFBMkIsUUFBUSxFQUFFLEVBQzNDLE1BQU0sRUFBRSxPQUFPLEVBQ2YsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksSUFDbkIsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUFDLFFBQWdCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdkQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDJCQUEyQixRQUFRLEVBQUUsRUFDM0MsTUFBTSxFQUFFLFFBQVEsSUFDYixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsWUFBWSxFQUFFLENBQUMsYUFBcUIsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNsRSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMkJBQTJCLGFBQWEsV0FBVyxFQUN6RCxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLHlCQUFvQixHQUFHO1lBQ3JCOzs7Ozs7O2VBT0c7WUFDSCxJQUFJLEVBQUUsQ0FBQyxNQUFjLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDbkQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLFVBQVUsRUFDL0MsTUFBTSxFQUFFLEtBQUssSUFDVixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsTUFBYyxFQUFFLElBQWdCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdkUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLFVBQVUsRUFDL0MsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxJQUNuQixNQUFNLEVBQ1Q7U0FDTCxDQUFDO1FBQ0YsZUFBVSxHQUFHO1lBQ1g7Ozs7Ozs7O2VBUUc7WUFDSCxJQUFJLEVBQUUsQ0FDSixJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsS0FBd0QsRUFDeEQsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsRUFBRSxFQUMzRCxNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLEVBQ1osTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILE1BQU0sRUFBRSxDQUNOLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixJQUFTLEVBQ1QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsRUFBRSxFQUMzRCxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxPQUFPLEVBQUUsQ0FDUCxJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsS0FBd0QsRUFDeEQsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsV0FBVyxFQUNwRSxNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLEVBQ1osTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILE9BQU8sRUFBRSxDQUNQLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixLQU1DLEVBQ0QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsVUFBVSxFQUNuRSxNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLEVBQ1osTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILElBQUksRUFBRSxDQUNKLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixLQUFhLEVBQ2IsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsSUFBSSxLQUFLLEVBQUUsRUFDcEUsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsTUFBTSxFQUFFLENBQ04sSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLEtBQWEsRUFDYixJQUFTLEVBQ1QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsSUFBSSxLQUFLLEVBQUUsRUFDcEUsTUFBTSxFQUFFLE9BQU8sRUFDZixJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsTUFBTSxFQUFFLENBQ04sSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLEtBQWEsRUFDYixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsbUJBQW1CLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxJQUFJLEtBQUssRUFBRSxFQUNwRSxNQUFNLEVBQUUsUUFBUSxJQUNiLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsS0FBSyxFQUFFLENBQ0wsSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLEtBQWEsRUFDYixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsbUJBQW1CLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxJQUFJLEtBQUssUUFBUSxFQUMxRSxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxVQUFVLEVBQUUsQ0FDVixJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsSUFBUyxFQUNULFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx3QkFBd0IsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLEVBQUUsRUFDaEUsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsVUFBVSxFQUFFLENBQ1YsSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLElBQVMsRUFDVCxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsd0JBQXdCLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxFQUFFLEVBQ2hFLE1BQU0sRUFBRSxPQUFPLEVBQ2YsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILFVBQVUsRUFBRSxDQUNWLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixJQUFTLEVBQ1QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHdCQUF3QixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsRUFBRSxFQUNoRSxNQUFNLEVBQUUsUUFBUSxFQUNoQixJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsYUFBYSxFQUFFLENBQ2IsSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLElBQVMsRUFDVCxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsd0JBQXdCLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxNQUFNLEVBQ3BFLE1BQU0sRUFBRSxPQUFPLEVBQ2YsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILGFBQWEsRUFBRSxDQUNiLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixJQUFTLEVBQ1QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHdCQUF3QixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsTUFBTSxFQUNwRSxNQUFNLEVBQUUsUUFBUSxFQUNoQixJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsU0FBUyxFQUFFLENBQ1QsSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLElBQXFCLEVBQ3JCLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLFdBQVcsSUFBSSxFQUFFLEVBQzFFLE1BQU0sRUFBRSxLQUFLLEVBQ2IsT0FBTyxFQUFFLElBQUksSUFDVixNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILFVBQVUsRUFBRSxDQUNWLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixLQUFhLEVBQ2IsWUFBeUIsRUFDekIsVUFBa0IsRUFDbEIsS0FJQyxFQUNELFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLElBQUksS0FBSyxJQUFJLFlBQVksSUFBSSxVQUFVLEVBQUUsRUFDbEcsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxFQUNaLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxTQUFTLEVBQUUsQ0FDVCxJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsS0FBYSxFQUNiLFlBQXlCLEVBQ3pCLFVBQWtCLEVBQ2xCLFFBQWdCLEVBQ2hCLEtBQTJDLEVBQzNDLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLElBQUksS0FBSyxJQUFJLFlBQVksSUFBSSxVQUFVLElBQUksUUFBUSxFQUFFLEVBQzlHLE1BQU0sRUFBRSxNQUFNLEVBQ2QsS0FBSyxFQUFFLEtBQUssRUFDWixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsWUFBWSxFQUFFLENBQ1osSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLEtBQWEsRUFDYixZQUF5QixFQUN6QixVQUFrQixFQUNsQixRQUFnQixFQUNoQixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsbUJBQW1CLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxJQUFJLEtBQUssSUFBSSxZQUFZLElBQUksVUFBVSxJQUFJLFFBQVEsRUFBRSxFQUM5RyxNQUFNLEVBQUUsUUFBUSxFQUNoQixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsMEJBQTBCLEVBQUUsQ0FDMUIsSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLEtBQWEsRUFDYixZQUF5QixFQUN6QixVQUFrQixFQUNsQixLQUlDLEVBQ0QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsSUFBSSxLQUFLLElBQUksWUFBWSxJQUFJLFVBQVUsVUFBVSxFQUMxRyxNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLEVBQ1osTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7U0FDTCxDQUFDO1FBQ0YsY0FBUyxHQUFHO1lBQ1Y7Ozs7Ozs7O2VBUUc7WUFDSCxJQUFJLEVBQUUsQ0FDSixJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsUUFBZ0IsRUFDaEIsS0FBc0UsRUFDdEUsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsVUFBVSxRQUFRLEVBQUUsRUFDN0UsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxFQUNaLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxNQUFNLEVBQUUsQ0FDTixJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsUUFBZ0IsRUFDaEIsSUFBUyxFQUNULFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLFVBQVUsUUFBUSxFQUFFLEVBQzdFLE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILE9BQU8sRUFBRSxDQUNQLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixRQUFnQixFQUNoQixLQUFzRSxFQUN0RSxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsbUJBQW1CLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxVQUFVLFFBQVEsV0FBVyxFQUN0RixNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLEVBQ1osTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILE9BQU8sRUFBRSxDQUNQLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixRQUFnQixFQUNoQixLQU1DLEVBQ0QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsVUFBVSxRQUFRLFVBQVUsRUFDckYsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxFQUNaLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxLQUFLLEVBQUUsQ0FDTCxJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsUUFBZ0IsRUFDaEIsS0FBd0MsRUFDeEMsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsVUFBVSxRQUFRLFFBQVEsRUFDbkYsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxFQUNaLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxJQUFJLEVBQUUsQ0FDSixJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsUUFBZ0IsRUFDaEIsS0FBYSxFQUNiLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLFVBQVUsUUFBUSxJQUFJLEtBQUssRUFBRSxFQUN0RixNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxNQUFNLEVBQUUsQ0FDTixJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsUUFBZ0IsRUFDaEIsS0FBYSxFQUNiLElBQVMsRUFDVCxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsbUJBQW1CLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxVQUFVLFFBQVEsSUFBSSxLQUFLLEVBQUUsRUFDdEYsTUFBTSxFQUFFLE9BQU8sRUFDZixJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsTUFBTSxFQUFFLENBQ04sSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLFFBQWdCLEVBQ2hCLEtBQWEsRUFDYixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsbUJBQW1CLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxVQUFVLFFBQVEsSUFBSSxLQUFLLEVBQUUsRUFDdEYsTUFBTSxFQUFFLFFBQVEsSUFDYixNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILEtBQUssRUFBRSxDQUNMLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixRQUFnQixFQUNoQixLQUFhLEVBQ2IsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsVUFBVSxRQUFRLElBQUksS0FBSyxRQUFRLEVBQzVGLE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILE1BQU0sRUFBRSxDQUNOLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixRQUFnQixFQUNoQixJQUFxQixFQUNyQixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsbUJBQW1CLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxVQUFVLFFBQVEsV0FBVyxJQUFJLEVBQUUsRUFDNUYsTUFBTSxFQUFFLEtBQUssRUFDYixPQUFPLEVBQUUsSUFBSSxJQUNWLE1BQU0sRUFDVDtTQUNMLENBQUM7UUFDRixXQUFNLEdBQUc7WUFDUDs7Ozs7OztlQU9HO1lBQ0gsUUFBUSxFQUFFLENBQ1IsY0FBc0IsRUFDdEIsS0FBMkMsRUFDM0MsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLGlDQUFpQyxjQUFjLE9BQU8sRUFDNUQsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxFQUNaLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILFVBQVUsRUFBRSxDQUNWLGNBQXNCLEVBQ3RCLElBQVksRUFDWixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsaUNBQWlDLGNBQWMsT0FBTyxFQUM1RCxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRLEVBQzFCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGNBQWMsRUFBRSxDQUNkLGNBQXNCLEVBQ3RCLEtBQWEsRUFDYixZQUF5QixFQUN6QixVQUFrQixFQUNsQixLQUEyQyxFQUMzQyxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsaUNBQWlDLGNBQWMsU0FBUyxLQUFLLElBQUksWUFBWSxJQUFJLFVBQVUsRUFBRSxFQUNuRyxNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLEVBQ1osTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsU0FBUyxFQUFFLENBQ1QsY0FBc0IsRUFDdEIsSUFBcUIsRUFDckIsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLGlDQUFpQyxjQUFjLGdCQUFnQixJQUFJLEVBQUUsRUFDM0UsTUFBTSxFQUFFLEtBQUssRUFDYixPQUFPLEVBQUUsSUFBSSxJQUNWLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxnQkFBZ0IsRUFBRSxDQUNoQixjQUFzQixFQUN0QixVQUFrQixFQUNsQixLQUEyQyxFQUMzQyxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsaUNBQWlDLGNBQWMsV0FBVyxVQUFVLEVBQUUsRUFDNUUsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxFQUNaLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGlCQUFpQixFQUFFLENBQUMsY0FBc0IsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN4RSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsaUNBQWlDLGNBQWMsT0FBTyxFQUM1RCxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGFBQWEsRUFBRSxDQUFDLGNBQXNCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDcEUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLGlDQUFpQyxjQUFjLE9BQU8sRUFDNUQsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtTQUNMLENBQUM7UUFDRixVQUFLLEdBQUc7WUFDTjs7Ozs7OztlQU9HO1lBQ0gsV0FBVyxFQUFFLENBQ1gsS0FBdUUsRUFDdkUsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLGlDQUFpQyxFQUN2QyxNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLEVBQ1osTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsVUFBVSxFQUFFLENBQ1YsSUFBbUUsRUFDbkUsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLGlDQUFpQyxFQUN2QyxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLElBQ25CLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxZQUFZLEVBQUUsQ0FDWixLQUEwQyxFQUMxQyxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsdUNBQXVDLEVBQzdDLE1BQU0sRUFBRSxLQUFLLEVBQ2IsS0FBSyxFQUFFLEtBQUssRUFDWixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxjQUFjLEVBQUUsQ0FDZCxLQUFhLEVBQ2IsSUFNQyxFQUNELFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwrQkFBK0IsS0FBSyxTQUFTLEVBQ25ELE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksSUFDbkIsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGNBQWMsRUFBRSxDQUFDLElBQVMsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN4RCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsaUNBQWlDLEVBQ3ZDLE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsT0FBTyxFQUFFLENBQUMsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdEMsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDZCQUE2QixFQUNuQyxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGdCQUFnQixFQUFFLENBQUMsSUFBWSxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQzdELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxrQ0FBa0MsRUFDeEMsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxVQUFVLEVBQUUsQ0FBQyxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN6QyxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsaUJBQWlCLEVBQ3ZCLE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsU0FBUyxFQUFFLENBQUMsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDeEMsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLGdCQUFnQixFQUN0QixNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILFFBQVEsRUFBRSxDQUFDLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3ZDLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx1QkFBdUIsRUFDN0IsTUFBTSxFQUFFLEtBQUssSUFDVixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsV0FBVyxFQUFFLENBQUMsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDMUMsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHVCQUF1QixFQUM3QixNQUFNLEVBQUUsUUFBUSxJQUNiLE1BQU0sRUFDVDtTQUNMLENBQUM7UUFDRixtQkFBYyxHQUFHO1lBQ2Y7Ozs7Ozs7ZUFPRztZQUNILElBQUksRUFBRSxDQUFDLE9BQWUsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNwRCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMEJBQTBCLE9BQU8sUUFBUSxFQUMvQyxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUFDLE9BQWUsRUFBRSxJQUFlLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdkUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDBCQUEwQixPQUFPLFFBQVEsRUFDL0MsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxJQUFJLEVBQUUsQ0FDSixPQUFlLEVBQ2YsSUFBK0QsRUFDL0QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDBCQUEwQixPQUFPLGFBQWEsRUFDcEQsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxnQkFBZ0IsRUFBRSxDQUNoQixPQUFlLEVBQ2YsU0FBeUMsRUFDekMsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBSVYsSUFBSSxFQUFFLDBCQUEwQixPQUFPLHdCQUF3QixTQUFTLEVBQUUsRUFDMUUsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxNQUFNLEVBQUUsQ0FBQyxNQUFjLEVBQUUsSUFBYyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3JFLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxFQUFFLEVBQ3ZDLE1BQU0sRUFBRSxPQUFPLEVBQ2YsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsTUFBYyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3JELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxFQUFFLEVBQ3ZDLE1BQU0sRUFBRSxRQUFRLElBQ2IsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLFdBQU0sR0FBRztZQUNQOzs7Ozs7O2VBT0c7WUFDSCxJQUFJLEVBQUUsQ0FBQyxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNuQyxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLEVBQy9CLE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsV0FBbUIsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUMxRCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMkJBQTJCLFdBQVcsU0FBUyxFQUNyRCxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7OztlQVNHO1lBQ0gsSUFBSSxFQUFFLENBQ0osSUFBcUUsRUFDckUsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDhCQUE4QixFQUNwQyxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUFDLFFBQWdCLEVBQUUsSUFBZ0IsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN6RSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMkJBQTJCLFFBQVEsRUFBRSxFQUMzQyxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILElBQUksRUFBRSxDQUFDLFFBQWdCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDckQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDJCQUEyQixRQUFRLEVBQUUsRUFDM0MsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtTQUNMLENBQUM7UUFDRixhQUFRLEdBQUc7WUFDVDs7Ozs7Ozs7ZUFRRztZQUNILElBQUksRUFBRSxDQUFDLFNBQWlCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdEQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLGFBQWEsRUFDeEQsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsTUFBTSxFQUFFLENBQ04sU0FBaUIsRUFDakIsSUFBOEIsRUFDOUIsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLGFBQWEsRUFDeEQsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxJQUNuQixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsU0FBaUIsRUFBRSxLQUFhLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdkUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLGVBQWUsS0FBSyxFQUFFLEVBQ2pFLE1BQU0sRUFBRSxRQUFRLElBQ2IsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLFlBQU8sR0FBRztZQUNSOzs7Ozs7O2VBT0c7WUFDSCxNQUFNLEVBQUUsQ0FDTixLQUF1QixFQUN2QixJQUFvQyxFQUNwQyxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMkJBQTJCLEVBQ2pDLE1BQU0sRUFBRSxNQUFNLEVBQ2QsS0FBSyxFQUFFLEtBQUssRUFDWixJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsUUFBUSxJQUN2QixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsV0FBVyxFQUFFLENBQ1gsS0FBdUIsRUFDdkIsSUFLRyxFQUNILFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxrQ0FBa0MsRUFDeEMsTUFBTSxFQUFFLE1BQU0sRUFDZCxLQUFLLEVBQUUsS0FBSyxFQUNaLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLElBQ25CLE1BQU0sRUFDVDtTQUNMLENBQUM7SUFDSixDQUFDO0NBQUE7QUE1dUZELGtCQTR1RkMifQ==