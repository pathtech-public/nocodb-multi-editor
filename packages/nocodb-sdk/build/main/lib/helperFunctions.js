"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.isSystemColumn = exports.getSystemColumns = exports.getSystemColumnsIds = exports.filterOutSystemColumns = void 0;
const UITypes_1 = __importDefault(require("./UITypes"));
// import {RelationTypes} from "./globals";
// const systemCols = ['created_at', 'updated_at']
const filterOutSystemColumns = (columns) => {
    return (columns && columns.filter((c) => !isSystemColumn(c))) || [];
};
exports.filterOutSystemColumns = filterOutSystemColumns;
const getSystemColumnsIds = (columns) => {
    return ((columns && columns.filter(isSystemColumn)) || []).map((c) => c.id);
};
exports.getSystemColumnsIds = getSystemColumnsIds;
const getSystemColumns = (columns) => columns.filter(isSystemColumn) || [];
exports.getSystemColumns = getSystemColumns;
const isSystemColumn = (col) => col &&
    (col.uidt === UITypes_1.default.ForeignKey ||
        col.column_name === 'created_at' ||
        col.column_name === 'updated_at' ||
        (col.pk && (col.ai || col.cdf)) ||
        (col.pk && col.meta && col.meta.ag) ||
        col.system);
exports.isSystemColumn = isSystemColumn;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVscGVyRnVuY3Rpb25zLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9oZWxwZXJGdW5jdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsd0RBQWdDO0FBQ2hDLDJDQUEyQztBQUUzQyxrREFBa0Q7QUFDbEQsTUFBTSxzQkFBc0IsR0FBRyxDQUFDLE9BQU8sRUFBRSxFQUFFO0lBQ3pDLE9BQU8sQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztBQUN0RSxDQUFDLENBQUM7QUFpQkEsd0RBQXNCO0FBaEJ4QixNQUFNLG1CQUFtQixHQUFHLENBQUMsT0FBTyxFQUFFLEVBQUU7SUFDdEMsT0FBTyxDQUFDLENBQUMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztBQUM5RSxDQUFDLENBQUM7QUFlQSxrREFBbUI7QUFickIsTUFBTSxnQkFBZ0IsR0FBRyxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLENBQUM7QUFjekUsNENBQWdCO0FBWmxCLE1BQU0sY0FBYyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FDN0IsR0FBRztJQUNILENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxpQkFBTyxDQUFDLFVBQVU7UUFDOUIsR0FBRyxDQUFDLFdBQVcsS0FBSyxZQUFZO1FBQ2hDLEdBQUcsQ0FBQyxXQUFXLEtBQUssWUFBWTtRQUNoQyxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMvQixDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksR0FBRyxDQUFDLElBQUksSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUNuQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7QUFNZCx3Q0FBYyJ9