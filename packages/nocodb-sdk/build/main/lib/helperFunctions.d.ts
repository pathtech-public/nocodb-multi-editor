declare const filterOutSystemColumns: (columns: any) => any;
declare const getSystemColumnsIds: (columns: any) => any;
declare const getSystemColumns: (columns: any) => any;
declare const isSystemColumn: (col: any) => any;
export { filterOutSystemColumns, getSystemColumnsIds, getSystemColumns, isSystemColumn, };
