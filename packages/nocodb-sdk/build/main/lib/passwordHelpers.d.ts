export declare function validatePassword(p: any): {
    error: string;
    valid: boolean;
    progress: number;
    hint: any;
};
