"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validatePassword = void 0;
function validatePassword(p) {
    let error = '';
    let progress = 0;
    let hint = null;
    let valid = true;
    if (!p) {
        error =
            'At least 8 letters with one Uppercase, one number and one special letter';
        valid = false;
    }
    else {
        if (!(p.length >= 8)) {
            error += 'Atleast 8 letters. ';
            valid = false;
        }
        else {
            progress = Math.min(100, progress + 25);
        }
        if (!p.match(/.*[A-Z].*/)) {
            error += 'One Uppercase Letter. ';
            valid = false;
        }
        else {
            progress = Math.min(100, progress + 25);
        }
        if (!p.match(/.*[0-9].*/)) {
            error += 'One Number. ';
            valid = false;
        }
        else {
            progress = Math.min(100, progress + 25);
        }
        if (!p.match(/[$&+,:;=?@#|'<>.^*()%!_-]/)) {
            error += 'One special letter. ';
            hint = "Allowed special character list :  $&+,:;=?@#|'<>.^*()%!_-";
            valid = false;
        }
        else {
            progress = Math.min(100, progress + 25);
        }
    }
    return { error, valid, progress, hint };
}
exports.validatePassword = validatePassword;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFzc3dvcmRIZWxwZXJzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9wYXNzd29yZEhlbHBlcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsU0FBZ0IsZ0JBQWdCLENBQUMsQ0FBQztJQUNoQyxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUM7SUFDZixJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUM7SUFDakIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO0lBQ2hCLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQztJQUNqQixJQUFJLENBQUMsQ0FBQyxFQUFFO1FBQ04sS0FBSztZQUNILDBFQUEwRSxDQUFDO1FBQzdFLEtBQUssR0FBRyxLQUFLLENBQUM7S0FDZjtTQUFNO1FBQ0wsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsRUFBRTtZQUNwQixLQUFLLElBQUkscUJBQXFCLENBQUM7WUFDL0IsS0FBSyxHQUFHLEtBQUssQ0FBQztTQUNmO2FBQU07WUFDTCxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsUUFBUSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1NBQ3pDO1FBRUQsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDekIsS0FBSyxJQUFJLHdCQUF3QixDQUFDO1lBQ2xDLEtBQUssR0FBRyxLQUFLLENBQUM7U0FDZjthQUFNO1lBQ0wsUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLFFBQVEsR0FBRyxFQUFFLENBQUMsQ0FBQztTQUN6QztRQUVELElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQ3pCLEtBQUssSUFBSSxjQUFjLENBQUM7WUFDeEIsS0FBSyxHQUFHLEtBQUssQ0FBQztTQUNmO2FBQU07WUFDTCxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsUUFBUSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1NBQ3pDO1FBRUQsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsMkJBQTJCLENBQUMsRUFBRTtZQUN6QyxLQUFLLElBQUksc0JBQXNCLENBQUM7WUFDaEMsSUFBSSxHQUFHLDJEQUEyRCxDQUFDO1lBQ25FLEtBQUssR0FBRyxLQUFLLENBQUM7U0FDZjthQUFNO1lBQ0wsUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLFFBQVEsR0FBRyxFQUFFLENBQUMsQ0FBQztTQUN6QztLQUNGO0lBQ0QsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDO0FBQzFDLENBQUM7QUF4Q0QsNENBd0NDIn0=