import jsep from 'jsep';
import { ColumnType } from './Api';
export declare const jsepCurlyHook: jsep.IPlugin;
export declare function substituteColumnAliasWithIdInFormula(formula: any, columns: ColumnType[]): Promise<any>;
export declare function substituteColumnIdWithAliasInFormula(formula: any, columns: ColumnType[], rawFormula?: any): any;
export declare function jsepTreeToFormula(node: any): any;
