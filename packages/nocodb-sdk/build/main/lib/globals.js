"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModelTypes = exports.PluginCategory = exports.AuditOperationSubTypes = exports.AuditOperationTypes = exports.ErrorMessages = exports.ExportTypes = exports.RelationTypes = exports.ViewTypes = void 0;
var ViewTypes;
(function (ViewTypes) {
    ViewTypes[ViewTypes["FORM"] = 1] = "FORM";
    ViewTypes[ViewTypes["GALLERY"] = 2] = "GALLERY";
    ViewTypes[ViewTypes["GRID"] = 3] = "GRID";
    ViewTypes[ViewTypes["KANBAN"] = 4] = "KANBAN";
})(ViewTypes = exports.ViewTypes || (exports.ViewTypes = {}));
var RelationTypes;
(function (RelationTypes) {
    RelationTypes["HAS_MANY"] = "hm";
    RelationTypes["BELONGS_TO"] = "bt";
    RelationTypes["MANY_TO_MANY"] = "mm";
})(RelationTypes = exports.RelationTypes || (exports.RelationTypes = {}));
var ExportTypes;
(function (ExportTypes) {
    ExportTypes["EXCEL"] = "excel";
    ExportTypes["CSV"] = "csv";
})(ExportTypes = exports.ExportTypes || (exports.ExportTypes = {}));
var ErrorMessages;
(function (ErrorMessages) {
    ErrorMessages["INVALID_SHARED_VIEW_PASSWORD"] = "INVALID_SHARED_VIEW_PASSWORD";
    ErrorMessages["NOT_IMPLEMENTED"] = "NOT_IMPLEMENTED";
})(ErrorMessages = exports.ErrorMessages || (exports.ErrorMessages = {}));
var AuditOperationTypes;
(function (AuditOperationTypes) {
    AuditOperationTypes["COMMENT"] = "COMMENT";
    AuditOperationTypes["DATA"] = "DATA";
    AuditOperationTypes["PROJECT"] = "PROJECT";
    AuditOperationTypes["VIRTUAL_RELATION"] = "VIRTUAL_RELATION";
    AuditOperationTypes["RELATION"] = "RELATION";
    AuditOperationTypes["TABLE_VIEW"] = "TABLE_VIEW";
    AuditOperationTypes["TABLE"] = "TABLE";
    AuditOperationTypes["VIEW"] = "VIEW";
    AuditOperationTypes["META"] = "META";
    AuditOperationTypes["WEBHOOKS"] = "WEBHOOKS";
    AuditOperationTypes["AUTHENTICATION"] = "AUTHENTICATION";
    AuditOperationTypes["TABLE_COLUMN"] = "TABLE_COLUMN";
})(AuditOperationTypes = exports.AuditOperationTypes || (exports.AuditOperationTypes = {}));
var AuditOperationSubTypes;
(function (AuditOperationSubTypes) {
    AuditOperationSubTypes["UPDATE"] = "UPDATE";
    AuditOperationSubTypes["INSERT"] = "INSERT";
    AuditOperationSubTypes["DELETE"] = "DELETE";
    AuditOperationSubTypes["CREATED"] = "CREATED";
    AuditOperationSubTypes["DELETED"] = "DELETED";
    AuditOperationSubTypes["RENAMED"] = "RENAMED";
    AuditOperationSubTypes["IMPORT_FROM_ZIP"] = "IMPORT_FROM_ZIP";
    AuditOperationSubTypes["EXPORT_TO_FS"] = "EXPORT_TO_FS";
    AuditOperationSubTypes["EXPORT_TO_ZIP"] = "EXPORT_TO_ZIP";
    AuditOperationSubTypes["UPDATED"] = "UPDATED";
    AuditOperationSubTypes["SIGNIN"] = "SIGNIN";
    AuditOperationSubTypes["SIGN"] = "SIGN";
    AuditOperationSubTypes["PASSWORD_RESET"] = "PASSWORD_RESET";
    AuditOperationSubTypes["PASSWORD_FORGOT"] = "PASSWORD_FORGOT";
    AuditOperationSubTypes["PASSWORD_CHANGE"] = "PASSWORD_CHANGE";
    AuditOperationSubTypes["EMAIL_VERIFICATION"] = "EMAIL_VERIFICATION";
    AuditOperationSubTypes["ROLES_MANAGEMENT"] = "ROLES_MANAGEMENT";
    AuditOperationSubTypes["INVITE"] = "INVITE";
    AuditOperationSubTypes["RESEND_INVITE"] = "RESEND_INVITE";
})(AuditOperationSubTypes = exports.AuditOperationSubTypes || (exports.AuditOperationSubTypes = {}));
var PluginCategory;
(function (PluginCategory) {
    PluginCategory["STORAGE"] = "Storage";
    PluginCategory["EMAIL"] = "Email";
})(PluginCategory = exports.PluginCategory || (exports.PluginCategory = {}));
var ModelTypes;
(function (ModelTypes) {
    ModelTypes["TABLE"] = "table";
    ModelTypes["VIEW"] = "view";
})(ModelTypes = exports.ModelTypes || (exports.ModelTypes = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2xvYmFscy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvZ2xvYmFscy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSxJQUFZLFNBS1g7QUFMRCxXQUFZLFNBQVM7SUFDbkIseUNBQVEsQ0FBQTtJQUNSLCtDQUFXLENBQUE7SUFDWCx5Q0FBUSxDQUFBO0lBQ1IsNkNBQVUsQ0FBQTtBQUNaLENBQUMsRUFMVyxTQUFTLEdBQVQsaUJBQVMsS0FBVCxpQkFBUyxRQUtwQjtBQUVELElBQVksYUFJWDtBQUpELFdBQVksYUFBYTtJQUN2QixnQ0FBZSxDQUFBO0lBQ2Ysa0NBQWlCLENBQUE7SUFDakIsb0NBQW1CLENBQUE7QUFDckIsQ0FBQyxFQUpXLGFBQWEsR0FBYixxQkFBYSxLQUFiLHFCQUFhLFFBSXhCO0FBRUQsSUFBWSxXQUdYO0FBSEQsV0FBWSxXQUFXO0lBQ3JCLDhCQUFlLENBQUE7SUFDZiwwQkFBVyxDQUFBO0FBQ2IsQ0FBQyxFQUhXLFdBQVcsR0FBWCxtQkFBVyxLQUFYLG1CQUFXLFFBR3RCO0FBRUQsSUFBWSxhQUdYO0FBSEQsV0FBWSxhQUFhO0lBQ3ZCLDhFQUE2RCxDQUFBO0lBQzdELG9EQUFtQyxDQUFBO0FBQ3JDLENBQUMsRUFIVyxhQUFhLEdBQWIscUJBQWEsS0FBYixxQkFBYSxRQUd4QjtBQUVELElBQVksbUJBYVg7QUFiRCxXQUFZLG1CQUFtQjtJQUM3QiwwQ0FBbUIsQ0FBQTtJQUNuQixvQ0FBYSxDQUFBO0lBQ2IsMENBQW1CLENBQUE7SUFDbkIsNERBQXFDLENBQUE7SUFDckMsNENBQXFCLENBQUE7SUFDckIsZ0RBQXlCLENBQUE7SUFDekIsc0NBQWUsQ0FBQTtJQUNmLG9DQUFhLENBQUE7SUFDYixvQ0FBYSxDQUFBO0lBQ2IsNENBQXFCLENBQUE7SUFDckIsd0RBQWlDLENBQUE7SUFDakMsb0RBQTZCLENBQUE7QUFDL0IsQ0FBQyxFQWJXLG1CQUFtQixHQUFuQiwyQkFBbUIsS0FBbkIsMkJBQW1CLFFBYTlCO0FBRUQsSUFBWSxzQkFvQlg7QUFwQkQsV0FBWSxzQkFBc0I7SUFDaEMsMkNBQWlCLENBQUE7SUFDakIsMkNBQWlCLENBQUE7SUFDakIsMkNBQWlCLENBQUE7SUFDakIsNkNBQW1CLENBQUE7SUFDbkIsNkNBQW1CLENBQUE7SUFDbkIsNkNBQW1CLENBQUE7SUFDbkIsNkRBQW1DLENBQUE7SUFDbkMsdURBQTZCLENBQUE7SUFDN0IseURBQStCLENBQUE7SUFDL0IsNkNBQW1CLENBQUE7SUFDbkIsMkNBQWlCLENBQUE7SUFDakIsdUNBQWEsQ0FBQTtJQUNiLDJEQUFpQyxDQUFBO0lBQ2pDLDZEQUFtQyxDQUFBO0lBQ25DLDZEQUFtQyxDQUFBO0lBQ25DLG1FQUF5QyxDQUFBO0lBQ3pDLCtEQUFxQyxDQUFBO0lBQ3JDLDJDQUFpQixDQUFBO0lBQ2pCLHlEQUErQixDQUFBO0FBQ2pDLENBQUMsRUFwQlcsc0JBQXNCLEdBQXRCLDhCQUFzQixLQUF0Qiw4QkFBc0IsUUFvQmpDO0FBRUQsSUFBWSxjQUdYO0FBSEQsV0FBWSxjQUFjO0lBQ3hCLHFDQUFtQixDQUFBO0lBQ25CLGlDQUFlLENBQUE7QUFDakIsQ0FBQyxFQUhXLGNBQWMsR0FBZCxzQkFBYyxLQUFkLHNCQUFjLFFBR3pCO0FBRUQsSUFBWSxVQUdYO0FBSEQsV0FBWSxVQUFVO0lBQ3BCLDZCQUFlLENBQUE7SUFDZiwyQkFBYSxDQUFBO0FBQ2YsQ0FBQyxFQUhXLFVBQVUsR0FBVixrQkFBVSxLQUFWLGtCQUFVLFFBR3JCIn0=