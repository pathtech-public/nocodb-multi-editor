"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OracleUi = void 0;
class OracleUi {
    static getNewTableColumns() {
        return [
            {
                column_name: 'id',
                title: 'Id',
                dt: 'integer',
                dtx: 'integer',
                ct: 'int(11)',
                nrqd: false,
                rqd: true,
                ck: false,
                pk: true,
                un: false,
                ai: false,
                cdf: null,
                clen: null,
                np: null,
                ns: null,
                dtxp: '',
                dtxs: '',
                altered: 1,
                uidt: 'ID',
                uip: '',
                uicn: '',
            },
            {
                column_name: 'title',
                title: 'Title',
                dt: 'varchar',
                dtx: 'specificType',
                ct: 'varchar(45)',
                nrqd: true,
                rqd: false,
                ck: false,
                pk: false,
                un: false,
                ai: false,
                cdf: null,
                clen: 45,
                np: null,
                ns: null,
                dtxp: '45',
                dtxs: '',
                altered: 1,
                uidt: 'SingleLineText',
                uip: '',
                uicn: '',
            },
            // {
            //  column_name: "created_at",
            //   dt: "timestamp",
            //   dtx: "specificType",
            //   ct: "varchar(45)",
            //   nrqd: true,
            //   rqd: false,
            //   ck: false,
            //   pk: false,
            //   un: false,
            //   ai: false,
            //   cdf: 'CURRENT_TIMESTAMP',
            //   clen: 45,
            //   np: null,
            //   ns: null,
            //   dtxp: '',
            //   dtxs: ''
            // },
            // {
            //  column_name: "updated_at",
            //   dt: "timestamp",
            //   dtx: "specificType",
            //   ct: "varchar(45)",
            //   nrqd: true,
            //   rqd: false,
            //   ck: false,
            //   pk: false,
            //   un: false,
            //   ai: false,
            //   cdf: 'CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP',
            //   clen: 45,
            //   np: null,
            //   ns: null,
            //   dtxp: '',
            //   dtxs: ''
            // }
        ];
    }
    static getNewColumn(suffix) {
        return {
            column_name: 'title' + suffix,
            dt: 'varchar',
            dtx: 'specificType',
            ct: 'varchar(45)',
            nrqd: true,
            rqd: false,
            ck: false,
            pk: false,
            un: false,
            ai: false,
            cdf: null,
            clen: 45,
            np: null,
            ns: null,
            dtxp: '45',
            dtxs: '',
            altered: 1,
            uidt: 'SingleLineText',
            uip: '',
            uicn: '',
        };
    }
    // static getDefaultLengthForDatatype(type) {
    //   switch (type) {
    //     case "int":
    //       return 11;
    //       break;
    //     case "tinyint":
    //       return 1;
    //       break;
    //     case "smallint":
    //       return 5;
    //       break;
    //
    //     case "mediumint":
    //       return 9;
    //       break;
    //     case "bigint":
    //       return 20;
    //       break;
    //     case "bit":
    //       return 64;
    //       break;
    //     case "boolean":
    //       return '';
    //       break;
    //     case "float":
    //       return 12;
    //       break;
    //     case "decimal":
    //       return 10;
    //       break;
    //     case "double":
    //       return 22;
    //       break;
    //     case "serial":
    //       return 20;
    //       break;
    //     case "date":
    //       return '';
    //       break;
    //     case "datetime":
    //     case "timestamp":
    //       return 6;
    //       break;
    //     case "time":
    //       return '';
    //       break;
    //     case "year":
    //       return '';
    //       break;
    //     case "char":
    //       return 255;
    //       break;
    //     case "varchar":
    //       return 45;
    //       break;
    //     case "nchar":
    //       return 255;
    //       break;
    //     case "text":
    //       return '';
    //       break;
    //     case "tinytext":
    //       return '';
    //       break;
    //     case "mediumtext":
    //       return '';
    //       break;
    //     case "longtext":
    //       return ''
    //       break;
    //     case "binary":
    //       return 255;
    //       break;
    //     case "varbinary":
    //       return 65500;
    //       break;
    //     case "blob":
    //       return '';
    //       break;
    //     case "tinyblob":
    //       return '';
    //       break;
    //     case "mediumblob":
    //       return '';
    //       break;
    //     case "longblob":
    //       return '';
    //       break;
    //     case "enum":
    //       return '\'a\',\'b\'';
    //       break;
    //     case "set":
    //       return '\'a\',\'b\'';
    //       break;
    //     case "geometry":
    //       return '';
    //     case "point":
    //       return '';
    //     case "linestring":
    //       return '';
    //     case "polygon":
    //       return '';
    //     case "multipoint":
    //       return '';
    //     case "multilinestring":
    //       return '';
    //     case "multipolygon":
    //       return '';
    //     case "json":
    //       return ''
    //       break;
    //
    //   }
    //
    // }
    static getDefaultLengthForDatatype(type) {
        switch (type) {
            default:
                return '';
        }
    }
    static getDefaultLengthIsDisabled(type) {
        switch (type) {
            case 'integer':
                return true;
            case 'bfile':
            case 'binary rowid':
            case 'binary double':
            case 'binary_float':
            case 'blob':
            case 'canoical':
            case 'cfile':
            case 'char':
            case 'clob':
            case 'content pointer':
            case 'contigous array':
            case 'date':
            case 'decimal':
            case 'double precision':
            case 'float':
            case 'interval day to second':
            case 'interval year to month':
            case 'lob pointer':
            case 'long':
            case 'long raw':
            case 'named collection':
            case 'named object':
            case 'nchar':
            case 'nclob':
            case 'number':
            case 'nvarchar2':
            case 'octet':
            case 'oid':
            case 'pointer':
            case 'raw':
            case 'real':
            case 'ref':
            case 'ref cursor':
            case 'rowid':
            case 'signed binary integer':
            case 'smallint':
            case 'table':
            case 'time':
            case 'time with tz':
            case 'timestamp':
            case 'timestamp with local time zone':
            case 'timestamp with local tz':
            case 'timestamp with timezone':
            case 'timestamp with tz':
            case 'unsigned binary integer':
            case 'urowid':
            case 'varchar':
            case 'varchar2':
            case 'varray':
            case 'varying array':
                return false;
        }
    }
    static getDefaultValueForDatatype(type) {
        switch (type) {
            default:
                return '';
        }
    }
    static getDefaultScaleForDatatype(type) {
        switch (type) {
            case 'integer':
            case 'bfile':
            case 'binary rowid':
            case 'binary double':
            case 'binary_float':
            case 'blob':
            case 'canoical':
            case 'cfile':
            case 'char':
            case 'clob':
            case 'content pointer':
            case 'contigous array':
            case 'date':
            case 'decimal':
            case 'double precision':
            case 'float':
            case 'interval day to second':
            case 'interval year to month':
            case 'lob pointer':
            case 'long':
            case 'long raw':
            case 'named collection':
            case 'named object':
            case 'nchar':
            case 'nclob':
            case 'number':
            case 'nvarchar2':
            case 'octet':
            case 'oid':
            case 'pointer':
            case 'raw':
            case 'real':
            case 'ref':
            case 'ref cursor':
            case 'rowid':
            case 'signed binary integer':
            case 'smallint':
            case 'table':
            case 'time':
            case 'time with tz':
            case 'timestamp':
            case 'timestamp with local time zone':
            case 'timestamp with local tz':
            case 'timestamp with timezone':
            case 'timestamp with tz':
            case 'unsigned binary integer':
            case 'urowid':
            case 'varchar':
            case 'varchar2':
            case 'varray':
            case 'varying array':
                return ' ';
        }
    }
    static colPropAIDisabled(col, columns) {
        // console.log(col);
        if (col.dt === 'int4' ||
            col.dt === 'integer' ||
            col.dt === 'bigint' ||
            col.dt === 'smallint') {
            for (let i = 0; i < columns.length; ++i) {
                if (columns[i].cn !== col.cn && columns[i].ai) {
                    return true;
                }
            }
            return false;
        }
        else {
            return true;
        }
    }
    static colPropUNDisabled(_col) {
        // console.log(col);
        return true;
        // if (col.dt === 'int' ||
        //   col.dt === 'tinyint' ||
        //   col.dt === 'smallint' ||
        //   col.dt === 'mediumint' ||
        //   col.dt === 'bigint') {
        //   return false;
        // } else {
        //   return true;
        // }
    }
    static onCheckboxChangeAI(col) {
        console.log(col);
        if (col.dt === 'int' ||
            col.dt === 'bigint' ||
            col.dt === 'smallint' ||
            col.dt === 'tinyint') {
            col.altered = col.altered || 2;
        }
        // if (!col.ai) {
        //   col.dtx = 'specificType'
        // } else {
        //   col.dtx = ''
        // }
    }
    static showScale(_columnObj) {
        return false;
    }
    static removeUnsigned(columns) {
        for (let i = 0; i < columns.length; ++i) {
            if (columns[i].altered === 1 &&
                !(columns[i].dt === 'int' ||
                    columns[i].dt === 'bigint' ||
                    columns[i].dt === 'tinyint' ||
                    columns[i].dt === 'smallint' ||
                    columns[i].dt === 'mediumint')) {
                columns[i].un = false;
                console.log('>> resetting unsigned value', columns[i].cn);
            }
            console.log(columns[i].cn);
        }
    }
    static columnEditable(colObj) {
        return colObj.tn !== '_evolutions' || colObj.tn !== 'nc_evolutions';
    }
    static extractFunctionName(query) {
        const reg = /^\s*CREATE\s+(?:OR\s+REPLACE\s*)?\s*FUNCTION\s+(?:[\w\d_]+\.)?([\w_\d]+)/i;
        const match = query.match(reg);
        return match && match[1];
    }
    static extractProcedureName(query) {
        const reg = /^\s*CREATE\s+(?:OR\s+REPLACE\s*)?\s*PROCEDURE\s+(?:[\w\d_]+\.)?([\w_\d]+)/i;
        const match = query.match(reg);
        return match && match[1];
    }
    static splitQueries(query) {
        /***
         * we are splitting based on semicolon
         * there are mechanism to escape semicolon within single/double quotes(string)
         */
        return query.match(/\b("[^"]*;[^"]*"|'[^']*;[^']*'|[^;])*;/g);
    }
    static onCheckboxChangeAU(col) {
        console.log(col);
        // if (1) {
        col.altered = col.altered || 2;
        // }
        // if (!col.ai) {
        //   col.dtx = 'specificType'
        // } else {
        //   col.dtx = ''
        // }
    }
    /**
     * if sql statement is SELECT - it limits to a number
     * @param args
     * @returns {string|*}
     */
    sanitiseQuery(args) {
        let q = args.query.trim().split(';');
        if (q[0].startsWith('Select')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else if (q[0].startsWith('select')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else if (q[0].startsWith('SELECT')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else {
            return args.query;
        }
        return q;
    }
    getColumnsFromJson(json, tn) {
        const columns = [];
        try {
            if (typeof json === 'object' && !Array.isArray(json)) {
                const keys = Object.keys(json);
                for (let i = 0; i < keys.length; ++i) {
                    switch (typeof json[keys[i]]) {
                        case 'number':
                            if (Number.isInteger(json[keys[i]])) {
                                columns.push({
                                    dp: null,
                                    tn,
                                    column_name: keys[i],
                                    cno: keys[i],
                                    dt: 'int',
                                    np: 10,
                                    ns: 0,
                                    clen: null,
                                    cop: 1,
                                    pk: false,
                                    nrqd: false,
                                    rqd: false,
                                    un: false,
                                    ct: 'int(11) unsigned',
                                    ai: false,
                                    unique: false,
                                    cdf: null,
                                    cc: '',
                                    csn: null,
                                    dtx: 'specificType',
                                    dtxp: '11',
                                    dtxs: 0,
                                    altered: 1,
                                });
                            }
                            else {
                                columns.push({
                                    dp: null,
                                    tn,
                                    column_name: keys[i],
                                    cno: keys[i],
                                    dt: 'float',
                                    np: 10,
                                    ns: 2,
                                    clen: null,
                                    cop: 1,
                                    pk: false,
                                    nrqd: false,
                                    rqd: false,
                                    un: false,
                                    ct: 'int(11) unsigned',
                                    ai: false,
                                    unique: false,
                                    cdf: null,
                                    cc: '',
                                    csn: null,
                                    dtx: 'specificType',
                                    dtxp: '11',
                                    dtxs: 2,
                                    altered: 1,
                                });
                            }
                            break;
                        case 'string':
                            if (json[keys[i]].length <= 255) {
                                columns.push({
                                    dp: null,
                                    tn,
                                    column_name: keys[i],
                                    cno: keys[i],
                                    dt: 'varchar',
                                    np: 45,
                                    ns: 0,
                                    clen: null,
                                    cop: 1,
                                    pk: false,
                                    nrqd: false,
                                    rqd: false,
                                    un: false,
                                    ct: 'int(11) unsigned',
                                    ai: false,
                                    unique: false,
                                    cdf: null,
                                    cc: '',
                                    csn: null,
                                    dtx: 'specificType',
                                    dtxp: '45',
                                    dtxs: 0,
                                    altered: 1,
                                });
                            }
                            else {
                                columns.push({
                                    dp: null,
                                    tn,
                                    column_name: keys[i],
                                    cno: keys[i],
                                    dt: 'text',
                                    np: null,
                                    ns: 0,
                                    clen: null,
                                    cop: 1,
                                    pk: false,
                                    nrqd: false,
                                    rqd: false,
                                    un: false,
                                    ct: 'int(11) unsigned',
                                    ai: false,
                                    unique: false,
                                    cdf: null,
                                    cc: '',
                                    csn: null,
                                    dtx: 'specificType',
                                    dtxp: null,
                                    dtxs: 0,
                                    altered: 1,
                                });
                            }
                            break;
                        case 'boolean':
                            columns.push({
                                dp: null,
                                tn,
                                column_name: keys[i],
                                cno: keys[i],
                                dt: 'boolean',
                                np: 3,
                                ns: 0,
                                clen: null,
                                cop: 1,
                                pk: false,
                                nrqd: false,
                                rqd: false,
                                un: false,
                                ct: 'int(11) unsigned',
                                ai: false,
                                unique: false,
                                cdf: null,
                                cc: '',
                                csn: null,
                                dtx: 'specificType',
                                dtxp: '1',
                                dtxs: 0,
                                altered: 1,
                            });
                            break;
                        case 'object':
                            columns.push({
                                dp: null,
                                tn,
                                column_name: keys[i],
                                cno: keys[i],
                                dt: 'json',
                                np: 3,
                                ns: 0,
                                clen: null,
                                cop: 1,
                                pk: false,
                                nrqd: false,
                                rqd: false,
                                un: false,
                                ct: 'int(11) unsigned',
                                ai: false,
                                unique: false,
                                cdf: null,
                                cc: '',
                                csn: null,
                                dtx: 'specificType',
                                dtxp: null,
                                dtxs: 0,
                                altered: 1,
                            });
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        catch (e) {
            console.log('Error in getColumnsFromJson', e);
        }
        return columns;
    }
    static colPropAuDisabled(_col) {
        return true;
    }
    static getAbstractType(col) {
        switch ((col.dt || col.dt).toLowerCase()) {
            case 'integer':
                return 'integer';
            case 'bfile':
            case 'binary rowid':
            case 'binary double':
            case 'binary_float':
                return 'string';
            case 'blob':
                return 'blob';
            case 'canoical':
            case 'cfile':
            case 'char':
            case 'clob':
            case 'content pointer':
            case 'contigous array':
                return 'string';
            case 'date':
                return 'date';
            case 'decimal':
            case 'double precision':
            case 'float':
                return 'float';
            case 'interval day to second':
            case 'interval year to month':
                return 'string';
            case 'lob pointer':
                return 'string';
            case 'long':
                return 'integer';
            case 'long raw':
                return 'string';
            case 'named collection':
            case 'named object':
            case 'nchar':
            case 'nclob':
                return 'string';
            case 'nvarchar2':
            case 'octet':
            case 'oid':
            case 'pointer':
            case 'raw':
                return 'string';
            case 'real':
            case 'number':
                return 'float';
            case 'ref':
            case 'ref cursor':
            case 'rowid':
            case 'signed binary integer':
                return 'string';
            case 'smallint':
                return 'integer';
            case 'table':
                return 'string';
            case 'time':
            case 'time with tz':
                return 'time';
            case 'timestamp':
            case 'timestamp with local time zone':
            case 'timestamp with local tz':
            case 'timestamp with timezone':
            case 'timestamp with tz':
                return 'datetime';
            case 'unsigned binary integer':
            case 'urowid':
            case 'varchar':
            case 'varchar2':
                return 'string';
            case 'varray':
            case 'varying array':
                return 'string';
        }
    }
    static getUIType(col) {
        switch (this.getAbstractType(col)) {
            case 'integer':
                return 'Number';
            case 'boolean':
                return 'Checkbox';
            case 'float':
                return 'Decimal';
            case 'date':
                return 'Date';
            case 'datetime':
                return 'CreateTime';
            case 'time':
                return 'Time';
            case 'year':
                return 'Year';
            case 'string':
                return 'SingleLineText';
            case 'text':
                return 'LongText';
            case 'blob':
                return 'Attachment';
            case 'enum':
                return 'SingleSelect';
            case 'set':
                return 'MultiSelect';
            case 'json':
                return 'LongText';
        }
    }
    static getDataTypeForUiType(col, idType) {
        const colProp = {};
        switch (col.uidt) {
            case 'ID':
                {
                    const isAutoIncId = idType === 'AI';
                    const isAutoGenId = idType === 'AG';
                    colProp.dt = isAutoGenId ? 'varchar' : 'integer';
                    colProp.pk = true;
                    colProp.un = isAutoIncId;
                    colProp.ai = isAutoIncId;
                    colProp.rqd = true;
                    colProp.meta = isAutoGenId ? { ag: 'nc' } : undefined;
                }
                break;
            case 'ForeignKey':
                colProp.dt = 'varchar';
                break;
            case 'SingleLineText':
                colProp.dt = 'varchar';
                break;
            case 'LongText':
                colProp.dt = 'clob';
                break;
            case 'Attachment':
                colProp.dt = 'clob';
                break;
            case 'Checkbox':
                colProp.dt = 'tinyint';
                colProp.dtxp = 1;
                break;
            case 'MultiSelect':
                colProp.dt = 'varchar2';
                break;
            case 'SingleSelect':
                colProp.dt = 'varchar2';
                break;
            case 'Collaborator':
                colProp.dt = 'varchar';
                break;
            case 'Date':
                colProp.dt = 'varchar';
                break;
            case 'Year':
                colProp.dt = 'year';
                break;
            case 'Time':
                colProp.dt = 'time';
                break;
            case 'PhoneNumber':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isMobilePhone'],
                    args: [''],
                    msg: ['Validation failed : isMobilePhone'],
                };
                break;
            case 'Email':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isEmail'],
                    args: [''],
                    msg: ['Validation failed : isEmail'],
                };
                break;
            case 'URL':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isURL'],
                    args: [''],
                    msg: ['Validation failed : isURL'],
                };
                break;
            case 'Number':
                colProp.dt = 'integer';
                break;
            case 'Decimal':
                colProp.dt = 'decimal';
                break;
            case 'Currency':
                colProp.dt = 'decimal';
                colProp.validate = {
                    func: ['isCurrency'],
                    args: [''],
                    msg: ['Validation failed : isCurrency'],
                };
                break;
            case 'Percent':
                colProp.dt = 'double';
                break;
            case 'Duration':
                colProp.dt = 'integer';
                break;
            case 'Rating':
                colProp.dt = 'integer';
                break;
            case 'Formula':
                colProp.dt = 'varchar';
                break;
            case 'Rollup':
                colProp.dt = 'varchar';
                break;
            case 'Count':
                colProp.dt = 'integer';
                break;
            case 'Lookup':
                colProp.dt = 'varchar';
                break;
            case 'DateTime':
                colProp.dt = 'timestamp';
                break;
            case 'CreateTime':
                colProp.dt = 'timestamp';
                break;
            case 'LastModifiedTime':
                colProp.dt = 'timestamp';
                break;
            case 'AutoNumber':
                colProp.dt = 'integer';
                break;
            case 'Barcode':
                colProp.dt = 'varchar';
                break;
            case 'Button':
                colProp.dt = 'varchar';
                break;
            default:
                colProp.dt = 'varchar';
                break;
        }
        return colProp;
    }
    static getUnsupportedFnList() {
        return [];
    }
}
exports.OracleUi = OracleUi;
// module.exports = PgUiHelp;
/**
 * @copyright Copyright (c) 2021, Xgene Cloud Ltd
 *
 * @author Naveen MR <oof1lab@gmail.com>
 * @author Pranav C Balan <pranavxc@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiT3JhY2xlVWkuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvbGliL3NxbFVpL09yYWNsZVVpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUdBLE1BQWEsUUFBUTtJQUNuQixNQUFNLENBQUMsa0JBQWtCO1FBQ3ZCLE9BQU87WUFDTDtnQkFDRSxXQUFXLEVBQUUsSUFBSTtnQkFDakIsS0FBSyxFQUFFLElBQUk7Z0JBQ1gsRUFBRSxFQUFFLFNBQVM7Z0JBQ2IsR0FBRyxFQUFFLFNBQVM7Z0JBQ2QsRUFBRSxFQUFFLFNBQVM7Z0JBQ2IsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLElBQUk7Z0JBQ1IsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsRUFBRSxFQUFFLElBQUk7Z0JBQ1IsRUFBRSxFQUFFLElBQUk7Z0JBQ1IsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsT0FBTyxFQUFFLENBQUM7Z0JBQ1YsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsR0FBRyxFQUFFLEVBQUU7Z0JBQ1AsSUFBSSxFQUFFLEVBQUU7YUFDVDtZQUNEO2dCQUNFLFdBQVcsRUFBRSxPQUFPO2dCQUNwQixLQUFLLEVBQUUsT0FBTztnQkFDZCxFQUFFLEVBQUUsU0FBUztnQkFDYixHQUFHLEVBQUUsY0FBYztnQkFDbkIsRUFBRSxFQUFFLGFBQWE7Z0JBQ2pCLElBQUksRUFBRSxJQUFJO2dCQUNWLEdBQUcsRUFBRSxLQUFLO2dCQUNWLEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEdBQUcsRUFBRSxJQUFJO2dCQUNULElBQUksRUFBRSxFQUFFO2dCQUNSLEVBQUUsRUFBRSxJQUFJO2dCQUNSLEVBQUUsRUFBRSxJQUFJO2dCQUNSLElBQUksRUFBRSxJQUFJO2dCQUNWLElBQUksRUFBRSxFQUFFO2dCQUNSLE9BQU8sRUFBRSxDQUFDO2dCQUNWLElBQUksRUFBRSxnQkFBZ0I7Z0JBQ3RCLEdBQUcsRUFBRSxFQUFFO2dCQUNQLElBQUksRUFBRSxFQUFFO2FBQ1Q7WUFDRCxJQUFJO1lBQ0osOEJBQThCO1lBQzlCLHFCQUFxQjtZQUNyQix5QkFBeUI7WUFDekIsdUJBQXVCO1lBQ3ZCLGdCQUFnQjtZQUNoQixnQkFBZ0I7WUFDaEIsZUFBZTtZQUNmLGVBQWU7WUFDZixlQUFlO1lBQ2YsZUFBZTtZQUNmLDhCQUE4QjtZQUM5QixjQUFjO1lBQ2QsY0FBYztZQUNkLGNBQWM7WUFDZCxjQUFjO1lBQ2QsYUFBYTtZQUNiLEtBQUs7WUFDTCxJQUFJO1lBQ0osOEJBQThCO1lBQzlCLHFCQUFxQjtZQUNyQix5QkFBeUI7WUFDekIsdUJBQXVCO1lBQ3ZCLGdCQUFnQjtZQUNoQixnQkFBZ0I7WUFDaEIsZUFBZTtZQUNmLGVBQWU7WUFDZixlQUFlO1lBQ2YsZUFBZTtZQUNmLDBEQUEwRDtZQUMxRCxjQUFjO1lBQ2QsY0FBYztZQUNkLGNBQWM7WUFDZCxjQUFjO1lBQ2QsYUFBYTtZQUNiLElBQUk7U0FDTCxDQUFDO0lBQ0osQ0FBQztJQUVELE1BQU0sQ0FBQyxZQUFZLENBQUMsTUFBTTtRQUN4QixPQUFPO1lBQ0wsV0FBVyxFQUFFLE9BQU8sR0FBRyxNQUFNO1lBQzdCLEVBQUUsRUFBRSxTQUFTO1lBQ2IsR0FBRyxFQUFFLGNBQWM7WUFDbkIsRUFBRSxFQUFFLGFBQWE7WUFDakIsSUFBSSxFQUFFLElBQUk7WUFDVixHQUFHLEVBQUUsS0FBSztZQUNWLEVBQUUsRUFBRSxLQUFLO1lBQ1QsRUFBRSxFQUFFLEtBQUs7WUFDVCxFQUFFLEVBQUUsS0FBSztZQUNULEVBQUUsRUFBRSxLQUFLO1lBQ1QsR0FBRyxFQUFFLElBQUk7WUFDVCxJQUFJLEVBQUUsRUFBRTtZQUNSLEVBQUUsRUFBRSxJQUFJO1lBQ1IsRUFBRSxFQUFFLElBQUk7WUFDUixJQUFJLEVBQUUsSUFBSTtZQUNWLElBQUksRUFBRSxFQUFFO1lBQ1IsT0FBTyxFQUFFLENBQUM7WUFDVixJQUFJLEVBQUUsZ0JBQWdCO1lBQ3RCLEdBQUcsRUFBRSxFQUFFO1lBQ1AsSUFBSSxFQUFFLEVBQUU7U0FDVCxDQUFDO0lBQ0osQ0FBQztJQUVELDZDQUE2QztJQUM3QyxvQkFBb0I7SUFDcEIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysc0JBQXNCO0lBQ3RCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsRUFBRTtJQUNGLHdCQUF3QjtJQUN4QixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHNCQUFzQjtJQUN0QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLG9CQUFvQjtJQUNwQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHNCQUFzQjtJQUN0QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHFCQUFxQjtJQUNyQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHVCQUF1QjtJQUN2Qix3QkFBd0I7SUFDeEIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsb0JBQW9CO0lBQ3BCLGVBQWU7SUFDZixzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixvQkFBb0I7SUFDcEIsb0JBQW9CO0lBQ3BCLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZix5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZix1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsb0JBQW9CO0lBQ3BCLGVBQWU7SUFDZix3QkFBd0I7SUFDeEIsc0JBQXNCO0lBQ3RCLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZix5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsOEJBQThCO0lBQzlCLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsOEJBQThCO0lBQzlCLGVBQWU7SUFDZix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixtQkFBbUI7SUFDbkIseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsOEJBQThCO0lBQzlCLG1CQUFtQjtJQUNuQiwyQkFBMkI7SUFDM0IsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLEVBQUU7SUFDRixNQUFNO0lBQ04sRUFBRTtJQUNGLElBQUk7SUFFSixNQUFNLENBQUMsMkJBQTJCLENBQUMsSUFBSTtRQUNyQyxRQUFRLElBQUksRUFBRTtZQUNaO2dCQUNFLE9BQU8sRUFBRSxDQUFDO1NBQ2I7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLDBCQUEwQixDQUFDLElBQUk7UUFDcEMsUUFBUSxJQUFJLEVBQUU7WUFDWixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxJQUFJLENBQUM7WUFDZCxLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssY0FBYyxDQUFDO1lBQ3BCLEtBQUssZUFBZSxDQUFDO1lBQ3JCLEtBQUssY0FBYyxDQUFDO1lBQ3BCLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxpQkFBaUIsQ0FBQztZQUN2QixLQUFLLGlCQUFpQixDQUFDO1lBQ3ZCLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLGtCQUFrQixDQUFDO1lBQ3hCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyx3QkFBd0IsQ0FBQztZQUM5QixLQUFLLHdCQUF3QixDQUFDO1lBQzlCLEtBQUssYUFBYSxDQUFDO1lBQ25CLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxrQkFBa0IsQ0FBQztZQUN4QixLQUFLLGNBQWMsQ0FBQztZQUNwQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssS0FBSyxDQUFDO1lBQ1gsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLEtBQUssQ0FBQztZQUNYLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLFlBQVksQ0FBQztZQUNsQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssdUJBQXVCLENBQUM7WUFDN0IsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssY0FBYyxDQUFDO1lBQ3BCLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssZ0NBQWdDLENBQUM7WUFDdEMsS0FBSyx5QkFBeUIsQ0FBQztZQUMvQixLQUFLLHlCQUF5QixDQUFDO1lBQy9CLEtBQUssbUJBQW1CLENBQUM7WUFDekIsS0FBSyx5QkFBeUIsQ0FBQztZQUMvQixLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLGVBQWU7Z0JBQ2xCLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQywwQkFBMEIsQ0FBQyxJQUFJO1FBQ3BDLFFBQVEsSUFBSSxFQUFFO1lBQ1o7Z0JBQ0UsT0FBTyxFQUFFLENBQUM7U0FDYjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsMEJBQTBCLENBQUMsSUFBSTtRQUNwQyxRQUFRLElBQUksRUFBRTtZQUNaLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLGNBQWMsQ0FBQztZQUNwQixLQUFLLGVBQWUsQ0FBQztZQUNyQixLQUFLLGNBQWMsQ0FBQztZQUNwQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssaUJBQWlCLENBQUM7WUFDdkIsS0FBSyxpQkFBaUIsQ0FBQztZQUN2QixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxrQkFBa0IsQ0FBQztZQUN4QixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssd0JBQXdCLENBQUM7WUFDOUIsS0FBSyx3QkFBd0IsQ0FBQztZQUM5QixLQUFLLGFBQWEsQ0FBQztZQUNuQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssa0JBQWtCLENBQUM7WUFDeEIsS0FBSyxjQUFjLENBQUM7WUFDcEIsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxXQUFXLENBQUM7WUFDakIsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLEtBQUssQ0FBQztZQUNYLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssS0FBSyxDQUFDO1lBQ1gsS0FBSyxZQUFZLENBQUM7WUFDbEIsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLHVCQUF1QixDQUFDO1lBQzdCLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLGNBQWMsQ0FBQztZQUNwQixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLGdDQUFnQyxDQUFDO1lBQ3RDLEtBQUsseUJBQXlCLENBQUM7WUFDL0IsS0FBSyx5QkFBeUIsQ0FBQztZQUMvQixLQUFLLG1CQUFtQixDQUFDO1lBQ3pCLEtBQUsseUJBQXlCLENBQUM7WUFDL0IsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxlQUFlO2dCQUNsQixPQUFPLEdBQUcsQ0FBQztTQUNkO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLEVBQUUsT0FBTztRQUNuQyxvQkFBb0I7UUFDcEIsSUFDRSxHQUFHLENBQUMsRUFBRSxLQUFLLE1BQU07WUFDakIsR0FBRyxDQUFDLEVBQUUsS0FBSyxTQUFTO1lBQ3BCLEdBQUcsQ0FBQyxFQUFFLEtBQUssUUFBUTtZQUNuQixHQUFHLENBQUMsRUFBRSxLQUFLLFVBQVUsRUFDckI7WUFDQSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsRUFBRTtnQkFDdkMsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEdBQUcsQ0FBQyxFQUFFLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDN0MsT0FBTyxJQUFJLENBQUM7aUJBQ2I7YUFDRjtZQUNELE9BQU8sS0FBSyxDQUFDO1NBQ2Q7YUFBTTtZQUNMLE9BQU8sSUFBSSxDQUFDO1NBQ2I7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLGlCQUFpQixDQUFDLElBQUk7UUFDM0Isb0JBQW9CO1FBQ3BCLE9BQU8sSUFBSSxDQUFDO1FBQ1osMEJBQTBCO1FBQzFCLDRCQUE0QjtRQUM1Qiw2QkFBNkI7UUFDN0IsOEJBQThCO1FBQzlCLDJCQUEyQjtRQUMzQixrQkFBa0I7UUFDbEIsV0FBVztRQUNYLGlCQUFpQjtRQUNqQixJQUFJO0lBQ04sQ0FBQztJQUVELE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakIsSUFDRSxHQUFHLENBQUMsRUFBRSxLQUFLLEtBQUs7WUFDaEIsR0FBRyxDQUFDLEVBQUUsS0FBSyxRQUFRO1lBQ25CLEdBQUcsQ0FBQyxFQUFFLEtBQUssVUFBVTtZQUNyQixHQUFHLENBQUMsRUFBRSxLQUFLLFNBQVMsRUFDcEI7WUFDQSxHQUFHLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDO1NBQ2hDO1FBRUQsaUJBQWlCO1FBQ2pCLDZCQUE2QjtRQUM3QixXQUFXO1FBQ1gsaUJBQWlCO1FBQ2pCLElBQUk7SUFDTixDQUFDO0lBRUQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxVQUFVO1FBQ3pCLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVELE1BQU0sQ0FBQyxjQUFjLENBQUMsT0FBTztRQUMzQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsRUFBRTtZQUN2QyxJQUNFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEtBQUssQ0FBQztnQkFDeEIsQ0FBQyxDQUNDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssS0FBSztvQkFDdkIsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxRQUFRO29CQUMxQixPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLFNBQVM7b0JBQzNCLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssVUFBVTtvQkFDNUIsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxXQUFXLENBQzlCLEVBQ0Q7Z0JBQ0EsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxLQUFLLENBQUM7Z0JBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkJBQTZCLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQzNEO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDNUI7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNO1FBQzFCLE9BQU8sTUFBTSxDQUFDLEVBQUUsS0FBSyxhQUFhLElBQUksTUFBTSxDQUFDLEVBQUUsS0FBSyxlQUFlLENBQUM7SUFDdEUsQ0FBQztJQUVELE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLO1FBQzlCLE1BQU0sR0FBRyxHQUNQLDJFQUEyRSxDQUFDO1FBQzlFLE1BQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDL0IsT0FBTyxLQUFLLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCxNQUFNLENBQUMsb0JBQW9CLENBQUMsS0FBSztRQUMvQixNQUFNLEdBQUcsR0FDUCw0RUFBNEUsQ0FBQztRQUMvRSxNQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLE9BQU8sS0FBSyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRUQsTUFBTSxDQUFDLFlBQVksQ0FBQyxLQUFLO1FBQ3ZCOzs7V0FHRztRQUNILE9BQU8sS0FBSyxDQUFDLEtBQUssQ0FBQyx5Q0FBeUMsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFRCxNQUFNLENBQUMsa0JBQWtCLENBQUMsR0FBRztRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLFdBQVc7UUFDWCxHQUFHLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDO1FBQy9CLElBQUk7UUFFSixpQkFBaUI7UUFDakIsNkJBQTZCO1FBQzdCLFdBQVc7UUFDWCxpQkFBaUI7UUFDakIsSUFBSTtJQUNOLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsYUFBYSxDQUFDLElBQUk7UUFDaEIsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFckMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsWUFBWSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQztTQUN6RDthQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNwQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLFlBQVksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUM7U0FDekQ7YUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDcEMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxZQUFZLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1NBQ3pEO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7U0FDbkI7UUFFRCxPQUFPLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsRUFBRTtRQUN6QixNQUFNLE9BQU8sR0FBRyxFQUFFLENBQUM7UUFFbkIsSUFBSTtZQUNGLElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDcEQsTUFBTSxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFL0IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLEVBQUU7b0JBQ3BDLFFBQVEsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQzVCLEtBQUssUUFBUTs0QkFDWCxJQUFJLE1BQU0sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0NBQ25DLE9BQU8sQ0FBQyxJQUFJLENBQUM7b0NBQ1gsRUFBRSxFQUFFLElBQUk7b0NBQ1IsRUFBRTtvQ0FDRixXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztvQ0FDcEIsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7b0NBQ1osRUFBRSxFQUFFLEtBQUs7b0NBQ1QsRUFBRSxFQUFFLEVBQUU7b0NBQ04sRUFBRSxFQUFFLENBQUM7b0NBQ0wsSUFBSSxFQUFFLElBQUk7b0NBQ1YsR0FBRyxFQUFFLENBQUM7b0NBQ04sRUFBRSxFQUFFLEtBQUs7b0NBQ1QsSUFBSSxFQUFFLEtBQUs7b0NBQ1gsR0FBRyxFQUFFLEtBQUs7b0NBQ1YsRUFBRSxFQUFFLEtBQUs7b0NBQ1QsRUFBRSxFQUFFLGtCQUFrQjtvQ0FDdEIsRUFBRSxFQUFFLEtBQUs7b0NBQ1QsTUFBTSxFQUFFLEtBQUs7b0NBQ2IsR0FBRyxFQUFFLElBQUk7b0NBQ1QsRUFBRSxFQUFFLEVBQUU7b0NBQ04sR0FBRyxFQUFFLElBQUk7b0NBQ1QsR0FBRyxFQUFFLGNBQWM7b0NBQ25CLElBQUksRUFBRSxJQUFJO29DQUNWLElBQUksRUFBRSxDQUFDO29DQUNQLE9BQU8sRUFBRSxDQUFDO2lDQUNYLENBQUMsQ0FBQzs2QkFDSjtpQ0FBTTtnQ0FDTCxPQUFPLENBQUMsSUFBSSxDQUFDO29DQUNYLEVBQUUsRUFBRSxJQUFJO29DQUNSLEVBQUU7b0NBQ0YsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7b0NBQ3BCLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO29DQUNaLEVBQUUsRUFBRSxPQUFPO29DQUNYLEVBQUUsRUFBRSxFQUFFO29DQUNOLEVBQUUsRUFBRSxDQUFDO29DQUNMLElBQUksRUFBRSxJQUFJO29DQUNWLEdBQUcsRUFBRSxDQUFDO29DQUNOLEVBQUUsRUFBRSxLQUFLO29DQUNULElBQUksRUFBRSxLQUFLO29DQUNYLEdBQUcsRUFBRSxLQUFLO29DQUNWLEVBQUUsRUFBRSxLQUFLO29DQUNULEVBQUUsRUFBRSxrQkFBa0I7b0NBQ3RCLEVBQUUsRUFBRSxLQUFLO29DQUNULE1BQU0sRUFBRSxLQUFLO29DQUNiLEdBQUcsRUFBRSxJQUFJO29DQUNULEVBQUUsRUFBRSxFQUFFO29DQUNOLEdBQUcsRUFBRSxJQUFJO29DQUNULEdBQUcsRUFBRSxjQUFjO29DQUNuQixJQUFJLEVBQUUsSUFBSTtvQ0FDVixJQUFJLEVBQUUsQ0FBQztvQ0FDUCxPQUFPLEVBQUUsQ0FBQztpQ0FDWCxDQUFDLENBQUM7NkJBQ0o7NEJBRUQsTUFBTTt3QkFFUixLQUFLLFFBQVE7NEJBQ1gsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTtnQ0FDL0IsT0FBTyxDQUFDLElBQUksQ0FBQztvQ0FDWCxFQUFFLEVBQUUsSUFBSTtvQ0FDUixFQUFFO29DQUNGLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO29DQUNwQixHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztvQ0FDWixFQUFFLEVBQUUsU0FBUztvQ0FDYixFQUFFLEVBQUUsRUFBRTtvQ0FDTixFQUFFLEVBQUUsQ0FBQztvQ0FDTCxJQUFJLEVBQUUsSUFBSTtvQ0FDVixHQUFHLEVBQUUsQ0FBQztvQ0FDTixFQUFFLEVBQUUsS0FBSztvQ0FDVCxJQUFJLEVBQUUsS0FBSztvQ0FDWCxHQUFHLEVBQUUsS0FBSztvQ0FDVixFQUFFLEVBQUUsS0FBSztvQ0FDVCxFQUFFLEVBQUUsa0JBQWtCO29DQUN0QixFQUFFLEVBQUUsS0FBSztvQ0FDVCxNQUFNLEVBQUUsS0FBSztvQ0FDYixHQUFHLEVBQUUsSUFBSTtvQ0FDVCxFQUFFLEVBQUUsRUFBRTtvQ0FDTixHQUFHLEVBQUUsSUFBSTtvQ0FDVCxHQUFHLEVBQUUsY0FBYztvQ0FDbkIsSUFBSSxFQUFFLElBQUk7b0NBQ1YsSUFBSSxFQUFFLENBQUM7b0NBQ1AsT0FBTyxFQUFFLENBQUM7aUNBQ1gsQ0FBQyxDQUFDOzZCQUNKO2lDQUFNO2dDQUNMLE9BQU8sQ0FBQyxJQUFJLENBQUM7b0NBQ1gsRUFBRSxFQUFFLElBQUk7b0NBQ1IsRUFBRTtvQ0FDRixXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztvQ0FDcEIsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7b0NBQ1osRUFBRSxFQUFFLE1BQU07b0NBQ1YsRUFBRSxFQUFFLElBQUk7b0NBQ1IsRUFBRSxFQUFFLENBQUM7b0NBQ0wsSUFBSSxFQUFFLElBQUk7b0NBQ1YsR0FBRyxFQUFFLENBQUM7b0NBQ04sRUFBRSxFQUFFLEtBQUs7b0NBQ1QsSUFBSSxFQUFFLEtBQUs7b0NBQ1gsR0FBRyxFQUFFLEtBQUs7b0NBQ1YsRUFBRSxFQUFFLEtBQUs7b0NBQ1QsRUFBRSxFQUFFLGtCQUFrQjtvQ0FDdEIsRUFBRSxFQUFFLEtBQUs7b0NBQ1QsTUFBTSxFQUFFLEtBQUs7b0NBQ2IsR0FBRyxFQUFFLElBQUk7b0NBQ1QsRUFBRSxFQUFFLEVBQUU7b0NBQ04sR0FBRyxFQUFFLElBQUk7b0NBQ1QsR0FBRyxFQUFFLGNBQWM7b0NBQ25CLElBQUksRUFBRSxJQUFJO29DQUNWLElBQUksRUFBRSxDQUFDO29DQUNQLE9BQU8sRUFBRSxDQUFDO2lDQUNYLENBQUMsQ0FBQzs2QkFDSjs0QkFFRCxNQUFNO3dCQUVSLEtBQUssU0FBUzs0QkFDWixPQUFPLENBQUMsSUFBSSxDQUFDO2dDQUNYLEVBQUUsRUFBRSxJQUFJO2dDQUNSLEVBQUU7Z0NBQ0YsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7Z0NBQ3BCLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO2dDQUNaLEVBQUUsRUFBRSxTQUFTO2dDQUNiLEVBQUUsRUFBRSxDQUFDO2dDQUNMLEVBQUUsRUFBRSxDQUFDO2dDQUNMLElBQUksRUFBRSxJQUFJO2dDQUNWLEdBQUcsRUFBRSxDQUFDO2dDQUNOLEVBQUUsRUFBRSxLQUFLO2dDQUNULElBQUksRUFBRSxLQUFLO2dDQUNYLEdBQUcsRUFBRSxLQUFLO2dDQUNWLEVBQUUsRUFBRSxLQUFLO2dDQUNULEVBQUUsRUFBRSxrQkFBa0I7Z0NBQ3RCLEVBQUUsRUFBRSxLQUFLO2dDQUNULE1BQU0sRUFBRSxLQUFLO2dDQUNiLEdBQUcsRUFBRSxJQUFJO2dDQUNULEVBQUUsRUFBRSxFQUFFO2dDQUNOLEdBQUcsRUFBRSxJQUFJO2dDQUNULEdBQUcsRUFBRSxjQUFjO2dDQUNuQixJQUFJLEVBQUUsR0FBRztnQ0FDVCxJQUFJLEVBQUUsQ0FBQztnQ0FDUCxPQUFPLEVBQUUsQ0FBQzs2QkFDWCxDQUFDLENBQUM7NEJBQ0gsTUFBTTt3QkFFUixLQUFLLFFBQVE7NEJBQ1gsT0FBTyxDQUFDLElBQUksQ0FBQztnQ0FDWCxFQUFFLEVBQUUsSUFBSTtnQ0FDUixFQUFFO2dDQUNGLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO2dDQUNwQixHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztnQ0FDWixFQUFFLEVBQUUsTUFBTTtnQ0FDVixFQUFFLEVBQUUsQ0FBQztnQ0FDTCxFQUFFLEVBQUUsQ0FBQztnQ0FDTCxJQUFJLEVBQUUsSUFBSTtnQ0FDVixHQUFHLEVBQUUsQ0FBQztnQ0FDTixFQUFFLEVBQUUsS0FBSztnQ0FDVCxJQUFJLEVBQUUsS0FBSztnQ0FDWCxHQUFHLEVBQUUsS0FBSztnQ0FDVixFQUFFLEVBQUUsS0FBSztnQ0FDVCxFQUFFLEVBQUUsa0JBQWtCO2dDQUN0QixFQUFFLEVBQUUsS0FBSztnQ0FDVCxNQUFNLEVBQUUsS0FBSztnQ0FDYixHQUFHLEVBQUUsSUFBSTtnQ0FDVCxFQUFFLEVBQUUsRUFBRTtnQ0FDTixHQUFHLEVBQUUsSUFBSTtnQ0FDVCxHQUFHLEVBQUUsY0FBYztnQ0FDbkIsSUFBSSxFQUFFLElBQUk7Z0NBQ1YsSUFBSSxFQUFFLENBQUM7Z0NBQ1AsT0FBTyxFQUFFLENBQUM7NkJBQ1gsQ0FBQyxDQUFDOzRCQUNILE1BQU07d0JBRVI7NEJBQ0UsTUFBTTtxQkFDVDtpQkFDRjthQUNGO1NBQ0Y7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNWLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkJBQTZCLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDL0M7UUFDRCxPQUFPLE9BQU8sQ0FBQztJQUNqQixDQUFDO0lBRUQsTUFBTSxDQUFDLGlCQUFpQixDQUFDLElBQUk7UUFDM0IsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQsTUFBTSxDQUFDLGVBQWUsQ0FBQyxHQUFHO1FBQ3hCLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBRTtZQUN4QyxLQUFLLFNBQVM7Z0JBQ1osT0FBTyxTQUFTLENBQUM7WUFDbkIsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLGNBQWMsQ0FBQztZQUNwQixLQUFLLGVBQWUsQ0FBQztZQUNyQixLQUFLLGNBQWM7Z0JBQ2pCLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLGlCQUFpQixDQUFDO1lBQ3ZCLEtBQUssaUJBQWlCO2dCQUNwQixPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFDaEIsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLGtCQUFrQixDQUFDO1lBQ3hCLEtBQUssT0FBTztnQkFDVixPQUFPLE9BQU8sQ0FBQztZQUNqQixLQUFLLHdCQUF3QixDQUFDO1lBQzlCLEtBQUssd0JBQXdCO2dCQUMzQixPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLGFBQWE7Z0JBQ2hCLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssTUFBTTtnQkFDVCxPQUFPLFNBQVMsQ0FBQztZQUNuQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxRQUFRLENBQUM7WUFDbEIsS0FBSyxrQkFBa0IsQ0FBQztZQUN4QixLQUFLLGNBQWMsQ0FBQztZQUNwQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssT0FBTztnQkFDVixPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssS0FBSyxDQUFDO1lBQ1gsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxRQUFRLENBQUM7WUFDbEIsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxPQUFPLENBQUM7WUFDakIsS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLFlBQVksQ0FBQztZQUNsQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssdUJBQXVCO2dCQUMxQixPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxTQUFTLENBQUM7WUFDbkIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxjQUFjO2dCQUNqQixPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLGdDQUFnQyxDQUFDO1lBQ3RDLEtBQUsseUJBQXlCLENBQUM7WUFDL0IsS0FBSyx5QkFBeUIsQ0FBQztZQUMvQixLQUFLLG1CQUFtQjtnQkFDdEIsT0FBTyxVQUFVLENBQUM7WUFDcEIsS0FBSyx5QkFBeUIsQ0FBQztZQUMvQixLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxVQUFVO2dCQUNiLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxlQUFlO2dCQUNsQixPQUFPLFFBQVEsQ0FBQztTQUNuQjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUc7UUFDbEIsUUFBUSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ2pDLEtBQUssU0FBUztnQkFDWixPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxVQUFVLENBQUM7WUFDcEIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sU0FBUyxDQUFDO1lBQ25CLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxZQUFZLENBQUM7WUFDdEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBQ2hCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxnQkFBZ0IsQ0FBQztZQUMxQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxVQUFVLENBQUM7WUFDcEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sWUFBWSxDQUFDO1lBQ3RCLEtBQUssTUFBTTtnQkFDVCxPQUFPLGNBQWMsQ0FBQztZQUN4QixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxhQUFhLENBQUM7WUFDdkIsS0FBSyxNQUFNO2dCQUNULE9BQU8sVUFBVSxDQUFDO1NBQ3JCO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxHQUFzQixFQUFFLE1BQWU7UUFDakUsTUFBTSxPQUFPLEdBQVEsRUFBRSxDQUFDO1FBQ3hCLFFBQVEsR0FBRyxDQUFDLElBQUksRUFBRTtZQUNoQixLQUFLLElBQUk7Z0JBQ1A7b0JBQ0UsTUFBTSxXQUFXLEdBQUcsTUFBTSxLQUFLLElBQUksQ0FBQztvQkFDcEMsTUFBTSxXQUFXLEdBQUcsTUFBTSxLQUFLLElBQUksQ0FBQztvQkFDcEMsT0FBTyxDQUFDLEVBQUUsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO29CQUNqRCxPQUFPLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQztvQkFDbEIsT0FBTyxDQUFDLEVBQUUsR0FBRyxXQUFXLENBQUM7b0JBQ3pCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsV0FBVyxDQUFDO29CQUN6QixPQUFPLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQztvQkFDbkIsT0FBTyxDQUFDLElBQUksR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7aUJBQ3ZEO2dCQUNELE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLGdCQUFnQjtnQkFDbkIsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE9BQU8sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO2dCQUNqQixNQUFNO1lBQ1IsS0FBSyxhQUFhO2dCQUNoQixPQUFPLENBQUMsRUFBRSxHQUFHLFVBQVUsQ0FBQztnQkFDeEIsTUFBTTtZQUNSLEtBQUssY0FBYztnQkFDakIsT0FBTyxDQUFDLEVBQUUsR0FBRyxVQUFVLENBQUM7Z0JBQ3hCLE1BQU07WUFDUixLQUFLLGNBQWM7Z0JBQ2pCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxNQUFNO2dCQUNULE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUV2QixNQUFNO1lBQ1IsS0FBSyxNQUFNO2dCQUNULE9BQU8sQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDO2dCQUNwQixNQUFNO1lBQ1IsS0FBSyxNQUFNO2dCQUNULE9BQU8sQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDO2dCQUNwQixNQUFNO1lBQ1IsS0FBSyxhQUFhO2dCQUNoQixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLFFBQVEsR0FBRztvQkFDakIsSUFBSSxFQUFFLENBQUMsZUFBZSxDQUFDO29CQUN2QixJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBQ1YsR0FBRyxFQUFFLENBQUMsbUNBQW1DLENBQUM7aUJBQzNDLENBQUM7Z0JBQ0YsTUFBTTtZQUNSLEtBQUssT0FBTztnQkFDVixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLFFBQVEsR0FBRztvQkFDakIsSUFBSSxFQUFFLENBQUMsU0FBUyxDQUFDO29CQUNqQixJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBQ1YsR0FBRyxFQUFFLENBQUMsNkJBQTZCLENBQUM7aUJBQ3JDLENBQUM7Z0JBQ0YsTUFBTTtZQUNSLEtBQUssS0FBSztnQkFDUixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLFFBQVEsR0FBRztvQkFDakIsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUFDO29CQUNmLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQztvQkFDVixHQUFHLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQztpQkFDbkMsQ0FBQztnQkFDRixNQUFNO1lBQ1IsS0FBSyxRQUFRO2dCQUNYLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxVQUFVO2dCQUNiLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixPQUFPLENBQUMsUUFBUSxHQUFHO29CQUNqQixJQUFJLEVBQUUsQ0FBQyxZQUFZLENBQUM7b0JBQ3BCLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQztvQkFDVixHQUFHLEVBQUUsQ0FBQyxnQ0FBZ0MsQ0FBQztpQkFDeEMsQ0FBQztnQkFDRixNQUFNO1lBQ1IsS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxFQUFFLEdBQUcsUUFBUSxDQUFDO2dCQUN0QixNQUFNO1lBQ1IsS0FBSyxVQUFVO2dCQUNiLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxRQUFRO2dCQUNYLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxRQUFRO2dCQUNYLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxPQUFPO2dCQUNWLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxRQUFRO2dCQUNYLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxVQUFVO2dCQUNiLE9BQU8sQ0FBQyxFQUFFLEdBQUcsV0FBVyxDQUFDO2dCQUN6QixNQUFNO1lBQ1IsS0FBSyxZQUFZO2dCQUNmLE9BQU8sQ0FBQyxFQUFFLEdBQUcsV0FBVyxDQUFDO2dCQUN6QixNQUFNO1lBQ1IsS0FBSyxrQkFBa0I7Z0JBQ3JCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsV0FBVyxDQUFDO2dCQUN6QixNQUFNO1lBQ1IsS0FBSyxZQUFZO2dCQUNmLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxRQUFRO2dCQUNYLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1I7Z0JBQ0UsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07U0FDVDtRQUNELE9BQU8sT0FBTyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxNQUFNLENBQUMsb0JBQW9CO1FBQ3pCLE9BQU8sRUFBRSxDQUFDO0lBQ1osQ0FBQztDQUNGO0FBbDZCRCw0QkFrNkJDO0FBRUQsNkJBQTZCO0FBQzdCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FxQkcifQ==