"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PgUi = void 0;
const UITypes_1 = __importDefault(require("../UITypes"));
const dbTypes = [
    'int',
    'integer',
    'bigint',
    'bigserial',
    'char',
    'int2',
    'int4',
    'int8',
    'int4range',
    'int8range',
    'serial',
    'serial2',
    'serial8',
    'character',
    'bit',
    'bool',
    'boolean',
    'date',
    'double precision',
    'event_trigger',
    'fdw_handler',
    'float4',
    'float8',
    'uuid',
    'smallint',
    'smallserial',
    'character varying',
    'text',
    'real',
    'time',
    'time without time zone',
    'timestamp',
    'timestamp without time zone',
    'timestamptz',
    'timestamp with time zone',
    'timetz',
    'time with time zone',
    'daterange',
    'json',
    'jsonb',
    'gtsvector',
    'index_am_handler',
    'anyenum',
    'anynonarray',
    'anyrange',
    'box',
    'bpchar',
    'bytea',
    'cid',
    'cidr',
    'circle',
    'cstring',
    'inet',
    'internal',
    'interval',
    'language_handler',
    'line',
    'lsec',
    'macaddr',
    'money',
    'name',
    'numeric',
    'numrange',
    'oid',
    'opaque',
    'path',
    'pg_ddl_command',
    'pg_lsn',
    'pg_node_tree',
    'point',
    'polygon',
    'record',
    'refcursor',
    'regclass',
    'regconfig',
    'regdictionary',
    'regnamespace',
    'regoper',
    'regoperator',
    'regproc',
    'regpreocedure',
    'regrole',
    'regtype',
    'reltime',
    'smgr',
    'tid',
    'tinterval',
    'trigger',
    'tsm_handler',
    'tsquery',
    'tsrange',
    'tstzrange',
    'tsvector',
    'txid_snapshot',
    'unknown',
    'void',
    'xid',
    'xml',
];
class PgUi {
    static getNewTableColumns() {
        return [
            {
                column_name: 'id',
                title: 'Id',
                dt: 'int4',
                dtx: 'integer',
                ct: 'int(11)',
                nrqd: false,
                rqd: true,
                ck: false,
                pk: true,
                un: false,
                ai: true,
                cdf: null,
                clen: null,
                np: 11,
                ns: 0,
                dtxp: '11',
                dtxs: '',
                altered: 1,
                uidt: 'ID',
                uip: '',
                uicn: '',
            },
            {
                column_name: 'title',
                title: 'Title',
                dt: 'character varying',
                dtx: 'specificType',
                ct: 'varchar(45)',
                nrqd: true,
                rqd: false,
                ck: false,
                pk: false,
                un: false,
                ai: false,
                cdf: null,
                clen: 45,
                np: null,
                ns: null,
                dtxp: '45',
                dtxs: '',
                altered: 1,
                uidt: 'SingleLineText',
                uip: '',
                uicn: '',
            },
            {
                column_name: 'created_at',
                title: 'CreatedAt',
                dt: 'timestamp',
                dtx: 'specificType',
                ct: 'varchar(45)',
                nrqd: true,
                rqd: false,
                ck: false,
                pk: false,
                un: false,
                ai: false,
                cdf: 'now()',
                clen: 45,
                np: null,
                ns: null,
                dtxp: '',
                dtxs: '',
                altered: 1,
                uidt: UITypes_1.default.DateTime,
                uip: '',
                uicn: '',
            },
            {
                column_name: 'updated_at',
                title: 'UpdatedAt',
                dt: 'timestamp',
                dtx: 'specificType',
                ct: 'varchar(45)',
                nrqd: true,
                rqd: false,
                ck: false,
                pk: false,
                un: false,
                ai: false,
                au: true,
                cdf: 'now()',
                clen: 45,
                np: null,
                ns: null,
                dtxp: '',
                dtxs: '',
                altered: 1,
                uidt: UITypes_1.default.DateTime,
                uip: '',
                uicn: '',
            },
        ];
    }
    static getNewColumn(suffix) {
        return {
            column_name: 'title' + suffix,
            dt: 'character varying',
            dtx: 'specificType',
            ct: 'varchar(45)',
            nrqd: true,
            rqd: false,
            ck: false,
            pk: false,
            un: false,
            ai: false,
            cdf: null,
            clen: 45,
            np: null,
            ns: null,
            dtxp: '45',
            dtxs: '',
            altered: 1,
            uidt: 'SingleLineText',
            uip: '',
            uicn: '',
        };
    }
    // static getDefaultLengthForDatatype(type) {
    //   switch (type) {
    //     case "int":
    //       return 11;
    //       break;
    //     case "tinyint":
    //       return 1;
    //       break;
    //     case "smallint":
    //       return 5;
    //       break;
    //
    //     case "mediumint":
    //       return 9;
    //       break;
    //     case "bigint":
    //       return 20;
    //       break;
    //     case "bit":
    //       return 64;
    //       break;
    //     case "boolean":
    //       return '';
    //       break;
    //     case "float":
    //       return 12;
    //       break;
    //     case "decimal":
    //       return 10;
    //       break;
    //     case "double":
    //       return 22;
    //       break;
    //     case "serial":
    //       return 20;
    //       break;
    //     case "date":
    //       return '';
    //       break;
    //     case "datetime":
    //     case "timestamp":
    //       return 6;
    //       break;
    //     case "time":
    //       return '';
    //       break;
    //     case "year":
    //       return '';
    //       break;
    //     case "char":
    //       return 255;
    //       break;
    //     case "varchar":
    //       return 45;
    //       break;
    //     case "nchar":
    //       return 255;
    //       break;
    //     case "text":
    //       return '';
    //       break;
    //     case "tinytext":
    //       return '';
    //       break;
    //     case "mediumtext":
    //       return '';
    //       break;
    //     case "longtext":
    //       return ''
    //       break;
    //     case "binary":
    //       return 255;
    //       break;
    //     case "varbinary":
    //       return 65500;
    //       break;
    //     case "blob":
    //       return '';
    //       break;
    //     case "tinyblob":
    //       return '';
    //       break;
    //     case "mediumblob":
    //       return '';
    //       break;
    //     case "longblob":
    //       return '';
    //       break;
    //     case "enum":
    //       return '\'a\',\'b\'';
    //       break;
    //     case "set":
    //       return '\'a\',\'b\'';
    //       break;
    //     case "geometry":
    //       return '';
    //     case "point":
    //       return '';
    //     case "linestring":
    //       return '';
    //     case "polygon":
    //       return '';
    //     case "multipoint":
    //       return '';
    //     case "multilinestring":
    //       return '';
    //     case "multipolygon":
    //       return '';
    //     case "json":
    //       return ''
    //       break;
    //
    //   }
    //
    // }
    static getDefaultLengthForDatatype(type) {
        switch (type) {
            case 'int':
                return '';
            case 'tinyint':
                return '';
            case 'smallint':
                return '';
            case 'mediumint':
                return '';
            case 'bigint':
                return '';
            case 'bit':
                return '';
            case 'boolean':
                return '';
            case 'float':
                return '';
            case 'decimal':
                return '';
            case 'double':
                return '';
            case 'serial':
                return '';
            case 'date':
                return '';
            case 'datetime':
            case 'timestamp':
                return '';
            case 'time':
                return '';
            case 'year':
                return '';
            case 'char':
                return '';
            case 'varchar':
                return '';
            case 'nchar':
                return '';
            case 'text':
                return '';
            case 'tinytext':
                return '';
            case 'mediumtext':
                return '';
            case 'longtext':
                return '';
            case 'binary':
                return '';
            case 'varbinary':
                return '';
            case 'blob':
                return '';
            case 'tinyblob':
                return '';
            case 'mediumblob':
                return '';
            case 'longblob':
                return '';
            case 'enum':
                return '';
            case 'set':
                return '';
            case 'geometry':
                return '';
            case 'point':
                return '';
            case 'linestring':
                return '';
            case 'polygon':
                return '';
            case 'multipoint':
                return '';
            case 'multilinestring':
                return '';
            case 'multipolygon':
                return '';
            case 'json':
                return '';
        }
    }
    static getDefaultLengthIsDisabled(type) {
        switch (type) {
            case 'anyenum':
            case 'anynonarray':
            case 'anyrange':
            case 'bigint':
            case 'bigserial':
            case 'bit':
            case 'bool':
            case 'box':
            case 'bpchar':
            case 'bytea':
            case 'char':
            case 'character':
            case 'cid':
            case 'cidr':
            case 'circle':
            case 'cstring':
            case 'date':
            case 'daterange':
            case 'double precision':
            case 'event_trigger':
            case 'fdw_handler':
            case 'float4':
            case 'float8':
            case 'gtsvector':
            case 'index_am_handler':
            case 'inet':
            case 'int':
            case 'int2':
            case 'int4':
            case 'int8':
            case 'int4range':
            case 'int8range':
            case 'integer':
            case 'internal':
            case 'interval':
            case 'jsonb':
            case 'language_handler':
            case 'line':
            case 'lsec':
            case 'macaddr':
            case 'money':
            case 'name':
            case 'numeric':
            case 'numrange':
            case 'oid':
            case 'opaque':
            case 'path':
            case 'pg_ddl_command':
            case 'pg_lsn':
            case 'pg_node_tree':
            case 'real':
            case 'record':
            case 'refcursor':
            case 'regclass':
            case 'regconfig':
            case 'regdictionary':
            case 'regnamespace':
            case 'regoper':
            case 'regoperator':
            case 'regproc':
            case 'regpreocedure':
            case 'regrole':
            case 'regtype':
            case 'reltime':
            case 'serial':
            case 'serial2':
            case 'serial8':
            case 'smallint':
            case 'smallserial':
            case 'smgr':
            case 'text':
            case 'tid':
            case 'time':
            case 'time without time zone':
            case 'timestamp':
            case 'timestamp without time zone':
            case 'timestamptz':
            case 'timestamp with time zone':
            case 'timetz':
            case 'time with time zone':
            case 'tinterval':
            case 'trigger':
            case 'tsm_handler':
            case 'tsquery':
            case 'tsrange':
            case 'tstzrange':
            case 'tsvector':
            case 'txid_snapshot':
            case 'unknown':
            case 'void':
            case 'xid':
            case 'xml':
            case 'character varying':
            case 'tinyint':
            case 'mediumint':
            case 'float':
            case 'decimal':
            case 'double':
            case 'boolean':
            case 'datetime':
            case 'uuid':
            case 'year':
            case 'varchar':
            case 'nchar':
            case 'tinytext':
            case 'mediumtext':
            case 'longtext':
            case 'binary':
            case 'varbinary':
            case 'blob':
            case 'tinyblob':
            case 'mediumblob':
            case 'longblob':
            case 'enum':
            case 'set':
            case 'geometry':
            case 'point':
            case 'linestring':
            case 'polygon':
            case 'multipoint':
            case 'multilinestring':
            case 'multipolygon':
            case 'json':
                return true;
        }
    }
    static getDefaultValueForDatatype(type) {
        switch (type) {
            case 'anyenum':
                return 'eg: ';
            case 'anynonarray':
                return 'eg: ';
            case 'anyrange':
                return 'eg: ';
            case 'bigint':
                return 'eg: ';
            case 'bigserial':
                return 'eg: ';
            case 'bit':
                return 'eg: ';
            case 'bool':
                return 'eg: ';
            case 'box':
                return 'eg: ';
            case 'bpchar':
                return 'eg: ';
            case 'bytea':
                return 'eg: ';
            case 'char':
                return 'eg: ';
            case 'character':
                return "eg: 'sample'";
            case 'cid':
                return 'eg: ';
            case 'cidr':
                return 'eg: ';
            case 'circle':
                return 'eg: ';
            case 'cstring':
                return 'eg: ';
            case 'date':
                return "eg: '2020-09-09'";
            case 'daterange':
                return 'eg: ';
            case 'double precision':
                return 'eg: 1.2';
            case 'event_trigger':
                return 'eg: ';
            case 'fdw_handler':
                return 'eg: ';
            case 'float4':
                return 'eg: 1.2';
            case 'float8':
                return 'eg: 1.2';
            case 'gtsvector':
                return 'eg: ';
            case 'index_am_handler':
                return 'eg: ';
            case 'inet':
                return 'eg: ';
            case 'int':
                return 'eg: ';
            case 'int2':
                return 'eg: ';
            case 'int4':
                return 'eg: ';
            case 'int8':
                return 'eg: ';
            case 'int4range':
                return 'eg: ';
            case 'int8range':
                return 'eg: ';
            case 'integer':
                return 'eg: ';
            case 'internal':
                return 'eg: ';
            case 'interval':
                return 'eg: ';
            case 'json':
                return 'eg: ';
            case 'jsonb':
                return 'eg: ';
            case 'language_handler':
                return 'eg: ';
            case 'line':
                return 'eg: ';
            case 'lsec':
                return 'eg: ';
            case 'macaddr':
                return 'eg: ';
            case 'money':
                return 'eg: ';
            case 'name':
                return 'eg: ';
            case 'numeric':
                return 'eg: ';
            case 'numrange':
                return 'eg: ';
            case 'oid':
                return 'eg: ';
            case 'opaque':
                return 'eg: ';
            case 'path':
                return 'eg: ';
            case 'pg_ddl_command':
                return 'eg: ';
            case 'pg_lsn':
                return 'eg: ';
            case 'pg_node_tree':
                return 'eg: ';
            case 'point':
                return 'eg: ';
            case 'polygon':
                return 'eg: ';
            case 'real':
                return 'eg: 1.2';
            case 'record':
                return 'eg: ';
            case 'refcursor':
                return 'eg: ';
            case 'regclass':
                return 'eg: ';
            case 'regconfig':
                return 'eg: ';
            case 'regdictionary':
                return 'eg: ';
            case 'regnamespace':
                return 'eg: ';
            case 'regoper':
                return 'eg: ';
            case 'regoperator':
                return 'eg: ';
            case 'regproc':
                return 'eg: ';
            case 'regpreocedure':
                return 'eg: ';
            case 'regrole':
                return 'eg: ';
            case 'regtype':
                return 'eg: ';
            case 'reltime':
                return 'eg: ';
            case 'serial':
                return 'eg: ';
            case 'serial2':
                return 'eg: ';
            case 'serial8':
                return 'eg: ';
            case 'smallint':
                return 'eg: ';
            case 'smallserial':
                return 'eg: ';
            case 'smgr':
                return 'eg: ';
            case 'text':
                return "eg: 'sample text'";
            case 'tid':
                return 'eg: ';
            case 'time':
                return "eg: now()\n\n'04:05:06.789'";
            case 'time without time zone':
                return "eg: now()\n\n'04:05:06.789'";
            case 'timestamp':
                return "eg: now()\n\n'2016-06-22 19:10:25-07'";
            case 'timestamp without time zone':
                return "eg: now()\n\n'2016-06-22 19:10:25-07'";
            case 'timestamptz':
                return "eg: timezone('America/New_York','2016-06-01 00:00')\n\nnow()\n\n'2016-06-22 19:10:25-07'";
            case 'timestamp with time zone':
                return "eg: now()\n\n'2016-06-22 19:10:25-07'";
            case 'timetz':
                return 'eg: now()';
            case 'time with time zone':
                return 'eg: now()';
            case 'tinterval':
                return 'eg: ';
            case 'trigger':
                return 'eg: ';
            case 'tsm_handler':
                return 'eg: ';
            case 'tsquery':
                return 'eg: ';
            case 'tsrange':
                return 'eg: ';
            case 'tstzrange':
                return 'eg: ';
            case 'tsvector':
                return 'eg: ';
            case 'txid_snapshot':
                return 'eg: ';
            case 'unknown':
                return 'eg: ';
            case 'void':
                return 'eg: ';
            case 'xid':
                return 'eg: ';
            case 'xml':
                return 'eg: ';
            case 'character varying':
                return "eg: 'sample text'";
            case 'tinyint':
                return 'eg: ';
            case 'mediumint':
                return 'eg: ';
            case 'float':
                return 'eg: ';
            case 'decimal':
                return 'eg: ';
            case 'double':
                return 'eg: 1.2';
            case 'boolean':
                return 'eg: true\n\nfalse';
            case 'datetime':
                return 'eg: ';
            case 'uuid':
                return 'eg: ';
            case 'year':
                return 'eg: ';
            case 'varchar':
                return 'eg: ';
            case 'nchar':
                return 'eg: ';
            case 'tinytext':
                return 'eg: ';
            case 'mediumtext':
                return 'eg: ';
            case 'longtext':
                return 'eg: ';
            case 'binary':
                return 'eg: ';
            case 'varbinary':
                return 'eg: ';
            case 'blob':
                return 'eg: ';
            case 'tinyblob':
                return 'eg: ';
            case 'mediumblob':
                return 'eg: ';
            case 'longblob':
                return 'eg: ';
            case 'enum':
                return 'eg: ';
            case 'set':
                return 'eg: ';
            case 'geometry':
                return 'eg: ';
            case 'linestring':
                return 'eg: ';
            case 'multipoint':
                return 'eg: ';
            case 'multilinestring':
                return 'eg: ';
            case 'multipolygon':
                return 'eg: ';
        }
    }
    static getDefaultScaleForDatatype(type) {
        switch (type) {
            case 'int':
                return ' ';
            case 'tinyint':
                return ' ';
            case 'smallint':
                return ' ';
            case 'mediumint':
                return ' ';
            case 'bigint':
                return ' ';
            case 'bit':
                return ' ';
            case 'boolean':
                return ' ';
            case 'float':
                return '2';
            case 'decimal':
                return '2';
            case 'double':
                return '2';
            case 'serial':
                return ' ';
            case 'date':
            case 'datetime':
            case 'timestamp':
                return ' ';
            case 'time':
                return ' ';
            case 'year':
                return ' ';
            case 'char':
                return ' ';
            case 'varchar':
                return ' ';
            case 'nchar':
                return ' ';
            case 'text':
                return ' ';
            case 'tinytext':
                return ' ';
            case 'mediumtext':
                return ' ';
            case 'longtext':
                return ' ';
            case 'binary':
                return ' ';
            case 'varbinary':
                return ' ';
            case 'blob':
                return ' ';
            case 'tinyblob':
                return ' ';
            case 'mediumblob':
                return ' ';
            case 'longblob':
                return ' ';
            case 'enum':
                return ' ';
            case 'set':
                return ' ';
            case 'geometry':
                return ' ';
            case 'point':
                return ' ';
            case 'linestring':
                return ' ';
            case 'polygon':
                return ' ';
            case 'multipoint':
                return ' ';
            case 'multilinestring':
                return ' ';
            case 'multipolygon':
                return ' ';
            case 'json':
                return ' ';
        }
    }
    static colPropAIDisabled(col, columns) {
        // console.log(col);
        if (col.dt === 'int4' ||
            col.dt === 'integer' ||
            col.dt === 'bigint' ||
            col.dt === 'smallint') {
            for (let i = 0; i < columns.length; ++i) {
                if (columns[i].cn !== col.cn && columns[i].ai) {
                    return true;
                }
            }
            return false;
        }
        else {
            return true;
        }
    }
    static colPropUNDisabled(_col) {
        // console.log(col);
        return true;
        // if (col.dt === 'int' ||
        //   col.dt === 'tinyint' ||
        //   col.dt === 'smallint' ||
        //   col.dt === 'mediumint' ||
        //   col.dt === 'bigint') {
        //   return false;
        // } else {
        //   return true;
        // }
    }
    static onCheckboxChangeAI(col) {
        console.log(col);
        if (col.dt === 'int' ||
            col.dt === 'bigint' ||
            col.dt === 'smallint' ||
            col.dt === 'tinyint') {
            col.altered = col.altered || 2;
        }
        // if (!col.ai) {
        //   col.dtx = 'specificType'
        // } else {
        //   col.dtx = ''
        // }
    }
    static onCheckboxChangeAU(col) {
        console.log(col);
        // if (1) {
        col.altered = col.altered || 2;
        // }
        if (col.au) {
            col.cdf = 'now()';
        }
        // if (!col.ai) {
        //   col.dtx = 'specificType'
        // } else {
        //   col.dtx = ''
        // }
    }
    static showScale(_columnObj) {
        return false;
    }
    static removeUnsigned(columns) {
        for (let i = 0; i < columns.length; ++i) {
            if (columns[i].altered === 1 &&
                !(columns[i].dt === 'int' ||
                    columns[i].dt === 'bigint' ||
                    columns[i].dt === 'tinyint' ||
                    columns[i].dt === 'smallint' ||
                    columns[i].dt === 'mediumint')) {
                columns[i].un = false;
                console.log('>> resetting unsigned value', columns[i].cn);
            }
            console.log(columns[i].cn);
        }
    }
    static columnEditable(colObj) {
        return colObj.tn !== '_evolutions' || colObj.tn !== 'nc_evolutions';
    }
    static extractFunctionName(query) {
        const reg = /^\s*CREATE\s+(?:OR\s+REPLACE\s*)?\s*FUNCTION\s+(?:[\w\d_]+\.)?([\w_\d]+)/i;
        const match = query.match(reg);
        return match && match[1];
    }
    static extractProcedureName(query) {
        const reg = /^\s*CREATE\s+(?:OR\s+REPLACE\s*)?\s*PROCEDURE\s+(?:[\w\d_]+\.)?([\w_\d]+)/i;
        const match = query.match(reg);
        return match && match[1];
    }
    static handleRawOutput(result, headers) {
        if (['DELETE', 'INSERT', 'UPDATE'].includes(result.command.toUpperCase())) {
            headers.push({ text: 'Row count', value: 'rowCount', sortable: false });
            result = [
                {
                    rowCount: result.rowCount,
                },
            ];
        }
        else {
            result = result.rows;
            if (Array.isArray(result) && result[0]) {
                const keys = Object.keys(result[0]);
                // set headers before settings result
                for (let i = 0; i < keys.length; i++) {
                    const text = keys[i];
                    headers.push({ text, value: text, sortable: false });
                }
            }
        }
        return result;
    }
    static splitQueries(query) {
        /***
         * we are splitting based on semicolon
         * there are mechanism to escape semicolon within single/double quotes(string)
         */
        return query.match(/\b("[^"]*;[^"]*"|'[^']*;[^']*'|[^;])*;/g);
    }
    /**
     * if sql statement is SELECT - it limits to a number
     * @param args
     * @returns {string|*}
     */
    sanitiseQuery(args) {
        let q = args.query.trim().split(';');
        if (q[0].startsWith('Select')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else if (q[0].startsWith('select')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else if (q[0].startsWith('SELECT')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else {
            return args.query;
        }
        return q;
    }
    static getColumnsFromJson(json, tn) {
        const columns = [];
        try {
            if (typeof json === 'object' && !Array.isArray(json)) {
                const keys = Object.keys(json);
                for (let i = 0; i < keys.length; ++i) {
                    const column = {
                        dp: null,
                        tn,
                        column_name: keys[i],
                        cno: keys[i],
                        np: 10,
                        ns: 0,
                        clen: null,
                        cop: 1,
                        pk: false,
                        nrqd: false,
                        rqd: false,
                        un: false,
                        ct: 'int(11) unsigned',
                        ai: false,
                        unique: false,
                        cdf: null,
                        cc: '',
                        csn: null,
                        dtx: 'specificType',
                        dtxp: null,
                        dtxs: 0,
                        altered: 1,
                    };
                    switch (typeof json[keys[i]]) {
                        case 'number':
                            if (Number.isInteger(json[keys[i]])) {
                                if (PgUi.isValidTimestamp(keys[i], json[keys[i]])) {
                                    Object.assign(column, {
                                        dt: 'timestamp',
                                    });
                                }
                                else {
                                    Object.assign(column, {
                                        dt: 'int',
                                        np: 10,
                                        ns: 0,
                                    });
                                }
                            }
                            else {
                                Object.assign(column, {
                                    dt: 'float4',
                                    np: null,
                                    ns: null,
                                    dtxp: null,
                                    dtxs: null,
                                });
                            }
                            break;
                        case 'string':
                            if (PgUi.isValidDate(json[keys[i]])) {
                                Object.assign(column, {
                                    dt: 'date',
                                });
                            }
                            else if (json[keys[i]].length <= 255) {
                                Object.assign(column, {
                                    dt: 'character varying',
                                    np: null,
                                    ns: 0,
                                    dtxp: null,
                                });
                            }
                            else {
                                Object.assign(column, {
                                    dt: 'text',
                                });
                            }
                            break;
                        case 'boolean':
                            Object.assign(column, {
                                dt: 'boolean',
                                np: 3,
                                ns: 0,
                            });
                            break;
                        case 'object':
                            Object.assign(column, {
                                dt: 'json',
                                np: 3,
                                ns: 0,
                            });
                            break;
                        default:
                            break;
                    }
                    columns.push(column);
                }
            }
        }
        catch (e) {
            console.log('Error in getColumnsFromJson', e);
        }
        return columns;
    }
    static isValidTimestamp(key, value) {
        if (typeof value !== 'number') {
            return false;
        }
        return new Date(value).getTime() > 0 && /(?:_|(?=A))[aA]t$/.test(key);
    }
    static isValidDate(value) {
        return new Date(value).getTime() > 0;
    }
    static colPropAuDisabled(col) {
        if (col.altered !== 1) {
            return true;
        }
        switch (col.dt) {
            case 'time':
            case 'time without time zone':
            case 'timestamp':
            case 'timestamp without time zone':
            case 'timestamptz':
            case 'timestamp with time zone':
            case 'timetz':
            case 'time with time zone':
                return false;
            default:
                return true;
        }
    }
    static getAbstractType(col) {
        switch ((col.dt || col.dt).toLowerCase()) {
            case 'anyenum':
                return 'enum';
            case 'anynonarray':
            case 'anyrange':
                return 'string';
            case 'bit':
                return 'integer';
            case 'bigint':
            case 'bigserial':
                return 'string';
            case 'bool':
                return 'boolean';
            case 'box':
            case 'bpchar':
            case 'bytea':
            case 'char':
            case 'character':
                return 'string';
            case 'cid':
            case 'cidr':
            case 'circle':
            case 'cstring':
                return 'string';
            case 'date':
                return 'date';
            case 'daterange':
                return 'string';
            case 'double precision':
                return 'string';
            case 'event_trigger':
            case 'fdw_handler':
                return 'string';
            case 'float4':
            case 'float8':
                return 'float';
            case 'gtsvector':
            case 'index_am_handler':
            case 'inet':
                return 'string';
            case 'int':
            case 'int2':
            case 'int4':
            case 'int8':
            case 'integer':
                return 'integer';
            case 'int4range':
            case 'int8range':
            case 'internal':
            case 'interval':
                return 'string';
            case 'jsonb':
                return 'string';
            case 'language_handler':
            case 'line':
            case 'lsec':
            case 'macaddr':
            case 'money':
            case 'name':
            case 'numeric':
            case 'numrange':
            case 'oid':
            case 'opaque':
            case 'path':
            case 'pg_ddl_command':
            case 'pg_lsn':
            case 'pg_node_tree':
            case 'point':
            case 'polygon':
                return 'string';
            case 'real':
                return 'float';
            case 'record':
            case 'refcursor':
            case 'regclass':
            case 'regconfig':
            case 'regdictionary':
            case 'regnamespace':
            case 'regoper':
            case 'regoperator':
            case 'regproc':
            case 'regpreocedure':
            case 'regrole':
            case 'regtype':
            case 'reltime':
                return 'string';
            case 'serial':
            case 'serial2':
            case 'serial8':
            case 'smallint':
            case 'smallserial':
                return 'integer';
            case 'smgr':
                return 'string';
            case 'text':
                return 'text';
            case 'tid':
                return 'string';
            case 'time':
            case 'time without time zone':
                return 'time';
            case 'timestamp':
            case 'timestamp without time zone':
            case 'timestamptz':
            case 'timestamp with time zone':
                return 'datetime';
            case 'timetz':
            case 'time with time zone':
                return 'time';
            case 'tinterval':
            case 'trigger':
            case 'tsm_handler':
            case 'tsquery':
            case 'tsrange':
            case 'tstzrange':
            case 'tsvector':
            case 'txid_snapshot':
            case 'unknown':
            case 'void':
            case 'xid':
            case 'character varying':
            case 'xml':
                return 'string';
            case 'tinyint':
            case 'mediumint':
                return 'integer';
            case 'float':
            case 'decimal':
            case 'double':
                return 'float';
            case 'boolean':
                return 'boolean';
            case 'datetime':
                return 'datetime';
            case 'uuid':
            case 'year':
            case 'varchar':
            case 'nchar':
                return 'string';
            case 'tinytext':
            case 'mediumtext':
            case 'longtext':
                return 'text';
            case 'binary':
            case 'varbinary':
                return 'string';
            case 'blob':
            case 'tinyblob':
            case 'mediumblob':
            case 'longblob':
                return 'blob';
            case 'enum':
                return 'enum';
            case 'set':
                return 'set';
            case 'geometry':
            case 'linestring':
            case 'multipoint':
            case 'multilinestring':
            case 'multipolygon':
                return 'string';
            case 'json':
                return 'json';
        }
    }
    static getUIType(col) {
        switch (this.getAbstractType(col)) {
            case 'integer':
                return 'Number';
            case 'boolean':
                return 'Checkbox';
            case 'float':
                return 'Decimal';
            case 'date':
                return 'Date';
            case 'datetime':
                return 'CreateTime';
            case 'time':
                return 'Time';
            case 'year':
                return 'Year';
            case 'string':
                return 'SingleLineText';
            case 'text':
                return 'LongText';
            case 'blob':
                return 'Attachment';
            case 'enum':
                return 'SingleSelect';
            case 'set':
                return 'MultiSelect';
            case 'json':
                return 'LongText';
        }
    }
    static getDataTypeForUiType(col, idType) {
        const colProp = {};
        switch (col.uidt) {
            case 'ID':
                {
                    const isAutoIncId = idType === 'AI';
                    const isAutoGenId = idType === 'AG';
                    colProp.dt = isAutoGenId ? 'character varying' : 'int4';
                    colProp.pk = true;
                    colProp.un = isAutoIncId;
                    colProp.ai = isAutoIncId;
                    colProp.rqd = true;
                    colProp.meta = isAutoGenId ? { ag: 'nc' } : undefined;
                }
                break;
            case 'ForeignKey':
                colProp.dt = 'character varying';
                break;
            case 'SingleLineText':
                colProp.dt = 'character varying';
                break;
            case 'LongText':
                colProp.dt = 'text';
                break;
            case 'Attachment':
                colProp.dt = 'text';
                break;
            case 'Checkbox':
                colProp.dt = 'bool';
                break;
            case 'MultiSelect':
                colProp.dt = 'text';
                break;
            case 'SingleSelect':
                colProp.dt = 'text';
                break;
            case 'Collaborator':
                colProp.dt = 'character varying';
                break;
            case 'Date':
                colProp.dt = 'date';
                break;
            case 'Year':
                colProp.dt = 'int';
                break;
            case 'Time':
                colProp.dt = 'time';
                break;
            case 'PhoneNumber':
                colProp.dt = 'character varying';
                colProp.validate = {
                    func: ['isMobilePhone'],
                    args: [''],
                    msg: ['Validation failed : isMobilePhone'],
                };
                break;
            case 'Email':
                colProp.dt = 'character varying';
                colProp.validate = {
                    func: ['isEmail'],
                    args: [''],
                    msg: ['Validation failed : isEmail'],
                };
                break;
            case 'URL':
                colProp.dt = 'character varying';
                colProp.validate = {
                    func: ['isURL'],
                    args: [''],
                    msg: ['Validation failed : isURL'],
                };
                break;
            case 'Number':
                colProp.dt = 'bigint';
                break;
            case 'Decimal':
                colProp.dt = 'decimal';
                break;
            case 'Currency':
                colProp.dt = 'decimal';
                colProp.validate = {
                    func: ['isCurrency'],
                    args: [''],
                    msg: ['Validation failed : isCurrency'],
                };
                break;
            case 'Percent':
                colProp.dt = 'double precision';
                break;
            case 'Duration':
                colProp.dt = 'decimal';
                break;
            case 'Rating':
                colProp.dt = 'smallint';
                break;
            case 'Formula':
                colProp.dt = 'character varying';
                break;
            case 'Rollup':
                colProp.dt = 'character varying';
                break;
            case 'Count':
                colProp.dt = 'int8';
                break;
            case 'Lookup':
                colProp.dt = 'character varying';
                break;
            case 'DateTime':
                colProp.dt = 'timestamp';
                break;
            case 'CreateTime':
                colProp.dt = 'timestamp';
                break;
            case 'LastModifiedTime':
                colProp.dt = 'timestamp';
                break;
            case 'AutoNumber':
                colProp.dt = 'int';
                break;
            case 'Barcode':
                colProp.dt = 'character varying';
                break;
            case 'Button':
                colProp.dt = 'character varying';
                break;
            case 'JSON':
                colProp.dt = 'json';
                break;
            default:
                colProp.dt = 'character varying';
                break;
        }
        return colProp;
    }
    static getDataTypeListForUiType(col, idType) {
        switch (col.uidt) {
            case 'ID':
                if (idType === 'AG') {
                    return ['char', 'character', 'character varying'];
                }
                else if (idType === 'AI') {
                    return [
                        'int',
                        'integer',
                        'bigint',
                        'bigserial',
                        'int2',
                        'int4',
                        'int8',
                        'serial',
                        'serial2',
                        'serial8',
                        'smallint',
                        'smallserial',
                    ];
                }
                else {
                    return dbTypes;
                }
            case 'ForeignKey':
                return dbTypes;
            case 'SingleLineText':
            case 'LongText':
            case 'Collaborator':
                return ['char', 'character', 'character varying', 'text'];
            case 'Attachment':
                return ['json', 'char', 'character', 'character varying', 'text'];
            case 'JSON':
                return ['json', 'jsonb', 'text'];
            case 'Checkbox':
                return [
                    'bit',
                    'bool',
                    'int2',
                    'int4',
                    'int8',
                    'boolean',
                    'smallint',
                    'int',
                    'integer',
                    'bigint',
                    'bigserial',
                    'char',
                    'int4range',
                    'int8range',
                    'serial',
                    'serial2',
                    'serial8',
                ];
            case 'MultiSelect':
                return ['text'];
            case 'SingleSelect':
                return ['text'];
            case 'Year':
                return ['int'];
            case 'Time':
                return [
                    'time',
                    'time without time zone',
                    'timestamp',
                    'timestamp without time zone',
                    'timestamptz',
                    'timestamp with time zone',
                    'timetz',
                    'time with time zone',
                ];
            case 'PhoneNumber':
            case 'Email':
                return ['character varying'];
            case 'URL':
                return ['character varying', 'text'];
            case 'Number':
                return [
                    'int',
                    'integer',
                    'bigint',
                    'bigserial',
                    'int2',
                    'int4',
                    'int8',
                    'serial',
                    'serial2',
                    'serial8',
                    'double precision',
                    'float4',
                    'float8',
                    'smallint',
                    'smallserial',
                    'numeric',
                ];
            case 'Decimal':
                return ['double precision', 'float4', 'float8', 'numeric'];
            case 'Currency':
                return [
                    'int',
                    'integer',
                    'bigint',
                    'bigserial',
                    'int2',
                    'int4',
                    'int8',
                    'serial',
                    'serial2',
                    'serial8',
                    'double precision',
                    'money',
                    'float4',
                    'float8',
                    'numeric',
                ];
            case 'Percent':
                return [
                    'int',
                    'integer',
                    'bigint',
                    'bigserial',
                    'int2',
                    'int4',
                    'int8',
                    'serial',
                    'serial2',
                    'serial8',
                    'double precision',
                    'float4',
                    'float8',
                    'smallint',
                    'smallserial',
                    'numeric',
                ];
            case 'Duration':
                return [
                    'int',
                    'integer',
                    'bigint',
                    'bigserial',
                    'int2',
                    'int4',
                    'int8',
                    'serial',
                    'serial2',
                    'serial8',
                    'double precision',
                    'float4',
                    'float8',
                    'smallint',
                    'smallserial',
                    'numeric',
                ];
            case 'Rating':
                return [
                    'int',
                    'integer',
                    'bigint',
                    'bigserial',
                    'int2',
                    'int4',
                    'int8',
                    'serial',
                    'serial2',
                    'serial8',
                    'double precision',
                    'float4',
                    'float8',
                    'smallint',
                    'smallserial',
                    'numeric',
                ];
            case 'Formula':
                return ['text', 'character varying'];
            case 'Rollup':
                return ['character varying'];
            case 'Count':
                return [
                    'int',
                    'integer',
                    'bigint',
                    'bigserial',
                    'int2',
                    'int4',
                    'int8',
                    'serial',
                    'serial2',
                    'serial8',
                    'smallint',
                    'smallserial',
                ];
            case 'Lookup':
                return ['character varying'];
            case 'Date':
                return [
                    'date',
                    'timestamp',
                    'timestamp without time zone',
                    'timestamptz',
                    'timestamp with time zone',
                ];
            case 'DateTime':
            case 'CreateTime':
            case 'LastModifiedTime':
                return [
                    'timestamp',
                    'timestamp without time zone',
                    'timestamptz',
                    'timestamp with time zone',
                ];
            case 'AutoNumber':
                return [
                    'int',
                    'integer',
                    'bigint',
                    'bigserial',
                    'int2',
                    'int4',
                    'int8',
                    'serial',
                    'serial2',
                    'serial8',
                    'smallint',
                    'smallserial',
                ];
            case 'Barcode':
                return ['character varying'];
            case 'Geometry':
                return [
                    'polygon',
                    'point',
                    'circle',
                    'box',
                    'line',
                    'lseg',
                    'path',
                    'circle',
                ];
            case 'Button':
            default:
                return dbTypes;
        }
    }
    static getUnsupportedFnList() {
        return [];
    }
}
exports.PgUi = PgUi;
// module.exports = PgUiHelp;
/**
 * @copyright Copyright (c) 2021, Xgene Cloud Ltd
 *
 * @author Naveen MR <oof1lab@gmail.com>
 * @author Pranav C Balan <pranavxc@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiUGdVaS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9saWIvc3FsVWkvUGdVaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSx5REFBaUM7QUFHakMsTUFBTSxPQUFPLEdBQUc7SUFDZCxLQUFLO0lBQ0wsU0FBUztJQUNULFFBQVE7SUFDUixXQUFXO0lBQ1gsTUFBTTtJQUNOLE1BQU07SUFDTixNQUFNO0lBQ04sTUFBTTtJQUNOLFdBQVc7SUFDWCxXQUFXO0lBQ1gsUUFBUTtJQUNSLFNBQVM7SUFDVCxTQUFTO0lBQ1QsV0FBVztJQUNYLEtBQUs7SUFDTCxNQUFNO0lBQ04sU0FBUztJQUNULE1BQU07SUFDTixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLGFBQWE7SUFDYixRQUFRO0lBQ1IsUUFBUTtJQUNSLE1BQU07SUFDTixVQUFVO0lBQ1YsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixNQUFNO0lBQ04sTUFBTTtJQUNOLE1BQU07SUFDTix3QkFBd0I7SUFDeEIsV0FBVztJQUNYLDZCQUE2QjtJQUM3QixhQUFhO0lBQ2IsMEJBQTBCO0lBQzFCLFFBQVE7SUFDUixxQkFBcUI7SUFDckIsV0FBVztJQUNYLE1BQU07SUFDTixPQUFPO0lBQ1AsV0FBVztJQUNYLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsYUFBYTtJQUNiLFVBQVU7SUFDVixLQUFLO0lBQ0wsUUFBUTtJQUNSLE9BQU87SUFDUCxLQUFLO0lBQ0wsTUFBTTtJQUNOLFFBQVE7SUFDUixTQUFTO0lBQ1QsTUFBTTtJQUNOLFVBQVU7SUFDVixVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLE1BQU07SUFDTixNQUFNO0lBQ04sU0FBUztJQUNULE9BQU87SUFDUCxNQUFNO0lBQ04sU0FBUztJQUNULFVBQVU7SUFDVixLQUFLO0lBQ0wsUUFBUTtJQUNSLE1BQU07SUFDTixnQkFBZ0I7SUFDaEIsUUFBUTtJQUNSLGNBQWM7SUFDZCxPQUFPO0lBQ1AsU0FBUztJQUNULFFBQVE7SUFDUixXQUFXO0lBQ1gsVUFBVTtJQUNWLFdBQVc7SUFDWCxlQUFlO0lBQ2YsY0FBYztJQUNkLFNBQVM7SUFDVCxhQUFhO0lBQ2IsU0FBUztJQUNULGVBQWU7SUFDZixTQUFTO0lBQ1QsU0FBUztJQUNULFNBQVM7SUFDVCxNQUFNO0lBQ04sS0FBSztJQUNMLFdBQVc7SUFDWCxTQUFTO0lBQ1QsYUFBYTtJQUNiLFNBQVM7SUFDVCxTQUFTO0lBQ1QsV0FBVztJQUNYLFVBQVU7SUFDVixlQUFlO0lBQ2YsU0FBUztJQUNULE1BQU07SUFDTixLQUFLO0lBQ0wsS0FBSztDQUNOLENBQUM7QUFFRixNQUFhLElBQUk7SUFDZixNQUFNLENBQUMsa0JBQWtCO1FBQ3ZCLE9BQU87WUFDTDtnQkFDRSxXQUFXLEVBQUUsSUFBSTtnQkFDakIsS0FBSyxFQUFFLElBQUk7Z0JBQ1gsRUFBRSxFQUFFLE1BQU07Z0JBQ1YsR0FBRyxFQUFFLFNBQVM7Z0JBQ2QsRUFBRSxFQUFFLFNBQVM7Z0JBQ2IsSUFBSSxFQUFFLEtBQUs7Z0JBQ1gsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLElBQUk7Z0JBQ1IsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLElBQUk7Z0JBQ1IsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsRUFBRSxFQUFFLEVBQUU7Z0JBQ04sRUFBRSxFQUFFLENBQUM7Z0JBQ0wsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsT0FBTyxFQUFFLENBQUM7Z0JBQ1YsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsR0FBRyxFQUFFLEVBQUU7Z0JBQ1AsSUFBSSxFQUFFLEVBQUU7YUFDVDtZQUNEO2dCQUNFLFdBQVcsRUFBRSxPQUFPO2dCQUNwQixLQUFLLEVBQUUsT0FBTztnQkFDZCxFQUFFLEVBQUUsbUJBQW1CO2dCQUN2QixHQUFHLEVBQUUsY0FBYztnQkFDbkIsRUFBRSxFQUFFLGFBQWE7Z0JBQ2pCLElBQUksRUFBRSxJQUFJO2dCQUNWLEdBQUcsRUFBRSxLQUFLO2dCQUNWLEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEdBQUcsRUFBRSxJQUFJO2dCQUNULElBQUksRUFBRSxFQUFFO2dCQUNSLEVBQUUsRUFBRSxJQUFJO2dCQUNSLEVBQUUsRUFBRSxJQUFJO2dCQUNSLElBQUksRUFBRSxJQUFJO2dCQUNWLElBQUksRUFBRSxFQUFFO2dCQUNSLE9BQU8sRUFBRSxDQUFDO2dCQUNWLElBQUksRUFBRSxnQkFBZ0I7Z0JBQ3RCLEdBQUcsRUFBRSxFQUFFO2dCQUNQLElBQUksRUFBRSxFQUFFO2FBQ1Q7WUFDRDtnQkFDRSxXQUFXLEVBQUUsWUFBWTtnQkFDekIsS0FBSyxFQUFFLFdBQVc7Z0JBQ2xCLEVBQUUsRUFBRSxXQUFXO2dCQUNmLEdBQUcsRUFBRSxjQUFjO2dCQUNuQixFQUFFLEVBQUUsYUFBYTtnQkFDakIsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsR0FBRyxFQUFFLEtBQUs7Z0JBQ1YsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsR0FBRyxFQUFFLE9BQU87Z0JBQ1osSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsRUFBRSxFQUFFLElBQUk7Z0JBQ1IsRUFBRSxFQUFFLElBQUk7Z0JBQ1IsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsT0FBTyxFQUFFLENBQUM7Z0JBQ1YsSUFBSSxFQUFFLGlCQUFPLENBQUMsUUFBUTtnQkFDdEIsR0FBRyxFQUFFLEVBQUU7Z0JBQ1AsSUFBSSxFQUFFLEVBQUU7YUFDVDtZQUNEO2dCQUNFLFdBQVcsRUFBRSxZQUFZO2dCQUN6QixLQUFLLEVBQUUsV0FBVztnQkFDbEIsRUFBRSxFQUFFLFdBQVc7Z0JBQ2YsR0FBRyxFQUFFLGNBQWM7Z0JBQ25CLEVBQUUsRUFBRSxhQUFhO2dCQUNqQixJQUFJLEVBQUUsSUFBSTtnQkFDVixHQUFHLEVBQUUsS0FBSztnQkFDVixFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsSUFBSTtnQkFDUixHQUFHLEVBQUUsT0FBTztnQkFDWixJQUFJLEVBQUUsRUFBRTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixPQUFPLEVBQUUsQ0FBQztnQkFDVixJQUFJLEVBQUUsaUJBQU8sQ0FBQyxRQUFRO2dCQUN0QixHQUFHLEVBQUUsRUFBRTtnQkFDUCxJQUFJLEVBQUUsRUFBRTthQUNUO1NBQ0YsQ0FBQztJQUNKLENBQUM7SUFFRCxNQUFNLENBQUMsWUFBWSxDQUFDLE1BQU07UUFDeEIsT0FBTztZQUNMLFdBQVcsRUFBRSxPQUFPLEdBQUcsTUFBTTtZQUM3QixFQUFFLEVBQUUsbUJBQW1CO1lBQ3ZCLEdBQUcsRUFBRSxjQUFjO1lBQ25CLEVBQUUsRUFBRSxhQUFhO1lBQ2pCLElBQUksRUFBRSxJQUFJO1lBQ1YsR0FBRyxFQUFFLEtBQUs7WUFDVixFQUFFLEVBQUUsS0FBSztZQUNULEVBQUUsRUFBRSxLQUFLO1lBQ1QsRUFBRSxFQUFFLEtBQUs7WUFDVCxFQUFFLEVBQUUsS0FBSztZQUNULEdBQUcsRUFBRSxJQUFJO1lBQ1QsSUFBSSxFQUFFLEVBQUU7WUFDUixFQUFFLEVBQUUsSUFBSTtZQUNSLEVBQUUsRUFBRSxJQUFJO1lBQ1IsSUFBSSxFQUFFLElBQUk7WUFDVixJQUFJLEVBQUUsRUFBRTtZQUNSLE9BQU8sRUFBRSxDQUFDO1lBQ1YsSUFBSSxFQUFFLGdCQUFnQjtZQUN0QixHQUFHLEVBQUUsRUFBRTtZQUNQLElBQUksRUFBRSxFQUFFO1NBQ1QsQ0FBQztJQUNKLENBQUM7SUFFRCw2Q0FBNkM7SUFDN0Msb0JBQW9CO0lBQ3BCLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLEVBQUU7SUFDRix3QkFBd0I7SUFDeEIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZix1QkFBdUI7SUFDdkIsd0JBQXdCO0lBQ3hCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixlQUFlO0lBQ2Ysc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysb0JBQW9CO0lBQ3BCLG9CQUFvQjtJQUNwQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLG9CQUFvQjtJQUNwQixlQUFlO0lBQ2Ysd0JBQXdCO0lBQ3hCLHNCQUFzQjtJQUN0QixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLDhCQUE4QjtJQUM5QixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLDhCQUE4QjtJQUM5QixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLDhCQUE4QjtJQUM5QixtQkFBbUI7SUFDbkIsMkJBQTJCO0lBQzNCLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixFQUFFO0lBQ0YsTUFBTTtJQUNOLEVBQUU7SUFDRixJQUFJO0lBRUosTUFBTSxDQUFDLDJCQUEyQixDQUFDLElBQUk7UUFDckMsUUFBUSxJQUFJLEVBQUU7WUFDWixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxFQUFFLENBQUM7WUFDWixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxFQUFFLENBQUM7WUFDWixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxFQUFFLENBQUM7WUFDWixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxFQUFFLENBQUM7WUFDWixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxFQUFFLENBQUM7WUFDWixLQUFLLGlCQUFpQjtnQkFDcEIsT0FBTyxFQUFFLENBQUM7WUFDWixLQUFLLGNBQWM7Z0JBQ2pCLE9BQU8sRUFBRSxDQUFDO1lBQ1osS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1NBQ2I7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLDBCQUEwQixDQUFDLElBQUk7UUFDcEMsUUFBUSxJQUFJLEVBQUU7WUFDWixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssYUFBYSxDQUFDO1lBQ25CLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxXQUFXLENBQUM7WUFDakIsS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssS0FBSyxDQUFDO1lBQ1gsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxXQUFXLENBQUM7WUFDakIsS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssa0JBQWtCLENBQUM7WUFDeEIsS0FBSyxlQUFlLENBQUM7WUFDckIsS0FBSyxhQUFhLENBQUM7WUFDbkIsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssa0JBQWtCLENBQUM7WUFDeEIsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLEtBQUssQ0FBQztZQUNYLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLGtCQUFrQixDQUFDO1lBQ3hCLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssS0FBSyxDQUFDO1lBQ1gsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssZ0JBQWdCLENBQUM7WUFDdEIsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLGNBQWMsQ0FBQztZQUNwQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxXQUFXLENBQUM7WUFDakIsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxXQUFXLENBQUM7WUFDakIsS0FBSyxlQUFlLENBQUM7WUFDckIsS0FBSyxjQUFjLENBQUM7WUFDcEIsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLGFBQWEsQ0FBQztZQUNuQixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssZUFBZSxDQUFDO1lBQ3JCLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssYUFBYSxDQUFDO1lBQ25CLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLEtBQUssQ0FBQztZQUNYLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyx3QkFBd0IsQ0FBQztZQUM5QixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLDZCQUE2QixDQUFDO1lBQ25DLEtBQUssYUFBYSxDQUFDO1lBQ25CLEtBQUssMEJBQTBCLENBQUM7WUFDaEMsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLHFCQUFxQixDQUFDO1lBQzNCLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxhQUFhLENBQUM7WUFDbkIsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssZUFBZSxDQUFDO1lBQ3JCLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLEtBQUssQ0FBQztZQUNYLEtBQUssS0FBSyxDQUFDO1lBQ1gsS0FBSyxtQkFBbUIsQ0FBQztZQUN6QixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLFlBQVksQ0FBQztZQUNsQixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxZQUFZLENBQUM7WUFDbEIsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLEtBQUssQ0FBQztZQUNYLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxZQUFZLENBQUM7WUFDbEIsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLFlBQVksQ0FBQztZQUNsQixLQUFLLGlCQUFpQixDQUFDO1lBQ3ZCLEtBQUssY0FBYyxDQUFDO1lBQ3BCLEtBQUssTUFBTTtnQkFDVCxPQUFPLElBQUksQ0FBQztTQUNmO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQywwQkFBMEIsQ0FBQyxJQUFJO1FBQ3BDLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxTQUFTO2dCQUNaLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssYUFBYTtnQkFDaEIsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssUUFBUTtnQkFDWCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxLQUFLO2dCQUNSLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxRQUFRO2dCQUNYLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssT0FBTztnQkFDVixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sY0FBYyxDQUFDO1lBRXhCLEtBQUssS0FBSztnQkFDUixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxRQUFRO2dCQUNYLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssU0FBUztnQkFDWixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxrQkFBa0IsQ0FBQztZQUU1QixLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxrQkFBa0I7Z0JBQ3JCLE9BQU8sU0FBUyxDQUFDO1lBRW5CLEtBQUssZUFBZTtnQkFDbEIsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxhQUFhO2dCQUNoQixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxTQUFTLENBQUM7WUFFbkIsS0FBSyxRQUFRO2dCQUNYLE9BQU8sU0FBUyxDQUFDO1lBRW5CLEtBQUssV0FBVztnQkFDZCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLGtCQUFrQjtnQkFDckIsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssS0FBSztnQkFDUixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssU0FBUztnQkFDWixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxrQkFBa0I7Z0JBQ3JCLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxTQUFTO2dCQUNaLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssT0FBTztnQkFDVixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxTQUFTO2dCQUNaLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssVUFBVTtnQkFDYixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxRQUFRO2dCQUNYLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLGdCQUFnQjtnQkFDbkIsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxRQUFRO2dCQUNYLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssY0FBYztnQkFDakIsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssU0FBUztnQkFDWixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxTQUFTLENBQUM7WUFFbkIsS0FBSyxRQUFRO2dCQUNYLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssV0FBVztnQkFDZCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssZUFBZTtnQkFDbEIsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxjQUFjO2dCQUNqQixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxhQUFhO2dCQUNoQixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxlQUFlO2dCQUNsQixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxTQUFTO2dCQUNaLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssU0FBUztnQkFDWixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxTQUFTO2dCQUNaLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssU0FBUztnQkFDWixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxhQUFhO2dCQUNoQixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sbUJBQW1CLENBQUM7WUFFN0IsS0FBSyxLQUFLO2dCQUNSLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssTUFBTTtnQkFDVCxPQUFPLDZCQUE2QixDQUFDO1lBRXZDLEtBQUssd0JBQXdCO2dCQUMzQixPQUFPLDZCQUE2QixDQUFDO1lBRXZDLEtBQUssV0FBVztnQkFDZCxPQUFPLHVDQUF1QyxDQUFDO1lBRWpELEtBQUssNkJBQTZCO2dCQUNoQyxPQUFPLHVDQUF1QyxDQUFDO1lBRWpELEtBQUssYUFBYTtnQkFDaEIsT0FBTywwRkFBMEYsQ0FBQztZQUVwRyxLQUFLLDBCQUEwQjtnQkFDN0IsT0FBTyx1Q0FBdUMsQ0FBQztZQUVqRCxLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxXQUFXLENBQUM7WUFFckIsS0FBSyxxQkFBcUI7Z0JBQ3hCLE9BQU8sV0FBVyxDQUFDO1lBRXJCLEtBQUssV0FBVztnQkFDZCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxhQUFhO2dCQUNoQixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxTQUFTO2dCQUNaLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssV0FBVztnQkFDZCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxlQUFlO2dCQUNsQixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssS0FBSztnQkFDUixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxtQkFBbUI7Z0JBQ3RCLE9BQU8sbUJBQW1CLENBQUM7WUFFN0IsS0FBSyxTQUFTO2dCQUNaLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssV0FBVztnQkFDZCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxTQUFTO2dCQUNaLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssUUFBUTtnQkFDWCxPQUFPLFNBQVMsQ0FBQztZQUVuQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxtQkFBbUIsQ0FBQztZQUU3QixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssVUFBVTtnQkFDYixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssUUFBUTtnQkFDWCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssVUFBVTtnQkFDYixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssWUFBWTtnQkFDZixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxpQkFBaUI7Z0JBQ3BCLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssY0FBYztnQkFDakIsT0FBTyxNQUFNLENBQUM7U0FDakI7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLDBCQUEwQixDQUFDLElBQUk7UUFDcEMsUUFBUSxJQUFJLEVBQUU7WUFDWixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxHQUFHLENBQUM7WUFFYixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxHQUFHLENBQUM7WUFFYixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxHQUFHLENBQUM7WUFFYixLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxHQUFHLENBQUM7WUFFYixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxHQUFHLENBQUM7WUFFYixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxHQUFHLENBQUM7WUFFYixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxHQUFHLENBQUM7WUFFYixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxHQUFHLENBQUM7WUFFYixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxHQUFHLENBQUM7WUFFYixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxHQUFHLENBQUM7WUFFYixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxHQUFHLENBQUM7WUFFYixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssV0FBVztnQkFDZCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssTUFBTTtnQkFDVCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssTUFBTTtnQkFDVCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssTUFBTTtnQkFDVCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssU0FBUztnQkFDWixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssT0FBTztnQkFDVixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssTUFBTTtnQkFDVCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssVUFBVTtnQkFDYixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssWUFBWTtnQkFDZixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssVUFBVTtnQkFDYixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssUUFBUTtnQkFDWCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssV0FBVztnQkFDZCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssTUFBTTtnQkFDVCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssVUFBVTtnQkFDYixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssWUFBWTtnQkFDZixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssVUFBVTtnQkFDYixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssTUFBTTtnQkFDVCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssS0FBSztnQkFDUixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssVUFBVTtnQkFDYixPQUFPLEdBQUcsQ0FBQztZQUNiLEtBQUssT0FBTztnQkFDVixPQUFPLEdBQUcsQ0FBQztZQUNiLEtBQUssWUFBWTtnQkFDZixPQUFPLEdBQUcsQ0FBQztZQUNiLEtBQUssU0FBUztnQkFDWixPQUFPLEdBQUcsQ0FBQztZQUNiLEtBQUssWUFBWTtnQkFDZixPQUFPLEdBQUcsQ0FBQztZQUNiLEtBQUssaUJBQWlCO2dCQUNwQixPQUFPLEdBQUcsQ0FBQztZQUNiLEtBQUssY0FBYztnQkFDakIsT0FBTyxHQUFHLENBQUM7WUFDYixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxHQUFHLENBQUM7U0FDZDtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsaUJBQWlCLENBQUMsR0FBRyxFQUFFLE9BQU87UUFDbkMsb0JBQW9CO1FBQ3BCLElBQ0UsR0FBRyxDQUFDLEVBQUUsS0FBSyxNQUFNO1lBQ2pCLEdBQUcsQ0FBQyxFQUFFLEtBQUssU0FBUztZQUNwQixHQUFHLENBQUMsRUFBRSxLQUFLLFFBQVE7WUFDbkIsR0FBRyxDQUFDLEVBQUUsS0FBSyxVQUFVLEVBQ3JCO1lBQ0EsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLEVBQUU7Z0JBQ3ZDLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxHQUFHLENBQUMsRUFBRSxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUU7b0JBQzdDLE9BQU8sSUFBSSxDQUFDO2lCQUNiO2FBQ0Y7WUFDRCxPQUFPLEtBQUssQ0FBQztTQUNkO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQztTQUNiO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJO1FBQzNCLG9CQUFvQjtRQUNwQixPQUFPLElBQUksQ0FBQztRQUNaLDBCQUEwQjtRQUMxQiw0QkFBNEI7UUFDNUIsNkJBQTZCO1FBQzdCLDhCQUE4QjtRQUM5QiwyQkFBMkI7UUFDM0Isa0JBQWtCO1FBQ2xCLFdBQVc7UUFDWCxpQkFBaUI7UUFDakIsSUFBSTtJQUNOLENBQUM7SUFFRCxNQUFNLENBQUMsa0JBQWtCLENBQUMsR0FBRztRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLElBQ0UsR0FBRyxDQUFDLEVBQUUsS0FBSyxLQUFLO1lBQ2hCLEdBQUcsQ0FBQyxFQUFFLEtBQUssUUFBUTtZQUNuQixHQUFHLENBQUMsRUFBRSxLQUFLLFVBQVU7WUFDckIsR0FBRyxDQUFDLEVBQUUsS0FBSyxTQUFTLEVBQ3BCO1lBQ0EsR0FBRyxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsT0FBTyxJQUFJLENBQUMsQ0FBQztTQUNoQztRQUVELGlCQUFpQjtRQUNqQiw2QkFBNkI7UUFDN0IsV0FBVztRQUNYLGlCQUFpQjtRQUNqQixJQUFJO0lBQ04sQ0FBQztJQUVELE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakIsV0FBVztRQUNYLEdBQUcsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUM7UUFDL0IsSUFBSTtRQUNKLElBQUksR0FBRyxDQUFDLEVBQUUsRUFBRTtZQUNWLEdBQUcsQ0FBQyxHQUFHLEdBQUcsT0FBTyxDQUFDO1NBQ25CO1FBRUQsaUJBQWlCO1FBQ2pCLDZCQUE2QjtRQUM3QixXQUFXO1FBQ1gsaUJBQWlCO1FBQ2pCLElBQUk7SUFDTixDQUFDO0lBRUQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxVQUFVO1FBQ3pCLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVELE1BQU0sQ0FBQyxjQUFjLENBQUMsT0FBTztRQUMzQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsRUFBRTtZQUN2QyxJQUNFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEtBQUssQ0FBQztnQkFDeEIsQ0FBQyxDQUNDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssS0FBSztvQkFDdkIsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxRQUFRO29CQUMxQixPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLFNBQVM7b0JBQzNCLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssVUFBVTtvQkFDNUIsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxXQUFXLENBQzlCLEVBQ0Q7Z0JBQ0EsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxLQUFLLENBQUM7Z0JBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkJBQTZCLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQzNEO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDNUI7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNO1FBQzFCLE9BQU8sTUFBTSxDQUFDLEVBQUUsS0FBSyxhQUFhLElBQUksTUFBTSxDQUFDLEVBQUUsS0FBSyxlQUFlLENBQUM7SUFDdEUsQ0FBQztJQUVELE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLO1FBQzlCLE1BQU0sR0FBRyxHQUNQLDJFQUEyRSxDQUFDO1FBQzlFLE1BQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDL0IsT0FBTyxLQUFLLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCxNQUFNLENBQUMsb0JBQW9CLENBQUMsS0FBSztRQUMvQixNQUFNLEdBQUcsR0FDUCw0RUFBNEUsQ0FBQztRQUMvRSxNQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLE9BQU8sS0FBSyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRUQsTUFBTSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsT0FBTztRQUNwQyxJQUFJLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxFQUFFO1lBQ3pFLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7WUFDeEUsTUFBTSxHQUFHO2dCQUNQO29CQUNFLFFBQVEsRUFBRSxNQUFNLENBQUMsUUFBUTtpQkFDMUI7YUFDRixDQUFDO1NBQ0g7YUFBTTtZQUNMLE1BQU0sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQ3JCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3RDLE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BDLHFDQUFxQztnQkFDckMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3BDLE1BQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDckIsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO2lCQUN0RDthQUNGO1NBQ0Y7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDO0lBRUQsTUFBTSxDQUFDLFlBQVksQ0FBQyxLQUFLO1FBQ3ZCOzs7V0FHRztRQUNILE9BQU8sS0FBSyxDQUFDLEtBQUssQ0FBQyx5Q0FBeUMsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsYUFBYSxDQUFDLElBQUk7UUFDaEIsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFckMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsWUFBWSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQztTQUN6RDthQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNwQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLFlBQVksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUM7U0FDekQ7YUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDcEMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxZQUFZLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1NBQ3pEO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7U0FDbkI7UUFFRCxPQUFPLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxNQUFNLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLEVBQUU7UUFDaEMsTUFBTSxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBRW5CLElBQUk7WUFDRixJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3BELE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQy9CLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxFQUFFO29CQUNwQyxNQUFNLE1BQU0sR0FBRzt3QkFDYixFQUFFLEVBQUUsSUFBSTt3QkFDUixFQUFFO3dCQUNGLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO3dCQUNwQixHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDWixFQUFFLEVBQUUsRUFBRTt3QkFDTixFQUFFLEVBQUUsQ0FBQzt3QkFDTCxJQUFJLEVBQUUsSUFBSTt3QkFDVixHQUFHLEVBQUUsQ0FBQzt3QkFDTixFQUFFLEVBQUUsS0FBSzt3QkFDVCxJQUFJLEVBQUUsS0FBSzt3QkFDWCxHQUFHLEVBQUUsS0FBSzt3QkFDVixFQUFFLEVBQUUsS0FBSzt3QkFDVCxFQUFFLEVBQUUsa0JBQWtCO3dCQUN0QixFQUFFLEVBQUUsS0FBSzt3QkFDVCxNQUFNLEVBQUUsS0FBSzt3QkFDYixHQUFHLEVBQUUsSUFBSTt3QkFDVCxFQUFFLEVBQUUsRUFBRTt3QkFDTixHQUFHLEVBQUUsSUFBSTt3QkFDVCxHQUFHLEVBQUUsY0FBYzt3QkFDbkIsSUFBSSxFQUFFLElBQUk7d0JBQ1YsSUFBSSxFQUFFLENBQUM7d0JBQ1AsT0FBTyxFQUFFLENBQUM7cUJBQ1gsQ0FBQztvQkFFRixRQUFRLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUM1QixLQUFLLFFBQVE7NEJBQ1gsSUFBSSxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dDQUNuQyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0NBQ2pELE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO3dDQUNwQixFQUFFLEVBQUUsV0FBVztxQ0FDaEIsQ0FBQyxDQUFDO2lDQUNKO3FDQUFNO29DQUNMLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO3dDQUNwQixFQUFFLEVBQUUsS0FBSzt3Q0FDVCxFQUFFLEVBQUUsRUFBRTt3Q0FDTixFQUFFLEVBQUUsQ0FBQztxQ0FDTixDQUFDLENBQUM7aUNBQ0o7NkJBQ0Y7aUNBQU07Z0NBQ0wsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0NBQ3BCLEVBQUUsRUFBRSxRQUFRO29DQUNaLEVBQUUsRUFBRSxJQUFJO29DQUNSLEVBQUUsRUFBRSxJQUFJO29DQUNSLElBQUksRUFBRSxJQUFJO29DQUNWLElBQUksRUFBRSxJQUFJO2lDQUNYLENBQUMsQ0FBQzs2QkFDSjs0QkFDRCxNQUFNO3dCQUNSLEtBQUssUUFBUTs0QkFDWCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0NBQ25DLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO29DQUNwQixFQUFFLEVBQUUsTUFBTTtpQ0FDWCxDQUFDLENBQUM7NkJBQ0o7aUNBQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTtnQ0FDdEMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0NBQ3BCLEVBQUUsRUFBRSxtQkFBbUI7b0NBQ3ZCLEVBQUUsRUFBRSxJQUFJO29DQUNSLEVBQUUsRUFBRSxDQUFDO29DQUNMLElBQUksRUFBRSxJQUFJO2lDQUNYLENBQUMsQ0FBQzs2QkFDSjtpQ0FBTTtnQ0FDTCxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRTtvQ0FDcEIsRUFBRSxFQUFFLE1BQU07aUNBQ1gsQ0FBQyxDQUFDOzZCQUNKOzRCQUNELE1BQU07d0JBQ1IsS0FBSyxTQUFTOzRCQUNaLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO2dDQUNwQixFQUFFLEVBQUUsU0FBUztnQ0FDYixFQUFFLEVBQUUsQ0FBQztnQ0FDTCxFQUFFLEVBQUUsQ0FBQzs2QkFDTixDQUFDLENBQUM7NEJBQ0gsTUFBTTt3QkFDUixLQUFLLFFBQVE7NEJBQ1gsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7Z0NBQ3BCLEVBQUUsRUFBRSxNQUFNO2dDQUNWLEVBQUUsRUFBRSxDQUFDO2dDQUNMLEVBQUUsRUFBRSxDQUFDOzZCQUNOLENBQUMsQ0FBQzs0QkFDSCxNQUFNO3dCQUNSOzRCQUNFLE1BQU07cUJBQ1Q7b0JBQ0QsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDdEI7YUFDRjtTQUNGO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDVixPQUFPLENBQUMsR0FBRyxDQUFDLDZCQUE2QixFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQy9DO1FBRUQsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQztJQUVELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsS0FBSztRQUNoQyxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtZQUM3QixPQUFPLEtBQUssQ0FBQztTQUNkO1FBQ0QsT0FBTyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLElBQUksbUJBQW1CLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3hFLENBQUM7SUFFRCxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUs7UUFDdEIsT0FBTyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHO1FBQzFCLElBQUksR0FBRyxDQUFDLE9BQU8sS0FBSyxDQUFDLEVBQUU7WUFDckIsT0FBTyxJQUFJLENBQUM7U0FDYjtRQUVELFFBQVEsR0FBRyxDQUFDLEVBQUUsRUFBRTtZQUNkLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyx3QkFBd0IsQ0FBQztZQUM5QixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLDZCQUE2QixDQUFDO1lBQ25DLEtBQUssYUFBYSxDQUFDO1lBQ25CLEtBQUssMEJBQTBCLENBQUM7WUFDaEMsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLHFCQUFxQjtnQkFDeEIsT0FBTyxLQUFLLENBQUM7WUFDZjtnQkFDRSxPQUFPLElBQUksQ0FBQztTQUNmO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxlQUFlLENBQUMsR0FBRztRQUN4QixRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSSxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsV0FBVyxFQUFFLEVBQUU7WUFDeEMsS0FBSyxTQUFTO2dCQUNaLE9BQU8sTUFBTSxDQUFDO1lBQ2hCLEtBQUssYUFBYSxDQUFDO1lBQ25CLEtBQUssVUFBVTtnQkFDYixPQUFPLFFBQVEsQ0FBQztZQUVsQixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxTQUFTLENBQUM7WUFDbkIsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxRQUFRLENBQUM7WUFFbEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sU0FBUyxDQUFDO1lBRW5CLEtBQUssS0FBSyxDQUFDO1lBQ1gsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxXQUFXO2dCQUNkLE9BQU8sUUFBUSxDQUFDO1lBRWxCLEtBQUssS0FBSyxDQUFDO1lBQ1gsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUssU0FBUztnQkFDWixPQUFPLFFBQVEsQ0FBQztZQUVsQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFDaEIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssa0JBQWtCO2dCQUNyQixPQUFPLFFBQVEsQ0FBQztZQUVsQixLQUFLLGVBQWUsQ0FBQztZQUNyQixLQUFLLGFBQWE7Z0JBQ2hCLE9BQU8sUUFBUSxDQUFDO1lBRWxCLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxRQUFRO2dCQUNYLE9BQU8sT0FBTyxDQUFDO1lBRWpCLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssa0JBQWtCLENBQUM7WUFDeEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sUUFBUSxDQUFDO1lBRWxCLEtBQUssS0FBSyxDQUFDO1lBQ1gsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxTQUFTO2dCQUNaLE9BQU8sU0FBUyxDQUFDO1lBQ25CLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssVUFBVTtnQkFDYixPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxRQUFRLENBQUM7WUFFbEIsS0FBSyxrQkFBa0IsQ0FBQztZQUN4QixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLEtBQUssQ0FBQztZQUNYLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLGdCQUFnQixDQUFDO1lBQ3RCLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxjQUFjLENBQUM7WUFDcEIsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxRQUFRLENBQUM7WUFDbEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sT0FBTyxDQUFDO1lBQ2pCLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxXQUFXLENBQUM7WUFDakIsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxXQUFXLENBQUM7WUFDakIsS0FBSyxlQUFlLENBQUM7WUFDckIsS0FBSyxjQUFjLENBQUM7WUFDcEIsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLGFBQWEsQ0FBQztZQUNuQixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssZUFBZSxDQUFDO1lBQ3JCLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxRQUFRLENBQUM7WUFDbEIsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxhQUFhO2dCQUNoQixPQUFPLFNBQVMsQ0FBQztZQUNuQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxRQUFRLENBQUM7WUFDbEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBQ2hCLEtBQUssS0FBSztnQkFDUixPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssd0JBQXdCO2dCQUMzQixPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLDZCQUE2QixDQUFDO1lBQ25DLEtBQUssYUFBYSxDQUFDO1lBQ25CLEtBQUssMEJBQTBCO2dCQUM3QixPQUFPLFVBQVUsQ0FBQztZQUNwQixLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUsscUJBQXFCO2dCQUN4QixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssYUFBYSxDQUFDO1lBQ25CLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLGVBQWUsQ0FBQztZQUNyQixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLG1CQUFtQixDQUFDO1lBQ3pCLEtBQUssS0FBSztnQkFDUixPQUFPLFFBQVEsQ0FBQztZQUVsQixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssV0FBVztnQkFDZCxPQUFPLFNBQVMsQ0FBQztZQUVuQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxRQUFRO2dCQUNYLE9BQU8sT0FBTyxDQUFDO1lBQ2pCLEtBQUssU0FBUztnQkFDWixPQUFPLFNBQVMsQ0FBQztZQUNuQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxVQUFVLENBQUM7WUFFcEIsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxPQUFPO2dCQUNWLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssVUFBVTtnQkFDYixPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUssV0FBVztnQkFDZCxPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssVUFBVTtnQkFDYixPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFDaEIsS0FBSyxLQUFLO2dCQUNSLE9BQU8sS0FBSyxDQUFDO1lBQ2YsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxZQUFZLENBQUM7WUFDbEIsS0FBSyxZQUFZLENBQUM7WUFDbEIsS0FBSyxpQkFBaUIsQ0FBQztZQUN2QixLQUFLLGNBQWM7Z0JBQ2pCLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztTQUNqQjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUc7UUFDbEIsUUFBUSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ2pDLEtBQUssU0FBUztnQkFDWixPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxVQUFVLENBQUM7WUFDcEIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sU0FBUyxDQUFDO1lBQ25CLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxZQUFZLENBQUM7WUFDdEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBQ2hCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxnQkFBZ0IsQ0FBQztZQUMxQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxVQUFVLENBQUM7WUFDcEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sWUFBWSxDQUFDO1lBQ3RCLEtBQUssTUFBTTtnQkFDVCxPQUFPLGNBQWMsQ0FBQztZQUN4QixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxhQUFhLENBQUM7WUFDdkIsS0FBSyxNQUFNO2dCQUNULE9BQU8sVUFBVSxDQUFDO1NBQ3JCO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxHQUFzQixFQUFFLE1BQWU7UUFDakUsTUFBTSxPQUFPLEdBQVEsRUFBRSxDQUFDO1FBQ3hCLFFBQVEsR0FBRyxDQUFDLElBQUksRUFBRTtZQUNoQixLQUFLLElBQUk7Z0JBQ1A7b0JBQ0UsTUFBTSxXQUFXLEdBQUcsTUFBTSxLQUFLLElBQUksQ0FBQztvQkFDcEMsTUFBTSxXQUFXLEdBQUcsTUFBTSxLQUFLLElBQUksQ0FBQztvQkFDcEMsT0FBTyxDQUFDLEVBQUUsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7b0JBQ3hELE9BQU8sQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDO29CQUNsQixPQUFPLENBQUMsRUFBRSxHQUFHLFdBQVcsQ0FBQztvQkFDekIsT0FBTyxDQUFDLEVBQUUsR0FBRyxXQUFXLENBQUM7b0JBQ3pCLE9BQU8sQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDO29CQUNuQixPQUFPLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztpQkFDdkQ7Z0JBQ0QsTUFBTTtZQUNSLEtBQUssWUFBWTtnQkFDZixPQUFPLENBQUMsRUFBRSxHQUFHLG1CQUFtQixDQUFDO2dCQUNqQyxNQUFNO1lBQ1IsS0FBSyxnQkFBZ0I7Z0JBQ25CLE9BQU8sQ0FBQyxFQUFFLEdBQUcsbUJBQW1CLENBQUM7Z0JBQ2pDLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUixLQUFLLGFBQWE7Z0JBQ2hCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDO2dCQUNwQixNQUFNO1lBQ1IsS0FBSyxjQUFjO2dCQUNqQixPQUFPLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQztnQkFDcEIsTUFBTTtZQUNSLEtBQUssY0FBYztnQkFDakIsT0FBTyxDQUFDLEVBQUUsR0FBRyxtQkFBbUIsQ0FBQztnQkFDakMsTUFBTTtZQUNSLEtBQUssTUFBTTtnQkFDVCxPQUFPLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQztnQkFFcEIsTUFBTTtZQUNSLEtBQUssTUFBTTtnQkFDVCxPQUFPLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQztnQkFDbkIsTUFBTTtZQUNSLEtBQUssTUFBTTtnQkFDVCxPQUFPLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQztnQkFDcEIsTUFBTTtZQUNSLEtBQUssYUFBYTtnQkFDaEIsT0FBTyxDQUFDLEVBQUUsR0FBRyxtQkFBbUIsQ0FBQztnQkFDakMsT0FBTyxDQUFDLFFBQVEsR0FBRztvQkFDakIsSUFBSSxFQUFFLENBQUMsZUFBZSxDQUFDO29CQUN2QixJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBQ1YsR0FBRyxFQUFFLENBQUMsbUNBQW1DLENBQUM7aUJBQzNDLENBQUM7Z0JBQ0YsTUFBTTtZQUNSLEtBQUssT0FBTztnQkFDVixPQUFPLENBQUMsRUFBRSxHQUFHLG1CQUFtQixDQUFDO2dCQUNqQyxPQUFPLENBQUMsUUFBUSxHQUFHO29CQUNqQixJQUFJLEVBQUUsQ0FBQyxTQUFTLENBQUM7b0JBQ2pCLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQztvQkFDVixHQUFHLEVBQUUsQ0FBQyw2QkFBNkIsQ0FBQztpQkFDckMsQ0FBQztnQkFDRixNQUFNO1lBQ1IsS0FBSyxLQUFLO2dCQUNSLE9BQU8sQ0FBQyxFQUFFLEdBQUcsbUJBQW1CLENBQUM7Z0JBQ2pDLE9BQU8sQ0FBQyxRQUFRLEdBQUc7b0JBQ2pCLElBQUksRUFBRSxDQUFDLE9BQU8sQ0FBQztvQkFDZixJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBQ1YsR0FBRyxFQUFFLENBQUMsMkJBQTJCLENBQUM7aUJBQ25DLENBQUM7Z0JBQ0YsTUFBTTtZQUNSLEtBQUssUUFBUTtnQkFDWCxPQUFPLENBQUMsRUFBRSxHQUFHLFFBQVEsQ0FBQztnQkFDdEIsTUFBTTtZQUNSLEtBQUssU0FBUztnQkFDWixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsTUFBTTtZQUNSLEtBQUssVUFBVTtnQkFDYixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLFFBQVEsR0FBRztvQkFDakIsSUFBSSxFQUFFLENBQUMsWUFBWSxDQUFDO29CQUNwQixJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBQ1YsR0FBRyxFQUFFLENBQUMsZ0NBQWdDLENBQUM7aUJBQ3hDLENBQUM7Z0JBQ0YsTUFBTTtZQUNSLEtBQUssU0FBUztnQkFDWixPQUFPLENBQUMsRUFBRSxHQUFHLGtCQUFrQixDQUFDO2dCQUNoQyxNQUFNO1lBQ1IsS0FBSyxVQUFVO2dCQUNiLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxRQUFRO2dCQUNYLE9BQU8sQ0FBQyxFQUFFLEdBQUcsVUFBVSxDQUFDO2dCQUN4QixNQUFNO1lBQ1IsS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxFQUFFLEdBQUcsbUJBQW1CLENBQUM7Z0JBQ2pDLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxtQkFBbUIsQ0FBQztnQkFDakMsTUFBTTtZQUNSLEtBQUssT0FBTztnQkFDVixPQUFPLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQztnQkFDcEIsTUFBTTtZQUNSLEtBQUssUUFBUTtnQkFDWCxPQUFPLENBQUMsRUFBRSxHQUFHLG1CQUFtQixDQUFDO2dCQUNqQyxNQUFNO1lBQ1IsS0FBSyxVQUFVO2dCQUNiLE9BQU8sQ0FBQyxFQUFFLEdBQUcsV0FBVyxDQUFDO2dCQUN6QixNQUFNO1lBQ1IsS0FBSyxZQUFZO2dCQUNmLE9BQU8sQ0FBQyxFQUFFLEdBQUcsV0FBVyxDQUFDO2dCQUN6QixNQUFNO1lBQ1IsS0FBSyxrQkFBa0I7Z0JBQ3JCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsV0FBVyxDQUFDO2dCQUN6QixNQUFNO1lBQ1IsS0FBSyxZQUFZO2dCQUNmLE9BQU8sQ0FBQyxFQUFFLEdBQUcsS0FBSyxDQUFDO2dCQUNuQixNQUFNO1lBQ1IsS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxFQUFFLEdBQUcsbUJBQW1CLENBQUM7Z0JBQ2pDLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxtQkFBbUIsQ0FBQztnQkFDakMsTUFBTTtZQUNSLEtBQUssTUFBTTtnQkFDVCxPQUFPLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQztnQkFDcEIsTUFBTTtZQUNSO2dCQUNFLE9BQU8sQ0FBQyxFQUFFLEdBQUcsbUJBQW1CLENBQUM7Z0JBQ2pDLE1BQU07U0FDVDtRQUNELE9BQU8sT0FBTyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxNQUFNLENBQUMsd0JBQXdCLENBQUMsR0FBdUIsRUFBRSxNQUFjO1FBQ3JFLFFBQVEsR0FBRyxDQUFDLElBQUksRUFBRTtZQUNoQixLQUFLLElBQUk7Z0JBQ1AsSUFBSSxNQUFNLEtBQUssSUFBSSxFQUFFO29CQUNuQixPQUFPLENBQUMsTUFBTSxFQUFFLFdBQVcsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO2lCQUNuRDtxQkFBTSxJQUFJLE1BQU0sS0FBSyxJQUFJLEVBQUU7b0JBQzFCLE9BQU87d0JBQ0wsS0FBSzt3QkFDTCxTQUFTO3dCQUNULFFBQVE7d0JBQ1IsV0FBVzt3QkFDWCxNQUFNO3dCQUNOLE1BQU07d0JBQ04sTUFBTTt3QkFDTixRQUFRO3dCQUNSLFNBQVM7d0JBQ1QsU0FBUzt3QkFDVCxVQUFVO3dCQUNWLGFBQWE7cUJBQ2QsQ0FBQztpQkFDSDtxQkFBTTtvQkFDTCxPQUFPLE9BQU8sQ0FBQztpQkFDaEI7WUFDSCxLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxPQUFPLENBQUM7WUFFakIsS0FBSyxnQkFBZ0IsQ0FBQztZQUN0QixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLGNBQWM7Z0JBQ2pCLE9BQU8sQ0FBQyxNQUFNLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBRTVELEtBQUssWUFBWTtnQkFDZixPQUFPLENBQUMsTUFBTSxFQUFFLE1BQU0sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFFcEUsS0FBSyxNQUFNO2dCQUNULE9BQU8sQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ25DLEtBQUssVUFBVTtnQkFDYixPQUFPO29CQUNMLEtBQUs7b0JBQ0wsTUFBTTtvQkFDTixNQUFNO29CQUNOLE1BQU07b0JBQ04sTUFBTTtvQkFDTixTQUFTO29CQUNULFVBQVU7b0JBQ1YsS0FBSztvQkFDTCxTQUFTO29CQUNULFFBQVE7b0JBQ1IsV0FBVztvQkFDWCxNQUFNO29CQUNOLFdBQVc7b0JBQ1gsV0FBVztvQkFDWCxRQUFRO29CQUNSLFNBQVM7b0JBQ1QsU0FBUztpQkFDVixDQUFDO1lBRUosS0FBSyxhQUFhO2dCQUNoQixPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFbEIsS0FBSyxjQUFjO2dCQUNqQixPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFbEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUVqQixLQUFLLE1BQU07Z0JBQ1QsT0FBTztvQkFDTCxNQUFNO29CQUNOLHdCQUF3QjtvQkFDeEIsV0FBVztvQkFDWCw2QkFBNkI7b0JBQzdCLGFBQWE7b0JBQ2IsMEJBQTBCO29CQUMxQixRQUFRO29CQUNSLHFCQUFxQjtpQkFDdEIsQ0FBQztZQUVKLEtBQUssYUFBYSxDQUFDO1lBQ25CLEtBQUssT0FBTztnQkFDVixPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQztZQUUvQixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxDQUFDLG1CQUFtQixFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBRXZDLEtBQUssUUFBUTtnQkFDWCxPQUFPO29CQUNMLEtBQUs7b0JBQ0wsU0FBUztvQkFDVCxRQUFRO29CQUNSLFdBQVc7b0JBQ1gsTUFBTTtvQkFDTixNQUFNO29CQUNOLE1BQU07b0JBQ04sUUFBUTtvQkFDUixTQUFTO29CQUNULFNBQVM7b0JBQ1Qsa0JBQWtCO29CQUNsQixRQUFRO29CQUNSLFFBQVE7b0JBQ1IsVUFBVTtvQkFDVixhQUFhO29CQUNiLFNBQVM7aUJBQ1YsQ0FBQztZQUVKLEtBQUssU0FBUztnQkFDWixPQUFPLENBQUMsa0JBQWtCLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxTQUFTLENBQUMsQ0FBQztZQUU3RCxLQUFLLFVBQVU7Z0JBQ2IsT0FBTztvQkFDTCxLQUFLO29CQUNMLFNBQVM7b0JBQ1QsUUFBUTtvQkFDUixXQUFXO29CQUNYLE1BQU07b0JBQ04sTUFBTTtvQkFDTixNQUFNO29CQUNOLFFBQVE7b0JBQ1IsU0FBUztvQkFDVCxTQUFTO29CQUNULGtCQUFrQjtvQkFDbEIsT0FBTztvQkFDUCxRQUFRO29CQUNSLFFBQVE7b0JBQ1IsU0FBUztpQkFDVixDQUFDO1lBRUosS0FBSyxTQUFTO2dCQUNaLE9BQU87b0JBQ0wsS0FBSztvQkFDTCxTQUFTO29CQUNULFFBQVE7b0JBQ1IsV0FBVztvQkFDWCxNQUFNO29CQUNOLE1BQU07b0JBQ04sTUFBTTtvQkFDTixRQUFRO29CQUNSLFNBQVM7b0JBQ1QsU0FBUztvQkFDVCxrQkFBa0I7b0JBQ2xCLFFBQVE7b0JBQ1IsUUFBUTtvQkFDUixVQUFVO29CQUNWLGFBQWE7b0JBQ2IsU0FBUztpQkFDVixDQUFDO1lBRUosS0FBSyxVQUFVO2dCQUNiLE9BQU87b0JBQ0wsS0FBSztvQkFDTCxTQUFTO29CQUNULFFBQVE7b0JBQ1IsV0FBVztvQkFDWCxNQUFNO29CQUNOLE1BQU07b0JBQ04sTUFBTTtvQkFDTixRQUFRO29CQUNSLFNBQVM7b0JBQ1QsU0FBUztvQkFDVCxrQkFBa0I7b0JBQ2xCLFFBQVE7b0JBQ1IsUUFBUTtvQkFDUixVQUFVO29CQUNWLGFBQWE7b0JBQ2IsU0FBUztpQkFDVixDQUFDO1lBRUosS0FBSyxRQUFRO2dCQUNYLE9BQU87b0JBQ0wsS0FBSztvQkFDTCxTQUFTO29CQUNULFFBQVE7b0JBQ1IsV0FBVztvQkFDWCxNQUFNO29CQUNOLE1BQU07b0JBQ04sTUFBTTtvQkFDTixRQUFRO29CQUNSLFNBQVM7b0JBQ1QsU0FBUztvQkFDVCxrQkFBa0I7b0JBQ2xCLFFBQVE7b0JBQ1IsUUFBUTtvQkFDUixVQUFVO29CQUNWLGFBQWE7b0JBQ2IsU0FBUztpQkFDVixDQUFDO1lBRUosS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxNQUFNLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztZQUV2QyxLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFFL0IsS0FBSyxPQUFPO2dCQUNWLE9BQU87b0JBQ0wsS0FBSztvQkFDTCxTQUFTO29CQUNULFFBQVE7b0JBQ1IsV0FBVztvQkFDWCxNQUFNO29CQUNOLE1BQU07b0JBQ04sTUFBTTtvQkFDTixRQUFRO29CQUNSLFNBQVM7b0JBQ1QsU0FBUztvQkFDVCxVQUFVO29CQUNWLGFBQWE7aUJBQ2QsQ0FBQztZQUVKLEtBQUssUUFBUTtnQkFDWCxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQztZQUUvQixLQUFLLE1BQU07Z0JBQ1QsT0FBTztvQkFDTCxNQUFNO29CQUNOLFdBQVc7b0JBQ1gsNkJBQTZCO29CQUM3QixhQUFhO29CQUNiLDBCQUEwQjtpQkFDM0IsQ0FBQztZQUVKLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssa0JBQWtCO2dCQUNyQixPQUFPO29CQUNMLFdBQVc7b0JBQ1gsNkJBQTZCO29CQUM3QixhQUFhO29CQUNiLDBCQUEwQjtpQkFDM0IsQ0FBQztZQUVKLEtBQUssWUFBWTtnQkFDZixPQUFPO29CQUNMLEtBQUs7b0JBQ0wsU0FBUztvQkFDVCxRQUFRO29CQUNSLFdBQVc7b0JBQ1gsTUFBTTtvQkFDTixNQUFNO29CQUNOLE1BQU07b0JBQ04sUUFBUTtvQkFDUixTQUFTO29CQUNULFNBQVM7b0JBQ1QsVUFBVTtvQkFDVixhQUFhO2lCQUNkLENBQUM7WUFFSixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFFL0IsS0FBSyxVQUFVO2dCQUNiLE9BQU87b0JBQ0wsU0FBUztvQkFDVCxPQUFPO29CQUNQLFFBQVE7b0JBQ1IsS0FBSztvQkFDTCxNQUFNO29CQUNOLE1BQU07b0JBQ04sTUFBTTtvQkFDTixRQUFRO2lCQUNULENBQUM7WUFFSixLQUFLLFFBQVEsQ0FBQztZQUNkO2dCQUNFLE9BQU8sT0FBTyxDQUFDO1NBQ2xCO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxvQkFBb0I7UUFDekIsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDO0NBQ0Y7QUFsMURELG9CQWsxREM7QUFFRCw2QkFBNkI7QUFDN0I7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQXFCRyJ9