"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MysqlUi = void 0;
const UITypes_1 = __importDefault(require("../UITypes"));
const dbTypes = [
    'int',
    'smallint',
    'mediumint',
    'bigint',
    'bit',
    'boolean',
    'float',
    'decimal',
    'double',
    'serial',
    'tinyint',
    'date',
    'datetime',
    'timestamp',
    'time',
    'year',
    'char',
    'varchar',
    'nchar',
    'text',
    'tinytext',
    'mediumtext',
    'longtext',
    'binary',
    'varbinary',
    'blob',
    'tinyblob',
    'mediumblob',
    'longblob',
    'enum',
    'set',
    'geometry',
    'point',
    'linestring',
    'polygon',
    'multipoint',
    'multilinestring',
    'multipolygon',
    'json',
];
class MysqlUi {
    static getNewTableColumns() {
        return [
            {
                column_name: 'id',
                title: 'Id',
                dt: 'int',
                dtx: 'integer',
                ct: 'int(11)',
                nrqd: false,
                rqd: true,
                ck: false,
                pk: true,
                un: true,
                ai: true,
                cdf: null,
                clen: null,
                np: 11,
                ns: 0,
                dtxp: '11',
                dtxs: '',
                altered: 1,
                uidt: 'ID',
                uip: '',
                uicn: '',
            },
            {
                column_name: 'title',
                title: 'Title',
                dt: 'varchar',
                dtx: 'specificType',
                ct: 'varchar(45)',
                nrqd: true,
                rqd: false,
                ck: false,
                pk: false,
                un: false,
                ai: false,
                cdf: null,
                clen: 45,
                np: null,
                ns: null,
                dtxp: '45',
                dtxs: '',
                altered: 1,
                uidt: 'SingleLineText',
                uip: '',
                uicn: '',
            },
            {
                column_name: 'created_at',
                title: 'CreatedAt',
                dt: 'timestamp',
                dtx: 'specificType',
                ct: 'varchar(45)',
                nrqd: true,
                rqd: false,
                ck: false,
                pk: false,
                un: false,
                ai: false,
                cdf: 'CURRENT_TIMESTAMP',
                clen: 45,
                np: null,
                ns: null,
                dtxp: '',
                dtxs: '',
                altered: 1,
                uidt: UITypes_1.default.DateTime,
                uip: '',
                uicn: '',
            },
            {
                column_name: 'updated_at',
                title: 'UpdatedAt',
                dt: 'timestamp',
                dtx: 'specificType',
                ct: 'varchar(45)',
                nrqd: true,
                rqd: false,
                ck: false,
                pk: false,
                un: false,
                ai: false,
                cdf: 'CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP',
                clen: 45,
                np: null,
                ns: null,
                dtxp: '',
                dtxs: '',
                altered: 1,
                uidt: UITypes_1.default.DateTime,
                uip: '',
                uicn: '',
            },
        ];
    }
    static getNewColumn(suffix) {
        return {
            column_name: 'title' + suffix,
            dt: 'varchar',
            dtx: 'specificType',
            ct: 'varchar(45)',
            nrqd: true,
            rqd: false,
            ck: false,
            pk: false,
            un: false,
            ai: false,
            cdf: null,
            clen: 45,
            np: null,
            ns: null,
            dtxp: '45',
            dtxs: '',
            altered: 1,
            uidt: 'SingleLineText',
            uip: '',
            uicn: '',
        };
    }
    static getDefaultLengthForDatatype(type) {
        switch (type) {
            case 'int':
                return 11;
            case 'tinyint':
                return 1;
            case 'smallint':
                return 5;
            case 'mediumint':
                return 9;
            case 'bigint':
                return 20;
            case 'bit':
                return 64;
            case 'boolean':
                return '';
            case 'float':
                return 12;
            case 'decimal':
                return 10;
            case 'double':
                return 22;
            case 'serial':
                return 20;
            case 'date':
                return '';
            case 'datetime':
            case 'timestamp':
                return 6;
            case 'time':
                return '';
            case 'year':
                return '';
            case 'char':
                return 255;
            case 'varchar':
                return 255;
            case 'nchar':
                return 255;
            case 'text':
                return '';
            case 'tinytext':
                return '';
            case 'mediumtext':
                return '';
            case 'longtext':
                return '';
            case 'binary':
                return 255;
            case 'varbinary':
                return 65500;
            case 'blob':
                return '';
            case 'tinyblob':
                return '';
            case 'mediumblob':
                return '';
            case 'longblob':
                return '';
            case 'enum':
                return '';
            case 'set':
                return '';
            case 'geometry':
                return '';
            case 'point':
                return '';
            case 'linestring':
                return '';
            case 'polygon':
                return '';
            case 'multipoint':
                return '';
            case 'multilinestring':
                return '';
            case 'multipolygon':
                return '';
            case 'json':
                return '';
        }
    }
    static getDefaultLengthIsDisabled(type) {
        switch (type) {
            case 'int':
            case 'tinyint':
            case 'smallint':
            case 'mediumint':
            case 'bigint':
            case 'float':
            case 'decimal':
            case 'double':
            case 'serial':
            case 'datetime':
            case 'timestamp':
            case 'char':
            case 'varchar':
            case 'nchar':
            case 'binary':
            case 'varbinary':
            case 'enum':
            case 'set':
            case 'geometry':
            case 'point':
            case 'linestring':
            case 'polygon':
            case 'multipoint':
            case 'multilinestring':
            case 'multipolygon':
            case 'json':
            case 'bit':
                return false;
            case 'boolean':
            case 'date':
            case 'time':
            case 'year':
            case 'text':
            case 'tinytext':
            case 'mediumtext':
            case 'longtext':
            case 'blob':
            case 'tinyblob':
            case 'mediumblob':
            case 'longblob':
                return true;
        }
    }
    static getDefaultValueForDatatype(type) {
        switch (type) {
            case 'int':
                return 'eg : ' + 10;
            case 'tinyint':
                return 'eg : ' + 1;
            case 'smallint':
                return 'eg : ' + 10;
            case 'mediumint':
                return 'eg : ' + 10;
            case 'bigint':
                return 'eg : ' + 100;
            case 'bit':
                return 'eg : ' + 1;
            case 'boolean':
                return 'eg : ' + 1;
            case 'float':
                return 'eg : ' + 10.0;
            case 'decimal':
                return 'eg : ' + 10.0;
            case 'double':
                return 'eg : ' + 10.0;
            case 'serial':
                return 'eg : ' + 100;
            case 'date':
                return 'eg : ' + '2020-09-09';
            case 'datetime':
                return ('eg : ' +
                    'now()\n\nCURRENT_TIMESTAMP\n\nCURRENT_TIMESTAMP on update CURRENT_TIMESTAMP\n\n1992-10-12 00:00:00');
            case 'timestamp':
                return ('eg : ' +
                    'now()\n\nCURRENT_TIMESTAMP\n\nCURRENT_TIMESTAMP on update CURRENT_TIMESTAMP\n\n1992-10-12 00:00:00');
            case 'time':
                return 'eg : ' + '00:00:00';
            case 'year':
                return 'eg : ' + '2020';
            case 'char':
                return 'eg : ' + 'a';
            case 'varchar':
                return 'eg : ' + 'hey';
            case 'nchar':
                return 'eg : ' + 'hey';
            case 'text':
                return 'eg : ' + 'hey';
            case 'tinytext':
                return 'eg : ' + 'hey';
            case 'mediumtext':
                return 'eg : ' + 'hey';
            case 'longtext':
                return 'eg : ' + 'hey';
            case 'binary':
                return 'eg : ' + 1;
            case 'varbinary':
                return 'eg : ' + 'hey';
            case 'blob':
                return 'eg : ' + 'hey';
            case 'tinyblob':
                return 'eg : ' + 'hey';
            case 'mediumblob':
                return 'eg : ' + 'hey';
            case 'longblob':
                return 'eg : ' + 'hey';
            case 'enum':
                return 'eg : ' + 'a';
            case 'set':
                return 'eg : ' + 'a';
            case 'geometry':
                return "geometry can't have default value";
            case 'point':
                return "point can't have default value";
            case 'linestring':
                return "linestring can't have default value";
            case 'polygon':
                return "polygon can't have default value";
            case 'multipoint':
                return "multipoint can't have default value";
            case 'multilinestring':
                return "multilinestring can't have default value";
            case 'multipolygon':
                return "multipolygon can't have default value";
            case 'json':
                return "JSON can't have default value";
        }
    }
    static getDefaultScaleForDatatype(type) {
        switch (type) {
            case 'int':
                return ' ';
            case 'tinyint':
                return ' ';
            case 'smallint':
                return ' ';
            case 'mediumint':
                return ' ';
            case 'bigint':
                return ' ';
            case 'bit':
                return ' ';
            case 'boolean':
                return ' ';
            case 'float':
                return '2';
            case 'decimal':
                return '2';
            case 'double':
                return '2';
            case 'serial':
                return ' ';
            case 'date':
            case 'datetime':
            case 'timestamp':
                return ' ';
            case 'time':
                return ' ';
            case 'year':
                return ' ';
            case 'char':
                return ' ';
            case 'varchar':
                return ' ';
            case 'nchar':
                return ' ';
            case 'text':
                return ' ';
            case 'tinytext':
                return ' ';
            case 'mediumtext':
                return ' ';
            case 'longtext':
                return ' ';
            case 'binary':
                return ' ';
            case 'varbinary':
                return ' ';
            case 'blob':
                return ' ';
            case 'tinyblob':
                return ' ';
            case 'mediumblob':
                return ' ';
            case 'longblob':
                return ' ';
            case 'enum':
                return ' ';
            case 'set':
                return ' ';
            case 'geometry':
                return ' ';
            case 'point':
                return ' ';
            case 'linestring':
                return ' ';
            case 'polygon':
                return ' ';
            case 'multipoint':
                return ' ';
            case 'multilinestring':
                return ' ';
            case 'multipolygon':
                return ' ';
            case 'json':
                return ' ';
        }
    }
    static colPropAIDisabled(col, columns) {
        // console.log(col);
        if (col.dt === 'int' ||
            col.dt === 'tinyint' ||
            col.dt === 'bigint' ||
            col.dt === 'smallint') {
            for (let i = 0; i < columns.length; ++i) {
                if (columns[i].cn !== col.cn && columns[i].ai) {
                    return true;
                }
            }
            return false;
        }
        else {
            return true;
        }
    }
    static colPropUNDisabled(col) {
        // console.log(col);
        if (col.dt === 'int' ||
            col.dt === 'tinyint' ||
            col.dt === 'smallint' ||
            col.dt === 'mediumint' ||
            col.dt === 'bigint') {
            return false;
        }
        else {
            return true;
        }
    }
    static onCheckboxChangeAI(col) {
        console.log(col);
        if (col.dt === 'int' ||
            col.dt === 'bigint' ||
            col.dt === 'smallint' ||
            col.dt === 'tinyint') {
            col.altered = col.altered || 2;
        }
        // if (!col.ai) {
        //   col.dtx = 'specificType'
        // } else {
        //   col.dtx = ''
        // }
    }
    static onCheckboxChangeAU(col) {
        console.log(col);
        // if (1) {
        col.altered = col.altered || 2;
        // }
        // if (!col.ai) {
        //   col.dtx = 'specificType'
        // } else {
        //   col.dtx = ''
        // }
    }
    static showScale(columnObj) {
        return (columnObj.dt === 'float' ||
            columnObj.dt === 'decimal' ||
            columnObj.dt === 'double' ||
            columnObj.dt === 'real');
    }
    static removeUnsigned(columns) {
        for (let i = 0; i < columns.length; ++i) {
            if (columns[i].altered === 1 &&
                !(columns[i].dt === 'int' ||
                    columns[i].dt === 'bigint' ||
                    columns[i].dt === 'tinyint' ||
                    columns[i].dt === 'smallint' ||
                    columns[i].dt === 'mediumint')) {
                columns[i].un = false;
                console.log('>> resetting unsigned value', columns[i].cn);
            }
            console.log(columns[i].cn);
        }
    }
    static columnEditable(colObj) {
        return colObj.tn !== '_evolutions' || colObj.tn !== 'nc_evolutions';
    }
    static extractFunctionName(query) {
        const reg = /^\s*CREATE\s+.*?(?:OR\s+REPLACE\s*)?\s*FUNCTION\s+(?:`?[\w\d_]+`?\.)?`?([\w_\d]+)`?/i;
        const match = query.match(reg);
        return match && match[1];
    }
    static extractProcedureName(query) {
        const reg = /^\s*CREATE.*?\s+(?:OR\s+REPLACE\s*)?\s*PROCEDURE\s+(?:[\w\d_]+\.)?([\w_\d]+)/i;
        const match = query.match(reg);
        return match && match[1];
    }
    static handleRawOutput(result, headers) {
        result = result[0] ? result[0] : [];
        if (Array.isArray(result) && result[0]) {
            const keys = Object.keys(result[0]);
            // set headers before settings result
            for (let i = 0; i < keys.length; i++) {
                const text = keys[i];
                headers.push({ text, value: text, sortable: false });
            }
        }
        else {
            const keys = Object.keys(result);
            for (let i = 0; i < keys.length; i++) {
                const text = keys[i];
                if (typeof text !== 'function') {
                    headers.push({ text, value: text, sortable: false });
                }
            }
            result = [result];
        }
        return result;
    }
    static splitQueries(query) {
        /***
         * we are splitting based on semicolon
         * there are mechanism to escape semicolon within single/double quotes(string)
         */
        return query.match(/\b("[^"]*;[^"]*"|'[^']*;[^']*'|[^;])*;/g);
    }
    /**
     * if sql statement is SELECT - it limits to a number
     * @param args
     * @returns {string|*}
     */
    static sanitiseQuery(args) {
        let q = args.query.trim().split(';');
        if (q[0].startsWith('Select')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else if (q[0].startsWith('select')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else if (q[0].startsWith('SELECT')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else {
            return args.query;
        }
        return q;
    }
    static getColumnsFromJson(json, tn) {
        const columns = [];
        try {
            if (typeof json === 'object' && !Array.isArray(json)) {
                const keys = Object.keys(json);
                for (let i = 0; i < keys.length; ++i) {
                    const column = {
                        dp: null,
                        tn,
                        column_name: keys[i],
                        cno: keys[i],
                        np: 10,
                        ns: 0,
                        clen: null,
                        cop: 1,
                        pk: false,
                        nrqd: false,
                        rqd: false,
                        un: false,
                        ct: 'int(11) unsigned',
                        ai: false,
                        unique: false,
                        cdf: null,
                        cc: '',
                        csn: null,
                        dtx: 'specificType',
                        dtxp: null,
                        dtxs: 0,
                        altered: 1,
                    };
                    switch (typeof json[keys[i]]) {
                        case 'number':
                            if (Number.isInteger(json[keys[i]])) {
                                if (MysqlUi.isValidTimestamp(keys[i], json[keys[i]])) {
                                    Object.assign(column, {
                                        dt: 'timestamp',
                                    });
                                }
                                else {
                                    Object.assign(column, {
                                        dt: 'int',
                                        np: 10,
                                        ns: 0,
                                    });
                                }
                            }
                            else {
                                Object.assign(column, {
                                    dt: 'float',
                                    np: 10,
                                    ns: 2,
                                    dtxp: '11',
                                    dtxs: 2,
                                });
                            }
                            break;
                        case 'string':
                            if (MysqlUi.isValidDate(json[keys[i]])) {
                                Object.assign(column, {
                                    dt: 'datetime',
                                });
                            }
                            else if (json[keys[i]].length <= 255) {
                                Object.assign(column, {
                                    dt: 'varchar',
                                    np: 255,
                                    ns: 0,
                                    dtxp: '255',
                                });
                            }
                            else {
                                Object.assign(column, {
                                    dt: 'text',
                                });
                            }
                            break;
                        case 'boolean':
                            Object.assign(column, {
                                dt: 'boolean',
                                np: 3,
                                ns: 0,
                            });
                            break;
                        case 'object':
                            Object.assign(column, {
                                dt: 'json',
                                np: 3,
                                ns: 0,
                            });
                            break;
                        default:
                            break;
                    }
                    columns.push(column);
                }
            }
        }
        catch (e) {
            console.log('Error in getColumnsFromJson', e);
        }
        return columns;
    }
    static isValidTimestamp(key, value) {
        if (typeof value !== 'number') {
            return false;
        }
        return new Date(value).getTime() > 0 && /(?:_|(?=A))[aA]t$/.test(key);
    }
    static isValidDate(value) {
        return new Date(value).getTime() > 0;
    }
    static colPropAuDisabled(_col) {
        return true;
    }
    static getUIType(col) {
        switch (this.getAbstractType(col)) {
            case 'integer':
                return 'Number';
            case 'boolean':
                return 'Checkbox';
            case 'float':
                return 'Decimal';
            case 'date':
                return 'Date';
            case 'datetime':
                return 'DateTime';
            case 'time':
                return 'Time';
            case 'year':
                return 'Year';
            case 'string':
                return 'SingleLineText';
            case 'text':
                return 'LongText';
            case 'blob':
                return 'Attachment';
            case 'enum':
                return 'SingleSelect';
            case 'set':
                return 'MultiSelect';
            case 'json':
                return 'LongText';
        }
    }
    static getAbstractType(col) {
        switch (col.dt.toLowerCase()) {
            case 'int':
            case 'smallint':
            case 'mediumint':
            case 'bigint':
            case 'bit':
                return 'integer';
            case 'boolean':
                return 'boolean';
            case 'float':
            case 'decimal':
            case 'double':
            case 'serial':
                return 'float';
            case 'tinyint':
                if (col.dtxp == '1') {
                    return 'boolean';
                }
                else {
                    return 'integer';
                }
            case 'date':
                return 'date';
            case 'datetime':
            case 'timestamp':
                return 'datetime';
            case 'time':
                return 'time';
            case 'year':
                return 'year';
            case 'char':
            case 'varchar':
            case 'nchar':
                return 'string';
            case 'text':
            case 'tinytext':
            case 'mediumtext':
            case 'longtext':
                return 'text';
            // todo: use proper type
            case 'binary':
                return 'string';
            case 'varbinary':
                return 'text';
            case 'blob':
            case 'tinyblob':
            case 'mediumblob':
            case 'longblob':
                return 'blob';
            case 'enum':
                return 'enum';
            case 'set':
                return 'set';
            case 'geometry':
            case 'point':
            case 'linestring':
            case 'polygon':
            case 'multipoint':
            case 'multilinestring':
            case 'multipolygon':
                return 'string';
            case 'json':
                return 'json';
        }
    }
    static getDataTypeForUiType(col, idType) {
        const colProp = {};
        switch (col.uidt) {
            case 'ID':
                {
                    const isAutoIncId = idType === 'AI';
                    const isAutoGenId = idType === 'AG';
                    colProp.dt = isAutoGenId ? 'varchar' : 'int';
                    colProp.pk = true;
                    colProp.un = isAutoIncId;
                    colProp.ai = isAutoIncId;
                    colProp.rqd = true;
                    colProp.meta = isAutoGenId ? { ag: 'nc' } : undefined;
                }
                break;
            case 'ForeignKey':
                colProp.dt = 'varchar';
                break;
            case 'SingleLineText':
                colProp.dt = 'varchar';
                break;
            case 'LongText':
                colProp.dt = 'text';
                break;
            case 'Attachment':
                colProp.dt = 'text';
                break;
            case 'Checkbox':
                colProp.dt = 'tinyint';
                colProp.dtxp = 1;
                break;
            case 'MultiSelect':
                colProp.dt = 'set';
                break;
            case 'SingleSelect':
                colProp.dt = 'enum';
                break;
            case 'Collaborator':
                colProp.dt = 'varchar';
                break;
            case 'Date':
                colProp.dt = 'varchar';
                break;
            case 'Year':
                colProp.dt = 'year';
                break;
            case 'Time':
                colProp.dt = 'time';
                break;
            case 'PhoneNumber':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isMobilePhone'],
                    args: [''],
                    msg: ['Validation failed : isMobilePhone ({cn})'],
                };
                break;
            case 'Email':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isEmail'],
                    args: [''],
                    msg: ['Validation failed : isEmail ({cn})'],
                };
                break;
            case 'URL':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isURL'],
                    args: [''],
                    msg: ['Validation failed : isURL ({cn})'],
                };
                break;
            case 'Number':
                colProp.dt = 'bigint';
                break;
            case 'Decimal':
                colProp.dt = 'decimal';
                break;
            case 'Currency':
                colProp.dt = 'decimal';
                colProp.validate = {
                    func: ['isCurrency'],
                    args: [''],
                    msg: ['Validation failed : isCurrency'],
                };
                break;
            case 'Percent':
                colProp.dt = 'double';
                break;
            case 'Duration':
                colProp.dt = 'decimal';
                break;
            case 'Rating':
                colProp.dt = 'int';
                break;
            case 'Formula':
                colProp.dt = 'varchar';
                break;
            case 'Rollup':
                colProp.dt = 'varchar';
                break;
            case 'Count':
                colProp.dt = 'int';
                break;
            case 'Lookup':
                colProp.dt = 'varchar';
                break;
            case 'DateTime':
                colProp.dt = 'datetime';
                break;
            case 'CreateTime':
                colProp.dt = 'datetime';
                break;
            case 'LastModifiedTime':
                colProp.dt = 'datetime';
                break;
            case 'AutoNumber':
                colProp.dt = 'int';
                break;
            case 'Barcode':
                colProp.dt = 'varchar';
                break;
            case 'Button':
                colProp.dt = 'varchar';
                break;
            case 'JSON':
                colProp.dt = 'json';
                break;
            default:
                colProp.dt = 'varchar';
                break;
        }
        return colProp;
    }
    static getDataTypeListForUiType(col, idType) {
        switch (col.uidt) {
            case 'ID':
                if (idType === 'AG') {
                    return ['varchar', 'char', 'nchar'];
                }
                else if (idType === 'AI') {
                    return ['int', 'smallint', 'mediumint', 'bigint', 'bit', 'serial'];
                }
                else {
                    return dbTypes;
                }
            case 'ForeignKey':
                return dbTypes;
            case 'SingleLineText':
            case 'LongText':
            case 'Collaborator':
                return [
                    'char',
                    'varchar',
                    'nchar',
                    'text',
                    'tinytext',
                    'mediumtext',
                    'longtext',
                ];
            case 'Attachment':
                return [
                    'json',
                    'char',
                    'varchar',
                    'nchar',
                    'text',
                    'tinytext',
                    'mediumtext',
                    'longtext',
                ];
            case 'JSON':
                return ['json', 'text', 'tinytext', 'mediumtext', 'longtext'];
            case 'Checkbox':
                return [
                    'int',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'bit',
                    'boolean',
                    'serial',
                    'tinyint',
                ];
            case 'MultiSelect':
                return ['set', 'text', 'tinytext', 'mediumtext', 'longtext'];
            case 'SingleSelect':
                return ['enum', 'text', 'tinytext', 'mediumtext', 'longtext'];
            case 'Year':
                return ['year'];
            case 'Time':
                return ['time'];
            case 'PhoneNumber':
            case 'Email':
                return ['varchar'];
            case 'URL':
                return ['text', 'tinytext', 'mediumtext', 'longtext', 'varchar'];
            case 'Number':
                return [
                    'int',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'bit',
                    'float',
                    'decimal',
                    'double',
                    'serial',
                ];
            case 'Decimal':
                return ['float', 'decimal', 'double', 'serial'];
            case 'Currency':
                return [
                    'decimal',
                    'float',
                    'double',
                    'serial',
                    'int',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'bit',
                ];
            case 'Percent':
                return [
                    'decimal',
                    'float',
                    'double',
                    'serial',
                    'int',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'bit',
                ];
            case 'Duration':
                return [
                    'decimal',
                    'float',
                    'double',
                    'serial',
                    'int',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'bit',
                ];
            case 'Rating':
                return [
                    'decimal',
                    'float',
                    'double',
                    'serial',
                    'int',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'bit',
                ];
            case 'Formula':
                return [
                    'char',
                    'varchar',
                    'nchar',
                    'text',
                    'tinytext',
                    'mediumtext',
                    'longtext',
                ];
            case 'Rollup':
                return ['varchar'];
            case 'Count':
                return ['int', 'smallint', 'mediumint', 'bigint', 'serial'];
            case 'Lookup':
                return ['varchar'];
            case 'Date':
                return ['date', 'datetime', 'timestamp', 'varchar'];
            case 'DateTime':
            case 'CreateTime':
            case 'LastModifiedTime':
                return ['datetime', 'timestamp', 'varchar'];
            case 'AutoNumber':
                return ['int', 'smallint', 'mediumint', 'bigint'];
            case 'Barcode':
                return ['varchar'];
            case 'Geometry':
                return [
                    'geometry',
                    'point',
                    'linestring',
                    'polygon',
                    'multipoint',
                    'multilinestring',
                    'multipolygon',
                ];
            case 'Button':
            default:
                return dbTypes;
        }
    }
    static getUnsupportedFnList() {
        return [];
    }
}
exports.MysqlUi = MysqlUi;
// module.exports = MysqlUiHelp;
/**
 * @copyright Copyright (c) 2021, Xgene Cloud Ltd
 *
 * @author Naveen MR <oof1lab@gmail.com>
 * @author Pranav C Balan <pranavxc@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTXlzcWxVaS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9saWIvc3FsVWkvTXlzcWxVaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSx5REFBaUM7QUFHakMsTUFBTSxPQUFPLEdBQUc7SUFDZCxLQUFLO0lBQ0wsVUFBVTtJQUNWLFdBQVc7SUFDWCxRQUFRO0lBQ1IsS0FBSztJQUNMLFNBQVM7SUFDVCxPQUFPO0lBQ1AsU0FBUztJQUNULFFBQVE7SUFDUixRQUFRO0lBQ1IsU0FBUztJQUNULE1BQU07SUFDTixVQUFVO0lBQ1YsV0FBVztJQUNYLE1BQU07SUFDTixNQUFNO0lBQ04sTUFBTTtJQUNOLFNBQVM7SUFDVCxPQUFPO0lBQ1AsTUFBTTtJQUNOLFVBQVU7SUFDVixZQUFZO0lBQ1osVUFBVTtJQUNWLFFBQVE7SUFDUixXQUFXO0lBQ1gsTUFBTTtJQUNOLFVBQVU7SUFDVixZQUFZO0lBQ1osVUFBVTtJQUNWLE1BQU07SUFDTixLQUFLO0lBQ0wsVUFBVTtJQUNWLE9BQU87SUFDUCxZQUFZO0lBQ1osU0FBUztJQUNULFlBQVk7SUFDWixpQkFBaUI7SUFDakIsY0FBYztJQUNkLE1BQU07Q0FDUCxDQUFDO0FBRUYsTUFBYSxPQUFPO0lBQ2xCLE1BQU0sQ0FBQyxrQkFBa0I7UUFDdkIsT0FBTztZQUNMO2dCQUNFLFdBQVcsRUFBRSxJQUFJO2dCQUNqQixLQUFLLEVBQUUsSUFBSTtnQkFDWCxFQUFFLEVBQUUsS0FBSztnQkFDVCxHQUFHLEVBQUUsU0FBUztnQkFDZCxFQUFFLEVBQUUsU0FBUztnQkFDYixJQUFJLEVBQUUsS0FBSztnQkFDWCxHQUFHLEVBQUUsSUFBSTtnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsSUFBSTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixHQUFHLEVBQUUsSUFBSTtnQkFDVCxJQUFJLEVBQUUsSUFBSTtnQkFDVixFQUFFLEVBQUUsRUFBRTtnQkFDTixFQUFFLEVBQUUsQ0FBQztnQkFDTCxJQUFJLEVBQUUsSUFBSTtnQkFDVixJQUFJLEVBQUUsRUFBRTtnQkFDUixPQUFPLEVBQUUsQ0FBQztnQkFDVixJQUFJLEVBQUUsSUFBSTtnQkFDVixHQUFHLEVBQUUsRUFBRTtnQkFDUCxJQUFJLEVBQUUsRUFBRTthQUNUO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLE9BQU87Z0JBQ3BCLEtBQUssRUFBRSxPQUFPO2dCQUNkLEVBQUUsRUFBRSxTQUFTO2dCQUNiLEdBQUcsRUFBRSxjQUFjO2dCQUNuQixFQUFFLEVBQUUsYUFBYTtnQkFDakIsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsR0FBRyxFQUFFLEtBQUs7Z0JBQ1YsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsRUFBRSxFQUFFLElBQUk7Z0JBQ1IsRUFBRSxFQUFFLElBQUk7Z0JBQ1IsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsT0FBTyxFQUFFLENBQUM7Z0JBQ1YsSUFBSSxFQUFFLGdCQUFnQjtnQkFDdEIsR0FBRyxFQUFFLEVBQUU7Z0JBQ1AsSUFBSSxFQUFFLEVBQUU7YUFDVDtZQUNEO2dCQUNFLFdBQVcsRUFBRSxZQUFZO2dCQUN6QixLQUFLLEVBQUUsV0FBVztnQkFDbEIsRUFBRSxFQUFFLFdBQVc7Z0JBQ2YsR0FBRyxFQUFFLGNBQWM7Z0JBQ25CLEVBQUUsRUFBRSxhQUFhO2dCQUNqQixJQUFJLEVBQUUsSUFBSTtnQkFDVixHQUFHLEVBQUUsS0FBSztnQkFDVixFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxHQUFHLEVBQUUsbUJBQW1CO2dCQUN4QixJQUFJLEVBQUUsRUFBRTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixPQUFPLEVBQUUsQ0FBQztnQkFDVixJQUFJLEVBQUUsaUJBQU8sQ0FBQyxRQUFRO2dCQUN0QixHQUFHLEVBQUUsRUFBRTtnQkFDUCxJQUFJLEVBQUUsRUFBRTthQUNUO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLFlBQVk7Z0JBQ3pCLEtBQUssRUFBRSxXQUFXO2dCQUNsQixFQUFFLEVBQUUsV0FBVztnQkFDZixHQUFHLEVBQUUsY0FBYztnQkFDbkIsRUFBRSxFQUFFLGFBQWE7Z0JBQ2pCLElBQUksRUFBRSxJQUFJO2dCQUNWLEdBQUcsRUFBRSxLQUFLO2dCQUNWLEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEdBQUcsRUFBRSwrQ0FBK0M7Z0JBQ3BELElBQUksRUFBRSxFQUFFO2dCQUNSLEVBQUUsRUFBRSxJQUFJO2dCQUNSLEVBQUUsRUFBRSxJQUFJO2dCQUNSLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksRUFBRSxFQUFFO2dCQUNSLE9BQU8sRUFBRSxDQUFDO2dCQUNWLElBQUksRUFBRSxpQkFBTyxDQUFDLFFBQVE7Z0JBQ3RCLEdBQUcsRUFBRSxFQUFFO2dCQUNQLElBQUksRUFBRSxFQUFFO2FBQ1Q7U0FDRixDQUFDO0lBQ0osQ0FBQztJQUVELE1BQU0sQ0FBQyxZQUFZLENBQUMsTUFBTTtRQUN4QixPQUFPO1lBQ0wsV0FBVyxFQUFFLE9BQU8sR0FBRyxNQUFNO1lBQzdCLEVBQUUsRUFBRSxTQUFTO1lBQ2IsR0FBRyxFQUFFLGNBQWM7WUFDbkIsRUFBRSxFQUFFLGFBQWE7WUFDakIsSUFBSSxFQUFFLElBQUk7WUFDVixHQUFHLEVBQUUsS0FBSztZQUNWLEVBQUUsRUFBRSxLQUFLO1lBQ1QsRUFBRSxFQUFFLEtBQUs7WUFDVCxFQUFFLEVBQUUsS0FBSztZQUNULEVBQUUsRUFBRSxLQUFLO1lBQ1QsR0FBRyxFQUFFLElBQUk7WUFDVCxJQUFJLEVBQUUsRUFBRTtZQUNSLEVBQUUsRUFBRSxJQUFJO1lBQ1IsRUFBRSxFQUFFLElBQUk7WUFDUixJQUFJLEVBQUUsSUFBSTtZQUNWLElBQUksRUFBRSxFQUFFO1lBQ1IsT0FBTyxFQUFFLENBQUM7WUFDVixJQUFJLEVBQUUsZ0JBQWdCO1lBQ3RCLEdBQUcsRUFBRSxFQUFFO1lBQ1AsSUFBSSxFQUFFLEVBQUU7U0FDVCxDQUFDO0lBQ0osQ0FBQztJQUVELE1BQU0sQ0FBQywyQkFBMkIsQ0FBQyxJQUFJO1FBQ3JDLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxLQUFLO2dCQUNSLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxDQUFDO1lBRVgsS0FBSyxVQUFVO2dCQUNiLE9BQU8sQ0FBQyxDQUFDO1lBRVgsS0FBSyxXQUFXO2dCQUNkLE9BQU8sQ0FBQyxDQUFDO1lBRVgsS0FBSyxRQUFRO2dCQUNYLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxLQUFLO2dCQUNSLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxTQUFTO2dCQUNaLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxPQUFPO2dCQUNWLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxTQUFTO2dCQUNaLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxRQUFRO2dCQUNYLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxRQUFRO2dCQUNYLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sQ0FBQyxDQUFDO1lBRVgsS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxNQUFNO2dCQUNULE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxTQUFTO2dCQUNaLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVO2dCQUNiLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxZQUFZO2dCQUNmLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVO2dCQUNiLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxRQUFRO2dCQUNYLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sS0FBSyxDQUFDO1lBRWYsS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVO2dCQUNiLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxZQUFZO2dCQUNmLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVO2dCQUNiLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxLQUFLO2dCQUNSLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVO2dCQUNiLE9BQU8sRUFBRSxDQUFDO1lBQ1osS0FBSyxPQUFPO2dCQUNWLE9BQU8sRUFBRSxDQUFDO1lBQ1osS0FBSyxZQUFZO2dCQUNmLE9BQU8sRUFBRSxDQUFDO1lBQ1osS0FBSyxTQUFTO2dCQUNaLE9BQU8sRUFBRSxDQUFDO1lBQ1osS0FBSyxZQUFZO2dCQUNmLE9BQU8sRUFBRSxDQUFDO1lBQ1osS0FBSyxpQkFBaUI7Z0JBQ3BCLE9BQU8sRUFBRSxDQUFDO1lBQ1osS0FBSyxjQUFjO2dCQUNqQixPQUFPLEVBQUUsQ0FBQztZQUNaLEtBQUssTUFBTTtnQkFDVCxPQUFPLEVBQUUsQ0FBQztTQUNiO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQywwQkFBMEIsQ0FBQyxJQUFJO1FBQ3BDLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxZQUFZLENBQUM7WUFDbEIsS0FBSyxpQkFBaUIsQ0FBQztZQUN2QixLQUFLLGNBQWMsQ0FBQztZQUNwQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssS0FBSztnQkFDUixPQUFPLEtBQUssQ0FBQztZQUVmLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLFlBQVksQ0FBQztZQUNsQixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssVUFBVTtnQkFDYixPQUFPLElBQUksQ0FBQztTQUNmO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQywwQkFBMEIsQ0FBQyxJQUFJO1FBQ3BDLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxLQUFLO2dCQUNSLE9BQU8sT0FBTyxHQUFHLEVBQUUsQ0FBQztZQUV0QixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1lBRXJCLEtBQUssVUFBVTtnQkFDYixPQUFPLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFFdEIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sT0FBTyxHQUFHLEVBQUUsQ0FBQztZQUV0QixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBRXZCLEtBQUssS0FBSztnQkFDUixPQUFPLE9BQU8sR0FBRyxDQUFDLENBQUM7WUFFckIsS0FBSyxTQUFTO2dCQUNaLE9BQU8sT0FBTyxHQUFHLENBQUMsQ0FBQztZQUVyQixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBRXhCLEtBQUssU0FBUztnQkFDWixPQUFPLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFFeEIsS0FBSyxRQUFRO2dCQUNYLE9BQU8sT0FBTyxHQUFHLElBQUksQ0FBQztZQUV4QixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBRXZCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE9BQU8sR0FBRyxZQUFZLENBQUM7WUFFaEMsS0FBSyxVQUFVO2dCQUNiLE9BQU8sQ0FDTCxPQUFPO29CQUNQLG9HQUFvRyxDQUNyRyxDQUFDO1lBRUosS0FBSyxXQUFXO2dCQUNkLE9BQU8sQ0FDTCxPQUFPO29CQUNQLG9HQUFvRyxDQUNyRyxDQUFDO1lBRUosS0FBSyxNQUFNO2dCQUNULE9BQU8sT0FBTyxHQUFHLFVBQVUsQ0FBQztZQUU5QixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxPQUFPLEdBQUcsTUFBTSxDQUFDO1lBRTFCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE9BQU8sR0FBRyxHQUFHLENBQUM7WUFFdkIsS0FBSyxTQUFTO2dCQUNaLE9BQU8sT0FBTyxHQUFHLEtBQUssQ0FBQztZQUV6QixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBRXpCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFFekIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sT0FBTyxHQUFHLEtBQUssQ0FBQztZQUV6QixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBRXpCLEtBQUssVUFBVTtnQkFDYixPQUFPLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFFekIsS0FBSyxRQUFRO2dCQUNYLE9BQU8sT0FBTyxHQUFHLENBQUMsQ0FBQztZQUVyQixLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBRXpCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFFekIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sT0FBTyxHQUFHLEtBQUssQ0FBQztZQUV6QixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBRXpCLEtBQUssVUFBVTtnQkFDYixPQUFPLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFFekIsS0FBSyxNQUFNO2dCQUNULE9BQU8sT0FBTyxHQUFHLEdBQUcsQ0FBQztZQUV2QixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxPQUFPLEdBQUcsR0FBRyxDQUFDO1lBRXZCLEtBQUssVUFBVTtnQkFDYixPQUFPLG1DQUFtQyxDQUFDO1lBRTdDLEtBQUssT0FBTztnQkFDVixPQUFPLGdDQUFnQyxDQUFDO1lBRTFDLEtBQUssWUFBWTtnQkFDZixPQUFPLHFDQUFxQyxDQUFDO1lBRS9DLEtBQUssU0FBUztnQkFDWixPQUFPLGtDQUFrQyxDQUFDO1lBRTVDLEtBQUssWUFBWTtnQkFDZixPQUFPLHFDQUFxQyxDQUFDO1lBRS9DLEtBQUssaUJBQWlCO2dCQUNwQixPQUFPLDBDQUEwQyxDQUFDO1lBRXBELEtBQUssY0FBYztnQkFDakIsT0FBTyx1Q0FBdUMsQ0FBQztZQUVqRCxLQUFLLE1BQU07Z0JBQ1QsT0FBTywrQkFBK0IsQ0FBQztTQUMxQztJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsMEJBQTBCLENBQUMsSUFBSTtRQUNwQyxRQUFRLElBQUksRUFBRTtZQUNaLEtBQUssS0FBSztnQkFDUixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssU0FBUztnQkFDWixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssVUFBVTtnQkFDYixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssV0FBVztnQkFDZCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssUUFBUTtnQkFDWCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssS0FBSztnQkFDUixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssU0FBUztnQkFDWixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssT0FBTztnQkFDVixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssU0FBUztnQkFDWixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssUUFBUTtnQkFDWCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssUUFBUTtnQkFDWCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxNQUFNO2dCQUNULE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxNQUFNO2dCQUNULE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxNQUFNO2dCQUNULE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxTQUFTO2dCQUNaLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxNQUFNO2dCQUNULE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxZQUFZO2dCQUNmLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxRQUFRO2dCQUNYLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxNQUFNO2dCQUNULE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxZQUFZO2dCQUNmLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxNQUFNO2dCQUNULE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxLQUFLO2dCQUNSLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sR0FBRyxDQUFDO1lBQ2IsS0FBSyxPQUFPO2dCQUNWLE9BQU8sR0FBRyxDQUFDO1lBQ2IsS0FBSyxZQUFZO2dCQUNmLE9BQU8sR0FBRyxDQUFDO1lBQ2IsS0FBSyxTQUFTO2dCQUNaLE9BQU8sR0FBRyxDQUFDO1lBQ2IsS0FBSyxZQUFZO2dCQUNmLE9BQU8sR0FBRyxDQUFDO1lBQ2IsS0FBSyxpQkFBaUI7Z0JBQ3BCLE9BQU8sR0FBRyxDQUFDO1lBQ2IsS0FBSyxjQUFjO2dCQUNqQixPQUFPLEdBQUcsQ0FBQztZQUNiLEtBQUssTUFBTTtnQkFDVCxPQUFPLEdBQUcsQ0FBQztTQUNkO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLEVBQUUsT0FBTztRQUNuQyxvQkFBb0I7UUFDcEIsSUFDRSxHQUFHLENBQUMsRUFBRSxLQUFLLEtBQUs7WUFDaEIsR0FBRyxDQUFDLEVBQUUsS0FBSyxTQUFTO1lBQ3BCLEdBQUcsQ0FBQyxFQUFFLEtBQUssUUFBUTtZQUNuQixHQUFHLENBQUMsRUFBRSxLQUFLLFVBQVUsRUFDckI7WUFDQSxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsRUFBRTtnQkFDdkMsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEdBQUcsQ0FBQyxFQUFFLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDN0MsT0FBTyxJQUFJLENBQUM7aUJBQ2I7YUFDRjtZQUNELE9BQU8sS0FBSyxDQUFDO1NBQ2Q7YUFBTTtZQUNMLE9BQU8sSUFBSSxDQUFDO1NBQ2I7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLGlCQUFpQixDQUFDLEdBQUc7UUFDMUIsb0JBQW9CO1FBQ3BCLElBQ0UsR0FBRyxDQUFDLEVBQUUsS0FBSyxLQUFLO1lBQ2hCLEdBQUcsQ0FBQyxFQUFFLEtBQUssU0FBUztZQUNwQixHQUFHLENBQUMsRUFBRSxLQUFLLFVBQVU7WUFDckIsR0FBRyxDQUFDLEVBQUUsS0FBSyxXQUFXO1lBQ3RCLEdBQUcsQ0FBQyxFQUFFLEtBQUssUUFBUSxFQUNuQjtZQUNBLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7YUFBTTtZQUNMLE9BQU8sSUFBSSxDQUFDO1NBQ2I7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLGtCQUFrQixDQUFDLEdBQUc7UUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNqQixJQUNFLEdBQUcsQ0FBQyxFQUFFLEtBQUssS0FBSztZQUNoQixHQUFHLENBQUMsRUFBRSxLQUFLLFFBQVE7WUFDbkIsR0FBRyxDQUFDLEVBQUUsS0FBSyxVQUFVO1lBQ3JCLEdBQUcsQ0FBQyxFQUFFLEtBQUssU0FBUyxFQUNwQjtZQUNBLEdBQUcsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUM7U0FDaEM7UUFFRCxpQkFBaUI7UUFDakIsNkJBQTZCO1FBQzdCLFdBQVc7UUFDWCxpQkFBaUI7UUFDakIsSUFBSTtJQUNOLENBQUM7SUFFRCxNQUFNLENBQUMsa0JBQWtCLENBQUMsR0FBRztRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLFdBQVc7UUFDWCxHQUFHLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDO1FBQy9CLElBQUk7UUFFSixpQkFBaUI7UUFDakIsNkJBQTZCO1FBQzdCLFdBQVc7UUFDWCxpQkFBaUI7UUFDakIsSUFBSTtJQUNOLENBQUM7SUFFRCxNQUFNLENBQUMsU0FBUyxDQUFDLFNBQVM7UUFDeEIsT0FBTyxDQUNMLFNBQVMsQ0FBQyxFQUFFLEtBQUssT0FBTztZQUN4QixTQUFTLENBQUMsRUFBRSxLQUFLLFNBQVM7WUFDMUIsU0FBUyxDQUFDLEVBQUUsS0FBSyxRQUFRO1lBQ3pCLFNBQVMsQ0FBQyxFQUFFLEtBQUssTUFBTSxDQUN4QixDQUFDO0lBQ0osQ0FBQztJQUVELE1BQU0sQ0FBQyxjQUFjLENBQUMsT0FBTztRQUMzQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsRUFBRTtZQUN2QyxJQUNFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEtBQUssQ0FBQztnQkFDeEIsQ0FBQyxDQUNDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssS0FBSztvQkFDdkIsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxRQUFRO29CQUMxQixPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLFNBQVM7b0JBQzNCLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssVUFBVTtvQkFDNUIsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxXQUFXLENBQzlCLEVBQ0Q7Z0JBQ0EsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxLQUFLLENBQUM7Z0JBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkJBQTZCLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQzNEO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDNUI7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxNQUFNO1FBQzFCLE9BQU8sTUFBTSxDQUFDLEVBQUUsS0FBSyxhQUFhLElBQUksTUFBTSxDQUFDLEVBQUUsS0FBSyxlQUFlLENBQUM7SUFDdEUsQ0FBQztJQUVELE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLO1FBQzlCLE1BQU0sR0FBRyxHQUNQLHNGQUFzRixDQUFDO1FBQ3pGLE1BQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDL0IsT0FBTyxLQUFLLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCxNQUFNLENBQUMsb0JBQW9CLENBQUMsS0FBSztRQUMvQixNQUFNLEdBQUcsR0FDUCwrRUFBK0UsQ0FBQztRQUNsRixNQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLE9BQU8sS0FBSyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRUQsTUFBTSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsT0FBTztRQUNwQyxNQUFNLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNwQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ3RDLE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDcEMscUNBQXFDO1lBQ3JDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNwQyxNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JCLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQzthQUN0RDtTQUNGO2FBQU07WUFDTCxNQUFNLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2pDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNwQyxNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JCLElBQUksT0FBTyxJQUFJLEtBQUssVUFBVSxFQUFFO29CQUM5QixPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7aUJBQ3REO2FBQ0Y7WUFDRCxNQUFNLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNuQjtRQUNELE9BQU8sTUFBTSxDQUFDO0lBQ2hCLENBQUM7SUFFRCxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUs7UUFDdkI7OztXQUdHO1FBQ0gsT0FBTyxLQUFLLENBQUMsS0FBSyxDQUFDLHlDQUF5QyxDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxNQUFNLENBQUMsYUFBYSxDQUFDLElBQUk7UUFDdkIsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFckMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsWUFBWSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQztTQUN6RDthQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNwQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLFlBQVksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUM7U0FDekQ7YUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDcEMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxZQUFZLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1NBQ3pEO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7U0FDbkI7UUFFRCxPQUFPLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxNQUFNLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLEVBQUU7UUFDaEMsTUFBTSxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBRW5CLElBQUk7WUFDRixJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3BELE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQy9CLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxFQUFFO29CQUNwQyxNQUFNLE1BQU0sR0FBRzt3QkFDYixFQUFFLEVBQUUsSUFBSTt3QkFDUixFQUFFO3dCQUNGLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO3dCQUNwQixHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDWixFQUFFLEVBQUUsRUFBRTt3QkFDTixFQUFFLEVBQUUsQ0FBQzt3QkFDTCxJQUFJLEVBQUUsSUFBSTt3QkFDVixHQUFHLEVBQUUsQ0FBQzt3QkFDTixFQUFFLEVBQUUsS0FBSzt3QkFDVCxJQUFJLEVBQUUsS0FBSzt3QkFDWCxHQUFHLEVBQUUsS0FBSzt3QkFDVixFQUFFLEVBQUUsS0FBSzt3QkFDVCxFQUFFLEVBQUUsa0JBQWtCO3dCQUN0QixFQUFFLEVBQUUsS0FBSzt3QkFDVCxNQUFNLEVBQUUsS0FBSzt3QkFDYixHQUFHLEVBQUUsSUFBSTt3QkFDVCxFQUFFLEVBQUUsRUFBRTt3QkFDTixHQUFHLEVBQUUsSUFBSTt3QkFDVCxHQUFHLEVBQUUsY0FBYzt3QkFDbkIsSUFBSSxFQUFFLElBQUk7d0JBQ1YsSUFBSSxFQUFFLENBQUM7d0JBQ1AsT0FBTyxFQUFFLENBQUM7cUJBQ1gsQ0FBQztvQkFFRixRQUFRLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUM1QixLQUFLLFFBQVE7NEJBQ1gsSUFBSSxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dDQUNuQyxJQUFJLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0NBQ3BELE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO3dDQUNwQixFQUFFLEVBQUUsV0FBVztxQ0FDaEIsQ0FBQyxDQUFDO2lDQUNKO3FDQUFNO29DQUNMLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO3dDQUNwQixFQUFFLEVBQUUsS0FBSzt3Q0FDVCxFQUFFLEVBQUUsRUFBRTt3Q0FDTixFQUFFLEVBQUUsQ0FBQztxQ0FDTixDQUFDLENBQUM7aUNBQ0o7NkJBQ0Y7aUNBQU07Z0NBQ0wsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0NBQ3BCLEVBQUUsRUFBRSxPQUFPO29DQUNYLEVBQUUsRUFBRSxFQUFFO29DQUNOLEVBQUUsRUFBRSxDQUFDO29DQUNMLElBQUksRUFBRSxJQUFJO29DQUNWLElBQUksRUFBRSxDQUFDO2lDQUNSLENBQUMsQ0FBQzs2QkFDSjs0QkFDRCxNQUFNO3dCQUNSLEtBQUssUUFBUTs0QkFDWCxJQUFJLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0NBQ3RDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO29DQUNwQixFQUFFLEVBQUUsVUFBVTtpQ0FDZixDQUFDLENBQUM7NkJBQ0o7aUNBQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTtnQ0FDdEMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0NBQ3BCLEVBQUUsRUFBRSxTQUFTO29DQUNiLEVBQUUsRUFBRSxHQUFHO29DQUNQLEVBQUUsRUFBRSxDQUFDO29DQUNMLElBQUksRUFBRSxLQUFLO2lDQUNaLENBQUMsQ0FBQzs2QkFDSjtpQ0FBTTtnQ0FDTCxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRTtvQ0FDcEIsRUFBRSxFQUFFLE1BQU07aUNBQ1gsQ0FBQyxDQUFDOzZCQUNKOzRCQUNELE1BQU07d0JBQ1IsS0FBSyxTQUFTOzRCQUNaLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO2dDQUNwQixFQUFFLEVBQUUsU0FBUztnQ0FDYixFQUFFLEVBQUUsQ0FBQztnQ0FDTCxFQUFFLEVBQUUsQ0FBQzs2QkFDTixDQUFDLENBQUM7NEJBQ0gsTUFBTTt3QkFDUixLQUFLLFFBQVE7NEJBQ1gsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7Z0NBQ3BCLEVBQUUsRUFBRSxNQUFNO2dDQUNWLEVBQUUsRUFBRSxDQUFDO2dDQUNMLEVBQUUsRUFBRSxDQUFDOzZCQUNOLENBQUMsQ0FBQzs0QkFDSCxNQUFNO3dCQUNSOzRCQUNFLE1BQU07cUJBQ1Q7b0JBQ0QsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDdEI7YUFDRjtTQUNGO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDVixPQUFPLENBQUMsR0FBRyxDQUFDLDZCQUE2QixFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQy9DO1FBRUQsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQztJQUVELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsS0FBSztRQUNoQyxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtZQUM3QixPQUFPLEtBQUssQ0FBQztTQUNkO1FBQ0QsT0FBTyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLElBQUksbUJBQW1CLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3hFLENBQUM7SUFFRCxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUs7UUFDdEIsT0FBTyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJO1FBQzNCLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBRztRQUNsQixRQUFRLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDakMsS0FBSyxTQUFTO2dCQUNaLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssU0FBUztnQkFDWixPQUFPLFVBQVUsQ0FBQztZQUNwQixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxTQUFTLENBQUM7WUFDbkIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBQ2hCLEtBQUssVUFBVTtnQkFDYixPQUFPLFVBQVUsQ0FBQztZQUNwQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFDaEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBQ2hCLEtBQUssUUFBUTtnQkFDWCxPQUFPLGdCQUFnQixDQUFDO1lBQzFCLEtBQUssTUFBTTtnQkFDVCxPQUFPLFVBQVUsQ0FBQztZQUNwQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxZQUFZLENBQUM7WUFDdEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sY0FBYyxDQUFDO1lBQ3hCLEtBQUssS0FBSztnQkFDUixPQUFPLGFBQWEsQ0FBQztZQUN2QixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxVQUFVLENBQUM7U0FDckI7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLGVBQWUsQ0FBQyxHQUFHO1FBQ3hCLFFBQVEsR0FBRyxDQUFDLEVBQUUsQ0FBQyxXQUFXLEVBQUUsRUFBRTtZQUM1QixLQUFLLEtBQUssQ0FBQztZQUNYLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxLQUFLO2dCQUNSLE9BQU8sU0FBUyxDQUFDO1lBRW5CLEtBQUssU0FBUztnQkFDWixPQUFPLFNBQVMsQ0FBQztZQUVuQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxPQUFPLENBQUM7WUFDakIsS0FBSyxTQUFTO2dCQUNaLElBQUksR0FBRyxDQUFDLElBQUksSUFBSSxHQUFHLEVBQUU7b0JBQ25CLE9BQU8sU0FBUyxDQUFDO2lCQUNsQjtxQkFBTTtvQkFDTCxPQUFPLFNBQVMsQ0FBQztpQkFDbEI7WUFDSCxLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFDaEIsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sVUFBVSxDQUFDO1lBQ3BCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFDaEIsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssT0FBTztnQkFDVixPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssVUFBVTtnQkFDYixPQUFPLE1BQU0sQ0FBQztZQUVoQix3QkFBd0I7WUFDeEIsS0FBSyxRQUFRO2dCQUNYLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssV0FBVztnQkFDZCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssVUFBVTtnQkFDYixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFDaEIsS0FBSyxLQUFLO2dCQUNSLE9BQU8sS0FBSyxDQUFDO1lBRWYsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLFlBQVksQ0FBQztZQUNsQixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssaUJBQWlCLENBQUM7WUFDdkIsS0FBSyxjQUFjO2dCQUNqQixPQUFPLFFBQVEsQ0FBQztZQUVsQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7U0FDakI7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLG9CQUFvQixDQUFDLEdBQXNCLEVBQUUsTUFBZTtRQUNqRSxNQUFNLE9BQU8sR0FBUSxFQUFFLENBQUM7UUFDeEIsUUFBUSxHQUFHLENBQUMsSUFBSSxFQUFFO1lBQ2hCLEtBQUssSUFBSTtnQkFDUDtvQkFDRSxNQUFNLFdBQVcsR0FBRyxNQUFNLEtBQUssSUFBSSxDQUFDO29CQUNwQyxNQUFNLFdBQVcsR0FBRyxNQUFNLEtBQUssSUFBSSxDQUFDO29CQUNwQyxPQUFPLENBQUMsRUFBRSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7b0JBQzdDLE9BQU8sQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDO29CQUNsQixPQUFPLENBQUMsRUFBRSxHQUFHLFdBQVcsQ0FBQztvQkFDekIsT0FBTyxDQUFDLEVBQUUsR0FBRyxXQUFXLENBQUM7b0JBQ3pCLE9BQU8sQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDO29CQUNuQixPQUFPLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztpQkFDdkQ7Z0JBQ0QsTUFBTTtZQUNSLEtBQUssWUFBWTtnQkFDZixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsTUFBTTtZQUNSLEtBQUssZ0JBQWdCO2dCQUNuQixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsTUFBTTtZQUNSLEtBQUssVUFBVTtnQkFDYixPQUFPLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQztnQkFDcEIsTUFBTTtZQUNSLEtBQUssWUFBWTtnQkFDZixPQUFPLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQztnQkFDcEIsTUFBTTtZQUNSLEtBQUssVUFBVTtnQkFDYixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7Z0JBQ2pCLE1BQU07WUFDUixLQUFLLGFBQWE7Z0JBQ2hCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsS0FBSyxDQUFDO2dCQUNuQixNQUFNO1lBQ1IsS0FBSyxjQUFjO2dCQUNqQixPQUFPLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQztnQkFDcEIsTUFBTTtZQUNSLEtBQUssY0FBYztnQkFDakIsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBRXZCLE1BQU07WUFDUixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUixLQUFLLGFBQWE7Z0JBQ2hCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixPQUFPLENBQUMsUUFBUSxHQUFHO29CQUNqQixJQUFJLEVBQUUsQ0FBQyxlQUFlLENBQUM7b0JBQ3ZCLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQztvQkFDVixHQUFHLEVBQUUsQ0FBQywwQ0FBMEMsQ0FBQztpQkFDbEQsQ0FBQztnQkFDRixNQUFNO1lBQ1IsS0FBSyxPQUFPO2dCQUNWLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixPQUFPLENBQUMsUUFBUSxHQUFHO29CQUNqQixJQUFJLEVBQUUsQ0FBQyxTQUFTLENBQUM7b0JBQ2pCLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQztvQkFDVixHQUFHLEVBQUUsQ0FBQyxvQ0FBb0MsQ0FBQztpQkFDNUMsQ0FBQztnQkFDRixNQUFNO1lBQ1IsS0FBSyxLQUFLO2dCQUNSLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixPQUFPLENBQUMsUUFBUSxHQUFHO29CQUNqQixJQUFJLEVBQUUsQ0FBQyxPQUFPLENBQUM7b0JBQ2YsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDO29CQUNWLEdBQUcsRUFBRSxDQUFDLGtDQUFrQyxDQUFDO2lCQUMxQyxDQUFDO2dCQUNGLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxRQUFRLENBQUM7Z0JBQ3RCLE1BQU07WUFDUixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE9BQU8sQ0FBQyxRQUFRLEdBQUc7b0JBQ2pCLElBQUksRUFBRSxDQUFDLFlBQVksQ0FBQztvQkFDcEIsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDO29CQUNWLEdBQUcsRUFBRSxDQUFDLGdDQUFnQyxDQUFDO2lCQUN4QyxDQUFDO2dCQUNGLE1BQU07WUFDUixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxDQUFDLEVBQUUsR0FBRyxRQUFRLENBQUM7Z0JBQ3RCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxLQUFLLENBQUM7Z0JBQ25CLE1BQU07WUFDUixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxDQUFDLEVBQUUsR0FBRyxLQUFLLENBQUM7Z0JBQ25CLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxDQUFDLEVBQUUsR0FBRyxVQUFVLENBQUM7Z0JBQ3hCLE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxDQUFDLEVBQUUsR0FBRyxVQUFVLENBQUM7Z0JBQ3hCLE1BQU07WUFDUixLQUFLLGtCQUFrQjtnQkFDckIsT0FBTyxDQUFDLEVBQUUsR0FBRyxVQUFVLENBQUM7Z0JBQ3hCLE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxDQUFDLEVBQUUsR0FBRyxLQUFLLENBQUM7Z0JBQ25CLE1BQU07WUFDUixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUjtnQkFDRSxPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsTUFBTTtTQUNUO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQztJQUVELE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQyxHQUFHLEVBQUUsTUFBZTtRQUNsRCxRQUFRLEdBQUcsQ0FBQyxJQUFJLEVBQUU7WUFDaEIsS0FBSyxJQUFJO2dCQUNQLElBQUksTUFBTSxLQUFLLElBQUksRUFBRTtvQkFDbkIsT0FBTyxDQUFDLFNBQVMsRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7aUJBQ3JDO3FCQUFNLElBQUksTUFBTSxLQUFLLElBQUksRUFBRTtvQkFDMUIsT0FBTyxDQUFDLEtBQUssRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUM7aUJBQ3BFO3FCQUFNO29CQUNMLE9BQU8sT0FBTyxDQUFDO2lCQUNoQjtZQUNILEtBQUssWUFBWTtnQkFDZixPQUFPLE9BQU8sQ0FBQztZQUVqQixLQUFLLGdCQUFnQixDQUFDO1lBQ3RCLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssY0FBYztnQkFDakIsT0FBTztvQkFDTCxNQUFNO29CQUNOLFNBQVM7b0JBQ1QsT0FBTztvQkFDUCxNQUFNO29CQUNOLFVBQVU7b0JBQ1YsWUFBWTtvQkFDWixVQUFVO2lCQUNYLENBQUM7WUFFSixLQUFLLFlBQVk7Z0JBQ2YsT0FBTztvQkFDTCxNQUFNO29CQUNOLE1BQU07b0JBQ04sU0FBUztvQkFDVCxPQUFPO29CQUNQLE1BQU07b0JBQ04sVUFBVTtvQkFDVixZQUFZO29CQUNaLFVBQVU7aUJBQ1gsQ0FBQztZQUVKLEtBQUssTUFBTTtnQkFDVCxPQUFPLENBQUMsTUFBTSxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBRWhFLEtBQUssVUFBVTtnQkFDYixPQUFPO29CQUNMLEtBQUs7b0JBQ0wsVUFBVTtvQkFDVixXQUFXO29CQUNYLFFBQVE7b0JBQ1IsS0FBSztvQkFDTCxTQUFTO29CQUNULFFBQVE7b0JBQ1IsU0FBUztpQkFDVixDQUFDO1lBRUosS0FBSyxhQUFhO2dCQUNoQixPQUFPLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBRS9ELEtBQUssY0FBYztnQkFDakIsT0FBTyxDQUFDLE1BQU0sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxVQUFVLENBQUMsQ0FBQztZQUVoRSxLQUFLLE1BQU07Z0JBQ1QsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBRWxCLEtBQUssTUFBTTtnQkFDVCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFbEIsS0FBSyxhQUFhLENBQUM7WUFDbkIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUVyQixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxDQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLENBQUMsQ0FBQztZQUVuRSxLQUFLLFFBQVE7Z0JBQ1gsT0FBTztvQkFDTCxLQUFLO29CQUNMLFVBQVU7b0JBQ1YsV0FBVztvQkFDWCxRQUFRO29CQUNSLEtBQUs7b0JBQ0wsT0FBTztvQkFDUCxTQUFTO29CQUNULFFBQVE7b0JBQ1IsUUFBUTtpQkFDVCxDQUFDO1lBRUosS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUVsRCxLQUFLLFVBQVU7Z0JBQ2IsT0FBTztvQkFDTCxTQUFTO29CQUNULE9BQU87b0JBQ1AsUUFBUTtvQkFDUixRQUFRO29CQUNSLEtBQUs7b0JBQ0wsVUFBVTtvQkFDVixXQUFXO29CQUNYLFFBQVE7b0JBQ1IsS0FBSztpQkFDTixDQUFDO1lBRUosS0FBSyxTQUFTO2dCQUNaLE9BQU87b0JBQ0wsU0FBUztvQkFDVCxPQUFPO29CQUNQLFFBQVE7b0JBQ1IsUUFBUTtvQkFDUixLQUFLO29CQUNMLFVBQVU7b0JBQ1YsV0FBVztvQkFDWCxRQUFRO29CQUNSLEtBQUs7aUJBQ04sQ0FBQztZQUVKLEtBQUssVUFBVTtnQkFDYixPQUFPO29CQUNMLFNBQVM7b0JBQ1QsT0FBTztvQkFDUCxRQUFRO29CQUNSLFFBQVE7b0JBQ1IsS0FBSztvQkFDTCxVQUFVO29CQUNWLFdBQVc7b0JBQ1gsUUFBUTtvQkFDUixLQUFLO2lCQUNOLENBQUM7WUFFSixLQUFLLFFBQVE7Z0JBQ1gsT0FBTztvQkFDTCxTQUFTO29CQUNULE9BQU87b0JBQ1AsUUFBUTtvQkFDUixRQUFRO29CQUNSLEtBQUs7b0JBQ0wsVUFBVTtvQkFDVixXQUFXO29CQUNYLFFBQVE7b0JBQ1IsS0FBSztpQkFDTixDQUFDO1lBRUosS0FBSyxTQUFTO2dCQUNaLE9BQU87b0JBQ0wsTUFBTTtvQkFDTixTQUFTO29CQUNULE9BQU87b0JBQ1AsTUFBTTtvQkFDTixVQUFVO29CQUNWLFlBQVk7b0JBQ1osVUFBVTtpQkFDWCxDQUFDO1lBRUosS0FBSyxRQUFRO2dCQUNYLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUVyQixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxDQUFDLEtBQUssRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUU5RCxLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBRXJCLEtBQUssTUFBTTtnQkFDVCxPQUFPLENBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFFdEQsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxZQUFZLENBQUM7WUFDbEIsS0FBSyxrQkFBa0I7Z0JBQ3JCLE9BQU8sQ0FBQyxVQUFVLEVBQUUsV0FBVyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBRTlDLEtBQUssWUFBWTtnQkFDZixPQUFPLENBQUMsS0FBSyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFFcEQsS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUVyQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTztvQkFDTCxVQUFVO29CQUNWLE9BQU87b0JBQ1AsWUFBWTtvQkFDWixTQUFTO29CQUNULFlBQVk7b0JBQ1osaUJBQWlCO29CQUNqQixjQUFjO2lCQUNmLENBQUM7WUFFSixLQUFLLFFBQVEsQ0FBQztZQUNkO2dCQUNFLE9BQU8sT0FBTyxDQUFDO1NBQ2xCO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxvQkFBb0I7UUFDekIsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDO0NBQ0Y7QUFodENELDBCQWd0Q0M7QUFFRCxnQ0FBZ0M7QUFDaEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQXFCRyJ9