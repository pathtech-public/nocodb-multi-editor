"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SqliteUi = void 0;
const UITypes_1 = __importDefault(require("../UITypes"));
const dbTypes = [
    'int',
    'integer',
    'tinyint',
    'smallint',
    'mediumint',
    'bigint',
    'int2',
    'int8',
    'character',
    'blob sub_type text',
    'blob',
    'real',
    'double',
    'double precision',
    'float',
    'numeric',
    'boolean',
    'date',
    'datetime',
    'text',
    'varchar',
    'timestamp',
];
class SqliteUi {
    static getNewTableColumns() {
        return [
            {
                column_name: 'id',
                title: 'Id',
                dt: 'integer',
                dtx: 'integer',
                ct: 'int(11)',
                nrqd: false,
                rqd: true,
                ck: false,
                pk: true,
                un: false,
                ai: true,
                cdf: null,
                clen: null,
                np: null,
                ns: 0,
                dtxp: '',
                dtxs: '',
                altered: 1,
                uidt: 'ID',
                uip: '',
                uicn: '',
            },
            {
                column_name: 'title',
                title: 'Title',
                dt: 'varchar',
                dtx: 'specificType',
                ct: 'varchar',
                nrqd: true,
                rqd: false,
                ck: false,
                pk: false,
                un: false,
                ai: false,
                cdf: null,
                clen: 45,
                np: null,
                ns: null,
                dtxp: '',
                dtxs: '',
                altered: 1,
                uidt: 'SingleLineText',
                uip: '',
                uicn: '',
            },
            {
                column_name: 'created_at',
                title: 'CreatedAt',
                dt: 'datetime',
                dtx: 'specificType',
                ct: 'varchar',
                nrqd: true,
                rqd: false,
                ck: false,
                pk: false,
                un: false,
                ai: false,
                cdf: 'CURRENT_TIMESTAMP',
                clen: 45,
                np: null,
                ns: null,
                dtxp: '',
                dtxs: '',
                altered: 1,
                uidt: UITypes_1.default.DateTime,
                uip: '',
                uicn: '',
            },
            {
                column_name: 'updated_at',
                title: 'UpdatedAt',
                dt: 'datetime',
                dtx: 'specificType',
                ct: 'varchar',
                nrqd: true,
                rqd: false,
                ck: false,
                pk: false,
                un: false,
                ai: false,
                cdf: 'CURRENT_TIMESTAMP',
                clen: 45,
                np: null,
                ns: null,
                dtxp: '',
                dtxs: '',
                altered: 1,
                uidt: UITypes_1.default.DateTime,
                uip: '',
                uicn: '',
            },
        ];
    }
    static getNewColumn(suffix) {
        return {
            column_name: 'title' + suffix,
            dt: 'varchar',
            dtx: 'specificType',
            ct: 'varchar',
            nrqd: true,
            rqd: false,
            ck: false,
            pk: false,
            un: false,
            ai: false,
            cdf: null,
            clen: 45,
            np: null,
            ns: null,
            dtxp: '',
            dtxs: '',
            altered: 1,
            uidt: 'SingleLineText',
            uip: '',
            uicn: '',
        };
    }
    // static getDefaultLengthForDatatype(type) {
    //   switch (type) {
    //     case "int":
    //       return 11;
    //       break;
    //     case "tinyint":
    //       return 1;
    //       break;
    //     case "smallint":
    //       return 5;
    //       break;
    //
    //     case "mediumint":
    //       return 9;
    //       break;
    //     case "bigint":
    //       return 20;
    //       break;
    //     case "bit":
    //       return 64;
    //       break;
    //     case "boolean":
    //       return '';
    //       break;
    //     case "float":
    //       return 12;
    //       break;
    //     case "decimal":
    //       return 10;
    //       break;
    //     case "double":
    //       return 22;
    //       break;
    //     case "serial":
    //       return 20;
    //       break;
    //     case "date":
    //       return '';
    //       break;
    //     case "datetime":
    //     case "timestamp":
    //       return 6;
    //       break;
    //     case "time":
    //       return '';
    //       break;
    //     case "year":
    //       return '';
    //       break;
    //     case "char":
    //       return 255;
    //       break;
    //     case "varchar":
    //       return 45;
    //       break;
    //     case "nchar":
    //       return 255;
    //       break;
    //     case "text":
    //       return '';
    //       break;
    //     case "tinytext":
    //       return '';
    //       break;
    //     case "mediumtext":
    //       return '';
    //       break;
    //     case "longtext":
    //       return ''
    //       break;
    //     case "binary":
    //       return 255;
    //       break;
    //     case "varbinary":
    //       return 65500;
    //       break;
    //     case "blob":
    //       return '';
    //       break;
    //     case "tinyblob":
    //       return '';
    //       break;
    //     case "mediumblob":
    //       return '';
    //       break;
    //     case "longblob":
    //       return '';
    //       break;
    //     case "enum":
    //       return '\'a\',\'b\'';
    //       break;
    //     case "set":
    //       return '\'a\',\'b\'';
    //       break;
    //     case "geometry":
    //       return '';
    //     case "point":
    //       return '';
    //     case "linestring":
    //       return '';
    //     case "polygon":
    //       return '';
    //     case "multipoint":
    //       return '';
    //     case "multilinestring":
    //       return '';
    //     case "multipolygon":
    //       return '';
    //     case "json":
    //       return ''
    //       break;
    //
    //   }
    //
    // }
    static getDefaultLengthForDatatype(type) {
        switch (type) {
            case 'int':
                return '';
            case 'tinyint':
                return '';
            case 'smallint':
                return '';
            case 'mediumint':
                return '';
            case 'bigint':
                return '';
            case 'bit':
                return '';
            case 'boolean':
                return '';
            case 'float':
                return '';
            case 'decimal':
                return '';
            case 'double':
                return '';
            case 'serial':
                return '';
            case 'date':
                return '';
            case 'datetime':
            case 'timestamp':
                return '';
            case 'time':
                return '';
            case 'year':
                return '';
            case 'char':
                return '';
            case 'varchar':
                return '';
            case 'nchar':
                return '';
            case 'text':
                return '';
            case 'tinytext':
                return '';
            case 'mediumtext':
                return '';
            case 'longtext':
                return '';
            case 'binary':
                return '';
            case 'varbinary':
                return '';
            case 'blob':
                return '';
            case 'tinyblob':
                return '';
            case 'mediumblob':
                return '';
            case 'longblob':
                return '';
            case 'enum':
                return '';
            case 'set':
                return '';
            case 'geometry':
                return '';
            case 'point':
                return '';
            case 'linestring':
                return '';
            case 'polygon':
                return '';
            case 'multipoint':
                return '';
            case 'multilinestring':
                return '';
            case 'multipolygon':
                return '';
            case 'json':
                return '';
        }
        return '';
    }
    static getDefaultLengthIsDisabled(type) {
        switch (type) {
            case 'integer':
            case 'blob':
            case 'real':
            case 'numeric':
                return true;
            case 'text':
                return false;
        }
    }
    static getDefaultValueForDatatype(type) {
        switch (type) {
            case 'integer':
                return 'eg : ' + 10;
            case 'text':
                return 'eg : hey';
            case 'numeric':
                return 'eg : ' + 10;
            case 'real':
                return 'eg : ' + 10.0;
            case 'blob':
                return 'eg : ' + 100;
        }
    }
    static getDefaultScaleForDatatype(type) {
        switch (type) {
            case 'integer':
                return ' ';
            case 'text':
                return ' ';
            case 'numeric':
                return ' ';
            case 'real':
                return ' ';
            case 'blob':
                return ' ';
        }
    }
    static colPropAIDisabled(col, columns) {
        // console.log(col);
        if (col.dt === 'integer') {
            for (let i = 0; i < columns.length; ++i) {
                if (columns[i].cn !== col.cn && columns[i].ai) {
                    return true;
                }
            }
            return false;
        }
        else {
            return true;
        }
    }
    static colPropUNDisabled(_col) {
        // console.log(col);
        return true;
        // if (col.dt === 'int' ||
        //   col.dt === 'tinyint' ||
        //   col.dt === 'smallint' ||
        //   col.dt === 'mediumint' ||
        //   col.dt === 'bigint') {
        //   return false;
        // } else {
        //   return true;
        // }
    }
    static onCheckboxChangeAI(col) {
        console.log(col);
        if (col.dt === 'int' ||
            col.dt === 'bigint' ||
            col.dt === 'smallint' ||
            col.dt === 'tinyint') {
            col.altered = col.altered || 2;
        }
        // if (!col.ai) {
        //   col.dtx = 'specificType'
        // } else {
        //   col.dtx = ''
        // }
    }
    static showScale(_columnObj) {
        return false;
    }
    static removeUnsigned(columns) {
        for (let i = 0; i < columns.length; ++i) {
            if (columns[i].altered === 1 &&
                !(columns[i].dt === 'int' ||
                    columns[i].dt === 'bigint' ||
                    columns[i].dt === 'tinyint' ||
                    columns[i].dt === 'smallint' ||
                    columns[i].dt === 'mediumint')) {
                columns[i].un = false;
                console.log('>> resetting unsigned value', columns[i].cn);
            }
            console.log(columns[i].cn);
        }
    }
    static extractFunctionName(query) {
        const reg = /^\s*CREATE\s+(?:OR\s+REPLACE\s*)?\s*FUNCTION\s+(?:[\w\d_]+\.)?([\w_\d]+)/i;
        const match = query.match(reg);
        return match && match[1];
    }
    static extractProcedureName(query) {
        const reg = /^\s*CREATE\s+(?:OR\s+REPLACE\s*)?\s*PROCEDURE\s+(?:[\w\d_]+\.)?([\w_\d]+)/i;
        const match = query.match(reg);
        return match && match[1];
    }
    static columnEditable(_colObj) {
        return true; // colObj.altered === 1;
    }
    static handleRawOutput(result, headers) {
        console.log(result);
        if (Array.isArray(result) && result[0]) {
            const keys = Object.keys(result[0]);
            // set headers before settings result
            for (let i = 0; i < keys.length; i++) {
                const text = keys[i];
                headers.push({ text, value: text, sortable: false });
            }
        }
        return result;
    }
    static splitQueries(query) {
        /***
         * we are splitting based on semicolon
         * there are mechanism to escape semicolon within single/double quotes(string)
         */
        return query.match(/\b("[^"]*;[^"]*"|'[^']*;[^']*'|[^;])*;/g);
    }
    /**
     * if sql statement is SELECT - it limits to a number
     * @param args
     * @returns {string|*}
     */
    sanitiseQuery(args) {
        let q = args.query.trim().split(';');
        if (q[0].startsWith('Select')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else if (q[0].startsWith('select')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else if (q[0].startsWith('SELECT')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else {
            return args.query;
        }
        return q;
    }
    static getColumnsFromJson(json, tn) {
        const columns = [];
        try {
            if (typeof json === 'object' && !Array.isArray(json)) {
                const keys = Object.keys(json);
                for (let i = 0; i < keys.length; ++i) {
                    const column = {
                        dp: null,
                        tn,
                        column_name: keys[i],
                        cno: keys[i],
                        np: null,
                        ns: null,
                        clen: null,
                        cop: 1,
                        pk: false,
                        nrqd: false,
                        rqd: false,
                        un: false,
                        ct: 'int(11) unsigned',
                        ai: false,
                        unique: false,
                        cdf: null,
                        cc: '',
                        csn: null,
                        dtx: 'specificType',
                        dtxp: null,
                        dtxs: 0,
                        altered: 1,
                    };
                    switch (typeof json[keys[i]]) {
                        case 'number':
                            if (Number.isInteger(json[keys[i]])) {
                                if (SqliteUi.isValidTimestamp(keys[i], json[keys[i]])) {
                                    Object.assign(column, {
                                        dt: 'timestamp',
                                    });
                                }
                                else {
                                    Object.assign(column, {
                                        dt: 'integer',
                                    });
                                }
                            }
                            else {
                                Object.assign(column, {
                                    dt: 'real',
                                });
                            }
                            break;
                        case 'string':
                            // if (SqliteUi.isValidDate(json[keys[i]])) {
                            //   Object.assign(column, {
                            //     "dt": "datetime"
                            //   });
                            // } else
                            if (json[keys[i]].length <= 255) {
                                Object.assign(column, {
                                    dt: 'varchar',
                                });
                            }
                            else {
                                Object.assign(column, {
                                    dt: 'text',
                                });
                            }
                            break;
                        case 'boolean':
                            Object.assign(column, {
                                dt: 'integer',
                            });
                            break;
                        case 'object':
                            Object.assign(column, {
                                dt: 'text',
                                np: null,
                                dtxp: null,
                            });
                            break;
                        default:
                            break;
                    }
                    columns.push(column);
                }
            }
        }
        catch (e) {
            console.log('Error in getColumnsFromJson', e);
        }
        return columns;
    }
    static isValidTimestamp(key, value) {
        if (typeof value !== 'number') {
            return false;
        }
        return new Date(value).getTime() > 0 && /(?:_|(?=A))[aA]t$/.test(key);
    }
    static isValidDate(value) {
        return new Date(value).getTime() > 0;
    }
    static onCheckboxChangeAU(col) {
        console.log(col);
        // if (1) {
        col.altered = col.altered || 2;
        // }
        // if (!col.ai) {
        //   col.dtx = 'specificType'
        // } else {
        //   col.dtx = ''
        // }
    }
    static colPropAuDisabled(col) {
        if (col.altered !== 1) {
            return true;
        }
        switch (col.dt) {
            case 'date':
            case 'datetime':
            case 'timestamp':
            case 'time':
                return false;
            default:
                return true;
        }
    }
    static getAbstractType(col) {
        switch ((col.dt || col.dt).toLowerCase()) {
            case 'date':
                return 'date';
            case 'datetime':
            case 'timestamp':
                return 'datetime';
            case 'integer':
            case 'int':
            case 'tinyint':
            case 'smallint':
            case 'mediumint':
            case 'bigint':
            case 'int2':
            case 'int8':
                return 'integer';
            case 'text':
                return 'text';
            case 'boolean':
                return 'boolean';
            case 'real':
            case 'double':
            case 'double precision':
            case 'float':
            case 'decimal':
            case 'numeric':
                return 'float';
            case 'blob sub_type text':
            case 'blob':
                return 'blob';
            case 'character':
            case 'varchar':
                return 'string';
        }
        return 'string';
    }
    static getUIType(col) {
        switch (this.getAbstractType(col)) {
            case 'integer':
                return 'Number';
            case 'boolean':
                return 'Checkbox';
            case 'float':
                return 'Decimal';
            case 'date':
                return 'Date';
            case 'datetime':
                return 'CreateTime';
            case 'time':
                return 'Time';
            case 'year':
                return 'Year';
            case 'string':
                return 'SingleLineText';
            case 'text':
                return 'LongText';
            case 'blob':
                return 'Attachment';
            case 'enum':
                return 'SingleSelect';
            case 'set':
                return 'MultiSelect';
            case 'json':
                return 'LongText';
        }
    }
    static getDataTypeForUiType(col, idType) {
        const colProp = {};
        switch (col.uidt) {
            case 'ID':
                {
                    const isAutoIncId = idType === 'AI';
                    const isAutoGenId = idType === 'AG';
                    colProp.dt = isAutoGenId ? 'varchar' : 'integer';
                    colProp.pk = true;
                    colProp.un = isAutoIncId;
                    colProp.ai = isAutoIncId;
                    colProp.rqd = true;
                    colProp.meta = isAutoGenId ? { ag: 'nc' } : undefined;
                }
                break;
            case 'ForeignKey':
                colProp.dt = 'varchar';
                break;
            case 'SingleLineText':
                colProp.dt = 'varchar';
                break;
            case 'LongText':
                colProp.dt = 'text';
                break;
            case 'Attachment':
                colProp.dt = 'text';
                break;
            case 'Checkbox':
                // colProp.dt = 'tinyint';
                // colProp.dtxp = 1;
                colProp.dt = 'boolean';
                break;
            case 'MultiSelect':
                colProp.dt = 'text';
                break;
            case 'SingleSelect':
                colProp.dt = 'text';
                break;
            case 'Collaborator':
                colProp.dt = 'varchar';
                break;
            case 'Date':
                colProp.dt = 'date';
                break;
            case 'Year':
                colProp.dt = 'year';
                break;
            case 'Time':
                colProp.dt = 'time';
                break;
            case 'PhoneNumber':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isMobilePhone'],
                    args: [''],
                    msg: ['Validation failed : isMobilePhone'],
                };
                break;
            case 'Email':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isEmail'],
                    args: [''],
                    msg: ['Validation failed : isEmail'],
                };
                break;
            case 'URL':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isURL'],
                    args: [''],
                    msg: ['Validation failed : isURL'],
                };
                break;
            case 'Number':
                colProp.dt = 'integer';
                break;
            case 'Decimal':
                colProp.dt = 'decimal';
                break;
            case 'Currency':
                colProp.dt = 'double precision';
                colProp.validate = {
                    func: ['isCurrency'],
                    args: [''],
                    msg: ['Validation failed : isCurrency'],
                };
                break;
            case 'Percent':
                colProp.dt = 'double';
                break;
            case 'Duration':
                colProp.dt = 'decimal';
                break;
            case 'Rating':
                colProp.dt = 'integer';
                break;
            case 'Formula':
                colProp.dt = 'varchar';
                break;
            case 'Rollup':
                colProp.dt = 'varchar';
                break;
            case 'Count':
                colProp.dt = 'integer';
                break;
            case 'Lookup':
                colProp.dt = 'varchar';
                break;
            case 'DateTime':
                colProp.dt = 'datetime';
                break;
            case 'CreateTime':
                colProp.dt = 'datetime';
                break;
            case 'LastModifiedTime':
                colProp.dt = 'datetime';
                break;
            case 'AutoNumber':
                colProp.dt = 'integer';
                break;
            case 'Barcode':
                colProp.dt = 'varchar';
                break;
            case 'Button':
                colProp.dt = 'varchar';
                break;
            case 'JSON':
                colProp.dt = 'text';
                break;
            default:
                colProp.dt = 'varchar';
                break;
        }
        return colProp;
    }
    static getDataTypeListForUiType(col, idType) {
        switch (col.uidt) {
            case 'ID':
                if (idType === 'AG') {
                    return ['character', 'text', 'varchar'];
                }
                else if (idType === 'AI') {
                    return [
                        'int',
                        'integer',
                        'tinyint',
                        'smallint',
                        'mediumint',
                        'bigint',
                        'int2',
                        'int8',
                    ];
                }
                else {
                    return dbTypes;
                }
            case 'ForeignKey':
                return dbTypes;
            case 'SingleLineText':
            case 'LongText':
            case 'Attachment':
            case 'Collaborator':
                return ['character', 'text', 'varchar'];
            case 'Checkbox':
                return [
                    'int',
                    'integer',
                    'tinyint',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'int2',
                    'int8',
                    'boolean',
                ];
            case 'MultiSelect':
                return ['text', 'varchar'];
            case 'SingleSelect':
                return ['text', 'varchar'];
            case 'Year':
                return [
                    'int',
                    'integer',
                    'tinyint',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'int2',
                    'int8',
                ];
            case 'Time':
                return [
                    'int',
                    'integer',
                    'tinyint',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'int2',
                    'int8',
                ];
            case 'PhoneNumber':
            case 'Email':
                return ['varchar', 'text'];
            case 'URL':
                return ['varchar', 'text'];
            case 'Number':
                return [
                    'int',
                    'integer',
                    'tinyint',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'int2',
                    'int8',
                    'numeric',
                    'real',
                    'double',
                    'double precision',
                    'float',
                ];
            case 'Decimal':
                return ['real', 'double', 'double precision', 'float', 'numeric'];
            case 'Currency':
                return [
                    'real',
                    'double',
                    'double precision',
                    'float',
                    'int',
                    'integer',
                    'tinyint',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'int2',
                    'int8',
                    'numeric',
                ];
            case 'Percent':
                return [
                    'real',
                    'double',
                    'double precision',
                    'float',
                    'int',
                    'integer',
                    'tinyint',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'int2',
                    'int8',
                    'numeric',
                ];
            case 'Duration':
                return [
                    'int',
                    'integer',
                    'tinyint',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'int2',
                    'int8',
                ];
            case 'Rating':
                return [
                    'int',
                    'integer',
                    'tinyint',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'int2',
                    'int8',
                    'numeric',
                    'real',
                    'double',
                    'double precision',
                    'float',
                ];
            case 'Formula':
                return ['text', 'varchar'];
            case 'Rollup':
                return ['varchar'];
            case 'Count':
                return [
                    'int',
                    'integer',
                    'tinyint',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'int2',
                    'int8',
                ];
            case 'Lookup':
                return ['varchar'];
            case 'Date':
                return ['date', 'varchar'];
            case 'DateTime':
            case 'CreateTime':
            case 'LastModifiedTime':
                return ['datetime', 'timestamp'];
            case 'AutoNumber':
                return [
                    'int',
                    'integer',
                    'tinyint',
                    'smallint',
                    'mediumint',
                    'bigint',
                    'int2',
                    'int8',
                ];
            case 'Barcode':
                return ['varchar'];
            case 'Geometry':
                return ['text'];
            case 'JSON':
                return ['text'];
            case 'Button':
            default:
                return dbTypes;
        }
    }
    static getUnsupportedFnList() {
        return ['LOG', 'EXP', 'POWER', 'SQRT'];
    }
}
exports.SqliteUi = SqliteUi;
// module.exports = PgUiHelp;
/**
 * @copyright Copyright (c) 2021, Xgene Cloud Ltd
 *
 * @author Naveen MR <oof1lab@gmail.com>
 * @author Pranav C Balan <pranavxc@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3FsaXRlVWkuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvbGliL3NxbFVpL1NxbGl0ZVVpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLHlEQUFpQztBQUdqQyxNQUFNLE9BQU8sR0FBRztJQUNkLEtBQUs7SUFDTCxTQUFTO0lBQ1QsU0FBUztJQUNULFVBQVU7SUFDVixXQUFXO0lBQ1gsUUFBUTtJQUNSLE1BQU07SUFDTixNQUFNO0lBQ04sV0FBVztJQUNYLG9CQUFvQjtJQUNwQixNQUFNO0lBQ04sTUFBTTtJQUNOLFFBQVE7SUFDUixrQkFBa0I7SUFDbEIsT0FBTztJQUNQLFNBQVM7SUFDVCxTQUFTO0lBQ1QsTUFBTTtJQUNOLFVBQVU7SUFDVixNQUFNO0lBQ04sU0FBUztJQUNULFdBQVc7Q0FDWixDQUFDO0FBRUYsTUFBYSxRQUFRO0lBQ25CLE1BQU0sQ0FBQyxrQkFBa0I7UUFDdkIsT0FBTztZQUNMO2dCQUNFLFdBQVcsRUFBRSxJQUFJO2dCQUNqQixLQUFLLEVBQUUsSUFBSTtnQkFDWCxFQUFFLEVBQUUsU0FBUztnQkFDYixHQUFHLEVBQUUsU0FBUztnQkFDZCxFQUFFLEVBQUUsU0FBUztnQkFDYixJQUFJLEVBQUUsS0FBSztnQkFDWCxHQUFHLEVBQUUsSUFBSTtnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsSUFBSTtnQkFDUixFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsSUFBSTtnQkFDUixHQUFHLEVBQUUsSUFBSTtnQkFDVCxJQUFJLEVBQUUsSUFBSTtnQkFDVixFQUFFLEVBQUUsSUFBSTtnQkFDUixFQUFFLEVBQUUsQ0FBQztnQkFDTCxJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixPQUFPLEVBQUUsQ0FBQztnQkFDVixJQUFJLEVBQUUsSUFBSTtnQkFDVixHQUFHLEVBQUUsRUFBRTtnQkFDUCxJQUFJLEVBQUUsRUFBRTthQUNUO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLE9BQU87Z0JBQ3BCLEtBQUssRUFBRSxPQUFPO2dCQUNkLEVBQUUsRUFBRSxTQUFTO2dCQUNiLEdBQUcsRUFBRSxjQUFjO2dCQUNuQixFQUFFLEVBQUUsU0FBUztnQkFDYixJQUFJLEVBQUUsSUFBSTtnQkFDVixHQUFHLEVBQUUsS0FBSztnQkFDVixFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxHQUFHLEVBQUUsSUFBSTtnQkFDVCxJQUFJLEVBQUUsRUFBRTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixPQUFPLEVBQUUsQ0FBQztnQkFDVixJQUFJLEVBQUUsZ0JBQWdCO2dCQUN0QixHQUFHLEVBQUUsRUFBRTtnQkFDUCxJQUFJLEVBQUUsRUFBRTthQUNUO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLFlBQVk7Z0JBQ3pCLEtBQUssRUFBRSxXQUFXO2dCQUNsQixFQUFFLEVBQUUsVUFBVTtnQkFDZCxHQUFHLEVBQUUsY0FBYztnQkFDbkIsRUFBRSxFQUFFLFNBQVM7Z0JBQ2IsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsR0FBRyxFQUFFLEtBQUs7Z0JBQ1YsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsR0FBRyxFQUFFLG1CQUFtQjtnQkFDeEIsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsRUFBRSxFQUFFLElBQUk7Z0JBQ1IsRUFBRSxFQUFFLElBQUk7Z0JBQ1IsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsT0FBTyxFQUFFLENBQUM7Z0JBQ1YsSUFBSSxFQUFFLGlCQUFPLENBQUMsUUFBUTtnQkFDdEIsR0FBRyxFQUFFLEVBQUU7Z0JBQ1AsSUFBSSxFQUFFLEVBQUU7YUFDVDtZQUNEO2dCQUNFLFdBQVcsRUFBRSxZQUFZO2dCQUN6QixLQUFLLEVBQUUsV0FBVztnQkFDbEIsRUFBRSxFQUFFLFVBQVU7Z0JBQ2QsR0FBRyxFQUFFLGNBQWM7Z0JBQ25CLEVBQUUsRUFBRSxTQUFTO2dCQUNiLElBQUksRUFBRSxJQUFJO2dCQUNWLEdBQUcsRUFBRSxLQUFLO2dCQUNWLEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEdBQUcsRUFBRSxtQkFBbUI7Z0JBQ3hCLElBQUksRUFBRSxFQUFFO2dCQUNSLEVBQUUsRUFBRSxJQUFJO2dCQUNSLEVBQUUsRUFBRSxJQUFJO2dCQUNSLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksRUFBRSxFQUFFO2dCQUNSLE9BQU8sRUFBRSxDQUFDO2dCQUNWLElBQUksRUFBRSxpQkFBTyxDQUFDLFFBQVE7Z0JBQ3RCLEdBQUcsRUFBRSxFQUFFO2dCQUNQLElBQUksRUFBRSxFQUFFO2FBQ1Q7U0FDRixDQUFDO0lBQ0osQ0FBQztJQUVELE1BQU0sQ0FBQyxZQUFZLENBQUMsTUFBTTtRQUN4QixPQUFPO1lBQ0wsV0FBVyxFQUFFLE9BQU8sR0FBRyxNQUFNO1lBQzdCLEVBQUUsRUFBRSxTQUFTO1lBQ2IsR0FBRyxFQUFFLGNBQWM7WUFDbkIsRUFBRSxFQUFFLFNBQVM7WUFDYixJQUFJLEVBQUUsSUFBSTtZQUNWLEdBQUcsRUFBRSxLQUFLO1lBQ1YsRUFBRSxFQUFFLEtBQUs7WUFDVCxFQUFFLEVBQUUsS0FBSztZQUNULEVBQUUsRUFBRSxLQUFLO1lBQ1QsRUFBRSxFQUFFLEtBQUs7WUFDVCxHQUFHLEVBQUUsSUFBSTtZQUNULElBQUksRUFBRSxFQUFFO1lBQ1IsRUFBRSxFQUFFLElBQUk7WUFDUixFQUFFLEVBQUUsSUFBSTtZQUNSLElBQUksRUFBRSxFQUFFO1lBQ1IsSUFBSSxFQUFFLEVBQUU7WUFDUixPQUFPLEVBQUUsQ0FBQztZQUNWLElBQUksRUFBRSxnQkFBZ0I7WUFDdEIsR0FBRyxFQUFFLEVBQUU7WUFDUCxJQUFJLEVBQUUsRUFBRTtTQUNULENBQUM7SUFDSixDQUFDO0lBRUQsNkNBQTZDO0lBQzdDLG9CQUFvQjtJQUNwQixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixzQkFBc0I7SUFDdEIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZix1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixFQUFFO0lBQ0Ysd0JBQXdCO0lBQ3hCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysb0JBQW9CO0lBQ3BCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLHdCQUF3QjtJQUN4QixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixvQkFBb0I7SUFDcEIsZUFBZTtJQUNmLHNCQUFzQjtJQUN0QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLG9CQUFvQjtJQUNwQixvQkFBb0I7SUFDcEIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLHFCQUFxQjtJQUNyQixvQkFBb0I7SUFDcEIsZUFBZTtJQUNmLHdCQUF3QjtJQUN4QixzQkFBc0I7SUFDdEIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQiw4QkFBOEI7SUFDOUIsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsb0JBQW9CO0lBQ3BCLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLHNCQUFzQjtJQUN0QixtQkFBbUI7SUFDbkIseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsbUJBQW1CO0lBQ25CLDJCQUEyQjtJQUMzQixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsRUFBRTtJQUNGLE1BQU07SUFDTixFQUFFO0lBQ0YsSUFBSTtJQUVKLE1BQU0sQ0FBQywyQkFBMkIsQ0FBQyxJQUFJO1FBQ3JDLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxLQUFLO2dCQUNSLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxTQUFTO2dCQUNaLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVO2dCQUNiLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxXQUFXO2dCQUNkLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxRQUFRO2dCQUNYLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxLQUFLO2dCQUNSLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxTQUFTO2dCQUNaLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxPQUFPO2dCQUNWLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxTQUFTO2dCQUNaLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxRQUFRO2dCQUNYLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxRQUFRO2dCQUNYLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxTQUFTO2dCQUNaLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxPQUFPO2dCQUNWLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVO2dCQUNiLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxZQUFZO2dCQUNmLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVO2dCQUNiLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxRQUFRO2dCQUNYLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxXQUFXO2dCQUNkLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVO2dCQUNiLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxZQUFZO2dCQUNmLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVO2dCQUNiLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxLQUFLO2dCQUNSLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVO2dCQUNiLE9BQU8sRUFBRSxDQUFDO1lBQ1osS0FBSyxPQUFPO2dCQUNWLE9BQU8sRUFBRSxDQUFDO1lBQ1osS0FBSyxZQUFZO2dCQUNmLE9BQU8sRUFBRSxDQUFDO1lBQ1osS0FBSyxTQUFTO2dCQUNaLE9BQU8sRUFBRSxDQUFDO1lBQ1osS0FBSyxZQUFZO2dCQUNmLE9BQU8sRUFBRSxDQUFDO1lBQ1osS0FBSyxpQkFBaUI7Z0JBQ3BCLE9BQU8sRUFBRSxDQUFDO1lBQ1osS0FBSyxjQUFjO2dCQUNqQixPQUFPLEVBQUUsQ0FBQztZQUNaLEtBQUssTUFBTTtnQkFDVCxPQUFPLEVBQUUsQ0FBQztTQUNiO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDO0lBRUQsTUFBTSxDQUFDLDBCQUEwQixDQUFDLElBQUk7UUFDcEMsUUFBUSxJQUFJLEVBQUU7WUFDWixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxJQUFJLENBQUM7WUFFZCxLQUFLLE1BQU07Z0JBQ1QsT0FBTyxLQUFLLENBQUM7U0FDaEI7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLDBCQUEwQixDQUFDLElBQUk7UUFDcEMsUUFBUSxJQUFJLEVBQUU7WUFDWixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBRXRCLEtBQUssTUFBTTtnQkFDVCxPQUFPLFVBQVUsQ0FBQztZQUVwQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBRXRCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFFeEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sT0FBTyxHQUFHLEdBQUcsQ0FBQztTQUN4QjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsMEJBQTBCLENBQUMsSUFBSTtRQUNwQyxRQUFRLElBQUksRUFBRTtZQUNaLEtBQUssU0FBUztnQkFDWixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssTUFBTTtnQkFDVCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssU0FBUztnQkFDWixPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssTUFBTTtnQkFDVCxPQUFPLEdBQUcsQ0FBQztZQUViLEtBQUssTUFBTTtnQkFDVCxPQUFPLEdBQUcsQ0FBQztTQUNkO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLEVBQUUsT0FBTztRQUNuQyxvQkFBb0I7UUFDcEIsSUFBSSxHQUFHLENBQUMsRUFBRSxLQUFLLFNBQVMsRUFBRTtZQUN4QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsRUFBRTtnQkFDdkMsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEdBQUcsQ0FBQyxFQUFFLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDN0MsT0FBTyxJQUFJLENBQUM7aUJBQ2I7YUFDRjtZQUNELE9BQU8sS0FBSyxDQUFDO1NBQ2Q7YUFBTTtZQUNMLE9BQU8sSUFBSSxDQUFDO1NBQ2I7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLGlCQUFpQixDQUFDLElBQUk7UUFDM0Isb0JBQW9CO1FBQ3BCLE9BQU8sSUFBSSxDQUFDO1FBQ1osMEJBQTBCO1FBQzFCLDRCQUE0QjtRQUM1Qiw2QkFBNkI7UUFDN0IsOEJBQThCO1FBQzlCLDJCQUEyQjtRQUMzQixrQkFBa0I7UUFDbEIsV0FBVztRQUNYLGlCQUFpQjtRQUNqQixJQUFJO0lBQ04sQ0FBQztJQUVELE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakIsSUFDRSxHQUFHLENBQUMsRUFBRSxLQUFLLEtBQUs7WUFDaEIsR0FBRyxDQUFDLEVBQUUsS0FBSyxRQUFRO1lBQ25CLEdBQUcsQ0FBQyxFQUFFLEtBQUssVUFBVTtZQUNyQixHQUFHLENBQUMsRUFBRSxLQUFLLFNBQVMsRUFDcEI7WUFDQSxHQUFHLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDO1NBQ2hDO1FBRUQsaUJBQWlCO1FBQ2pCLDZCQUE2QjtRQUM3QixXQUFXO1FBQ1gsaUJBQWlCO1FBQ2pCLElBQUk7SUFDTixDQUFDO0lBRUQsTUFBTSxDQUFDLFNBQVMsQ0FBQyxVQUFVO1FBQ3pCLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVELE1BQU0sQ0FBQyxjQUFjLENBQUMsT0FBTztRQUMzQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsRUFBRTtZQUN2QyxJQUNFLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEtBQUssQ0FBQztnQkFDeEIsQ0FBQyxDQUNDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssS0FBSztvQkFDdkIsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxRQUFRO29CQUMxQixPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLFNBQVM7b0JBQzNCLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssVUFBVTtvQkFDNUIsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxXQUFXLENBQzlCLEVBQ0Q7Z0JBQ0EsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxLQUFLLENBQUM7Z0JBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkJBQTZCLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQzNEO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDNUI7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEtBQUs7UUFDOUIsTUFBTSxHQUFHLEdBQ1AsMkVBQTJFLENBQUM7UUFDOUUsTUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMvQixPQUFPLEtBQUssSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUVELE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLO1FBQy9CLE1BQU0sR0FBRyxHQUNQLDRFQUE0RSxDQUFDO1FBQy9FLE1BQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDL0IsT0FBTyxLQUFLLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCxNQUFNLENBQUMsY0FBYyxDQUFDLE9BQU87UUFDM0IsT0FBTyxJQUFJLENBQUMsQ0FBQyx3QkFBd0I7SUFDdkMsQ0FBQztJQUVELE1BQU0sQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLE9BQU87UUFDcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNwQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ3RDLE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDcEMscUNBQXFDO1lBQ3JDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNwQyxNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JCLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQzthQUN0RDtTQUNGO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDaEIsQ0FBQztJQUVELE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSztRQUN2Qjs7O1dBR0c7UUFDSCxPQUFPLEtBQUssQ0FBQyxLQUFLLENBQUMseUNBQXlDLENBQUMsQ0FBQztJQUNoRSxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILGFBQWEsQ0FBQyxJQUFJO1FBQ2hCLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRXJDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLFlBQVksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUM7U0FDekQ7YUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDcEMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxZQUFZLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1NBQ3pEO2FBQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3BDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsWUFBWSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQztTQUN6RDthQUFNO1lBQ0wsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO1NBQ25CO1FBRUQsT0FBTyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQsTUFBTSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxFQUFFO1FBQ2hDLE1BQU0sT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUVuQixJQUFJO1lBQ0YsSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNwRCxNQUFNLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUMvQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsRUFBRTtvQkFDcEMsTUFBTSxNQUFNLEdBQUc7d0JBQ2IsRUFBRSxFQUFFLElBQUk7d0JBQ1IsRUFBRTt3QkFDRixXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDcEIsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7d0JBQ1osRUFBRSxFQUFFLElBQUk7d0JBQ1IsRUFBRSxFQUFFLElBQUk7d0JBQ1IsSUFBSSxFQUFFLElBQUk7d0JBQ1YsR0FBRyxFQUFFLENBQUM7d0JBQ04sRUFBRSxFQUFFLEtBQUs7d0JBQ1QsSUFBSSxFQUFFLEtBQUs7d0JBQ1gsR0FBRyxFQUFFLEtBQUs7d0JBQ1YsRUFBRSxFQUFFLEtBQUs7d0JBQ1QsRUFBRSxFQUFFLGtCQUFrQjt3QkFDdEIsRUFBRSxFQUFFLEtBQUs7d0JBQ1QsTUFBTSxFQUFFLEtBQUs7d0JBQ2IsR0FBRyxFQUFFLElBQUk7d0JBQ1QsRUFBRSxFQUFFLEVBQUU7d0JBQ04sR0FBRyxFQUFFLElBQUk7d0JBQ1QsR0FBRyxFQUFFLGNBQWM7d0JBQ25CLElBQUksRUFBRSxJQUFJO3dCQUNWLElBQUksRUFBRSxDQUFDO3dCQUNQLE9BQU8sRUFBRSxDQUFDO3FCQUNYLENBQUM7b0JBRUYsUUFBUSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDNUIsS0FBSyxRQUFROzRCQUNYLElBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQ0FDbkMsSUFBSSxRQUFRLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO29DQUNyRCxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRTt3Q0FDcEIsRUFBRSxFQUFFLFdBQVc7cUNBQ2hCLENBQUMsQ0FBQztpQ0FDSjtxQ0FBTTtvQ0FDTCxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRTt3Q0FDcEIsRUFBRSxFQUFFLFNBQVM7cUNBQ2QsQ0FBQyxDQUFDO2lDQUNKOzZCQUNGO2lDQUFNO2dDQUNMLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO29DQUNwQixFQUFFLEVBQUUsTUFBTTtpQ0FDWCxDQUFDLENBQUM7NkJBQ0o7NEJBQ0QsTUFBTTt3QkFDUixLQUFLLFFBQVE7NEJBQ1gsNkNBQTZDOzRCQUM3Qyw0QkFBNEI7NEJBQzVCLHVCQUF1Qjs0QkFDdkIsUUFBUTs0QkFDUixTQUFTOzRCQUNULElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUU7Z0NBQy9CLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO29DQUNwQixFQUFFLEVBQUUsU0FBUztpQ0FDZCxDQUFDLENBQUM7NkJBQ0o7aUNBQU07Z0NBQ0wsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0NBQ3BCLEVBQUUsRUFBRSxNQUFNO2lDQUNYLENBQUMsQ0FBQzs2QkFDSjs0QkFDRCxNQUFNO3dCQUNSLEtBQUssU0FBUzs0QkFDWixNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRTtnQ0FDcEIsRUFBRSxFQUFFLFNBQVM7NkJBQ2QsQ0FBQyxDQUFDOzRCQUNILE1BQU07d0JBQ1IsS0FBSyxRQUFROzRCQUNYLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO2dDQUNwQixFQUFFLEVBQUUsTUFBTTtnQ0FDVixFQUFFLEVBQUUsSUFBSTtnQ0FDUixJQUFJLEVBQUUsSUFBSTs2QkFDWCxDQUFDLENBQUM7NEJBQ0gsTUFBTTt3QkFDUjs0QkFDRSxNQUFNO3FCQUNUO29CQUNELE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7aUJBQ3RCO2FBQ0Y7U0FDRjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1YsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUMvQztRQUVELE9BQU8sT0FBTyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxNQUFNLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLEtBQUs7UUFDaEMsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDN0IsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUNELE9BQU8sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxJQUFJLG1CQUFtQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBRUQsTUFBTSxDQUFDLFdBQVcsQ0FBQyxLQUFLO1FBQ3RCLE9BQU8sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRCxNQUFNLENBQUMsa0JBQWtCLENBQUMsR0FBRztRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLFdBQVc7UUFDWCxHQUFHLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDO1FBQy9CLElBQUk7UUFFSixpQkFBaUI7UUFDakIsNkJBQTZCO1FBQzdCLFdBQVc7UUFDWCxpQkFBaUI7UUFDakIsSUFBSTtJQUNOLENBQUM7SUFFRCxNQUFNLENBQUMsaUJBQWlCLENBQUMsR0FBRztRQUMxQixJQUFJLEdBQUcsQ0FBQyxPQUFPLEtBQUssQ0FBQyxFQUFFO1lBQ3JCLE9BQU8sSUFBSSxDQUFDO1NBQ2I7UUFFRCxRQUFRLEdBQUcsQ0FBQyxFQUFFLEVBQUU7WUFDZCxLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssTUFBTTtnQkFDVCxPQUFPLEtBQUssQ0FBQztZQUVmO2dCQUNFLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLGVBQWUsQ0FBQyxHQUFHO1FBQ3hCLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBRTtZQUN4QyxLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFDaEIsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sVUFBVSxDQUFDO1lBQ3BCLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxTQUFTLENBQUM7WUFDbkIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBQ2hCLEtBQUssU0FBUztnQkFDWixPQUFPLFNBQVMsQ0FBQztZQUNuQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxrQkFBa0IsQ0FBQztZQUN4QixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxTQUFTO2dCQUNaLE9BQU8sT0FBTyxDQUFDO1lBRWpCLEtBQUssb0JBQW9CLENBQUM7WUFDMUIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssU0FBUztnQkFDWixPQUFPLFFBQVEsQ0FBQztTQUNuQjtRQUNELE9BQU8sUUFBUSxDQUFDO0lBQ2xCLENBQUM7SUFFRCxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUc7UUFDbEIsUUFBUSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ2pDLEtBQUssU0FBUztnQkFDWixPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxVQUFVLENBQUM7WUFDcEIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sU0FBUyxDQUFDO1lBQ25CLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxZQUFZLENBQUM7WUFDdEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBQ2hCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxnQkFBZ0IsQ0FBQztZQUMxQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxVQUFVLENBQUM7WUFDcEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sWUFBWSxDQUFDO1lBQ3RCLEtBQUssTUFBTTtnQkFDVCxPQUFPLGNBQWMsQ0FBQztZQUN4QixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxhQUFhLENBQUM7WUFDdkIsS0FBSyxNQUFNO2dCQUNULE9BQU8sVUFBVSxDQUFDO1NBQ3JCO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxHQUFzQixFQUFFLE1BQWU7UUFDakUsTUFBTSxPQUFPLEdBQVEsRUFBRSxDQUFDO1FBQ3hCLFFBQVEsR0FBRyxDQUFDLElBQUksRUFBRTtZQUNoQixLQUFLLElBQUk7Z0JBQ1A7b0JBQ0UsTUFBTSxXQUFXLEdBQUcsTUFBTSxLQUFLLElBQUksQ0FBQztvQkFDcEMsTUFBTSxXQUFXLEdBQUcsTUFBTSxLQUFLLElBQUksQ0FBQztvQkFDcEMsT0FBTyxDQUFDLEVBQUUsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO29CQUNqRCxPQUFPLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQztvQkFDbEIsT0FBTyxDQUFDLEVBQUUsR0FBRyxXQUFXLENBQUM7b0JBQ3pCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsV0FBVyxDQUFDO29CQUN6QixPQUFPLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQztvQkFDbkIsT0FBTyxDQUFDLElBQUksR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7aUJBQ3ZEO2dCQUNELE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLGdCQUFnQjtnQkFDbkIsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsMEJBQTBCO2dCQUMxQixvQkFBb0I7Z0JBQ3BCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxhQUFhO2dCQUNoQixPQUFPLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQztnQkFDcEIsTUFBTTtZQUNSLEtBQUssY0FBYztnQkFDakIsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUixLQUFLLGNBQWM7Z0JBQ2pCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxNQUFNO2dCQUNULE9BQU8sQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDO2dCQUVwQixNQUFNO1lBQ1IsS0FBSyxNQUFNO2dCQUNULE9BQU8sQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDO2dCQUNwQixNQUFNO1lBQ1IsS0FBSyxNQUFNO2dCQUNULE9BQU8sQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDO2dCQUNwQixNQUFNO1lBQ1IsS0FBSyxhQUFhO2dCQUNoQixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLFFBQVEsR0FBRztvQkFDakIsSUFBSSxFQUFFLENBQUMsZUFBZSxDQUFDO29CQUN2QixJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBQ1YsR0FBRyxFQUFFLENBQUMsbUNBQW1DLENBQUM7aUJBQzNDLENBQUM7Z0JBQ0YsTUFBTTtZQUNSLEtBQUssT0FBTztnQkFDVixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLFFBQVEsR0FBRztvQkFDakIsSUFBSSxFQUFFLENBQUMsU0FBUyxDQUFDO29CQUNqQixJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBQ1YsR0FBRyxFQUFFLENBQUMsNkJBQTZCLENBQUM7aUJBQ3JDLENBQUM7Z0JBQ0YsTUFBTTtZQUNSLEtBQUssS0FBSztnQkFDUixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLFFBQVEsR0FBRztvQkFDakIsSUFBSSxFQUFFLENBQUMsT0FBTyxDQUFDO29CQUNmLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQztvQkFDVixHQUFHLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQztpQkFDbkMsQ0FBQztnQkFDRixNQUFNO1lBQ1IsS0FBSyxRQUFRO2dCQUNYLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxVQUFVO2dCQUNiLE9BQU8sQ0FBQyxFQUFFLEdBQUcsa0JBQWtCLENBQUM7Z0JBQ2hDLE9BQU8sQ0FBQyxRQUFRLEdBQUc7b0JBQ2pCLElBQUksRUFBRSxDQUFDLFlBQVksQ0FBQztvQkFDcEIsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDO29CQUNWLEdBQUcsRUFBRSxDQUFDLGdDQUFnQyxDQUFDO2lCQUN4QyxDQUFDO2dCQUNGLE1BQU07WUFDUixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxDQUFDLEVBQUUsR0FBRyxRQUFRLENBQUM7Z0JBQ3RCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxDQUFDLEVBQUUsR0FBRyxVQUFVLENBQUM7Z0JBQ3hCLE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxDQUFDLEVBQUUsR0FBRyxVQUFVLENBQUM7Z0JBQ3hCLE1BQU07WUFDUixLQUFLLGtCQUFrQjtnQkFDckIsT0FBTyxDQUFDLEVBQUUsR0FBRyxVQUFVLENBQUM7Z0JBQ3hCLE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUjtnQkFDRSxPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsTUFBTTtTQUNUO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQztJQUVELE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQyxHQUFzQixFQUFFLE1BQWU7UUFDckUsUUFBUSxHQUFHLENBQUMsSUFBSSxFQUFFO1lBQ2hCLEtBQUssSUFBSTtnQkFDUCxJQUFJLE1BQU0sS0FBSyxJQUFJLEVBQUU7b0JBQ25CLE9BQU8sQ0FBQyxXQUFXLEVBQUUsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDO2lCQUN6QztxQkFBTSxJQUFJLE1BQU0sS0FBSyxJQUFJLEVBQUU7b0JBQzFCLE9BQU87d0JBQ0wsS0FBSzt3QkFDTCxTQUFTO3dCQUNULFNBQVM7d0JBQ1QsVUFBVTt3QkFDVixXQUFXO3dCQUNYLFFBQVE7d0JBQ1IsTUFBTTt3QkFDTixNQUFNO3FCQUNQLENBQUM7aUJBQ0g7cUJBQU07b0JBQ0wsT0FBTyxPQUFPLENBQUM7aUJBQ2hCO1lBQ0gsS0FBSyxZQUFZO2dCQUNmLE9BQU8sT0FBTyxDQUFDO1lBQ2pCLEtBQUssZ0JBQWdCLENBQUM7WUFDdEIsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxZQUFZLENBQUM7WUFDbEIsS0FBSyxjQUFjO2dCQUNqQixPQUFPLENBQUMsV0FBVyxFQUFFLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQztZQUUxQyxLQUFLLFVBQVU7Z0JBQ2IsT0FBTztvQkFDTCxLQUFLO29CQUNMLFNBQVM7b0JBQ1QsU0FBUztvQkFDVCxVQUFVO29CQUNWLFdBQVc7b0JBQ1gsUUFBUTtvQkFDUixNQUFNO29CQUNOLE1BQU07b0JBQ04sU0FBUztpQkFDVixDQUFDO1lBRUosS0FBSyxhQUFhO2dCQUNoQixPQUFPLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBRTdCLEtBQUssY0FBYztnQkFDakIsT0FBTyxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQztZQUU3QixLQUFLLE1BQU07Z0JBQ1QsT0FBTztvQkFDTCxLQUFLO29CQUNMLFNBQVM7b0JBQ1QsU0FBUztvQkFDVCxVQUFVO29CQUNWLFdBQVc7b0JBQ1gsUUFBUTtvQkFDUixNQUFNO29CQUNOLE1BQU07aUJBQ1AsQ0FBQztZQUVKLEtBQUssTUFBTTtnQkFDVCxPQUFPO29CQUNMLEtBQUs7b0JBQ0wsU0FBUztvQkFDVCxTQUFTO29CQUNULFVBQVU7b0JBQ1YsV0FBVztvQkFDWCxRQUFRO29CQUNSLE1BQU07b0JBQ04sTUFBTTtpQkFDUCxDQUFDO1lBRUosS0FBSyxhQUFhLENBQUM7WUFDbkIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFFN0IsS0FBSyxLQUFLO2dCQUNSLE9BQU8sQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFFN0IsS0FBSyxRQUFRO2dCQUNYLE9BQU87b0JBQ0wsS0FBSztvQkFDTCxTQUFTO29CQUNULFNBQVM7b0JBQ1QsVUFBVTtvQkFDVixXQUFXO29CQUNYLFFBQVE7b0JBQ1IsTUFBTTtvQkFDTixNQUFNO29CQUNOLFNBQVM7b0JBQ1QsTUFBTTtvQkFDTixRQUFRO29CQUNSLGtCQUFrQjtvQkFDbEIsT0FBTztpQkFDUixDQUFDO1lBRUosS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxNQUFNLEVBQUUsUUFBUSxFQUFFLGtCQUFrQixFQUFFLE9BQU8sRUFBRSxTQUFTLENBQUMsQ0FBQztZQUVwRSxLQUFLLFVBQVU7Z0JBQ2IsT0FBTztvQkFDTCxNQUFNO29CQUNOLFFBQVE7b0JBQ1Isa0JBQWtCO29CQUNsQixPQUFPO29CQUNQLEtBQUs7b0JBQ0wsU0FBUztvQkFDVCxTQUFTO29CQUNULFVBQVU7b0JBQ1YsV0FBVztvQkFDWCxRQUFRO29CQUNSLE1BQU07b0JBQ04sTUFBTTtvQkFDTixTQUFTO2lCQUNWLENBQUM7WUFFSixLQUFLLFNBQVM7Z0JBQ1osT0FBTztvQkFDTCxNQUFNO29CQUNOLFFBQVE7b0JBQ1Isa0JBQWtCO29CQUNsQixPQUFPO29CQUNQLEtBQUs7b0JBQ0wsU0FBUztvQkFDVCxTQUFTO29CQUNULFVBQVU7b0JBQ1YsV0FBVztvQkFDWCxRQUFRO29CQUNSLE1BQU07b0JBQ04sTUFBTTtvQkFDTixTQUFTO2lCQUNWLENBQUM7WUFFSixLQUFLLFVBQVU7Z0JBQ2IsT0FBTztvQkFDTCxLQUFLO29CQUNMLFNBQVM7b0JBQ1QsU0FBUztvQkFDVCxVQUFVO29CQUNWLFdBQVc7b0JBQ1gsUUFBUTtvQkFDUixNQUFNO29CQUNOLE1BQU07aUJBQ1AsQ0FBQztZQUVKLEtBQUssUUFBUTtnQkFDWCxPQUFPO29CQUNMLEtBQUs7b0JBQ0wsU0FBUztvQkFDVCxTQUFTO29CQUNULFVBQVU7b0JBQ1YsV0FBVztvQkFDWCxRQUFRO29CQUNSLE1BQU07b0JBQ04sTUFBTTtvQkFDTixTQUFTO29CQUNULE1BQU07b0JBQ04sUUFBUTtvQkFDUixrQkFBa0I7b0JBQ2xCLE9BQU87aUJBQ1IsQ0FBQztZQUVKLEtBQUssU0FBUztnQkFDWixPQUFPLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBRTdCLEtBQUssUUFBUTtnQkFDWCxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7WUFFckIsS0FBSyxPQUFPO2dCQUNWLE9BQU87b0JBQ0wsS0FBSztvQkFDTCxTQUFTO29CQUNULFNBQVM7b0JBQ1QsVUFBVTtvQkFDVixXQUFXO29CQUNYLFFBQVE7b0JBQ1IsTUFBTTtvQkFDTixNQUFNO2lCQUNQLENBQUM7WUFFSixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBRXJCLEtBQUssTUFBTTtnQkFDVCxPQUFPLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBRTdCLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssa0JBQWtCO2dCQUNyQixPQUFPLENBQUMsVUFBVSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1lBRW5DLEtBQUssWUFBWTtnQkFDZixPQUFPO29CQUNMLEtBQUs7b0JBQ0wsU0FBUztvQkFDVCxTQUFTO29CQUNULFVBQVU7b0JBQ1YsV0FBVztvQkFDWCxRQUFRO29CQUNSLE1BQU07b0JBQ04sTUFBTTtpQkFDUCxDQUFDO1lBRUosS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUVyQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2xCLEtBQUssTUFBTTtnQkFDVCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFbEIsS0FBSyxRQUFRLENBQUM7WUFDZDtnQkFDRSxPQUFPLE9BQU8sQ0FBQztTQUNsQjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsb0JBQW9CO1FBQ3pCLE9BQU8sQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN6QyxDQUFDO0NBQ0Y7QUFsa0NELDRCQWtrQ0M7QUFFRCw2QkFBNkI7QUFDN0I7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQXFCRyJ9