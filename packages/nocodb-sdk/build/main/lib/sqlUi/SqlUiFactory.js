"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SqlUiFactory = void 0;
const MssqlUi_1 = require("./MssqlUi");
const MysqlUi_1 = require("./MysqlUi");
const OracleUi_1 = require("./OracleUi");
const PgUi_1 = require("./PgUi");
const SqliteUi_1 = require("./SqliteUi");
// import {YugabyteUi} from "./YugabyteUi";
// import {TidbUi} from "./TidbUi";
// import {VitessUi} from "./VitessUi";
class SqlUiFactory {
    static create(connectionConfig) {
        // connectionConfig.meta = connectionConfig.meta || {};
        // connectionConfig.meta.dbtype = connectionConfig.meta.dbtype || "";
        if (connectionConfig.client === 'mysql' ||
            connectionConfig.client === 'mysql2') {
            // if (connectionConfig.meta.dbtype === "tidb")
            //   return Tidb;
            // if (connectionConfig.meta.dbtype === "vitess")
            //   return Vitess;
            return MysqlUi_1.MysqlUi;
        }
        if (connectionConfig.client === 'sqlite3') {
            return SqliteUi_1.SqliteUi;
        }
        if (connectionConfig.client === 'mssql') {
            return MssqlUi_1.MssqlUi;
        }
        if (connectionConfig.client === 'oracledb') {
            return OracleUi_1.OracleUi;
        }
        if (connectionConfig.client === 'pg') {
            // if (connectionConfig.meta.dbtype === "yugabyte")
            //   return Yugabyte;
            return PgUi_1.PgUi;
        }
        throw new Error('Database not supported');
    }
}
exports.SqlUiFactory = SqlUiFactory;
/**
 * @copyright Copyright (c) 2021, Xgene Cloud Ltd
 *
 * @author Naveen MR <oof1lab@gmail.com>
 * @author Pranav C Balan <pranavxc@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3FsVWlGYWN0b3J5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2xpYi9zcWxVaS9TcWxVaUZhY3RvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBRUEsdUNBQW9DO0FBQ3BDLHVDQUFvQztBQUNwQyx5Q0FBc0M7QUFDdEMsaUNBQThCO0FBQzlCLHlDQUFzQztBQUV0QywyQ0FBMkM7QUFDM0MsbUNBQW1DO0FBQ25DLHVDQUF1QztBQUV2QyxNQUFhLFlBQVk7SUFDdkIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0I7UUFDNUIsdURBQXVEO1FBQ3ZELHFFQUFxRTtRQUNyRSxJQUNFLGdCQUFnQixDQUFDLE1BQU0sS0FBSyxPQUFPO1lBQ25DLGdCQUFnQixDQUFDLE1BQU0sS0FBSyxRQUFRLEVBQ3BDO1lBQ0EsK0NBQStDO1lBQy9DLGlCQUFpQjtZQUNqQixpREFBaUQ7WUFDakQsbUJBQW1CO1lBRW5CLE9BQU8saUJBQU8sQ0FBQztTQUNoQjtRQUVELElBQUksZ0JBQWdCLENBQUMsTUFBTSxLQUFLLFNBQVMsRUFBRTtZQUN6QyxPQUFPLG1CQUFRLENBQUM7U0FDakI7UUFDRCxJQUFJLGdCQUFnQixDQUFDLE1BQU0sS0FBSyxPQUFPLEVBQUU7WUFDdkMsT0FBTyxpQkFBTyxDQUFDO1NBQ2hCO1FBQ0QsSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO1lBQzFDLE9BQU8sbUJBQVEsQ0FBQztTQUNqQjtRQUVELElBQUksZ0JBQWdCLENBQUMsTUFBTSxLQUFLLElBQUksRUFBRTtZQUNwQyxtREFBbUQ7WUFDbkQscUJBQXFCO1lBQ3JCLE9BQU8sV0FBSSxDQUFDO1NBQ2I7UUFFRCxNQUFNLElBQUksS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUM7SUFDNUMsQ0FBQztDQUNGO0FBbENELG9DQWtDQztBQXdCRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBcUJHIn0=