"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MssqlUi = void 0;
const UITypes_1 = __importDefault(require("../UITypes"));
const dbTypes = [
    'bigint',
    'binary',
    'bit',
    'char',
    'date',
    'datetime',
    'datetime2',
    'datetimeoffset',
    'decimal',
    'float',
    'geography',
    'geometry',
    'heirarchyid',
    'image',
    'int',
    'money',
    'nchar',
    'ntext',
    'numeric',
    'nvarchar',
    'real',
    'json',
    'smalldatetime',
    'smallint',
    'smallmoney',
    'sql_variant',
    'sysname',
    'text',
    'time',
    'timestamp',
    'tinyint',
    'uniqueidentifier',
    'varbinary',
    'xml',
    'varchar',
];
class MssqlUi {
    static getNewTableColumns() {
        return [
            {
                column_name: 'id',
                title: 'Id',
                dt: 'int',
                dtx: 'integer',
                ct: 'int(11)',
                nrqd: false,
                rqd: true,
                ck: false,
                pk: true,
                un: false,
                ai: true,
                cdf: null,
                clen: null,
                np: null,
                ns: 0,
                dtxp: '',
                dtxs: '',
                altered: 1,
                uidt: 'ID',
                uip: '',
                uicn: '',
            },
            {
                column_name: 'title',
                title: 'Title',
                dt: 'varchar',
                dtx: 'specificType',
                ct: 'varchar(45)',
                nrqd: true,
                rqd: false,
                ck: false,
                pk: false,
                un: false,
                ai: false,
                cdf: null,
                clen: 45,
                np: null,
                ns: null,
                dtxp: '45',
                dtxs: '',
                altered: 1,
                uidt: 'SingleLineText',
                uip: '',
                uicn: '',
            },
            {
                column_name: 'created_at',
                title: 'CreatedAt',
                dt: 'datetime',
                dtx: 'specificType',
                ct: 'varchar(45)',
                nrqd: true,
                rqd: false,
                ck: false,
                pk: false,
                un: false,
                ai: false,
                cdf: 'GETDATE()',
                clen: 45,
                np: null,
                ns: null,
                dtxp: '',
                dtxs: '',
                altered: 1,
                uidt: UITypes_1.default.DateTime,
                uip: '',
                uicn: '',
            },
            {
                column_name: 'updated_at',
                title: 'UpdatedAt',
                dt: 'datetime',
                dtx: 'specificType',
                ct: 'varchar(45)',
                nrqd: true,
                rqd: false,
                ck: false,
                pk: false,
                un: false,
                ai: false,
                au: true,
                cdf: 'GETDATE()',
                clen: 45,
                np: null,
                ns: null,
                dtxp: '',
                dtxs: '',
                altered: 1,
                uidt: UITypes_1.default.DateTime,
                uip: '',
                uicn: '',
            },
        ];
    }
    static getNewColumn(suffix) {
        return {
            column_name: 'title' + suffix,
            dt: 'varchar',
            dtx: 'specificType',
            ct: 'varchar(45)',
            nrqd: true,
            rqd: false,
            ck: false,
            pk: false,
            un: false,
            ai: false,
            cdf: null,
            clen: 45,
            np: null,
            ns: null,
            dtxp: '45',
            dtxs: '',
            altered: 1,
            uidt: 'SingleLineText',
            uip: '',
            uicn: '',
        };
    }
    // static getDefaultLengthForDatatype(type) {
    //   switch (type) {
    //     case "int":
    //       return 11;
    //       break;
    //     case "tinyint":
    //       return 1;
    //       break;
    //     case "smallint":
    //       return 5;
    //       break;
    //
    //     case "mediumint":
    //       return 9;
    //       break;
    //     case "bigint":
    //       return 20;
    //       break;
    //     case "bit":
    //       return 64;
    //       break;
    //     case "boolean":
    //       return '';
    //       break;
    //     case "float":
    //       return 12;
    //       break;
    //     case "decimal":
    //       return 10;
    //       break;
    //     case "double":
    //       return 22;
    //       break;
    //     case "serial":
    //       return 20;
    //       break;
    //     case "date":
    //       return '';
    //       break;
    //     case "datetime":
    //     case "timestamp":
    //       return 6;
    //       break;
    //     case "time":
    //       return '';
    //       break;
    //     case "year":
    //       return '';
    //       break;
    //     case "char":
    //       return 255;
    //       break;
    //     case "varchar":
    //       return 45;
    //       break;
    //     case "nchar":
    //       return 255;
    //       break;
    //     case "text":
    //       return '';
    //       break;
    //     case "tinytext":
    //       return '';
    //       break;
    //     case "mediumtext":
    //       return '';
    //       break;
    //     case "longtext":
    //       return ''
    //       break;
    //     case "binary":
    //       return 255;
    //       break;
    //     case "varbinary":
    //       return 65500;
    //       break;
    //     case "blob":
    //       return '';
    //       break;
    //     case "tinyblob":
    //       return '';
    //       break;
    //     case "mediumblob":
    //       return '';
    //       break;
    //     case "longblob":
    //       return '';
    //       break;
    //     case "enum":
    //       return '\'a\',\'b\'';
    //       break;
    //     case "set":
    //       return '\'a\',\'b\'';
    //       break;
    //     case "geometry":
    //       return '';
    //     case "point":
    //       return '';
    //     case "linestring":
    //       return '';
    //     case "polygon":
    //       return '';
    //     case "multipoint":
    //       return '';
    //     case "multilinestring":
    //       return '';
    //     case "multipolygon":
    //       return '';
    //     case "json":
    //       return ''
    //       break;
    //
    //   }
    //
    // }
    static getDefaultLengthForDatatype(type) {
        switch (type) {
            case 'bigint':
                return '';
            case 'binary':
                return '';
            case 'bit':
                return '';
            case 'char':
                return '';
            case 'date':
                return '';
            case 'datetime':
                return '';
            case 'datetime2':
                return '';
            case 'datetimeoffset':
                return '';
            case 'decimal':
                return '';
            case 'float':
                return '';
            case 'geography':
                return '';
            case 'geometry':
                return '';
            case 'heirarchyid':
                return '';
            case 'image':
                return '';
            case 'int':
                return '';
            case 'money':
                return '';
            case 'nchar':
                return '';
            case 'ntext':
                return '';
            case 'numeric':
                return '';
            case 'nvarchar':
                return 255;
            case 'real':
                return '';
            case 'json':
                return '';
            case 'smalldatetime':
                return '';
            case 'smallint':
                return '';
            case 'smallmoney':
                return '';
            case 'sql_variant':
                return '';
            case 'sysname':
                return '';
            case 'text':
                return '';
            case 'time':
                return '';
            case 'timestamp':
                return '';
            case 'tinyint':
                return '';
            case 'uniqueidentifier':
                return '';
            case 'varbinary':
                return '';
            case 'xml':
                return '';
            case 'varchar':
                return '';
            default:
                return '';
        }
    }
    static getDefaultLengthIsDisabled(type) {
        switch (type) {
            case 'nvarchar':
                return false;
            case 'bigint':
            case 'binary':
            case 'bit':
            case 'char':
            case 'date':
            case 'datetime':
            case 'datetime2':
            case 'datetimeoffset':
            case 'decimal':
            case 'float':
            case 'geography':
            case 'geometry':
            case 'heirarchyid':
            case 'image':
            case 'int':
            case 'money':
            case 'nchar':
            case 'ntext':
            case 'numeric':
            case 'nvarchar':
            case 'real':
            case 'json':
            case 'smalldatetime':
            case 'smallint':
            case 'smallmoney':
            case 'sql_variant':
            case 'sysname':
            case 'text':
            case 'time':
            case 'timestamp':
            case 'tinyint':
            case 'uniqueidentifier':
            case 'varbinary':
            case 'xml':
                return true;
            case 'varchar':
                return false;
            default:
                return true;
        }
    }
    static getDefaultValueForDatatype(type) {
        switch (type) {
            case 'bigint':
                return 'eg: ';
            case 'binary':
                return 'eg: ';
            case 'bit':
                return 'eg: ';
            case 'char':
                return 'eg: ';
            case 'date':
                return 'eg: ';
            case 'datetime':
                return 'eg: ';
            case 'datetime2':
                return 'eg: ';
            case 'datetimeoffset':
                return 'eg: ';
            case 'decimal':
                return 'eg: ';
            case 'float':
                return 'eg: ';
            case 'geography':
                return 'eg: ';
            case 'geometry':
                return 'eg: ';
            case 'heirarchyid':
                return 'eg: ';
            case 'image':
                return 'eg: ';
            case 'int':
                return 'eg: ';
            case 'money':
                return 'eg: ';
            case 'nchar':
                return 'eg: ';
            case 'ntext':
                return 'eg: ';
            case 'numeric':
                return 'eg: ';
            case 'nvarchar':
                return 'eg: ';
            case 'real':
                return 'eg: ';
            case 'json':
                return 'eg: ';
            case 'smalldatetime':
                return 'eg: ';
            case 'smallint':
                return 'eg: ';
            case 'smallmoney':
                return 'eg: ';
            case 'sql_variant':
                return 'eg: ';
            case 'sysname':
                return 'eg: ';
            case 'text':
                return 'eg: ';
            case 'time':
                return 'eg: ';
            case 'timestamp':
                return 'eg: ';
            case 'tinyint':
                return 'eg: ';
            case 'uniqueidentifier':
                return 'eg: ';
            case 'varbinary':
                return 'eg: ';
            case 'xml':
                return 'eg: ';
            case 'varchar':
                return 'eg: ';
            default:
                return '';
        }
    }
    static getDefaultScaleForDatatype(type) {
        switch (type) {
            case 'bigint':
                return '';
            case 'binary':
                return '';
            case 'bit':
                return '';
            case 'char':
                return '';
            case 'date':
                return '';
            case 'datetime':
                return '';
            case 'datetime2':
                return '';
            case 'datetimeoffset':
                return '';
            case 'decimal':
                return '';
            case 'float':
                return '';
            case 'geography':
                return '';
            case 'geometry':
                return '';
            case 'heirarchyid':
                return '';
            case 'image':
                return '';
            case 'int':
                return '';
            case 'money':
                return '';
            case 'nchar':
                return '';
            case 'ntext':
                return '';
            case 'numeric':
                return '';
            case 'nvarchar':
                return '';
            case 'real':
                return '';
            case 'json':
                return '';
            case 'smalldatetime':
                return '';
            case 'smallint':
                return '';
            case 'smallmoney':
                return '';
            case 'sql_variant':
                return '';
            case 'sysname':
                return '';
            case 'text':
                return '';
            case 'time':
                return '';
            case 'timestamp':
                return '';
            case 'tinyint':
                return '';
            case 'uniqueidentifier':
                return '';
            case 'varbinary':
                return '';
            case 'xml':
                return '';
            case 'varchar':
                return '';
            default:
                return '';
        }
    }
    static colPropAIDisabled(col, columns) {
        // console.log(col);
        if (col.dt === 'int4' ||
            col.dt === 'integer' ||
            col.dt === 'bigint' ||
            col.dt === 'smallint') {
            for (let i = 0; i < columns.length; ++i) {
                if (columns[i].cn !== col.cn && columns[i].ai) {
                    return true;
                }
            }
            return false;
        }
        else {
            return true;
        }
    }
    static colPropUNDisabled(_col) {
        // console.log(col);
        return true;
        // if (col.dt === 'int' ||
        //   col.dt === 'tinyint' ||
        //   col.dt === 'smallint' ||
        //   col.dt === 'mediumint' ||
        //   col.dt === 'bigint') {
        //   return false;
        // } else {
        //   return true;
        // }
    }
    static onCheckboxChangeAI(col) {
        console.log(col);
        if (col.dt === 'int' ||
            col.dt === 'bigint' ||
            col.dt === 'smallint' ||
            col.dt === 'tinyint') {
            col.altered = col.altered || 2;
        }
        // if (!col.ai) {
        //   col.dtx = 'specificType'
        // } else {
        //   col.dtx = ''
        // }
    }
    static showScale(_columnObj) {
        return false;
    }
    static removeUnsigned(columns) {
        for (let i = 0; i < columns.length; ++i) {
            if (columns[i].altered === 1 &&
                !(columns[i].dt === 'int' ||
                    columns[i].dt === 'bigint' ||
                    columns[i].dt === 'tinyint' ||
                    columns[i].dt === 'smallint' ||
                    columns[i].dt === 'mediumint')) {
                columns[i].un = false;
                console.log('>> resetting unsigned value', columns[i].cn);
            }
            console.log(columns[i].cn);
        }
    }
    static columnEditable(colObj) {
        return colObj.tn !== '_evolutions' || colObj.tn !== 'nc_evolutions';
    }
    static extractFunctionName(query) {
        const reg = /^\s*CREATE\s+(?:OR\s+REPLACE\s*)?\s*FUNCTION\s+(?:[\w\d_]+\.)?([\w_\d]+)/i;
        const match = query.match(reg);
        return match && match[1];
    }
    static extractProcedureName(query) {
        const reg = /^\s*CREATE\s+(?:OR\s+REPLACE\s*)?\s*PROCEDURE\s+(?:[\w\d_]+\.)?([\w_\d]+)/i;
        const match = query.match(reg);
        return match && match[1];
    }
    static handleRawOutput(result, headers) {
        console.log(result);
        if (Array.isArray(result) && result[0]) {
            const keys = Object.keys(result[0]);
            // set headers before settings result
            for (let i = 0; i < keys.length; i++) {
                const text = keys[i];
                headers.push({ text, value: text, sortable: false });
            }
        }
        else if (result === undefined) {
            headers.push({ text: 'Message', value: 'message', sortable: false });
            result = [{ message: 'Success' }];
        }
        return result;
    }
    static splitQueries(query) {
        /***
         * we are splitting based on semicolon
         * there are mechanism to escape semicolon within single/double quotes(string)
         */
        return query.match(/\b("[^"]*;[^"]*"|'[^']*;[^']*'|[^;])*;/g);
    }
    /**
     * if sql statement is SELECT - it limits to a number
     * @param args
     * @returns {string|*}
     */
    sanitiseQuery(args) {
        let q = args.query.trim().split(';');
        if (q[0].startsWith('Select')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else if (q[0].startsWith('select')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else if (q[0].startsWith('SELECT')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else {
            return args.query;
        }
        return q;
    }
    static getColumnsFromJson(json, tn) {
        const columns = [];
        try {
            if (typeof json === 'object' && !Array.isArray(json)) {
                const keys = Object.keys(json);
                for (let i = 0; i < keys.length; ++i) {
                    const column = {
                        dp: null,
                        tn,
                        column_name: keys[i],
                        cno: keys[i],
                        np: 10,
                        ns: 0,
                        clen: null,
                        cop: 1,
                        pk: false,
                        nrqd: false,
                        rqd: false,
                        un: false,
                        ct: 'int(11) unsigned',
                        ai: false,
                        unique: false,
                        cdf: null,
                        cc: '',
                        csn: null,
                        dtx: 'specificType',
                        dtxp: null,
                        dtxs: 0,
                        altered: 1,
                    };
                    switch (typeof json[keys[i]]) {
                        case 'number':
                            if (Number.isInteger(json[keys[i]])) {
                                if (MssqlUi.isValidTimestamp(keys[i], json[keys[i]])) {
                                    Object.assign(column, {
                                        dt: 'timestamp',
                                    });
                                }
                                else {
                                    Object.assign(column, {
                                        dt: 'int',
                                        np: 10,
                                        ns: 0,
                                    });
                                }
                            }
                            else {
                                Object.assign(column, {
                                    dt: 'float',
                                    np: 10,
                                    ns: 2,
                                    dtxp: '11',
                                    dtxs: 2,
                                });
                            }
                            break;
                        case 'string':
                            if (MssqlUi.isValidDate(json[keys[i]])) {
                                Object.assign(column, {
                                    dt: 'datetime',
                                });
                            }
                            else if (json[keys[i]].length <= 255) {
                                Object.assign(column, {
                                    dt: 'varchar',
                                    np: 255,
                                    ns: 0,
                                    dtxp: '255',
                                });
                            }
                            else {
                                Object.assign(column, {
                                    dt: 'text',
                                });
                            }
                            break;
                        case 'boolean':
                            Object.assign(column, {
                                dt: 'bit',
                                np: null,
                                ns: 0,
                            });
                            break;
                        case 'object':
                            Object.assign(column, {
                                dt: 'varchar',
                                np: 255,
                                ns: 0,
                                dtxp: '255',
                            });
                            break;
                        default:
                            break;
                    }
                    columns.push(column);
                }
            }
        }
        catch (e) {
            console.log('Error in getColumnsFromJson', e);
        }
        return columns;
    }
    static isValidTimestamp(key, value) {
        if (typeof value !== 'number') {
            return false;
        }
        return new Date(value).getTime() > 0 && /(?:_|(?=A))[aA]t$/.test(key);
    }
    static isValidDate(value) {
        return new Date(value).getTime() > 0;
    }
    static onCheckboxChangeAU(col) {
        console.log(col);
        // if (1) {
        col.altered = col.altered || 2;
        // }
        if (col.au) {
            col.cdf = 'GETDATE()';
        }
        // if (!col.ai) {
        //   col.dtx = 'specificType'
        // } else {
        //   col.dtx = ''
        // }
    }
    static colPropAuDisabled(col) {
        if (col.altered !== 1) {
            return true;
        }
        switch (col.dt) {
            case 'date':
            case 'datetime':
            case 'datetime2':
            case 'datetimeoffset':
            case 'time':
            case 'timestamp':
                return false;
            default:
                return true;
        }
    }
    static getAbstractType(col) {
        switch ((col.dt || col.dt).toLowerCase()) {
            case 'bigint':
            case 'smallint':
            case 'bit':
            case 'tinyint':
            case 'int':
                return 'integer';
            case 'binary':
                return 'string';
            case 'char':
                return 'string';
            case 'date':
                return 'date';
            case 'datetime':
            case 'datetime2':
            case 'smalldatetime':
            case 'datetimeoffset':
                return 'datetime';
            case 'decimal':
            case 'float':
                return 'float';
            case 'geography':
            case 'geometry':
            case 'heirarchyid':
            case 'image':
                return 'string';
            case 'money':
            case 'nchar':
                return 'string';
            case 'ntext':
                return 'text';
            case 'numeric':
                return 'float';
            case 'nvarchar':
                return 'string';
            case 'real':
                return 'float';
            case 'json':
                return 'json';
            case 'smallmoney':
            case 'sql_variant':
            case 'sysname':
                return 'string';
            case 'text':
                return 'text';
            case 'time':
                return 'time';
            case 'timestamp':
                return 'timestamp';
            case 'uniqueidentifier':
            case 'varbinary':
            case 'xml':
                return 'string';
            case 'varchar':
                return 'string';
        }
        return 'string';
    }
    static getUIType(col) {
        switch (this.getAbstractType(col)) {
            case 'integer':
                return 'Number';
            case 'boolean':
                return 'Checkbox';
            case 'float':
                return 'Decimal';
            case 'date':
                return 'Date';
            case 'datetime':
                return 'CreateTime';
            case 'time':
                return 'Time';
            case 'year':
                return 'Year';
            case 'string':
                return 'SingleLineText';
            case 'text':
                return 'LongText';
            case 'blob':
                return 'Attachment';
            case 'enum':
                return 'SingleSelect';
            case 'set':
                return 'MultiSelect';
            case 'json':
                return 'LongText';
        }
    }
    static getDataTypeForUiType(col, idType) {
        const colProp = {};
        switch (col.uidt) {
            case 'ID':
                {
                    const isAutoIncId = idType === 'AI';
                    const isAutoGenId = idType === 'AG';
                    colProp.dt = isAutoGenId ? 'varchar' : 'int';
                    colProp.pk = true;
                    colProp.un = isAutoIncId;
                    colProp.ai = isAutoIncId;
                    colProp.rqd = true;
                    colProp.meta = isAutoGenId ? { ag: 'nc' } : undefined;
                }
                break;
            case 'ForeignKey':
                colProp.dt = 'varchar';
                break;
            case 'SingleLineText':
                colProp.dt = 'varchar';
                break;
            case 'LongText':
                colProp.dt = 'text';
                break;
            case 'Attachment':
                colProp.dt = 'text';
                break;
            case 'Checkbox':
                colProp.dt = 'tinyint';
                colProp.dtxp = 1;
                break;
            case 'MultiSelect':
                colProp.dt = 'text';
                break;
            case 'SingleSelect':
                colProp.dt = 'text';
                break;
            case 'Collaborator':
                colProp.dt = 'varchar';
                break;
            case 'Date':
                colProp.dt = 'varchar';
                break;
            case 'Year':
                colProp.dt = 'int';
                break;
            case 'Time':
                colProp.dt = 'time';
                break;
            case 'PhoneNumber':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isMobilePhone'],
                    args: [''],
                    msg: ['Validation failed : isMobilePhone'],
                };
                break;
            case 'Email':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isEmail'],
                    args: [''],
                    msg: ['Validation failed : isEmail'],
                };
                break;
            case 'URL':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isURL'],
                    args: [''],
                    msg: ['Validation failed : isURL'],
                };
                break;
            case 'Number':
                colProp.dt = 'int';
                break;
            case 'Decimal':
                colProp.dt = 'decimal';
                break;
            case 'Currency':
                colProp.dt = 'decimal';
                colProp.validate = {
                    func: ['isCurrency'],
                    args: [''],
                    msg: ['Validation failed : isCurrency'],
                };
                break;
            case 'Percent':
                colProp.dt = 'double';
                break;
            case 'Duration':
                colProp.dt = 'decimal';
                break;
            case 'Rating':
                colProp.dt = 'int';
                break;
            case 'Formula':
                colProp.dt = 'varchar';
                break;
            case 'Rollup':
                colProp.dt = 'varchar';
                break;
            case 'Count':
                colProp.dt = 'int';
                break;
            case 'Lookup':
                colProp.dt = 'varchar';
                break;
            case 'DateTime':
                colProp.dt = 'datetimeoffset';
                break;
            case 'CreateTime':
                colProp.dt = 'datetime';
                break;
            case 'LastModifiedTime':
                colProp.dt = 'datetime';
                break;
            case 'AutoNumber':
                colProp.dt = 'int';
                break;
            case 'Barcode':
                colProp.dt = 'varchar';
                break;
            case 'Button':
                colProp.dt = 'varchar';
                break;
            default:
                colProp.dt = 'varchar';
                break;
        }
        return colProp;
    }
    static getDataTypeListForUiType(col, idType) {
        switch (col.uidt) {
            case 'ID':
                if (idType === 'AG') {
                    return ['char', 'ntext', 'text', 'varchar', 'nvarchar'];
                }
                else if (idType === 'AI') {
                    return ['int', 'bigint', 'bit', 'smallint', 'tinyint'];
                }
                else {
                    return dbTypes;
                }
            case 'ForeignKey':
                return dbTypes;
            case 'SingleLineText':
            case 'LongText':
            case 'Attachment':
            case 'Collaborator':
                return ['char', 'ntext', 'text', 'varchar', 'nvarchar'];
            case 'JSON':
                return ['text', 'ntext'];
            case 'Checkbox':
                return ['bigint', 'bit', 'int', 'tinyint'];
            case 'MultiSelect':
                return ['text', 'ntext'];
            case 'SingleSelect':
                return ['text', 'ntext'];
            case 'Year':
                return ['int'];
            case 'Time':
                return ['time'];
            case 'PhoneNumber':
            case 'Email':
                return ['varchar'];
            case 'URL':
                return ['varchar', 'text'];
            case 'Number':
                return [
                    'int',
                    'bigint',
                    'bit',
                    'decimal',
                    'float',
                    'numeric',
                    'real',
                    'smallint',
                    'tinyint',
                ];
            case 'Decimal':
                return ['decimal', 'float'];
            case 'Currency':
                return [
                    'int',
                    'bigint',
                    'bit',
                    'decimal',
                    'float',
                    'numeric',
                    'real',
                    'smallint',
                    'tinyint',
                ];
            case 'Percent':
                return [
                    'int',
                    'bigint',
                    'bit',
                    'decimal',
                    'float',
                    'numeric',
                    'real',
                    'smallint',
                    'tinyint',
                ];
            case 'Duration':
                return [
                    'int',
                    'bigint',
                    'bit',
                    'decimal',
                    'float',
                    'numeric',
                    'real',
                    'smallint',
                    'tinyint',
                ];
            case 'Rating':
                return [
                    'int',
                    'bigint',
                    'bit',
                    'decimal',
                    'float',
                    'numeric',
                    'real',
                    'smallint',
                    'tinyint',
                ];
            case 'Formula':
                return ['text', 'ntext', 'varchar', 'nvarchar'];
            case 'Rollup':
                return ['varchar'];
            case 'Count':
                return ['int', 'bigint', 'smallint', 'tinyint'];
            case 'Lookup':
                return ['varchar'];
            case 'Date':
                return ['date'];
            case 'DateTime':
            case 'CreateTime':
            case 'LastModifiedTime':
                return [
                    'datetimeoffset',
                    'datetime2',
                    // 'datetime'
                ];
            case 'AutoNumber':
                return ['int', 'bigint', 'smallint', 'tinyint'];
            case 'Barcode':
                return ['varchar'];
            case 'Geometry':
                return ['geometry'];
            case 'Button':
            default:
                return dbTypes;
        }
    }
    static getUnsupportedFnList() {
        return [];
    }
}
exports.MssqlUi = MssqlUi;
// module.exports = PgUiHelp;
/**
 * @copyright Copyright (c) 2021, Xgene Cloud Ltd
 *
 * @author Naveen MR <oof1lab@gmail.com>
 * @author Pranav C Balan <pranavxc@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTXNzcWxVaS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9saWIvc3FsVWkvTXNzcWxVaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSx5REFBaUM7QUFHakMsTUFBTSxPQUFPLEdBQUc7SUFDZCxRQUFRO0lBQ1IsUUFBUTtJQUNSLEtBQUs7SUFDTCxNQUFNO0lBQ04sTUFBTTtJQUNOLFVBQVU7SUFDVixXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLFNBQVM7SUFDVCxPQUFPO0lBQ1AsV0FBVztJQUNYLFVBQVU7SUFDVixhQUFhO0lBQ2IsT0FBTztJQUNQLEtBQUs7SUFDTCxPQUFPO0lBQ1AsT0FBTztJQUNQLE9BQU87SUFDUCxTQUFTO0lBQ1QsVUFBVTtJQUNWLE1BQU07SUFDTixNQUFNO0lBQ04sZUFBZTtJQUNmLFVBQVU7SUFDVixZQUFZO0lBQ1osYUFBYTtJQUNiLFNBQVM7SUFDVCxNQUFNO0lBQ04sTUFBTTtJQUNOLFdBQVc7SUFDWCxTQUFTO0lBQ1Qsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxLQUFLO0lBQ0wsU0FBUztDQUNWLENBQUM7QUFFRixNQUFhLE9BQU87SUFDbEIsTUFBTSxDQUFDLGtCQUFrQjtRQUN2QixPQUFPO1lBQ0w7Z0JBQ0UsV0FBVyxFQUFFLElBQUk7Z0JBQ2pCLEtBQUssRUFBRSxJQUFJO2dCQUNYLEVBQUUsRUFBRSxLQUFLO2dCQUNULEdBQUcsRUFBRSxTQUFTO2dCQUNkLEVBQUUsRUFBRSxTQUFTO2dCQUNiLElBQUksRUFBRSxLQUFLO2dCQUNYLEdBQUcsRUFBRSxJQUFJO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxJQUFJO2dCQUNSLEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxJQUFJO2dCQUNSLEdBQUcsRUFBRSxJQUFJO2dCQUNULElBQUksRUFBRSxJQUFJO2dCQUNWLEVBQUUsRUFBRSxJQUFJO2dCQUNSLEVBQUUsRUFBRSxDQUFDO2dCQUNMLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksRUFBRSxFQUFFO2dCQUNSLE9BQU8sRUFBRSxDQUFDO2dCQUNWLElBQUksRUFBRSxJQUFJO2dCQUNWLEdBQUcsRUFBRSxFQUFFO2dCQUNQLElBQUksRUFBRSxFQUFFO2FBQ1Q7WUFDRDtnQkFDRSxXQUFXLEVBQUUsT0FBTztnQkFDcEIsS0FBSyxFQUFFLE9BQU87Z0JBQ2QsRUFBRSxFQUFFLFNBQVM7Z0JBQ2IsR0FBRyxFQUFFLGNBQWM7Z0JBQ25CLEVBQUUsRUFBRSxhQUFhO2dCQUNqQixJQUFJLEVBQUUsSUFBSTtnQkFDVixHQUFHLEVBQUUsS0FBSztnQkFDVixFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxHQUFHLEVBQUUsSUFBSTtnQkFDVCxJQUFJLEVBQUUsRUFBRTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixJQUFJLEVBQUUsSUFBSTtnQkFDVixJQUFJLEVBQUUsRUFBRTtnQkFDUixPQUFPLEVBQUUsQ0FBQztnQkFDVixJQUFJLEVBQUUsZ0JBQWdCO2dCQUN0QixHQUFHLEVBQUUsRUFBRTtnQkFDUCxJQUFJLEVBQUUsRUFBRTthQUNUO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLFlBQVk7Z0JBQ3pCLEtBQUssRUFBRSxXQUFXO2dCQUNsQixFQUFFLEVBQUUsVUFBVTtnQkFDZCxHQUFHLEVBQUUsY0FBYztnQkFDbkIsRUFBRSxFQUFFLGFBQWE7Z0JBQ2pCLElBQUksRUFBRSxJQUFJO2dCQUNWLEdBQUcsRUFBRSxLQUFLO2dCQUNWLEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEdBQUcsRUFBRSxXQUFXO2dCQUNoQixJQUFJLEVBQUUsRUFBRTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixPQUFPLEVBQUUsQ0FBQztnQkFDVixJQUFJLEVBQUUsaUJBQU8sQ0FBQyxRQUFRO2dCQUN0QixHQUFHLEVBQUUsRUFBRTtnQkFDUCxJQUFJLEVBQUUsRUFBRTthQUNUO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLFlBQVk7Z0JBQ3pCLEtBQUssRUFBRSxXQUFXO2dCQUNsQixFQUFFLEVBQUUsVUFBVTtnQkFDZCxHQUFHLEVBQUUsY0FBYztnQkFDbkIsRUFBRSxFQUFFLGFBQWE7Z0JBQ2pCLElBQUksRUFBRSxJQUFJO2dCQUNWLEdBQUcsRUFBRSxLQUFLO2dCQUNWLEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxLQUFLO2dCQUNULEVBQUUsRUFBRSxJQUFJO2dCQUNSLEdBQUcsRUFBRSxXQUFXO2dCQUNoQixJQUFJLEVBQUUsRUFBRTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixPQUFPLEVBQUUsQ0FBQztnQkFDVixJQUFJLEVBQUUsaUJBQU8sQ0FBQyxRQUFRO2dCQUN0QixHQUFHLEVBQUUsRUFBRTtnQkFDUCxJQUFJLEVBQUUsRUFBRTthQUNUO1NBQ0YsQ0FBQztJQUNKLENBQUM7SUFFRCxNQUFNLENBQUMsWUFBWSxDQUFDLE1BQU07UUFDeEIsT0FBTztZQUNMLFdBQVcsRUFBRSxPQUFPLEdBQUcsTUFBTTtZQUM3QixFQUFFLEVBQUUsU0FBUztZQUNiLEdBQUcsRUFBRSxjQUFjO1lBQ25CLEVBQUUsRUFBRSxhQUFhO1lBQ2pCLElBQUksRUFBRSxJQUFJO1lBQ1YsR0FBRyxFQUFFLEtBQUs7WUFDVixFQUFFLEVBQUUsS0FBSztZQUNULEVBQUUsRUFBRSxLQUFLO1lBQ1QsRUFBRSxFQUFFLEtBQUs7WUFDVCxFQUFFLEVBQUUsS0FBSztZQUNULEdBQUcsRUFBRSxJQUFJO1lBQ1QsSUFBSSxFQUFFLEVBQUU7WUFDUixFQUFFLEVBQUUsSUFBSTtZQUNSLEVBQUUsRUFBRSxJQUFJO1lBQ1IsSUFBSSxFQUFFLElBQUk7WUFDVixJQUFJLEVBQUUsRUFBRTtZQUNSLE9BQU8sRUFBRSxDQUFDO1lBQ1YsSUFBSSxFQUFFLGdCQUFnQjtZQUN0QixHQUFHLEVBQUUsRUFBRTtZQUNQLElBQUksRUFBRSxFQUFFO1NBQ1QsQ0FBQztJQUNKLENBQUM7SUFFRCw2Q0FBNkM7SUFDN0Msb0JBQW9CO0lBQ3BCLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLEVBQUU7SUFDRix3QkFBd0I7SUFDeEIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixxQkFBcUI7SUFDckIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZix1QkFBdUI7SUFDdkIsd0JBQXdCO0lBQ3hCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixlQUFlO0lBQ2Ysc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysb0JBQW9CO0lBQ3BCLG9CQUFvQjtJQUNwQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLG9CQUFvQjtJQUNwQixlQUFlO0lBQ2Ysd0JBQXdCO0lBQ3hCLHNCQUFzQjtJQUN0QixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLDhCQUE4QjtJQUM5QixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLDhCQUE4QjtJQUM5QixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLDhCQUE4QjtJQUM5QixtQkFBbUI7SUFDbkIsMkJBQTJCO0lBQzNCLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixFQUFFO0lBQ0YsTUFBTTtJQUNOLEVBQUU7SUFDRixJQUFJO0lBRUosTUFBTSxDQUFDLDJCQUEyQixDQUFDLElBQUk7UUFDckMsUUFBUSxJQUFJLEVBQUU7WUFDWixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLGdCQUFnQjtnQkFDbkIsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLGFBQWE7Z0JBQ2hCLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxPQUFPO2dCQUNWLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxLQUFLO2dCQUNSLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxPQUFPO2dCQUNWLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxPQUFPO2dCQUNWLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxPQUFPO2dCQUNWLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxTQUFTO2dCQUNaLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVO2dCQUNiLE9BQU8sR0FBRyxDQUFDO1lBRWIsS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxNQUFNO2dCQUNULE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxlQUFlO2dCQUNsQixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssVUFBVTtnQkFDYixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssWUFBWTtnQkFDZixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssYUFBYTtnQkFDaEIsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLGtCQUFrQjtnQkFDckIsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxFQUFFLENBQUM7WUFFWjtnQkFDRSxPQUFPLEVBQUUsQ0FBQztTQUNiO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQywwQkFBMEIsQ0FBQyxJQUFJO1FBQ3BDLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxVQUFVO2dCQUNiLE9BQU8sS0FBSyxDQUFDO1lBRWYsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUssS0FBSyxDQUFDO1lBQ1gsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssZ0JBQWdCLENBQUM7WUFDdEIsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssYUFBYSxDQUFDO1lBQ25CLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLGVBQWUsQ0FBQztZQUNyQixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLFlBQVksQ0FBQztZQUNsQixLQUFLLGFBQWEsQ0FBQztZQUNuQixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssa0JBQWtCLENBQUM7WUFDeEIsS0FBSyxXQUFXLENBQUM7WUFDakIsS0FBSyxLQUFLO2dCQUNSLE9BQU8sSUFBSSxDQUFDO1lBRWQsS0FBSyxTQUFTO2dCQUNaLE9BQU8sS0FBSyxDQUFDO1lBRWY7Z0JBQ0UsT0FBTyxJQUFJLENBQUM7U0FDZjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsMEJBQTBCLENBQUMsSUFBSTtRQUNwQyxRQUFRLElBQUksRUFBRTtZQUNaLEtBQUssUUFBUTtnQkFDWCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxLQUFLO2dCQUNSLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssV0FBVztnQkFDZCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLGdCQUFnQjtnQkFDbkIsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxTQUFTO2dCQUNaLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssT0FBTztnQkFDVixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFdBQVc7Z0JBQ2QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssYUFBYTtnQkFDaEIsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssS0FBSztnQkFDUixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssT0FBTztnQkFDVixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxlQUFlO2dCQUNsQixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxZQUFZO2dCQUNmLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssYUFBYTtnQkFDaEIsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxTQUFTO2dCQUNaLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssU0FBUztnQkFDWixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLGtCQUFrQjtnQkFDckIsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sTUFBTSxDQUFDO1lBRWhCLEtBQUssS0FBSztnQkFDUixPQUFPLE1BQU0sQ0FBQztZQUVoQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxNQUFNLENBQUM7WUFFaEI7Z0JBQ0UsT0FBTyxFQUFFLENBQUM7U0FDYjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsMEJBQTBCLENBQUMsSUFBSTtRQUNwQyxRQUFRLElBQUksRUFBRTtZQUNaLEtBQUssUUFBUTtnQkFDWCxPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssUUFBUTtnQkFDWCxPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssS0FBSztnQkFDUixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssTUFBTTtnQkFDVCxPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssTUFBTTtnQkFDVCxPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssVUFBVTtnQkFDYixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssV0FBVztnQkFDZCxPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssZ0JBQWdCO2dCQUNuQixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssU0FBUztnQkFDWixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssT0FBTztnQkFDVixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssV0FBVztnQkFDZCxPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssVUFBVTtnQkFDYixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssYUFBYTtnQkFDaEIsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxFQUFFLENBQUM7WUFFWixLQUFLLGVBQWU7Z0JBQ2xCLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxVQUFVO2dCQUNiLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxZQUFZO2dCQUNmLE9BQU8sRUFBRSxDQUFDO1lBRVosS0FBSyxhQUFhO2dCQUNoQixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssU0FBUztnQkFDWixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssTUFBTTtnQkFDVCxPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssTUFBTTtnQkFDVCxPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssV0FBVztnQkFDZCxPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssU0FBUztnQkFDWixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssa0JBQWtCO2dCQUNyQixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssV0FBVztnQkFDZCxPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssS0FBSztnQkFDUixPQUFPLEVBQUUsQ0FBQztZQUVaLEtBQUssU0FBUztnQkFDWixPQUFPLEVBQUUsQ0FBQztZQUVaO2dCQUNFLE9BQU8sRUFBRSxDQUFDO1NBQ2I7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsRUFBRSxPQUFPO1FBQ25DLG9CQUFvQjtRQUNwQixJQUNFLEdBQUcsQ0FBQyxFQUFFLEtBQUssTUFBTTtZQUNqQixHQUFHLENBQUMsRUFBRSxLQUFLLFNBQVM7WUFDcEIsR0FBRyxDQUFDLEVBQUUsS0FBSyxRQUFRO1lBQ25CLEdBQUcsQ0FBQyxFQUFFLEtBQUssVUFBVSxFQUNyQjtZQUNBLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxFQUFFO2dCQUN2QyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssR0FBRyxDQUFDLEVBQUUsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO29CQUM3QyxPQUFPLElBQUksQ0FBQztpQkFDYjthQUNGO1lBQ0QsT0FBTyxLQUFLLENBQUM7U0FDZDthQUFNO1lBQ0wsT0FBTyxJQUFJLENBQUM7U0FDYjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsaUJBQWlCLENBQUMsSUFBSTtRQUMzQixvQkFBb0I7UUFDcEIsT0FBTyxJQUFJLENBQUM7UUFDWiwwQkFBMEI7UUFDMUIsNEJBQTRCO1FBQzVCLDZCQUE2QjtRQUM3Qiw4QkFBOEI7UUFDOUIsMkJBQTJCO1FBQzNCLGtCQUFrQjtRQUNsQixXQUFXO1FBQ1gsaUJBQWlCO1FBQ2pCLElBQUk7SUFDTixDQUFDO0lBRUQsTUFBTSxDQUFDLGtCQUFrQixDQUFDLEdBQUc7UUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNqQixJQUNFLEdBQUcsQ0FBQyxFQUFFLEtBQUssS0FBSztZQUNoQixHQUFHLENBQUMsRUFBRSxLQUFLLFFBQVE7WUFDbkIsR0FBRyxDQUFDLEVBQUUsS0FBSyxVQUFVO1lBQ3JCLEdBQUcsQ0FBQyxFQUFFLEtBQUssU0FBUyxFQUNwQjtZQUNBLEdBQUcsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUM7U0FDaEM7UUFFRCxpQkFBaUI7UUFDakIsNkJBQTZCO1FBQzdCLFdBQVc7UUFDWCxpQkFBaUI7UUFDakIsSUFBSTtJQUNOLENBQUM7SUFFRCxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQVU7UUFDekIsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxPQUFPO1FBQzNCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ3ZDLElBQ0UsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sS0FBSyxDQUFDO2dCQUN4QixDQUFDLENBQ0MsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxLQUFLO29CQUN2QixPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLFFBQVE7b0JBQzFCLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssU0FBUztvQkFDM0IsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxVQUFVO29CQUM1QixPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLFdBQVcsQ0FDOUIsRUFDRDtnQkFDQSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQztnQkFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDM0Q7WUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUM1QjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU07UUFDMUIsT0FBTyxNQUFNLENBQUMsRUFBRSxLQUFLLGFBQWEsSUFBSSxNQUFNLENBQUMsRUFBRSxLQUFLLGVBQWUsQ0FBQztJQUN0RSxDQUFDO0lBRUQsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEtBQUs7UUFDOUIsTUFBTSxHQUFHLEdBQ1AsMkVBQTJFLENBQUM7UUFDOUUsTUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMvQixPQUFPLEtBQUssSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUVELE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLO1FBQy9CLE1BQU0sR0FBRyxHQUNQLDRFQUE0RSxDQUFDO1FBQy9FLE1BQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDL0IsT0FBTyxLQUFLLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCxNQUFNLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxPQUFPO1FBQ3BDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFcEIsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUN0QyxNQUFNLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLHFDQUFxQztZQUNyQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDcEMsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNyQixPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7YUFDdEQ7U0FDRjthQUFNLElBQUksTUFBTSxLQUFLLFNBQVMsRUFBRTtZQUMvQixPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1lBQ3JFLE1BQU0sR0FBRyxDQUFDLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7U0FDbkM7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNoQixDQUFDO0lBRUQsTUFBTSxDQUFDLFlBQVksQ0FBQyxLQUFLO1FBQ3ZCOzs7V0FHRztRQUNILE9BQU8sS0FBSyxDQUFDLEtBQUssQ0FBQyx5Q0FBeUMsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsYUFBYSxDQUFDLElBQUk7UUFDaEIsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFckMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzdCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsWUFBWSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQztTQUN6RDthQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNwQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLFlBQVksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUM7U0FDekQ7YUFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDcEMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxZQUFZLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1NBQ3pEO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7U0FDbkI7UUFFRCxPQUFPLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRCxNQUFNLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLEVBQUU7UUFDaEMsTUFBTSxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBRW5CLElBQUk7WUFDRixJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3BELE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQy9CLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxFQUFFO29CQUNwQyxNQUFNLE1BQU0sR0FBRzt3QkFDYixFQUFFLEVBQUUsSUFBSTt3QkFDUixFQUFFO3dCQUNGLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO3dCQUNwQixHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDWixFQUFFLEVBQUUsRUFBRTt3QkFDTixFQUFFLEVBQUUsQ0FBQzt3QkFDTCxJQUFJLEVBQUUsSUFBSTt3QkFDVixHQUFHLEVBQUUsQ0FBQzt3QkFDTixFQUFFLEVBQUUsS0FBSzt3QkFDVCxJQUFJLEVBQUUsS0FBSzt3QkFDWCxHQUFHLEVBQUUsS0FBSzt3QkFDVixFQUFFLEVBQUUsS0FBSzt3QkFDVCxFQUFFLEVBQUUsa0JBQWtCO3dCQUN0QixFQUFFLEVBQUUsS0FBSzt3QkFDVCxNQUFNLEVBQUUsS0FBSzt3QkFDYixHQUFHLEVBQUUsSUFBSTt3QkFDVCxFQUFFLEVBQUUsRUFBRTt3QkFDTixHQUFHLEVBQUUsSUFBSTt3QkFDVCxHQUFHLEVBQUUsY0FBYzt3QkFDbkIsSUFBSSxFQUFFLElBQUk7d0JBQ1YsSUFBSSxFQUFFLENBQUM7d0JBQ1AsT0FBTyxFQUFFLENBQUM7cUJBQ1gsQ0FBQztvQkFFRixRQUFRLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO3dCQUM1QixLQUFLLFFBQVE7NEJBQ1gsSUFBSSxNQUFNLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dDQUNuQyxJQUFJLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0NBQ3BELE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO3dDQUNwQixFQUFFLEVBQUUsV0FBVztxQ0FDaEIsQ0FBQyxDQUFDO2lDQUNKO3FDQUFNO29DQUNMLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO3dDQUNwQixFQUFFLEVBQUUsS0FBSzt3Q0FDVCxFQUFFLEVBQUUsRUFBRTt3Q0FDTixFQUFFLEVBQUUsQ0FBQztxQ0FDTixDQUFDLENBQUM7aUNBQ0o7NkJBQ0Y7aUNBQU07Z0NBQ0wsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0NBQ3BCLEVBQUUsRUFBRSxPQUFPO29DQUNYLEVBQUUsRUFBRSxFQUFFO29DQUNOLEVBQUUsRUFBRSxDQUFDO29DQUNMLElBQUksRUFBRSxJQUFJO29DQUNWLElBQUksRUFBRSxDQUFDO2lDQUNSLENBQUMsQ0FBQzs2QkFDSjs0QkFDRCxNQUFNO3dCQUNSLEtBQUssUUFBUTs0QkFDWCxJQUFJLE9BQU8sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0NBQ3RDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO29DQUNwQixFQUFFLEVBQUUsVUFBVTtpQ0FDZixDQUFDLENBQUM7NkJBQ0o7aUNBQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTtnQ0FDdEMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0NBQ3BCLEVBQUUsRUFBRSxTQUFTO29DQUNiLEVBQUUsRUFBRSxHQUFHO29DQUNQLEVBQUUsRUFBRSxDQUFDO29DQUNMLElBQUksRUFBRSxLQUFLO2lDQUNaLENBQUMsQ0FBQzs2QkFDSjtpQ0FBTTtnQ0FDTCxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRTtvQ0FDcEIsRUFBRSxFQUFFLE1BQU07aUNBQ1gsQ0FBQyxDQUFDOzZCQUNKOzRCQUNELE1BQU07d0JBQ1IsS0FBSyxTQUFTOzRCQUNaLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO2dDQUNwQixFQUFFLEVBQUUsS0FBSztnQ0FDVCxFQUFFLEVBQUUsSUFBSTtnQ0FDUixFQUFFLEVBQUUsQ0FBQzs2QkFDTixDQUFDLENBQUM7NEJBQ0gsTUFBTTt3QkFDUixLQUFLLFFBQVE7NEJBQ1gsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7Z0NBQ3BCLEVBQUUsRUFBRSxTQUFTO2dDQUNiLEVBQUUsRUFBRSxHQUFHO2dDQUNQLEVBQUUsRUFBRSxDQUFDO2dDQUNMLElBQUksRUFBRSxLQUFLOzZCQUNaLENBQUMsQ0FBQzs0QkFDSCxNQUFNO3dCQUNSOzRCQUNFLE1BQU07cUJBQ1Q7b0JBQ0QsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDdEI7YUFDRjtTQUNGO1FBQUMsT0FBTyxDQUFDLEVBQUU7WUFDVixPQUFPLENBQUMsR0FBRyxDQUFDLDZCQUE2QixFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQy9DO1FBRUQsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQztJQUVELE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsS0FBSztRQUNoQyxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtZQUM3QixPQUFPLEtBQUssQ0FBQztTQUNkO1FBQ0QsT0FBTyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLElBQUksbUJBQW1CLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3hFLENBQUM7SUFFRCxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUs7UUFDdEIsT0FBTyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVELE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakIsV0FBVztRQUNYLEdBQUcsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUM7UUFDL0IsSUFBSTtRQUNKLElBQUksR0FBRyxDQUFDLEVBQUUsRUFBRTtZQUNWLEdBQUcsQ0FBQyxHQUFHLEdBQUcsV0FBVyxDQUFDO1NBQ3ZCO1FBQ0QsaUJBQWlCO1FBQ2pCLDZCQUE2QjtRQUM3QixXQUFXO1FBQ1gsaUJBQWlCO1FBQ2pCLElBQUk7SUFDTixDQUFDO0lBRUQsTUFBTSxDQUFDLGlCQUFpQixDQUFDLEdBQUc7UUFDMUIsSUFBSSxHQUFHLENBQUMsT0FBTyxLQUFLLENBQUMsRUFBRTtZQUNyQixPQUFPLElBQUksQ0FBQztTQUNiO1FBRUQsUUFBUSxHQUFHLENBQUMsRUFBRSxFQUFFO1lBQ2QsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLGdCQUFnQixDQUFDO1lBQ3RCLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxXQUFXO2dCQUNkLE9BQU8sS0FBSyxDQUFDO1lBRWY7Z0JBQ0UsT0FBTyxJQUFJLENBQUM7U0FDZjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsZUFBZSxDQUFDLEdBQUc7UUFDeEIsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFdBQVcsRUFBRSxFQUFFO1lBQ3hDLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssS0FBSztnQkFDUixPQUFPLFNBQVMsQ0FBQztZQUVuQixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxRQUFRLENBQUM7WUFFbEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sUUFBUSxDQUFDO1lBRWxCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLGVBQWUsQ0FBQztZQUNyQixLQUFLLGdCQUFnQjtnQkFDbkIsT0FBTyxVQUFVLENBQUM7WUFDcEIsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxPQUFPLENBQUM7WUFFakIsS0FBSyxXQUFXLENBQUM7WUFDakIsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxhQUFhLENBQUM7WUFDbkIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sUUFBUSxDQUFDO1lBRWxCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxPQUFPO2dCQUNWLE9BQU8sUUFBUSxDQUFDO1lBRWxCLEtBQUssT0FBTztnQkFDVixPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxPQUFPLENBQUM7WUFDakIsS0FBSyxVQUFVO2dCQUNiLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE9BQU8sQ0FBQztZQUVqQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFFaEIsS0FBSyxZQUFZLENBQUM7WUFDbEIsS0FBSyxhQUFhLENBQUM7WUFDbkIsS0FBSyxTQUFTO2dCQUNaLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFDaEIsS0FBSyxXQUFXO2dCQUNkLE9BQU8sV0FBVyxDQUFDO1lBRXJCLEtBQUssa0JBQWtCLENBQUM7WUFDeEIsS0FBSyxXQUFXLENBQUM7WUFDakIsS0FBSyxLQUFLO2dCQUNSLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssU0FBUztnQkFDWixPQUFPLFFBQVEsQ0FBQztTQUNuQjtRQUNELE9BQU8sUUFBUSxDQUFDO0lBQ2xCLENBQUM7SUFFRCxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUc7UUFDbEIsUUFBUSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ2pDLEtBQUssU0FBUztnQkFDWixPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxVQUFVLENBQUM7WUFDcEIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sU0FBUyxDQUFDO1lBQ25CLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxZQUFZLENBQUM7WUFDdEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBQ2hCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxnQkFBZ0IsQ0FBQztZQUMxQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxVQUFVLENBQUM7WUFDcEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sWUFBWSxDQUFDO1lBQ3RCLEtBQUssTUFBTTtnQkFDVCxPQUFPLGNBQWMsQ0FBQztZQUN4QixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxhQUFhLENBQUM7WUFDdkIsS0FBSyxNQUFNO2dCQUNULE9BQU8sVUFBVSxDQUFDO1NBQ3JCO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxvQkFBb0IsQ0FDekIsR0FBc0IsRUFDdEIsTUFBZTtRQUtmLE1BQU0sT0FBTyxHQUFRLEVBQUUsQ0FBQztRQUN4QixRQUFRLEdBQUcsQ0FBQyxJQUFJLEVBQUU7WUFDaEIsS0FBSyxJQUFJO2dCQUNQO29CQUNFLE1BQU0sV0FBVyxHQUFHLE1BQU0sS0FBSyxJQUFJLENBQUM7b0JBQ3BDLE1BQU0sV0FBVyxHQUFHLE1BQU0sS0FBSyxJQUFJLENBQUM7b0JBQ3BDLE9BQU8sQ0FBQyxFQUFFLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztvQkFDN0MsT0FBTyxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUM7b0JBQ2xCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsV0FBVyxDQUFDO29CQUN6QixPQUFPLENBQUMsRUFBRSxHQUFHLFdBQVcsQ0FBQztvQkFDekIsT0FBTyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUM7b0JBQ25CLE9BQU8sQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO2lCQUN2RDtnQkFDRCxNQUFNO1lBQ1IsS0FBSyxZQUFZO2dCQUNmLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxnQkFBZ0I7Z0JBQ25CLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxVQUFVO2dCQUNiLE9BQU8sQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDO2dCQUNwQixNQUFNO1lBQ1IsS0FBSyxZQUFZO2dCQUNmLE9BQU8sQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDO2dCQUNwQixNQUFNO1lBQ1IsS0FBSyxVQUFVO2dCQUNiLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixPQUFPLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQztnQkFDakIsTUFBTTtZQUNSLEtBQUssYUFBYTtnQkFDaEIsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUixLQUFLLGNBQWM7Z0JBQ2pCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDO2dCQUNwQixNQUFNO1lBQ1IsS0FBSyxjQUFjO2dCQUNqQixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsTUFBTTtZQUNSLEtBQUssTUFBTTtnQkFDVCxPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFFdkIsTUFBTTtZQUNSLEtBQUssTUFBTTtnQkFDVCxPQUFPLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQztnQkFDbkIsTUFBTTtZQUNSLEtBQUssTUFBTTtnQkFDVCxPQUFPLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQztnQkFDcEIsTUFBTTtZQUNSLEtBQUssYUFBYTtnQkFDaEIsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE9BQU8sQ0FBQyxRQUFRLEdBQUc7b0JBQ2pCLElBQUksRUFBRSxDQUFDLGVBQWUsQ0FBQztvQkFDdkIsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDO29CQUNWLEdBQUcsRUFBRSxDQUFDLG1DQUFtQyxDQUFDO2lCQUMzQyxDQUFDO2dCQUNGLE1BQU07WUFDUixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE9BQU8sQ0FBQyxRQUFRLEdBQUc7b0JBQ2pCLElBQUksRUFBRSxDQUFDLFNBQVMsQ0FBQztvQkFDakIsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDO29CQUNWLEdBQUcsRUFBRSxDQUFDLDZCQUE2QixDQUFDO2lCQUNyQyxDQUFDO2dCQUNGLE1BQU07WUFDUixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE9BQU8sQ0FBQyxRQUFRLEdBQUc7b0JBQ2pCLElBQUksRUFBRSxDQUFDLE9BQU8sQ0FBQztvQkFDZixJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBQ1YsR0FBRyxFQUFFLENBQUMsMkJBQTJCLENBQUM7aUJBQ25DLENBQUM7Z0JBQ0YsTUFBTTtZQUNSLEtBQUssUUFBUTtnQkFDWCxPQUFPLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQztnQkFDbkIsTUFBTTtZQUNSLEtBQUssU0FBUztnQkFDWixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsTUFBTTtZQUNSLEtBQUssVUFBVTtnQkFDYixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLFFBQVEsR0FBRztvQkFDakIsSUFBSSxFQUFFLENBQUMsWUFBWSxDQUFDO29CQUNwQixJQUFJLEVBQUUsQ0FBQyxFQUFFLENBQUM7b0JBQ1YsR0FBRyxFQUFFLENBQUMsZ0NBQWdDLENBQUM7aUJBQ3hDLENBQUM7Z0JBQ0YsTUFBTTtZQUNSLEtBQUssU0FBUztnQkFDWixPQUFPLENBQUMsRUFBRSxHQUFHLFFBQVEsQ0FBQztnQkFDdEIsTUFBTTtZQUNSLEtBQUssVUFBVTtnQkFDYixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsTUFBTTtZQUNSLEtBQUssUUFBUTtnQkFDWCxPQUFPLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQztnQkFDbkIsTUFBTTtZQUNSLEtBQUssU0FBUztnQkFDWixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsTUFBTTtZQUNSLEtBQUssUUFBUTtnQkFDWCxPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsTUFBTTtZQUNSLEtBQUssT0FBTztnQkFDVixPQUFPLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQztnQkFDbkIsTUFBTTtZQUNSLEtBQUssUUFBUTtnQkFDWCxPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsTUFBTTtZQUNSLEtBQUssVUFBVTtnQkFDYixPQUFPLENBQUMsRUFBRSxHQUFHLGdCQUFnQixDQUFDO2dCQUM5QixNQUFNO1lBQ1IsS0FBSyxZQUFZO2dCQUNmLE9BQU8sQ0FBQyxFQUFFLEdBQUcsVUFBVSxDQUFDO2dCQUN4QixNQUFNO1lBQ1IsS0FBSyxrQkFBa0I7Z0JBQ3JCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsVUFBVSxDQUFDO2dCQUN4QixNQUFNO1lBQ1IsS0FBSyxZQUFZO2dCQUNmLE9BQU8sQ0FBQyxFQUFFLEdBQUcsS0FBSyxDQUFDO2dCQUNuQixNQUFNO1lBQ1IsS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1IsS0FBSyxRQUFRO2dCQUNYLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixNQUFNO1lBQ1I7Z0JBQ0UsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07U0FDVDtRQUNELE9BQU8sT0FBTyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxNQUFNLENBQUMsd0JBQXdCLENBQUMsR0FBRyxFQUFFLE1BQWU7UUFDbEQsUUFBUSxHQUFHLENBQUMsSUFBSSxFQUFFO1lBQ2hCLEtBQUssSUFBSTtnQkFDUCxJQUFJLE1BQU0sS0FBSyxJQUFJLEVBQUU7b0JBQ25CLE9BQU8sQ0FBQyxNQUFNLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsVUFBVSxDQUFDLENBQUM7aUJBQ3pEO3FCQUFNLElBQUksTUFBTSxLQUFLLElBQUksRUFBRTtvQkFDMUIsT0FBTyxDQUFDLEtBQUssRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxTQUFTLENBQUMsQ0FBQztpQkFDeEQ7cUJBQU07b0JBQ0wsT0FBTyxPQUFPLENBQUM7aUJBQ2hCO1lBQ0gsS0FBSyxZQUFZO2dCQUNmLE9BQU8sT0FBTyxDQUFDO1lBRWpCLEtBQUssZ0JBQWdCLENBQUM7WUFDdEIsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxZQUFZLENBQUM7WUFDbEIsS0FBSyxjQUFjO2dCQUNqQixPQUFPLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBRTFELEtBQUssTUFBTTtnQkFDVCxPQUFPLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRTNCLEtBQUssVUFBVTtnQkFDYixPQUFPLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFFN0MsS0FBSyxhQUFhO2dCQUNoQixPQUFPLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRTNCLEtBQUssY0FBYztnQkFDakIsT0FBTyxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQztZQUUzQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRWpCLEtBQUssTUFBTTtnQkFDVCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFbEIsS0FBSyxhQUFhLENBQUM7WUFDbkIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUVyQixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUU3QixLQUFLLFFBQVE7Z0JBQ1gsT0FBTztvQkFDTCxLQUFLO29CQUNMLFFBQVE7b0JBQ1IsS0FBSztvQkFDTCxTQUFTO29CQUNULE9BQU87b0JBQ1AsU0FBUztvQkFDVCxNQUFNO29CQUNOLFVBQVU7b0JBQ1YsU0FBUztpQkFDVixDQUFDO1lBRUosS0FBSyxTQUFTO2dCQUNaLE9BQU8sQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFFOUIsS0FBSyxVQUFVO2dCQUNiLE9BQU87b0JBQ0wsS0FBSztvQkFDTCxRQUFRO29CQUNSLEtBQUs7b0JBQ0wsU0FBUztvQkFDVCxPQUFPO29CQUNQLFNBQVM7b0JBQ1QsTUFBTTtvQkFDTixVQUFVO29CQUNWLFNBQVM7aUJBQ1YsQ0FBQztZQUVKLEtBQUssU0FBUztnQkFDWixPQUFPO29CQUNMLEtBQUs7b0JBQ0wsUUFBUTtvQkFDUixLQUFLO29CQUNMLFNBQVM7b0JBQ1QsT0FBTztvQkFDUCxTQUFTO29CQUNULE1BQU07b0JBQ04sVUFBVTtvQkFDVixTQUFTO2lCQUNWLENBQUM7WUFFSixLQUFLLFVBQVU7Z0JBQ2IsT0FBTztvQkFDTCxLQUFLO29CQUNMLFFBQVE7b0JBQ1IsS0FBSztvQkFDTCxTQUFTO29CQUNULE9BQU87b0JBQ1AsU0FBUztvQkFDVCxNQUFNO29CQUNOLFVBQVU7b0JBQ1YsU0FBUztpQkFDVixDQUFDO1lBRUosS0FBSyxRQUFRO2dCQUNYLE9BQU87b0JBQ0wsS0FBSztvQkFDTCxRQUFRO29CQUNSLEtBQUs7b0JBQ0wsU0FBUztvQkFDVCxPQUFPO29CQUNQLFNBQVM7b0JBQ1QsTUFBTTtvQkFDTixVQUFVO29CQUNWLFNBQVM7aUJBQ1YsQ0FBQztZQUVKLEtBQUssU0FBUztnQkFDWixPQUFPLENBQUMsTUFBTSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFFbEQsS0FBSyxRQUFRO2dCQUNYLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUVyQixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxDQUFDLEtBQUssRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1lBRWxELEtBQUssUUFBUTtnQkFDWCxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7WUFFckIsS0FBSyxNQUFNO2dCQUNULE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUVsQixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLFlBQVksQ0FBQztZQUNsQixLQUFLLGtCQUFrQjtnQkFDckIsT0FBTztvQkFDTCxnQkFBZ0I7b0JBQ2hCLFdBQVc7b0JBQ1gsYUFBYTtpQkFDZCxDQUFDO1lBRUosS0FBSyxZQUFZO2dCQUNmLE9BQU8sQ0FBQyxLQUFLLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxTQUFTLENBQUMsQ0FBQztZQUVsRCxLQUFLLFNBQVM7Z0JBQ1osT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBRXJCLEtBQUssVUFBVTtnQkFDYixPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7WUFFdEIsS0FBSyxRQUFRLENBQUM7WUFDZDtnQkFDRSxPQUFPLE9BQU8sQ0FBQztTQUNsQjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsb0JBQW9CO1FBQ3pCLE9BQU8sRUFBRSxDQUFDO0lBQ1osQ0FBQztDQUNGO0FBeHhDRCwwQkF3eENDO0FBRUQsNkJBQTZCO0FBQzdCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FxQkcifQ==