import { Api, ColumnType, FilterType, HookType, ProjectType, SortType } from './Api';
export interface SigninPayloadType {
    email: string;
    password: string;
}
export interface PasswordForgotPayloadType {
    email?: string;
}
export interface PasswordChangePayloadType {
    currentPassword?: string;
    newPassword?: string;
    verifyPassword?: string;
}
export interface PasswordResetPayloadType {
    new_password?: string;
}
export interface TokenVerifyPayloadType {
    token?: string;
    email?: string;
}
export declare type ProjectUserAddPayloadType = any;
export declare type ProjectUserUpdatePayloadType = any;
export interface ModelVisibilityListParamsType {
    includeM2M?: boolean;
    projectId: string;
}
export declare type ModelVisibilitySetPayloadType = any;
export interface ListParamsType {
    page?: number;
    pageSize?: number;
    sort?: string;
}
export declare type CreatePayloadType = ProjectType & {
    external?: boolean;
};
export interface SharedBaseCreatePayloadType {
    roles?: string;
    password?: string;
}
export interface SharedBaseUpdatePayloadType {
    roles?: string;
    password?: string;
}
export interface UploadPayloadType {
    files?: any;
    json?: string;
}
export interface ListParams7Type {
    page?: number;
    pageSize?: number;
    sort?: string;
    includeM2M?: boolean;
    projectId: string;
}
export interface UpdatePayloadType {
    title?: string;
}
export interface ReorderPayloadType {
    order?: string;
}
export declare type CreateInputType = ColumnType | {
    uidt: 'LinkToAnotherRecord';
    title: string;
    parentId: string;
    childId: string;
    type: 'hm' | 'bt' | 'mm';
};
export interface UpdateInputType {
    order?: string;
    title?: string;
    show_system_fields?: boolean;
    lock_type?: 'collaborative' | 'locked' | 'personal';
}
export declare type CreatePayload8Type = any;
export interface ShowAllColumnParamsType {
    ignoreIds?: any[];
    viewId: string;
}
export interface HideAllColumnParamsType {
    ignoreIds?: any[];
    viewId: string;
}
export declare type UpdatePayload4Type = any;
export interface UpdatePayload7Type {
    password?: string;
}
export declare type CreatePayload3Type = any;
export interface ListParams4Type {
    fields?: any[];
    sort?: any[];
    where?: string;
    orgs: string;
    projectName: string;
    tableAlias: string;
}
export interface CreatePayload9Type {
    description?: string;
}
export interface ListParams10Type {
    fields?: any[];
    sort?: any[];
    where?: string;
    /** Query params for nested data */
    nested?: any;
    orgs: string;
    projectName: string;
    tableAlias: string;
    viewName: string;
}
export declare type CreatePayload5Type = any;
export interface DataListPayloadType {
    password?: string;
    sorts?: SortType[];
    filters?: FilterType[];
}
export interface DataListParamsType {
    limit?: string;
    offset?: string;
    uuid: string;
}
export interface DataNestedListParamsType {
    limit?: string;
    offset?: string;
    uuid: string;
    rowId: string;
    relationType: 'mm' | 'hm';
    columnId: string;
}
export interface DataNestedExcludedListParamsType {
    limit?: string;
    offset?: string;
    uuid: string;
    rowId: string;
    relationType: 'mm' | 'hm';
    columnId: string;
}
export interface DataCreatePayloadType {
    data?: any;
    password?: string;
}
export interface CsvExportBodyType {
    password?: string;
    filters?: FilterType[];
    sorts?: SortType[];
}
export interface DataRelationListPayloadType {
    password?: string;
}
export interface DataRelationListParamsType {
    limit?: string;
    offset?: string;
    uuid: string;
    relationColumnId: string;
}
export interface SharedViewMetaGetPayloadType {
    password?: string;
}
export declare type UpdatePayload10Type = any;
export declare type UpdatePayload9Type = any;
export interface CommentListParamsType {
    row_id: string;
    fk_model_id: string;
    comments_only?: boolean;
}
export interface CommentRowPayloadType {
    row_id: string;
    fk_model_id: string;
    comment: string;
}
export interface CommentCountParamsType {
    ids: any[];
    fk_model_id: string;
}
export interface AuditListParamsType {
    offset?: string;
    limit?: string;
    projectId: string;
}
export interface AuditRowUpdatePayloadType {
    fk_model_id?: string;
    column_name?: string;
    row_id?: string;
    value?: string;
    prev_value?: string;
}
export interface TestPayloadType {
    payload?: {
        data?: any;
        user?: any;
    };
    hook?: HookType;
}
export interface TestBodyType {
    id?: string;
    title?: string;
    input?: any;
    category?: string;
}
export declare type TestConnectionPayloadType = any;
export declare type BulkDeletePayloadType = any[];
export declare type BulkInsertPayloadType = any[];
export interface BulkUpdateAllParamsType {
    where?: string;
    orgs: string;
    projectName: string;
    tableAlias: string;
}
export interface BulkDeleteAllParamsType {
    where?: string;
    orgs: string;
    projectName: string;
    tableAlias: string;
}
declare class CustomAPI extends Api<unknown> {
}
export default CustomAPI;
