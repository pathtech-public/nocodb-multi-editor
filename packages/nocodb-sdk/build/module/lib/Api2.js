/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
export var ContentType;
(function (ContentType) {
    ContentType["Json"] = "application/json";
    ContentType["FormData"] = "multipart/form-data";
    ContentType["UrlEncoded"] = "application/x-www-form-urlencoded";
})(ContentType || (ContentType = {}));
export class HttpClient {
    constructor(apiConfig = {}) {
        this.baseUrl = 'http://localhost:8080';
        this.securityData = null;
        this.abortControllers = new Map();
        this.customFetch = (...fetchParams) => fetch(...fetchParams);
        this.baseApiParams = {
            credentials: 'same-origin',
            headers: {},
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
        };
        this.setSecurityData = (data) => {
            this.securityData = data;
        };
        this.contentFormatters = {
            [ContentType.Json]: (input) => input !== null && (typeof input === 'object' || typeof input === 'string')
                ? JSON.stringify(input)
                : input,
            [ContentType.FormData]: (input) => Object.keys(input || {}).reduce((formData, key) => {
                const property = input[key];
                formData.append(key, property instanceof Blob
                    ? property
                    : typeof property === 'object' && property !== null
                        ? JSON.stringify(property)
                        : `${property}`);
                return formData;
            }, new FormData()),
            [ContentType.UrlEncoded]: (input) => this.toQueryString(input),
        };
        this.createAbortSignal = (cancelToken) => {
            if (this.abortControllers.has(cancelToken)) {
                const abortController = this.abortControllers.get(cancelToken);
                if (abortController) {
                    return abortController.signal;
                }
                return void 0;
            }
            const abortController = new AbortController();
            this.abortControllers.set(cancelToken, abortController);
            return abortController.signal;
        };
        this.abortRequest = (cancelToken) => {
            const abortController = this.abortControllers.get(cancelToken);
            if (abortController) {
                abortController.abort();
                this.abortControllers.delete(cancelToken);
            }
        };
        this.request = (_a) => __awaiter(this, void 0, void 0, function* () {
            var { body, secure, path, type, query, format, baseUrl, cancelToken } = _a, params = __rest(_a, ["body", "secure", "path", "type", "query", "format", "baseUrl", "cancelToken"]);
            const secureParams = ((typeof secure === 'boolean' ? secure : this.baseApiParams.secure) &&
                this.securityWorker &&
                (yield this.securityWorker(this.securityData))) ||
                {};
            const requestParams = this.mergeRequestParams(params, secureParams);
            const queryString = query && this.toQueryString(query);
            const payloadFormatter = this.contentFormatters[type || ContentType.Json];
            const responseFormat = format || requestParams.format;
            return this.customFetch(`${baseUrl || this.baseUrl || ''}${path}${queryString ? `?${queryString}` : ''}`, Object.assign(Object.assign({}, requestParams), { headers: Object.assign(Object.assign({}, (type && type !== ContentType.FormData
                    ? { 'Content-Type': type }
                    : {})), (requestParams.headers || {})), signal: cancelToken ? this.createAbortSignal(cancelToken) : void 0, body: typeof body === 'undefined' || body === null
                    ? null
                    : payloadFormatter(body) })).then((response) => __awaiter(this, void 0, void 0, function* () {
                const r = response;
                r.data = null;
                r.error = null;
                const data = !responseFormat
                    ? r
                    : yield response[responseFormat]()
                        .then((data) => {
                        if (r.ok) {
                            r.data = data;
                        }
                        else {
                            r.error = data;
                        }
                        return r;
                    })
                        .catch((e) => {
                        r.error = e;
                        return r;
                    });
                if (cancelToken) {
                    this.abortControllers.delete(cancelToken);
                }
                if (!response.ok)
                    throw data;
                return data.data;
            }));
        });
        Object.assign(this, apiConfig);
    }
    encodeQueryParam(key, value) {
        const encodedKey = encodeURIComponent(key);
        return `${encodedKey}=${encodeURIComponent(typeof value === 'number' ? value : `${value}`)}`;
    }
    addQueryParam(query, key) {
        return this.encodeQueryParam(key, query[key]);
    }
    addArrayQueryParam(query, key) {
        const value = query[key];
        return value.map((v) => this.encodeQueryParam(key, v)).join('&');
    }
    toQueryString(rawQuery) {
        const query = rawQuery || {};
        const keys = Object.keys(query).filter((key) => 'undefined' !== typeof query[key]);
        return keys
            .map((key) => Array.isArray(query[key])
            ? this.addArrayQueryParam(query, key)
            : this.addQueryParam(query, key))
            .join('&');
    }
    addQueryParams(rawQuery) {
        const queryString = this.toQueryString(rawQuery);
        return queryString ? `?${queryString}` : '';
    }
    mergeRequestParams(params1, params2) {
        return Object.assign(Object.assign(Object.assign(Object.assign({}, this.baseApiParams), params1), (params2 || {})), { headers: Object.assign(Object.assign(Object.assign({}, (this.baseApiParams.headers || {})), (params1.headers || {})), ((params2 && params2.headers) || {})) });
    }
}
/**
 * @title nocodb
 * @version 1.0
 * @baseUrl http://localhost:8080
 */
export class Api extends HttpClient {
    constructor() {
        super(...arguments);
        this.auth = {
            /**
             * @description Create a new user with provided email and password and first user is marked as super admin.
             *
             * @tags Auth
             * @name Signup
             * @summary Signup
             * @request POST:/api/v1/auth/user/signup
             * @response `200` `{ token?: string }` OK
             * @response `400` `{ msg?: string }` Bad Request
             * @response `401` `void` Unauthorized
             * @response `403` `void` Forbidden
             */
            signup: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/user/signup`, method: 'POST', body: data, format: 'json' }, params)),
            /**
             * @description Authenticate existing user with their email and password. Successful login will return a JWT access-token.
             *
             * @tags Auth
             * @name Signin
             * @summary Signin
             * @request POST:/api/v1/auth/user/signin
             * @response `200` `{ token?: string }` OK
             * @response `400` `{ msg?: string }` Bad Request
             */
            signin: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/user/signin`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * @description Returns authenticated user info
             *
             * @tags Auth
             * @name Me
             * @summary User info
             * @request GET:/api/v1/auth/user/me
             * @response `200` `UserInfoType` OK
             */
            me: (query, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/user/me`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * @description Emails user with a reset url.
             *
             * @tags Auth
             * @name PasswordForgot
             * @summary Password forgot
             * @request POST:/api/v1/auth/password/forgot
             * @response `200` `void` OK
             * @response `401` `void` Unauthorized
             */
            passwordForgot: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/password/forgot`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * @description Change password of authenticated user with a new one.
             *
             * @tags Auth
             * @name PasswordChange
             * @summary Password change
             * @request POST:/api/v1/auth/password/change
             * @response `200` `{ msg?: string }` OK
             * @response `400` `{ msg?: string }` Bad request
             */
            passwordChange: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/password/change`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * @description Validtae password reset url token.
             *
             * @tags Auth
             * @name PasswordResetTokenValidate
             * @summary Reset token verify
             * @request POST:/api/v1/auth/token/validate/{token}
             * @response `200` `void` OK
             */
            passwordResetTokenValidate: (token, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/token/validate/${token}`, method: 'POST' }, params)),
            /**
             * @description Api for verifying email where token need to be passed which is shared to user email.
             *
             * @tags Auth
             * @name EmailValidate
             * @summary Verify email
             * @request POST:/api/v1/auth/email/validate/{token}
             * @response `200` `void` OK
             */
            emailValidate: (token, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/email/validate/${token}`, method: 'POST' }, params)),
            /**
             * @description Update user password to new by using reset token.
             *
             * @tags Auth
             * @name PasswordReset
             * @summary Password reset
             * @request POST:/api/v1/auth/password/reset/{token}
             * @response `200` `void` OK
             */
            passwordReset: (token, data, params = {}) => this.request(Object.assign({ path: `/api/v1/auth/password/reset/${token}`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags Auth
             * @name TokenRefresh
             * @summary Refresh token
             * @request POST:/api/v1/auth/token/refresh
             * @response `200` `void` OK
             */
            tokenRefresh: (params = {}) => this.request(Object.assign({ path: `/api/v1/auth/token/refresh`, method: 'POST' }, params)),
            /**
             * No description
             *
             * @tags Auth
             * @name ProjectUserList
             * @summary Project users
             * @request GET:/api/v1/db/meta/projects/{projectId}/users
             * @response `200` `{ users?: { list: (UserType)[], pageInfo: PaginatedType } }` OK
             */
            projectUserList: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/users`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Auth
             * @name ProjectUserAdd
             * @summary Project User Add
             * @request POST:/api/v1/db/meta/projects/{projectId}/users
             * @response `200` `any` OK
             */
            projectUserAdd: (projectId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/users`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Auth
             * @name ProjectUserUpdate
             * @summary Project user update
             * @request PATCH:/api/v1/db/meta/projects/{projectId}/users/{userId}
             * @response `200` `any` OK
             */
            projectUserUpdate: (projectId, userId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/users/${userId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Auth
             * @name ProjectUserRemove
             * @summary Project user remove
             * @request DELETE:/api/v1/db/meta/projects/{projectId}/users/{userId}
             * @response `200` `any` OK
             */
            projectUserRemove: (projectId, userId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/users/${userId}`, method: 'DELETE', format: 'json' }, params)),
            /**
             * @description Resend Invitation to a specific user
             *
             * @tags Auth
             * @name ProjectUserResendInvite
             * @request POST:/api/v1/db/meta/projects/{projectId}/users/{userId}/resend-invite
             * @response `200` `any` OK
             */
            projectUserResendInvite: (projectId, userId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/users/${userId}/resend-invite`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
        };
        this.project = {
            /**
             * No description
             *
             * @tags Project
             * @name MetaGet
             * @summary Project info
             * @request GET:/api/v1/db/meta/projects/{projectId}/info
             * @response `200` `{ Node?: string, Arch?: string, Platform?: string, Docker?: boolean, Database?: string, ProjectOnRootDB?: string, RootDB?: string, PackageVersion?: string }` OK
             */
            metaGet: (projectId, params = {}, query) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/info`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name ModelVisibilityList
             * @summary UI ACL
             * @request GET:/api/v1/db/meta/projects/{projectId}/visibility-rules
             * @response `200` `(any)[]` OK
             */
            modelVisibilityList: (projectId, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/visibility-rules`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name ModelVisibilitySet
             * @request POST:/api/v1/db/meta/projects/{projectId}/visibility-rules
             * @response `200` `any` OK
             */
            modelVisibilitySet: (projectId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/visibility-rules`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * @description Read project details
             *
             * @tags Project
             * @name List
             * @summary Project list
             * @request GET:/api/v1/db/meta/projects/
             * @response `201` `ProjectListType`
             */
            list: (query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/`, method: 'GET', query: query }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name Create
             * @summary Project create
             * @request POST:/api/v1/db/meta/projects/
             * @response `200` `ProjectType` OK
             */
            create: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * @description Read project details
             *
             * @tags Project
             * @name Read
             * @summary Project read
             * @request GET:/api/v1/db/meta/projects/{projectId}
             * @response `200` `object` OK
             */
            read: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name Delete
             * @summary Project delete
             * @request DELETE:/api/v1/db/meta/projects/{projectId}
             * @response `200` `void` OK
             */
            delete: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}`, method: 'DELETE' }, params)),
            /**
             * @description Read project details
             *
             * @tags Project
             * @name SharedBaseGet
             * @request GET:/api/v1/db/meta/projects/{projectId}/shared
             * @response `200` `{ uuid?: string, url?: string }` OK
             */
            sharedBaseGet: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/shared`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name SharedBaseDisable
             * @request DELETE:/api/v1/db/meta/projects/{projectId}/shared
             * @response `200` `void` OK
             */
            sharedBaseDisable: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/shared`, method: 'DELETE' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name SharedBaseCreate
             * @request POST:/api/v1/db/meta/projects/{projectId}/shared
             * @response `200` `{ url?: string, uuid?: string }` OK
             */
            sharedBaseCreate: (projectId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/shared`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name SharedBaseUpdate
             * @request PATCH:/api/v1/db/meta/projects/{projectId}/shared
             * @response `200` `void` OK
             */
            sharedBaseUpdate: (projectId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/shared`, method: 'PATCH', body: data, type: ContentType.Json }, params)),
            /**
             * @description Project compare cost
             *
             * @tags Project
             * @name Cost
             * @summary Project compare cost
             * @request GET:/api/v1/db/meta/projects/{projectId}/cost
             * @response `200` `object` OK
             */
            cost: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/cost`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name MetaDiffSync
             * @request POST:/api/v1/db/meta/projects/{projectId}/meta-diff
             * @response `200` `any` OK
             */
            metaDiffSync: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/meta-diff`, method: 'POST', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name MetaDiffGet
             * @request GET:/api/v1/db/meta/projects/{projectId}/meta-diff
             * @response `200` `any` OK
             */
            metaDiffGet: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/meta-diff`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Project
             * @name AuditList
             * @request GET:/api/v1/db/meta/projects/{projectId}/audits
             * @response `200` `{ list: (AuditType)[], pageInfo: PaginatedType }` OK
             */
            auditList: (projectId, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/audits`, method: 'GET', query: query, format: 'json' }, params)),
        };
        this.dbTable = {
            /**
             * No description
             *
             * @tags DB table
             * @name Create
             * @request POST:/api/v1/db/meta/projects/{projectId}/tables
             * @response `200` `TableType` OK
             */
            create: (projectId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/tables`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table
             * @name List
             * @request GET:/api/v1/db/meta/projects/{projectId}/tables
             * @response `200` `TableListType`
             */
            list: (projectId, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/tables`, method: 'GET', query: query }, params)),
            /**
             * No description
             *
             * @tags DB table
             * @name Read
             * @request GET:/api/v1/db/meta/tables/{tableId}
             * @response `200` `TableInfoType` OK
             */
            read: (tableId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table
             * @name Update
             * @request PATCH:/api/v1/db/meta/tables/{tableId}
             * @response `200` `any` OK
             */
            update: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table
             * @name Delete
             * @request DELETE:/api/v1/db/meta/tables/{tableId}
             * @response `200` `void` OK
             */
            delete: (tableId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}`, method: 'DELETE' }, params)),
            /**
             * No description
             *
             * @tags DB table
             * @name Reorder
             * @request POST:/api/v1/db/meta/tables/{tableId}/reorder
             * @response `200` `void` OK
             */
            reorder: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/reorder`, method: 'POST', body: data, type: ContentType.Json }, params)),
        };
        this.dbTableColumn = {
            /**
             * No description
             *
             * @tags DB table column
             * @name Create
             * @summary Column create
             * @request POST:/api/v1/db/meta/tables/{tableId}/columns
             * @response `200` `void` OK
             */
            create: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/columns`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB table column
             * @name Update
             * @summary Column Update
             * @request PATCH:/api/v1/db/meta/columns/{columnId}
             * @response `200` `ColumnType` OK
             */
            update: (columnId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/columns/${columnId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table column
             * @name Delete
             * @request DELETE:/api/v1/db/meta/columns/{columnId}
             * @response `200` `void` OK
             */
            delete: (columnId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/columns/${columnId}`, method: 'DELETE' }, params)),
            /**
             * No description
             *
             * @tags DB table column
             * @name PrimaryColumnSet
             * @request POST:/api/v1/db/meta/columns/{columnId}/primary
             * @response `200` `void` OK
             */
            primaryColumnSet: (columnId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/columns/${columnId}/primary`, method: 'POST' }, params)),
        };
        this.dbView = {
            /**
             * No description
             *
             * @tags DB view
             * @name List
             * @request GET:/api/v1/db/meta/tables/{tableId}/views
             * @response `200` `ViewListType`
             */
            list: (tableId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/views`, method: 'GET' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name Update
             * @request PATCH:/api/v1/db/meta/views/{viewId}
             * @response `200` `void` OK
             */
            update: (viewId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}`, method: 'PATCH', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name Delete
             * @request DELETE:/api/v1/db/meta/views/{viewId}
             * @response `200` `void` OK
             */
            delete: (viewId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}`, method: 'DELETE' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name ShowAllColumn
             * @request POST:/api/v1/db/meta/views/{viewId}/show-all
             * @response `200` `void` OK
             */
            showAllColumn: (viewId, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/show-all`, method: 'POST', query: query }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name HideAllColumn
             * @request POST:/api/v1/db/meta/views/{viewId}/hide-all
             * @response `200` `void` OK
             */
            hideAllColumn: (viewId, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/hide-all`, method: 'POST', query: query }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name GridCreate
             * @request POST:/api/v1/db/meta/tables/{tableId}/grids
             * @response `200` `GridType` OK
             */
            gridCreate: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/grids`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name FormCreate
             * @request POST:/api/v1/db/meta/tables/{tableId}/forms
             * @response `200` `FormType` OK
             */
            formCreate: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/forms`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name FormUpdate
             * @request PATCH:/api/v1/db/meta/forms/{formId}
             * @response `200` `void` OK
             */
            formUpdate: (formId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/forms/${formId}`, method: 'PATCH', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name FormRead
             * @request GET:/api/v1/db/meta/forms/{formId}
             * @response `200` `FormType` OK
             */
            formRead: (formId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/forms/${formId}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name FormColumnUpdate
             * @request PATCH:/api/v1/db/meta/form-columns/{formViewColumnId}
             * @response `200` `any` OK
             */
            formColumnUpdate: (formViewColumnId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/form-columns/${formViewColumnId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name GridColumnsList
             * @request GET:/api/v1/db/meta/grids/{gridId}/grid-columns
             * @response `200` `(GridColumnType)[]` OK
             */
            gridColumnsList: (gridId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/grids/${gridId}/grid-columns`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name GridColumnUpdate
             * @request PATCH:/api/v1/db/meta/grid-columns/{columnId}
             * @response `200` `any` OK
             */
            gridColumnUpdate: (columnId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/grid-columns/${columnId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name GalleryCreate
             * @request POST:/api/v1/db/meta/tables/{tableId}/galleries
             * @response `200` `object` OK
             */
            galleryCreate: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/galleries`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name GalleryUpdate
             * @request PATCH:/api/v1/db/meta/galleries/{galleryId}
             * @response `200` `void` OK
             */
            galleryUpdate: (galleryId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/galleries/${galleryId}`, method: 'PATCH', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB view
             * @name GalleryRead
             * @request GET:/api/v1/db/meta/galleries/{galleryId}
             * @response `200` `GalleryType` OK
             */
            galleryRead: (galleryId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/galleries/${galleryId}`, method: 'GET', format: 'json' }, params)),
        };
        this.dbViewShare = {
            /**
             * No description
             *
             * @tags DB view share
             * @name List
             * @summary Shared view list
             * @request GET:/api/v1/db/meta/tables/{tableId}/share
             * @response `200` `(any)[]` OK
             */
            list: (tableId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/share`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view share
             * @name Create
             * @request POST:/api/v1/db/meta/views/{viewId}/share
             * @response `200` `{ uuid?: string }` OK
             */
            create: (viewId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/share`, method: 'POST', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view share
             * @name Update
             * @request PATCH:/api/v1/db/meta/views/{viewId}/share
             * @response `200` `SharedViewType` OK
             */
            update: (viewId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/share`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view share
             * @name Delete
             * @request DELETE:/api/v1/db/meta/views/{viewId}/share
             * @response `200` `void` OK
             */
            delete: (viewId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/share`, method: 'DELETE' }, params)),
        };
        this.dbViewColumn = {
            /**
             * No description
             *
             * @tags DB view column
             * @name List
             * @request GET:/api/v1/db/meta/views/{viewId}/columns
             */
            list: (viewId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/columns`, method: 'GET' }, params)),
            /**
             * No description
             *
             * @tags DB view column
             * @name Create
             * @request POST:/api/v1/db/meta/views/{viewId}/columns
             * @response `200` `void` OK
             */
            create: (viewId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/columns`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB view column
             * @name Update
             * @request PATCH:/api/v1/db/meta/views/{viewId}/columns/{columnId}
             * @response `200` `void` OK
             */
            update: (viewId, columnId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/columns/${columnId}`, method: 'PATCH', body: data, type: ContentType.Json }, params)),
        };
        this.dbTableSort = {
            /**
             * No description
             *
             * @tags DB table sort
             * @name List
             * @request GET:/api/v1/db/meta/views/{viewId}/sorts
             * @response `200` `{ uuid?: string, url?: string }` OK
             */
            list: (viewId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/sorts`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table sort
             * @name Create
             * @request POST:/api/v1/db/meta/views/{viewId}/sorts
             * @response `200` `void` OK
             */
            create: (viewId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/sorts`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB table sort
             * @name Get
             * @request GET:/api/v1/db/meta/sorts/{sortId}
             * @response `200` `SortType` OK
             */
            get: (sortId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/sorts/${sortId}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table sort
             * @name Update
             * @request PATCH:/api/v1/db/meta/sorts/{sortId}
             * @response `200` `void` OK
             */
            update: (sortId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/sorts/${sortId}`, method: 'PATCH', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB table sort
             * @name Delete
             * @request DELETE:/api/v1/db/meta/sorts/{sortId}
             * @response `200` `void` OK
             */
            delete: (sortId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/sorts/${sortId}`, method: 'DELETE' }, params)),
        };
        this.dbTableFilter = {
            /**
             * No description
             *
             * @tags DB table filter
             * @name Read
             * @request GET:/api/v1/db/meta/views/{viewId}/filters
             * @response `200` `FilterListType`
             */
            read: (viewId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/filters`, method: 'GET' }, params)),
            /**
             * No description
             *
             * @tags DB table filter
             * @name Create
             * @request POST:/api/v1/db/meta/views/{viewId}/filters
             * @response `200` `void` OK
             */
            create: (viewId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/views/${viewId}/filters`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB table filter
             * @name Get
             * @request GET:/api/v1/db/meta/filters/{filterId}
             * @response `200` `FilterType` OK
             */
            get: (filterId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/filters/${filterId}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table filter
             * @name Update
             * @request PATCH:/api/v1/db/meta/filters/{filterId}
             * @response `200` `void` OK
             */
            update: (filterId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/filters/${filterId}`, method: 'PATCH', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags DB table filter
             * @name Delete
             * @request DELETE:/api/v1/db/meta/filters/{filterId}
             * @response `200` `void` OK
             */
            delete: (filterId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/filters/${filterId}`, method: 'DELETE' }, params)),
            /**
             * No description
             *
             * @tags DB table filter
             * @name ChildrenRead
             * @request GET:/api/v1/db/meta/filters/{filterGroupId}/children
             * @response `200` `FilterType` OK
             */
            childrenRead: (filterGroupId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/filters/${filterGroupId}/children`, method: 'GET', format: 'json' }, params)),
        };
        this.dbTableWebhookFilter = {
            /**
             * No description
             *
             * @tags DB table webhook filter
             * @name Read
             * @request GET:/api/v1/db/meta/hooks/{hookId}/filters
             * @response `200` `FilterListType`
             */
            read: (hookId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/hooks/${hookId}/filters`, method: 'GET' }, params)),
            /**
             * No description
             *
             * @tags DB table webhook filter
             * @name Create
             * @request POST:/api/v1/db/meta/hooks/{hookId}/filters
             * @response `200` `void` OK
             */
            create: (hookId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/hooks/${hookId}/filters`, method: 'POST', body: data, type: ContentType.Json }, params)),
        };
        this.dbTableRow = {
            /**
             * No description
             *
             * @tags DB table row
             * @name List
             * @summary Table row list
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}
             * @response `200` `any` OK
             */
            list: (orgs, projectName, tableName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name Create
             * @summary Table row create
             * @request POST:/api/v1/db/data/{orgs}/{projectName}/{tableName}
             * @response `200` `any` OK
             */
            create: (orgs, projectName, tableName, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name FindOne
             * @summary Table row FindOne
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/find-one
             * @response `200` `any` OK
             */
            findOne: (orgs, projectName, tableName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/find-one`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name GroupBy
             * @summary Table row Group by
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/groupby
             * @response `200` `any` OK
             */
            groupBy: (orgs, projectName, tableName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/groupby`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name Read
             * @summary Table row read
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}
             * @response `201` `any` Created
             */
            read: (orgs, projectName, tableName, rowId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name Update
             * @summary Table row update
             * @request PATCH:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}
             * @response `200` `any` OK
             */
            update: (orgs, projectName, tableName, rowId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name Delete
             * @summary Table row delete
             * @request DELETE:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}
             * @response `200` `void` OK
             */
            delete: (orgs, projectName, tableName, rowId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}`, method: 'DELETE' }, params)),
            /**
             * @description check row with provided primary key exists or not
             *
             * @tags DB table row
             * @name Exist
             * @summary Table row exist
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}/exist
             * @response `201` `any` Created
             */
            exist: (orgs, projectName, tableName, rowId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}/exist`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name BulkCreate
             * @summary Bulk insert table rows
             * @request POST:/api/v1/db/data/bulk/{orgs}/{projectName}/{tableName}
             * @response `200` `any` OK
             */
            bulkCreate: (orgs, projectName, tableName, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/bulk/${orgs}/${projectName}/${tableName}`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name BulkUpdate
             * @summary Bulk update all table rows by IDs
             * @request PATCH:/api/v1/db/data/bulk/{orgs}/{projectName}/{tableName}
             * @response `200` `any` OK
             */
            bulkUpdate: (orgs, projectName, tableName, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/bulk/${orgs}/${projectName}/${tableName}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name BulkDelete
             * @summary Bulk delete all table rows by IDs
             * @request DELETE:/api/v1/db/data/bulk/{orgs}/{projectName}/{tableName}
             * @response `200` `any` OK
             */
            bulkDelete: (orgs, projectName, tableName, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/bulk/${orgs}/${projectName}/${tableName}`, method: 'DELETE', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name BulkUpdateAll
             * @summary Bulk update all table rows with conditions
             * @request PATCH:/api/v1/db/data/bulk/{orgs}/{projectName}/{tableName}/all
             * @response `200` `any` OK
             */
            bulkUpdateAll: (orgs, projectName, tableName, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/bulk/${orgs}/${projectName}/${tableName}/all`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name BulkDeleteAll
             * @summary Bulk delete all table rows with conditions
             * @request DELETE:/api/v1/db/data/bulk/{orgs}/{projectName}/{tableName}/all
             * @response `200` `any` OK
             */
            bulkDeleteAll: (orgs, projectName, tableName, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/bulk/${orgs}/${projectName}/${tableName}/all`, method: 'DELETE', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * @description CSV or Excel export
             *
             * @tags DB table row
             * @name CsvExport
             * @summary Tablerows export
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/export/{type}
             * @response `200` `any` OK
             */
            csvExport: (orgs, projectName, tableName, type, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/export/${type}`, method: 'GET', wrapped: true }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name NestedList
             * @summary Nested relations row list
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}/{relationType}/{columnName}
             * @response `200` `any` OK
             */
            nestedList: (orgs, projectName, tableName, rowId, relationType, columnName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}/${relationType}/${columnName}`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name NestedAdd
             * @summary Nested relations row add
             * @request POST:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}/{relationType}/{columnName}/{refRowId}
             * @response `200` `any` OK
             */
            nestedAdd: (orgs, projectName, tableName, rowId, relationType, columnName, refRowId, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}/${relationType}/${columnName}/${refRowId}`, method: 'POST', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name NestedRemove
             * @summary Nested relations row remove
             * @request DELETE:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}/{relationType}/{columnName}/{refRowId}
             * @response `200` `any` OK
             */
            nestedRemove: (orgs, projectName, tableName, rowId, relationType, columnName, refRowId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}/${relationType}/${columnName}/${refRowId}`, method: 'DELETE', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table row
             * @name NestedChildrenExcludedList
             * @summary Referenced tables rows excluding current records children/parent
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/{rowId}/{relationType}/{columnName}/exclude
             * @response `200` `any` OK
             */
            nestedChildrenExcludedList: (orgs, projectName, tableName, rowId, relationType, columnName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/${rowId}/${relationType}/${columnName}/exclude`, method: 'GET', query: query, format: 'json' }, params)),
        };
        this.dbViewRow = {
            /**
             * No description
             *
             * @tags DB view row
             * @name List
             * @summary Table view row list
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}
             * @response `200` `any` OK
             */
            list: (orgs, projectName, tableName, viewName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view row
             * @name Create
             * @summary Table view row create
             * @request POST:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}
             * @response `200` `any` OK
             */
            create: (orgs, projectName, tableName, viewName, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view row
             * @name FindOne
             * @summary Table view row FindOne
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/find-one
             * @response `200` `any` OK
             */
            findOne: (orgs, projectName, tableName, viewName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/find-one`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view row
             * @name GroupBy
             * @summary Table view row Group by
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/groupby
             * @response `200` `any` OK
             */
            groupBy: (orgs, projectName, tableName, viewName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/groupby`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view row
             * @name Count
             * @summary Table view rows count
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/count
             * @response `200` `any` OK
             */
            count: (orgs, projectName, tableName, viewName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/count`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view row
             * @name Read
             * @summary Table view row read
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/{rowId}
             * @response `201` `any` Created
             */
            read: (orgs, projectName, tableName, viewName, rowId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/${rowId}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view row
             * @name Update
             * @summary Table view row update
             * @request PATCH:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/{rowId}
             * @response `200` `any` OK
             */
            update: (orgs, projectName, tableName, viewName, rowId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/${rowId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB view row
             * @name Delete
             * @summary Table view row delete
             * @request DELETE:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/{rowId}
             * @response `200` `void` OK
             */
            delete: (orgs, projectName, tableName, viewName, rowId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/${rowId}`, method: 'DELETE' }, params)),
            /**
             * @description check row with provided primary key exists or not
             *
             * @tags DB view row
             * @name Exist
             * @summary Table view row exist
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/{rowId}/exist
             * @response `201` `any` Created
             */
            exist: (orgs, projectName, tableName, viewName, rowId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/${rowId}/exist`, method: 'GET', format: 'json' }, params)),
            /**
             * @description CSV or Excel export
             *
             * @tags DB view row
             * @name Export
             * @summary Table view rows export
             * @request GET:/api/v1/db/data/{orgs}/{projectName}/{tableName}/views/{viewName}/export/{type}
             * @response `200` `any` OK
             */
            export: (orgs, projectName, tableName, viewName, type, params = {}) => this.request(Object.assign({ path: `/api/v1/db/data/${orgs}/${projectName}/${tableName}/views/${viewName}/export/${type}`, method: 'GET', wrapped: true }, params)),
        };
        this.public = {
            /**
             * No description
             *
             * @tags Public
             * @name DataList
             * @request GET:/api/v1/db/public/shared-view/{sharedViewUuid}/rows
             * @response `200` `any` OK
             */
            dataList: (sharedViewUuid, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/public/shared-view/${sharedViewUuid}/rows`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Public
             * @name DataCreate
             * @request POST:/api/v1/db/public/shared-view/{sharedViewUuid}/rows
             * @response `200` `any` OK
             */
            dataCreate: (sharedViewUuid, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/public/shared-view/${sharedViewUuid}/rows`, method: 'POST', body: data, type: ContentType.FormData, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Public
             * @name DataNestedList
             * @request GET:/api/v1/db/public/shared-view/{sharedViewUuid}/rows/{rowId}/{relationType}/{columnName}
             * @response `200` `any` OK
             */
            dataNestedList: (sharedViewUuid, rowId, relationType, columnName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/public/shared-view/${sharedViewUuid}/rows/${rowId}/${relationType}/${columnName}`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Public
             * @name CsvExport
             * @request GET:/api/v1/db/public/shared-view/{sharedViewUuid}/rows/export/{type}
             * @response `200` `any` OK
             */
            csvExport: (sharedViewUuid, type, params = {}) => this.request(Object.assign({ path: `/api/v1/db/public/shared-view/${sharedViewUuid}/rows/export/${type}`, method: 'GET', wrapped: true }, params)),
            /**
             * No description
             *
             * @tags Public
             * @name DataRelationList
             * @request GET:/api/v1/db/public/shared-view/{sharedViewUuid}/nested/{columnName}
             * @response `200` `any` OK
             */
            dataRelationList: (sharedViewUuid, columnName, query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/public/shared-view/${sharedViewUuid}/nested/${columnName}`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Public
             * @name SharedViewMetaGet
             * @request GET:/api/v1/db/public/shared-view/{sharedViewUuid}/meta
             * @response `200` `object` OK
             */
            sharedViewMetaGet: (sharedViewUuid, params = {}) => this.request(Object.assign({ path: `/api/v1/db/public/shared-view/${sharedViewUuid}/meta`, method: 'GET', format: 'json' }, params)),
            /**
             * @description Read project details
             *
             * @tags Public
             * @name SharedBaseGet
             * @request GET:/api/v1/db/public/shared-base/{sharedBaseUuid}/meta
             * @response `200` `{ project_id?: string }` OK
             */
            sharedBaseGet: (sharedBaseUuid, params = {}) => this.request(Object.assign({ path: `/api/v1/db/public/shared-base/${sharedBaseUuid}/meta`, method: 'GET', format: 'json' }, params)),
        };
        this.utils = {
            /**
             * No description
             *
             * @tags Utils
             * @name CommentList
             * @request GET:/api/v1/db/meta/audits/comments
             * @response `201` `any` Created
             */
            commentList: (query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/audits/comments`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Utils
             * @name CommentRow
             * @request POST:/api/v1/db/meta/audits/comments
             * @response `200` `void` OK
             */
            commentRow: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/audits/comments`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags Utils
             * @name CommentCount
             * @request GET:/api/v1/db/meta/audits/comments/count
             * @response `201` `any` Created
             */
            commentCount: (query, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/audits/comments/count`, method: 'GET', query: query, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Utils
             * @name AuditRowUpdate
             * @request POST:/api/v1/db/meta/audits/rows/{rowId}/update
             * @response `200` `void` OK
             */
            auditRowUpdate: (rowId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/audits/rows/${rowId}/update`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags Utils
             * @name TestConnection
             * @request POST:/api/v1/db/meta/connection/test
             * @response `200` `{ code?: number, message?: string }` OK
             */
            testConnection: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/connection/test`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Utils
             * @name AppInfo
             * @request GET:/api/v1/db/meta/nocodb/info
             * @response `200` `any` OK
             */
            appInfo: (params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/nocodb/info`, method: 'GET', format: 'json' }, params)),
            /**
             * @description Generic Axios Call
             *
             * @tags Utils
             * @name AxiosRequestMake
             * @request POST:/api/v1/db/meta/axiosRequestMake
             * @response `200` `object` OK
             */
            axiosRequestMake: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/axiosRequestMake`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Utils
             * @name AppVersion
             * @request GET:/api/v1/version
             * @response `200` `any` OK
             */
            appVersion: (params = {}) => this.request(Object.assign({ path: `/api/v1/version`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Utils
             * @name AppHealth
             * @request GET:/api/v1/health
             * @response `200` `any` OK
             */
            appHealth: (params = {}) => this.request(Object.assign({ path: `/api/v1/health`, method: 'GET', format: 'json' }, params)),
            /**
             * @description Get All K/V pairs in NocoCache
             *
             * @tags Utils
             * @name CacheGet
             * @summary Your GET endpoint
             * @request GET:/api/v1/db/meta/cache
             */
            cacheGet: (params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/cache`, method: 'GET' }, params)),
            /**
             * @description Delete All K/V pairs in NocoCache
             *
             * @tags Utils
             * @name CacheDelete
             * @request DELETE:/api/v1/db/meta/cache
             * @response `200` `void` OK
             */
            cacheDelete: (params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/cache`, method: 'DELETE' }, params)),
        };
        this.dbTableWebhook = {
            /**
             * No description
             *
             * @tags DB table webhook
             * @name List
             * @request GET:/api/v1/db/meta/tables/{tableId}/hooks
             * @response `200` `{ list: (HookType)[], pageInfo: PaginatedType }` OK
             */
            list: (tableId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/hooks`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table webhook
             * @name Create
             * @request POST:/api/v1/db/meta/tables/{tableId}/hooks
             * @response `200` `AuditType` OK
             */
            create: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/hooks`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table webhook
             * @name Test
             * @request POST:/api/v1/db/meta/tables/{tableId}/hooks/test
             * @response `200` `any` OK
             */
            test: (tableId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/hooks/test`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table webhook
             * @name SamplePayloadGet
             * @request GET:/api/v1/db/meta/tables/{tableId}/hooks/samplePayload/{operation}
             * @response `200` `{ plugins?: { list: (PluginType)[], pageInfo: PaginatedType } }` OK
             */
            samplePayloadGet: (tableId, operation, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/tables/${tableId}/hooks/samplePayload/${operation}`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table webhook
             * @name Update
             * @request PATCH:/api/v1/db/meta/hooks/{hookId}
             * @response `200` `HookType` OK
             */
            update: (hookId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/hooks/${hookId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags DB table webhook
             * @name Delete
             * @request DELETE:/api/v1/db/meta/hooks/{hookId}
             * @response `200` `void` OK
             */
            delete: (hookId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/hooks/${hookId}`, method: 'DELETE' }, params)),
        };
        this.plugin = {
            /**
             * No description
             *
             * @tags Plugin
             * @name List
             * @request GET:/api/v1/db/meta/plugins
             * @response `200` `{ list?: (PluginType)[], pageInfo?: PaginatedType }` OK
             */
            list: (params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/plugins`, method: 'GET', format: 'json' }, params)),
            /**
             * @description Check plugin is active or not
             *
             * @tags Plugin
             * @name Status
             * @request GET:/api/v1/db/meta/plugins/{pluginTitle}/status
             * @response `200` `boolean` OK
             */
            status: (pluginTitle, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/plugins/${pluginTitle}/status`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Plugin
             * @name Test
             * @request POST:/api/v1/db/meta/plugins/test
             * @response `200` `any` OK
             * @response `400` `void` Bad Request
             * @response `401` `void` Unauthorized
             */
            test: (data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/plugins/test`, method: 'POST', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Plugin
             * @name Update
             * @request PATCH:/api/v1/db/meta/plugins/{pluginId}
             * @response `200` `PluginType` OK
             */
            update: (pluginId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/plugins/${pluginId}`, method: 'PATCH', body: data, type: ContentType.Json, format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Plugin
             * @name Read
             * @request GET:/api/v1/db/meta/plugins/{pluginId}
             * @response `200` `PluginType` OK
             */
            read: (pluginId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/plugins/${pluginId}`, method: 'GET', format: 'json' }, params)),
        };
        this.apiToken = {
            /**
             * No description
             *
             * @tags Api token
             * @name List
             * @summary Your GET endpoint
             * @request GET:/api/v1/db/meta/projects/{projectId}/api-tokens
             * @response `200` `(ApiTokenType)[]` OK
             */
            list: (projectId, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/api-tokens`, method: 'GET', format: 'json' }, params)),
            /**
             * No description
             *
             * @tags Api token
             * @name Create
             * @request POST:/api/v1/db/meta/projects/{projectId}/api-tokens
             * @response `200` `void` OK
             * @response `201` `ApiTokenType` Created
             */
            create: (projectId, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/api-tokens`, method: 'POST', body: data, type: ContentType.Json }, params)),
            /**
             * No description
             *
             * @tags Api token
             * @name Delete
             * @request DELETE:/api/v1/db/meta/projects/{projectId}/api-tokens/{token}
             * @response `200` `void` OK
             */
            delete: (projectId, token, params = {}) => this.request(Object.assign({ path: `/api/v1/db/meta/projects/${projectId}/api-tokens/${token}`, method: 'DELETE' }, params)),
        };
        this.storage = {
            /**
             * No description
             *
             * @tags Storage
             * @name Upload
             * @summary Attachment
             * @request POST:/api/v1/db/storage/upload
             */
            upload: (query, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/storage/upload`, method: 'POST', query: query, body: data, type: ContentType.FormData }, params)),
            /**
             * No description
             *
             * @tags Storage
             * @name UploadByUrl
             * @summary Attachment
             * @request POST:/api/v1/db/storage/upload-by-url
             */
            uploadByUrl: (query, data, params = {}) => this.request(Object.assign({ path: `/api/v1/db/storage/upload-by-url`, method: 'POST', query: query, body: data, type: ContentType.Json }, params)),
        };
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXBpMi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvQXBpMi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxvQkFBb0I7QUFDcEIsb0JBQW9CO0FBQ3BCOzs7Ozs7O0dBT0c7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTBwQkgsTUFBTSxDQUFOLElBQVksV0FJWDtBQUpELFdBQVksV0FBVztJQUNyQix3Q0FBeUIsQ0FBQTtJQUN6QiwrQ0FBZ0MsQ0FBQTtJQUNoQywrREFBZ0QsQ0FBQTtBQUNsRCxDQUFDLEVBSlcsV0FBVyxLQUFYLFdBQVcsUUFJdEI7QUFFRCxNQUFNLE9BQU8sVUFBVTtJQWVyQixZQUFZLFlBQXlDLEVBQUU7UUFkaEQsWUFBTyxHQUFXLHVCQUF1QixDQUFDO1FBQ3pDLGlCQUFZLEdBQTRCLElBQUksQ0FBQztRQUU3QyxxQkFBZ0IsR0FBRyxJQUFJLEdBQUcsRUFBZ0MsQ0FBQztRQUMzRCxnQkFBVyxHQUFHLENBQUMsR0FBRyxXQUFxQyxFQUFFLEVBQUUsQ0FDakUsS0FBSyxDQUFDLEdBQUcsV0FBVyxDQUFDLENBQUM7UUFFaEIsa0JBQWEsR0FBa0I7WUFDckMsV0FBVyxFQUFFLGFBQWE7WUFDMUIsT0FBTyxFQUFFLEVBQUU7WUFDWCxRQUFRLEVBQUUsUUFBUTtZQUNsQixjQUFjLEVBQUUsYUFBYTtTQUM5QixDQUFDO1FBTUssb0JBQWUsR0FBRyxDQUFDLElBQTZCLEVBQUUsRUFBRTtZQUN6RCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUMzQixDQUFDLENBQUM7UUFxQ00sc0JBQWlCLEdBQTZDO1lBQ3BFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsS0FBVSxFQUFFLEVBQUUsQ0FDakMsS0FBSyxLQUFLLElBQUksSUFBSSxDQUFDLE9BQU8sS0FBSyxLQUFLLFFBQVEsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLENBQUM7Z0JBQ3hFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQztnQkFDdkIsQ0FBQyxDQUFDLEtBQUs7WUFDWCxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLEtBQVUsRUFBRSxFQUFFLENBQ3JDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsRUFBRSxHQUFHLEVBQUUsRUFBRTtnQkFDaEQsTUFBTSxRQUFRLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUM1QixRQUFRLENBQUMsTUFBTSxDQUNiLEdBQUcsRUFDSCxRQUFRLFlBQVksSUFBSTtvQkFDdEIsQ0FBQyxDQUFDLFFBQVE7b0JBQ1YsQ0FBQyxDQUFDLE9BQU8sUUFBUSxLQUFLLFFBQVEsSUFBSSxRQUFRLEtBQUssSUFBSTt3QkFDbkQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDO3dCQUMxQixDQUFDLENBQUMsR0FBRyxRQUFRLEVBQUUsQ0FDbEIsQ0FBQztnQkFDRixPQUFPLFFBQVEsQ0FBQztZQUNsQixDQUFDLEVBQUUsSUFBSSxRQUFRLEVBQUUsQ0FBQztZQUNwQixDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLEtBQVUsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUM7U0FDcEUsQ0FBQztRQWtCTSxzQkFBaUIsR0FBRyxDQUMxQixXQUF3QixFQUNDLEVBQUU7WUFDM0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxFQUFFO2dCQUMxQyxNQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUMvRCxJQUFJLGVBQWUsRUFBRTtvQkFDbkIsT0FBTyxlQUFlLENBQUMsTUFBTSxDQUFDO2lCQUMvQjtnQkFDRCxPQUFPLEtBQUssQ0FBQyxDQUFDO2FBQ2Y7WUFFRCxNQUFNLGVBQWUsR0FBRyxJQUFJLGVBQWUsRUFBRSxDQUFDO1lBQzlDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLGVBQWUsQ0FBQyxDQUFDO1lBQ3hELE9BQU8sZUFBZSxDQUFDLE1BQU0sQ0FBQztRQUNoQyxDQUFDLENBQUM7UUFFSyxpQkFBWSxHQUFHLENBQUMsV0FBd0IsRUFBRSxFQUFFO1lBQ2pELE1BQU0sZUFBZSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7WUFFL0QsSUFBSSxlQUFlLEVBQUU7Z0JBQ25CLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDeEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUMzQztRQUNILENBQUMsQ0FBQztRQUVLLFlBQU8sR0FBRyxDQUF5QixFQVVMLEVBQWMsRUFBRTtnQkFWWCxFQUN4QyxJQUFJLEVBQ0osTUFBTSxFQUNOLElBQUksRUFDSixJQUFJLEVBQ0osS0FBSyxFQUNMLE1BQU0sRUFDTixPQUFPLEVBQ1AsV0FBVyxPQUV3QixFQURoQyxNQUFNLGNBVCtCLCtFQVV6QyxDQURVO1lBRVQsTUFBTSxZQUFZLEdBQ2hCLENBQUMsQ0FBQyxPQUFPLE1BQU0sS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7Z0JBQ2pFLElBQUksQ0FBQyxjQUFjO2dCQUNuQixDQUFDLE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztnQkFDakQsRUFBRSxDQUFDO1lBQ0wsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQztZQUNwRSxNQUFNLFdBQVcsR0FBRyxLQUFLLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN2RCxNQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzFFLE1BQU0sY0FBYyxHQUFHLE1BQU0sSUFBSSxhQUFhLENBQUMsTUFBTSxDQUFDO1lBRXRELE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FDckIsR0FBRyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxFQUFFLEdBQUcsSUFBSSxHQUNyQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQ3BDLEVBQUUsa0NBRUcsYUFBYSxLQUNoQixPQUFPLGtDQUNGLENBQUMsSUFBSSxJQUFJLElBQUksS0FBSyxXQUFXLENBQUMsUUFBUTtvQkFDdkMsQ0FBQyxDQUFDLEVBQUUsY0FBYyxFQUFFLElBQUksRUFBRTtvQkFDMUIsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxHQUNKLENBQUMsYUFBYSxDQUFDLE9BQU8sSUFBSSxFQUFFLENBQUMsR0FFbEMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFDbEUsSUFBSSxFQUNGLE9BQU8sSUFBSSxLQUFLLFdBQVcsSUFBSSxJQUFJLEtBQUssSUFBSTtvQkFDMUMsQ0FBQyxDQUFDLElBQUk7b0JBQ04sQ0FBQyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUUvQixDQUFDLElBQUksQ0FBQyxDQUFPLFFBQVEsRUFBRSxFQUFFO2dCQUN4QixNQUFNLENBQUMsR0FBRyxRQUE4QixDQUFDO2dCQUN6QyxDQUFDLENBQUMsSUFBSSxHQUFHLElBQW9CLENBQUM7Z0JBQzlCLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBb0IsQ0FBQztnQkFFL0IsTUFBTSxJQUFJLEdBQUcsQ0FBQyxjQUFjO29CQUMxQixDQUFDLENBQUMsQ0FBQztvQkFDSCxDQUFDLENBQUMsTUFBTSxRQUFRLENBQUMsY0FBYyxDQUFDLEVBQUU7eUJBQzdCLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO3dCQUNiLElBQUksQ0FBQyxDQUFDLEVBQUUsRUFBRTs0QkFDUixDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQzt5QkFDZjs2QkFBTTs0QkFDTCxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQzt5QkFDaEI7d0JBQ0QsT0FBTyxDQUFDLENBQUM7b0JBQ1gsQ0FBQyxDQUFDO3lCQUNELEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO3dCQUNYLENBQUMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDO3dCQUNaLE9BQU8sQ0FBQyxDQUFDO29CQUNYLENBQUMsQ0FBQyxDQUFDO2dCQUVULElBQUksV0FBVyxFQUFFO29CQUNmLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7aUJBQzNDO2dCQUVELElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFBRSxNQUFNLElBQUksQ0FBQztnQkFDN0IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ25CLENBQUMsQ0FBQSxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUEsQ0FBQztRQTNLQSxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBTU8sZ0JBQWdCLENBQUMsR0FBVyxFQUFFLEtBQVU7UUFDOUMsTUFBTSxVQUFVLEdBQUcsa0JBQWtCLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDM0MsT0FBTyxHQUFHLFVBQVUsSUFBSSxrQkFBa0IsQ0FDeEMsT0FBTyxLQUFLLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQy9DLEVBQUUsQ0FBQztJQUNOLENBQUM7SUFFTyxhQUFhLENBQUMsS0FBc0IsRUFBRSxHQUFXO1FBQ3ZELE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRU8sa0JBQWtCLENBQUMsS0FBc0IsRUFBRSxHQUFXO1FBQzVELE1BQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN6QixPQUFPLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFNLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDeEUsQ0FBQztJQUVTLGFBQWEsQ0FBQyxRQUEwQjtRQUNoRCxNQUFNLEtBQUssR0FBRyxRQUFRLElBQUksRUFBRSxDQUFDO1FBQzdCLE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUNwQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsV0FBVyxLQUFLLE9BQU8sS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUMzQyxDQUFDO1FBQ0YsT0FBTyxJQUFJO2FBQ1IsR0FBRyxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FDWCxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN2QixDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssRUFBRSxHQUFHLENBQUM7WUFDckMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUNuQzthQUNBLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNmLENBQUM7SUFFUyxjQUFjLENBQUMsUUFBMEI7UUFDakQsTUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNqRCxPQUFPLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQzlDLENBQUM7SUF1Qk8sa0JBQWtCLENBQ3hCLE9BQXNCLEVBQ3RCLE9BQXVCO1FBRXZCLG1FQUNLLElBQUksQ0FBQyxhQUFhLEdBQ2xCLE9BQU8sR0FDUCxDQUFDLE9BQU8sSUFBSSxFQUFFLENBQUMsS0FDbEIsT0FBTyxnREFDRixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQyxHQUNsQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDLEdBQ3ZCLENBQUMsQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUV6QztJQUNKLENBQUM7Q0ErRkY7QUFFRDs7OztHQUlHO0FBQ0gsTUFBTSxPQUFPLEdBRVgsU0FBUSxVQUE0QjtJQUZ0Qzs7UUFHRSxTQUFJLEdBQUc7WUFDTDs7Ozs7Ozs7Ozs7ZUFXRztZQUNILE1BQU0sRUFBRSxDQUNOLElBQTJDLEVBQzNDLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwwQkFBMEIsRUFDaEMsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7OztlQVNHO1lBQ0gsTUFBTSxFQUFFLENBQ04sSUFBeUMsRUFDekMsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDBCQUEwQixFQUNoQyxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxFQUFFLEVBQUUsQ0FBQyxLQUErQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ2xFLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxzQkFBc0IsRUFDNUIsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxFQUNaLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7OztlQVNHO1lBQ0gsY0FBYyxFQUFFLENBQUMsSUFBd0IsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN2RSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsOEJBQThCLEVBQ3BDLE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksSUFDbkIsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7OztlQVNHO1lBQ0gsY0FBYyxFQUFFLENBQ2QsSUFBd0QsRUFDeEQsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDhCQUE4QixFQUNwQyxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCwwQkFBMEIsRUFBRSxDQUFDLEtBQWEsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN4RSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsK0JBQStCLEtBQUssRUFBRSxFQUM1QyxNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsYUFBYSxFQUFFLENBQUMsS0FBYSxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQzNELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwrQkFBK0IsS0FBSyxFQUFFLEVBQzVDLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxhQUFhLEVBQUUsQ0FDYixLQUFhLEVBQ2IsSUFBK0IsRUFDL0IsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLCtCQUErQixLQUFLLEVBQUUsRUFDNUMsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxJQUNuQixNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILFlBQVksRUFBRSxDQUFDLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQzNDLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSw0QkFBNEIsRUFDbEMsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILGVBQWUsRUFBRSxDQUFDLFNBQWlCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDakUsSUFBSSxDQUFDLE9BQU8saUJBSVYsSUFBSSxFQUFFLDRCQUE0QixTQUFTLFFBQVEsRUFDbkQsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsY0FBYyxFQUFFLENBQ2QsU0FBaUIsRUFDakIsSUFBUyxFQUNULFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSw0QkFBNEIsU0FBUyxRQUFRLEVBQ25ELE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILGlCQUFpQixFQUFFLENBQ2pCLFNBQWlCLEVBQ2pCLE1BQWMsRUFDZCxJQUFTLEVBQ1QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLFVBQVUsTUFBTSxFQUFFLEVBQzdELE1BQU0sRUFBRSxPQUFPLEVBQ2YsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILGlCQUFpQixFQUFFLENBQ2pCLFNBQWlCLEVBQ2pCLE1BQWMsRUFDZCxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsVUFBVSxNQUFNLEVBQUUsRUFDN0QsTUFBTSxFQUFFLFFBQVEsRUFDaEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsdUJBQXVCLEVBQUUsQ0FDdkIsU0FBaUIsRUFDakIsTUFBYyxFQUNkLElBQVMsRUFDVCxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsVUFBVSxNQUFNLGdCQUFnQixFQUMzRSxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLFlBQU8sR0FBRztZQUNSOzs7Ozs7OztlQVFHO1lBQ0gsT0FBTyxFQUFFLENBQUMsU0FBaUIsRUFBRSxTQUF3QixFQUFFLEVBQUUsS0FBYSxFQUFFLEVBQUUsQ0FDeEUsSUFBSSxDQUFDLE9BQU8saUJBYVYsSUFBSSxFQUFFLDRCQUE0QixTQUFTLE9BQU8sRUFDbEQsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxFQUNaLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxtQkFBbUIsRUFBRSxDQUNuQixTQUFpQixFQUNqQixLQUFnQyxFQUNoQyxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsbUJBQW1CLEVBQzlELE1BQU0sRUFBRSxLQUFLLEVBQ2IsS0FBSyxFQUFFLEtBQUssRUFDWixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxrQkFBa0IsRUFBRSxDQUNsQixTQUFpQixFQUNqQixJQUFTLEVBQ1QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLG1CQUFtQixFQUM5RCxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxJQUFJLEVBQUUsQ0FDSixLQUEyRCxFQUMzRCxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMkJBQTJCLEVBQ2pDLE1BQU0sRUFBRSxLQUFLLEVBQ2IsS0FBSyxFQUFFLEtBQUssSUFDVCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILE1BQU0sRUFBRSxDQUNOLElBQTBDLEVBQzFDLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwyQkFBMkIsRUFDakMsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsSUFBSSxFQUFFLENBQUMsU0FBaUIsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN0RCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsRUFBRSxFQUM3QyxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxNQUFNLEVBQUUsQ0FBQyxTQUFpQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3hELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSw0QkFBNEIsU0FBUyxFQUFFLEVBQzdDLE1BQU0sRUFBRSxRQUFRLElBQ2IsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGFBQWEsRUFBRSxDQUFDLFNBQWlCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDL0QsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLFNBQVMsRUFDcEQsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxpQkFBaUIsRUFBRSxDQUFDLFNBQWlCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDbkUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLFNBQVMsRUFDcEQsTUFBTSxFQUFFLFFBQVEsSUFDYixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsZ0JBQWdCLEVBQUUsQ0FDaEIsU0FBaUIsRUFDakIsSUFBMkMsRUFDM0MsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLFNBQVMsRUFDcEQsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxnQkFBZ0IsRUFBRSxDQUNoQixTQUFpQixFQUNqQixJQUEyQyxFQUMzQyxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsU0FBUyxFQUNwRCxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLElBQ25CLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsSUFBSSxFQUFFLENBQUMsU0FBaUIsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN0RCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsT0FBTyxFQUNsRCxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILFlBQVksRUFBRSxDQUFDLFNBQWlCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDOUQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLFlBQVksRUFDdkQsTUFBTSxFQUFFLE1BQU0sRUFDZCxNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxXQUFXLEVBQUUsQ0FBQyxTQUFpQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQzdELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSw0QkFBNEIsU0FBUyxZQUFZLEVBQ3ZELE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsU0FBUyxFQUFFLENBQ1QsU0FBaUIsRUFDakIsS0FBMkMsRUFDM0MsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDRCQUE0QixTQUFTLFNBQVMsRUFDcEQsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxFQUNaLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLFlBQU8sR0FBRztZQUNSOzs7Ozs7O2VBT0c7WUFDSCxNQUFNLEVBQUUsQ0FDTixTQUFpQixFQUNqQixJQUFrQixFQUNsQixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsU0FBUyxFQUNwRCxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILElBQUksRUFBRSxDQUNKLFNBQWlCLEVBQ2pCLEtBS0MsRUFDRCxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsU0FBUyxFQUNwRCxNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLElBQ1QsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILElBQUksRUFBRSxDQUFDLE9BQWUsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNwRCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMEJBQTBCLE9BQU8sRUFBRSxFQUN6QyxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUNOLE9BQWUsRUFDZixJQUF3QixFQUN4QixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMEJBQTBCLE9BQU8sRUFBRSxFQUN6QyxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUFDLE9BQWUsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN0RCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMEJBQTBCLE9BQU8sRUFBRSxFQUN6QyxNQUFNLEVBQUUsUUFBUSxJQUNiLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxPQUFPLEVBQUUsQ0FDUCxPQUFlLEVBQ2YsSUFBd0IsRUFDeEIsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDBCQUEwQixPQUFPLFVBQVUsRUFDakQsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxJQUNuQixNQUFNLEVBQ1Q7U0FDTCxDQUFDO1FBQ0Ysa0JBQWEsR0FBRztZQUNkOzs7Ozs7OztlQVFHO1lBQ0gsTUFBTSxFQUFFLENBQ04sT0FBZSxFQUNmLElBQW1CLEVBQ25CLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwwQkFBMEIsT0FBTyxVQUFVLEVBQ2pELE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksSUFDbkIsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxNQUFNLEVBQUUsQ0FDTixRQUFnQixFQUNoQixJQUFtQixFQUNuQixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMkJBQTJCLFFBQVEsRUFBRSxFQUMzQyxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUFDLFFBQWdCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdkQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDJCQUEyQixRQUFRLEVBQUUsRUFDM0MsTUFBTSxFQUFFLFFBQVEsSUFDYixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsZ0JBQWdCLEVBQUUsQ0FBQyxRQUFnQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ2pFLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwyQkFBMkIsUUFBUSxVQUFVLEVBQ25ELE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLFdBQU0sR0FBRztZQUNQOzs7Ozs7O2VBT0c7WUFDSCxJQUFJLEVBQUUsQ0FBQyxPQUFlLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDcEQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDBCQUEwQixPQUFPLFFBQVEsRUFDL0MsTUFBTSxFQUFFLEtBQUssSUFDVixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQ04sTUFBYyxFQUNkLElBS0MsRUFDRCxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLE1BQU0sRUFBRSxFQUN2QyxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLElBQ25CLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxNQUFNLEVBQUUsQ0FBQyxNQUFjLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDckQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLEVBQUUsRUFDdkMsTUFBTSxFQUFFLFFBQVEsSUFDYixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsYUFBYSxFQUFFLENBQ2IsTUFBYyxFQUNkLEtBQTZCLEVBQzdCLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxXQUFXLEVBQ2hELE1BQU0sRUFBRSxNQUFNLEVBQ2QsS0FBSyxFQUFFLEtBQUssSUFDVCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsYUFBYSxFQUFFLENBQ2IsTUFBYyxFQUNkLEtBQTZCLEVBQzdCLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxXQUFXLEVBQ2hELE1BQU0sRUFBRSxNQUFNLEVBQ2QsS0FBSyxFQUFFLEtBQUssSUFDVCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsVUFBVSxFQUFFLENBQUMsT0FBZSxFQUFFLElBQWMsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUMxRSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMEJBQTBCLE9BQU8sUUFBUSxFQUMvQyxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILFVBQVUsRUFBRSxDQUFDLE9BQWUsRUFBRSxJQUFjLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDMUUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDBCQUEwQixPQUFPLFFBQVEsRUFDL0MsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxVQUFVLEVBQUUsQ0FBQyxNQUFjLEVBQUUsSUFBYyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3pFLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxFQUFFLEVBQ3ZDLE1BQU0sRUFBRSxPQUFPLEVBQ2YsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksSUFDbkIsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILFFBQVEsRUFBRSxDQUFDLE1BQWMsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN2RCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLE1BQU0sRUFBRSxFQUN2QyxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGdCQUFnQixFQUFFLENBQ2hCLGdCQUF3QixFQUN4QixJQUFvQixFQUNwQixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsZ0NBQWdDLGdCQUFnQixFQUFFLEVBQ3hELE1BQU0sRUFBRSxPQUFPLEVBQ2YsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsZUFBZSxFQUFFLENBQUMsTUFBYyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQzlELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxlQUFlLEVBQ3BELE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsZ0JBQWdCLEVBQUUsQ0FDaEIsUUFBZ0IsRUFDaEIsSUFBb0IsRUFDcEIsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLGdDQUFnQyxRQUFRLEVBQUUsRUFDaEQsTUFBTSxFQUFFLE9BQU8sRUFDZixJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxhQUFhLEVBQUUsQ0FDYixPQUFlLEVBQ2YsSUFBaUIsRUFDakIsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDBCQUEwQixPQUFPLFlBQVksRUFDbkQsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxhQUFhLEVBQUUsQ0FDYixTQUFpQixFQUNqQixJQUFpQixFQUNqQixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNkJBQTZCLFNBQVMsRUFBRSxFQUM5QyxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLElBQ25CLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxXQUFXLEVBQUUsQ0FBQyxTQUFpQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQzdELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSw2QkFBNkIsU0FBUyxFQUFFLEVBQzlDLE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7U0FDTCxDQUFDO1FBQ0YsZ0JBQVcsR0FBRztZQUNaOzs7Ozs7OztlQVFHO1lBQ0gsSUFBSSxFQUFFLENBQUMsT0FBZSxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3BELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwwQkFBMEIsT0FBTyxRQUFRLEVBQy9DLE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsTUFBYyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3JELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxRQUFRLEVBQzdDLE1BQU0sRUFBRSxNQUFNLEVBQ2QsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQ04sTUFBYyxFQUNkLElBQTJCLEVBQzNCLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxRQUFRLEVBQzdDLE1BQU0sRUFBRSxPQUFPLEVBQ2YsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsTUFBYyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3JELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxRQUFRLEVBQzdDLE1BQU0sRUFBRSxRQUFRLElBQ2IsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLGlCQUFZLEdBQUc7WUFDYjs7Ozs7O2VBTUc7WUFDSCxJQUFJLEVBQUUsQ0FBQyxNQUFjLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDbkQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLFVBQVUsRUFDL0MsTUFBTSxFQUFFLEtBQUssSUFDVixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsTUFBYyxFQUFFLElBQVMsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNoRSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLE1BQU0sVUFBVSxFQUMvQyxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLElBQ25CLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxNQUFNLEVBQUUsQ0FDTixNQUFjLEVBQ2QsUUFBZ0IsRUFDaEIsSUFBUyxFQUNULFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxZQUFZLFFBQVEsRUFBRSxFQUMzRCxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLElBQ25CLE1BQU0sRUFDVDtTQUNMLENBQUM7UUFDRixnQkFBVyxHQUFHO1lBQ1o7Ozs7Ozs7ZUFPRztZQUNILElBQUksRUFBRSxDQUFDLE1BQWMsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNuRCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLE1BQU0sUUFBUSxFQUM3QyxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUFDLE1BQWMsRUFBRSxJQUFjLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDckUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLFFBQVEsRUFDN0MsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxJQUNuQixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsR0FBRyxFQUFFLENBQUMsTUFBYyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ2xELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxFQUFFLEVBQ3ZDLE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsTUFBYyxFQUFFLElBQWMsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNyRSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUseUJBQXlCLE1BQU0sRUFBRSxFQUN2QyxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLElBQ25CLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxNQUFNLEVBQUUsQ0FBQyxNQUFjLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDckQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLEVBQUUsRUFDdkMsTUFBTSxFQUFFLFFBQVEsSUFDYixNQUFNLEVBQ1Q7U0FDTCxDQUFDO1FBQ0Ysa0JBQWEsR0FBRztZQUNkOzs7Ozs7O2VBT0c7WUFDSCxJQUFJLEVBQUUsQ0FBQyxNQUFjLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDbkQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLFVBQVUsRUFDL0MsTUFBTSxFQUFFLEtBQUssSUFDVixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsTUFBYyxFQUFFLElBQWdCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdkUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLFVBQVUsRUFDL0MsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxJQUNuQixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsR0FBRyxFQUFFLENBQUMsUUFBZ0IsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNwRCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMkJBQTJCLFFBQVEsRUFBRSxFQUMzQyxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUFDLFFBQWdCLEVBQUUsSUFBZ0IsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN6RSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMkJBQTJCLFFBQVEsRUFBRSxFQUMzQyxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLElBQ25CLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxNQUFNLEVBQUUsQ0FBQyxRQUFnQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3ZELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwyQkFBMkIsUUFBUSxFQUFFLEVBQzNDLE1BQU0sRUFBRSxRQUFRLElBQ2IsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILFlBQVksRUFBRSxDQUFDLGFBQXFCLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDbEUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLDJCQUEyQixhQUFhLFdBQVcsRUFDekQsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtTQUNMLENBQUM7UUFDRix5QkFBb0IsR0FBRztZQUNyQjs7Ozs7OztlQU9HO1lBQ0gsSUFBSSxFQUFFLENBQUMsTUFBYyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ25ELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxVQUFVLEVBQy9DLE1BQU0sRUFBRSxLQUFLLElBQ1YsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUFDLE1BQWMsRUFBRSxJQUFnQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3ZFLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsTUFBTSxVQUFVLEVBQy9DLE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksSUFDbkIsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLGVBQVUsR0FBRztZQUNYOzs7Ozs7OztlQVFHO1lBQ0gsSUFBSSxFQUFFLENBQ0osSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLEtBQXdELEVBQ3hELFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLEVBQUUsRUFDM0QsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxFQUNaLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxNQUFNLEVBQUUsQ0FDTixJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsSUFBUyxFQUNULFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLEVBQUUsRUFDM0QsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsT0FBTyxFQUFFLENBQ1AsSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLEtBQXdELEVBQ3hELFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLFdBQVcsRUFDcEUsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxFQUNaLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxPQUFPLEVBQUUsQ0FDUCxJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsS0FNQyxFQUNELFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLFVBQVUsRUFDbkUsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxFQUNaLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxJQUFJLEVBQUUsQ0FDSixJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsS0FBYSxFQUNiLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLElBQUksS0FBSyxFQUFFLEVBQ3BFLE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILE1BQU0sRUFBRSxDQUNOLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixLQUFhLEVBQ2IsSUFBUyxFQUNULFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLElBQUksS0FBSyxFQUFFLEVBQ3BFLE1BQU0sRUFBRSxPQUFPLEVBQ2YsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILE1BQU0sRUFBRSxDQUNOLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixLQUFhLEVBQ2IsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsSUFBSSxLQUFLLEVBQUUsRUFDcEUsTUFBTSxFQUFFLFFBQVEsSUFDYixNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILEtBQUssRUFBRSxDQUNMLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixLQUFhLEVBQ2IsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsSUFBSSxLQUFLLFFBQVEsRUFDMUUsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsVUFBVSxFQUFFLENBQ1YsSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLElBQVMsRUFDVCxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsd0JBQXdCLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxFQUFFLEVBQ2hFLE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILFVBQVUsRUFBRSxDQUNWLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixJQUFTLEVBQ1QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHdCQUF3QixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsRUFBRSxFQUNoRSxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxVQUFVLEVBQUUsQ0FDVixJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsSUFBUyxFQUNULFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx3QkFBd0IsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLEVBQUUsRUFDaEUsTUFBTSxFQUFFLFFBQVEsRUFDaEIsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILGFBQWEsRUFBRSxDQUNiLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixJQUFTLEVBQ1QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHdCQUF3QixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsTUFBTSxFQUNwRSxNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxhQUFhLEVBQUUsQ0FDYixJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsSUFBUyxFQUNULFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx3QkFBd0IsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLE1BQU0sRUFDcEUsTUFBTSxFQUFFLFFBQVEsRUFDaEIsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILFNBQVMsRUFBRSxDQUNULElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixJQUFxQixFQUNyQixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsbUJBQW1CLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxXQUFXLElBQUksRUFBRSxFQUMxRSxNQUFNLEVBQUUsS0FBSyxFQUNiLE9BQU8sRUFBRSxJQUFJLElBQ1YsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxVQUFVLEVBQUUsQ0FDVixJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsS0FBYSxFQUNiLFlBQXlCLEVBQ3pCLFVBQWtCLEVBQ2xCLEtBQTJDLEVBQzNDLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLElBQUksS0FBSyxJQUFJLFlBQVksSUFBSSxVQUFVLEVBQUUsRUFDbEcsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxFQUNaLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxTQUFTLEVBQUUsQ0FDVCxJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsS0FBYSxFQUNiLFlBQXlCLEVBQ3pCLFVBQWtCLEVBQ2xCLFFBQWdCLEVBQ2hCLEtBQTJDLEVBQzNDLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLElBQUksS0FBSyxJQUFJLFlBQVksSUFBSSxVQUFVLElBQUksUUFBUSxFQUFFLEVBQzlHLE1BQU0sRUFBRSxNQUFNLEVBQ2QsS0FBSyxFQUFFLEtBQUssRUFDWixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsWUFBWSxFQUFFLENBQ1osSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLEtBQWEsRUFDYixZQUF5QixFQUN6QixVQUFrQixFQUNsQixRQUFnQixFQUNoQixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsbUJBQW1CLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxJQUFJLEtBQUssSUFBSSxZQUFZLElBQUksVUFBVSxJQUFJLFFBQVEsRUFBRSxFQUM5RyxNQUFNLEVBQUUsUUFBUSxFQUNoQixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsMEJBQTBCLEVBQUUsQ0FDMUIsSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLEtBQWEsRUFDYixZQUF5QixFQUN6QixVQUFrQixFQUNsQixLQUEyQyxFQUMzQyxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsbUJBQW1CLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxJQUFJLEtBQUssSUFBSSxZQUFZLElBQUksVUFBVSxVQUFVLEVBQzFHLE1BQU0sRUFBRSxLQUFLLEVBQ2IsS0FBSyxFQUFFLEtBQUssRUFDWixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtTQUNMLENBQUM7UUFDRixjQUFTLEdBQUc7WUFDVjs7Ozs7Ozs7ZUFRRztZQUNILElBQUksRUFBRSxDQUNKLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixRQUFnQixFQUNoQixLQUFzRSxFQUN0RSxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsbUJBQW1CLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxVQUFVLFFBQVEsRUFBRSxFQUM3RSxNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLEVBQ1osTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILE1BQU0sRUFBRSxDQUNOLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixRQUFnQixFQUNoQixJQUFTLEVBQ1QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsVUFBVSxRQUFRLEVBQUUsRUFDN0UsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsT0FBTyxFQUFFLENBQ1AsSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLFFBQWdCLEVBQ2hCLEtBQXNFLEVBQ3RFLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLFVBQVUsUUFBUSxXQUFXLEVBQ3RGLE1BQU0sRUFBRSxLQUFLLEVBQ2IsS0FBSyxFQUFFLEtBQUssRUFDWixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsT0FBTyxFQUFFLENBQ1AsSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLFFBQWdCLEVBQ2hCLEtBTUMsRUFDRCxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsbUJBQW1CLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxVQUFVLFFBQVEsVUFBVSxFQUNyRixNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLEVBQ1osTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILEtBQUssRUFBRSxDQUNMLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixRQUFnQixFQUNoQixLQUF3QyxFQUN4QyxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsbUJBQW1CLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxVQUFVLFFBQVEsUUFBUSxFQUNuRixNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLEVBQ1osTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILElBQUksRUFBRSxDQUNKLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixRQUFnQixFQUNoQixLQUFhLEVBQ2IsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLG1CQUFtQixJQUFJLElBQUksV0FBVyxJQUFJLFNBQVMsVUFBVSxRQUFRLElBQUksS0FBSyxFQUFFLEVBQ3RGLE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7ZUFRRztZQUNILE1BQU0sRUFBRSxDQUNOLElBQVksRUFDWixXQUFtQixFQUNuQixTQUFpQixFQUNqQixRQUFnQixFQUNoQixLQUFhLEVBQ2IsSUFBUyxFQUNULFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLFVBQVUsUUFBUSxJQUFJLEtBQUssRUFBRSxFQUN0RixNQUFNLEVBQUUsT0FBTyxFQUNmLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxNQUFNLEVBQUUsQ0FDTixJQUFZLEVBQ1osV0FBbUIsRUFDbkIsU0FBaUIsRUFDakIsUUFBZ0IsRUFDaEIsS0FBYSxFQUNiLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLFVBQVUsUUFBUSxJQUFJLEtBQUssRUFBRSxFQUN0RixNQUFNLEVBQUUsUUFBUSxJQUNiLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsS0FBSyxFQUFFLENBQ0wsSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLFFBQWdCLEVBQ2hCLEtBQWEsRUFDYixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsbUJBQW1CLElBQUksSUFBSSxXQUFXLElBQUksU0FBUyxVQUFVLFFBQVEsSUFBSSxLQUFLLFFBQVEsRUFDNUYsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7OztlQVFHO1lBQ0gsTUFBTSxFQUFFLENBQ04sSUFBWSxFQUNaLFdBQW1CLEVBQ25CLFNBQWlCLEVBQ2pCLFFBQWdCLEVBQ2hCLElBQXFCLEVBQ3JCLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxtQkFBbUIsSUFBSSxJQUFJLFdBQVcsSUFBSSxTQUFTLFVBQVUsUUFBUSxXQUFXLElBQUksRUFBRSxFQUM1RixNQUFNLEVBQUUsS0FBSyxFQUNiLE9BQU8sRUFBRSxJQUFJLElBQ1YsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLFdBQU0sR0FBRztZQUNQOzs7Ozs7O2VBT0c7WUFDSCxRQUFRLEVBQUUsQ0FDUixjQUFzQixFQUN0QixLQUEyQyxFQUMzQyxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsaUNBQWlDLGNBQWMsT0FBTyxFQUM1RCxNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLEVBQ1osTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsVUFBVSxFQUFFLENBQ1YsY0FBc0IsRUFDdEIsSUFBWSxFQUNaLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxpQ0FBaUMsY0FBYyxPQUFPLEVBQzVELE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLFFBQVEsRUFDMUIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsY0FBYyxFQUFFLENBQ2QsY0FBc0IsRUFDdEIsS0FBYSxFQUNiLFlBQXlCLEVBQ3pCLFVBQWtCLEVBQ2xCLEtBQTJDLEVBQzNDLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxpQ0FBaUMsY0FBYyxTQUFTLEtBQUssSUFBSSxZQUFZLElBQUksVUFBVSxFQUFFLEVBQ25HLE1BQU0sRUFBRSxLQUFLLEVBQ2IsS0FBSyxFQUFFLEtBQUssRUFDWixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxTQUFTLEVBQUUsQ0FDVCxjQUFzQixFQUN0QixJQUFxQixFQUNyQixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsaUNBQWlDLGNBQWMsZ0JBQWdCLElBQUksRUFBRSxFQUMzRSxNQUFNLEVBQUUsS0FBSyxFQUNiLE9BQU8sRUFBRSxJQUFJLElBQ1YsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGdCQUFnQixFQUFFLENBQ2hCLGNBQXNCLEVBQ3RCLFVBQWtCLEVBQ2xCLEtBQTJDLEVBQzNDLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxpQ0FBaUMsY0FBYyxXQUFXLFVBQVUsRUFBRSxFQUM1RSxNQUFNLEVBQUUsS0FBSyxFQUNiLEtBQUssRUFBRSxLQUFLLEVBQ1osTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsaUJBQWlCLEVBQUUsQ0FBQyxjQUFzQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3hFLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxpQ0FBaUMsY0FBYyxPQUFPLEVBQzVELE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsYUFBYSxFQUFFLENBQUMsY0FBc0IsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNwRSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsaUNBQWlDLGNBQWMsT0FBTyxFQUM1RCxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLFVBQUssR0FBRztZQUNOOzs7Ozs7O2VBT0c7WUFDSCxXQUFXLEVBQUUsQ0FDWCxLQUF1RSxFQUN2RSxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsaUNBQWlDLEVBQ3ZDLE1BQU0sRUFBRSxLQUFLLEVBQ2IsS0FBSyxFQUFFLEtBQUssRUFDWixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxVQUFVLEVBQUUsQ0FDVixJQUE4RCxFQUM5RCxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsaUNBQWlDLEVBQ3ZDLE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksSUFDbkIsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILFlBQVksRUFBRSxDQUNaLEtBQTBDLEVBQzFDLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx1Q0FBdUMsRUFDN0MsTUFBTSxFQUFFLEtBQUssRUFDYixLQUFLLEVBQUUsS0FBSyxFQUNaLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGNBQWMsRUFBRSxDQUNkLEtBQWEsRUFDYixJQU1DLEVBQ0QsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLCtCQUErQixLQUFLLFNBQVMsRUFDbkQsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxJQUNuQixNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsY0FBYyxFQUFFLENBQUMsSUFBUyxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3hELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxpQ0FBaUMsRUFDdkMsTUFBTSxFQUFFLE1BQU0sRUFDZCxJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxPQUFPLEVBQUUsQ0FBQyxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN0QyxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNkJBQTZCLEVBQ25DLE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsZ0JBQWdCLEVBQUUsQ0FBQyxJQUFZLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDN0QsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLGtDQUFrQyxFQUN4QyxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILFVBQVUsRUFBRSxDQUFDLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3pDLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSxpQkFBaUIsRUFDdkIsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxTQUFTLEVBQUUsQ0FBQyxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN4QyxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsZ0JBQWdCLEVBQ3RCLE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsUUFBUSxFQUFFLENBQUMsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDdkMsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHVCQUF1QixFQUM3QixNQUFNLEVBQUUsS0FBSyxJQUNWLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxXQUFXLEVBQUUsQ0FBQyxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUMxQyxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsdUJBQXVCLEVBQzdCLE1BQU0sRUFBRSxRQUFRLElBQ2IsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLG1CQUFjLEdBQUc7WUFDZjs7Ozs7OztlQU9HO1lBQ0gsSUFBSSxFQUFFLENBQUMsT0FBZSxFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3BELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwwQkFBMEIsT0FBTyxRQUFRLEVBQy9DLE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsT0FBZSxFQUFFLElBQWUsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN2RSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMEJBQTBCLE9BQU8sUUFBUSxFQUMvQyxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILElBQUksRUFBRSxDQUNKLE9BQWUsRUFDZixJQUErRCxFQUMvRCxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMEJBQTBCLE9BQU8sYUFBYSxFQUNwRCxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLEVBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILGdCQUFnQixFQUFFLENBQ2hCLE9BQWUsRUFDZixTQUF5QyxFQUN6QyxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFJVixJQUFJLEVBQUUsMEJBQTBCLE9BQU8sd0JBQXdCLFNBQVMsRUFBRSxFQUMxRSxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUFDLE1BQWMsRUFBRSxJQUFjLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDckUsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLEVBQUUsRUFDdkMsTUFBTSxFQUFFLE9BQU8sRUFDZixJQUFJLEVBQUUsSUFBSSxFQUNWLElBQUksRUFBRSxXQUFXLENBQUMsSUFBSSxFQUN0QixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxNQUFNLEVBQUUsQ0FBQyxNQUFjLEVBQUUsU0FBd0IsRUFBRSxFQUFFLEVBQUUsQ0FDckQsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLHlCQUF5QixNQUFNLEVBQUUsRUFDdkMsTUFBTSxFQUFFLFFBQVEsSUFDYixNQUFNLEVBQ1Q7U0FDTCxDQUFDO1FBQ0YsV0FBTSxHQUFHO1lBQ1A7Ozs7Ozs7ZUFPRztZQUNILElBQUksRUFBRSxDQUFDLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ25DLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSx5QkFBeUIsRUFDL0IsTUFBTSxFQUFFLEtBQUssRUFDYixNQUFNLEVBQUUsTUFBTSxJQUNYLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxNQUFNLEVBQUUsQ0FBQyxXQUFtQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQzFELElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwyQkFBMkIsV0FBVyxTQUFTLEVBQ3JELE1BQU0sRUFBRSxLQUFLLEVBQ2IsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7Ozs7O2VBU0c7WUFDSCxJQUFJLEVBQUUsQ0FDSixJQUFxRSxFQUNyRSxTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsOEJBQThCLEVBQ3BDLE1BQU0sRUFBRSxNQUFNLEVBQ2QsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsTUFBTSxFQUFFLENBQUMsUUFBZ0IsRUFBRSxJQUFnQixFQUFFLFNBQXdCLEVBQUUsRUFBRSxFQUFFLENBQ3pFLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwyQkFBMkIsUUFBUSxFQUFFLEVBQzNDLE1BQU0sRUFBRSxPQUFPLEVBQ2YsSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksRUFDdEIsTUFBTSxFQUFFLE1BQU0sSUFDWCxNQUFNLEVBQ1Q7WUFFSjs7Ozs7OztlQU9HO1lBQ0gsSUFBSSxFQUFFLENBQUMsUUFBZ0IsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUNyRCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsMkJBQTJCLFFBQVEsRUFBRSxFQUMzQyxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1NBQ0wsQ0FBQztRQUNGLGFBQVEsR0FBRztZQUNUOzs7Ozs7OztlQVFHO1lBQ0gsSUFBSSxFQUFFLENBQUMsU0FBaUIsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN0RCxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsYUFBYSxFQUN4RCxNQUFNLEVBQUUsS0FBSyxFQUNiLE1BQU0sRUFBRSxNQUFNLElBQ1gsTUFBTSxFQUNUO1lBRUo7Ozs7Ozs7O2VBUUc7WUFDSCxNQUFNLEVBQUUsQ0FDTixTQUFpQixFQUNqQixJQUE4QixFQUM5QixTQUF3QixFQUFFLEVBQzFCLEVBQUUsQ0FDRixJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsYUFBYSxFQUN4RCxNQUFNLEVBQUUsTUFBTSxFQUNkLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxJQUFJLElBQ25CLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxNQUFNLEVBQUUsQ0FBQyxTQUFpQixFQUFFLEtBQWEsRUFBRSxTQUF3QixFQUFFLEVBQUUsRUFBRSxDQUN2RSxJQUFJLENBQUMsT0FBTyxpQkFDVixJQUFJLEVBQUUsNEJBQTRCLFNBQVMsZUFBZSxLQUFLLEVBQUUsRUFDakUsTUFBTSxFQUFFLFFBQVEsSUFDYixNQUFNLEVBQ1Q7U0FDTCxDQUFDO1FBQ0YsWUFBTyxHQUFHO1lBQ1I7Ozs7Ozs7ZUFPRztZQUNILE1BQU0sRUFBRSxDQUNOLEtBQXVCLEVBQ3ZCLElBQW9DLEVBQ3BDLFNBQXdCLEVBQUUsRUFDMUIsRUFBRSxDQUNGLElBQUksQ0FBQyxPQUFPLGlCQUNWLElBQUksRUFBRSwyQkFBMkIsRUFDakMsTUFBTSxFQUFFLE1BQU0sRUFDZCxLQUFLLEVBQUUsS0FBSyxFQUNaLElBQUksRUFBRSxJQUFJLEVBQ1YsSUFBSSxFQUFFLFdBQVcsQ0FBQyxRQUFRLElBQ3ZCLE1BQU0sRUFDVDtZQUVKOzs7Ozs7O2VBT0c7WUFDSCxXQUFXLEVBQUUsQ0FDWCxLQUF1QixFQUN2QixJQUtHLEVBQ0gsU0FBd0IsRUFBRSxFQUMxQixFQUFFLENBQ0YsSUFBSSxDQUFDLE9BQU8saUJBQ1YsSUFBSSxFQUFFLGtDQUFrQyxFQUN4QyxNQUFNLEVBQUUsTUFBTSxFQUNkLEtBQUssRUFBRSxLQUFLLEVBQ1osSUFBSSxFQUFFLElBQUksRUFDVixJQUFJLEVBQUUsV0FBVyxDQUFDLElBQUksSUFDbkIsTUFBTSxFQUNUO1NBQ0wsQ0FBQztJQUNKLENBQUM7Q0FBQSJ9