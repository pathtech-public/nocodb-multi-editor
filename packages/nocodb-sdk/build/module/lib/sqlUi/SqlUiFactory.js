import { MssqlUi } from './MssqlUi';
import { MysqlUi } from './MysqlUi';
import { OracleUi } from './OracleUi';
import { PgUi } from './PgUi';
import { SqliteUi } from './SqliteUi';
// import {YugabyteUi} from "./YugabyteUi";
// import {TidbUi} from "./TidbUi";
// import {VitessUi} from "./VitessUi";
export class SqlUiFactory {
    static create(connectionConfig) {
        // connectionConfig.meta = connectionConfig.meta || {};
        // connectionConfig.meta.dbtype = connectionConfig.meta.dbtype || "";
        if (connectionConfig.client === 'mysql' ||
            connectionConfig.client === 'mysql2') {
            // if (connectionConfig.meta.dbtype === "tidb")
            //   return Tidb;
            // if (connectionConfig.meta.dbtype === "vitess")
            //   return Vitess;
            return MysqlUi;
        }
        if (connectionConfig.client === 'sqlite3') {
            return SqliteUi;
        }
        if (connectionConfig.client === 'mssql') {
            return MssqlUi;
        }
        if (connectionConfig.client === 'oracledb') {
            return OracleUi;
        }
        if (connectionConfig.client === 'pg') {
            // if (connectionConfig.meta.dbtype === "yugabyte")
            //   return Yugabyte;
            return PgUi;
        }
        throw new Error('Database not supported');
    }
}
/**
 * @copyright Copyright (c) 2021, Xgene Cloud Ltd
 *
 * @author Naveen MR <oof1lab@gmail.com>
 * @author Pranav C Balan <pranavxc@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU3FsVWlGYWN0b3J5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vc3JjL2xpYi9zcWxVaS9TcWxVaUZhY3RvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUEsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLFdBQVcsQ0FBQztBQUNwQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sV0FBVyxDQUFDO0FBQ3BDLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxZQUFZLENBQUM7QUFDdEMsT0FBTyxFQUFFLElBQUksRUFBRSxNQUFNLFFBQVEsQ0FBQztBQUM5QixPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sWUFBWSxDQUFDO0FBRXRDLDJDQUEyQztBQUMzQyxtQ0FBbUM7QUFDbkMsdUNBQXVDO0FBRXZDLE1BQU0sT0FBTyxZQUFZO0lBQ3ZCLE1BQU0sQ0FBQyxNQUFNLENBQUMsZ0JBQWdCO1FBQzVCLHVEQUF1RDtRQUN2RCxxRUFBcUU7UUFDckUsSUFDRSxnQkFBZ0IsQ0FBQyxNQUFNLEtBQUssT0FBTztZQUNuQyxnQkFBZ0IsQ0FBQyxNQUFNLEtBQUssUUFBUSxFQUNwQztZQUNBLCtDQUErQztZQUMvQyxpQkFBaUI7WUFDakIsaURBQWlEO1lBQ2pELG1CQUFtQjtZQUVuQixPQUFPLE9BQU8sQ0FBQztTQUNoQjtRQUVELElBQUksZ0JBQWdCLENBQUMsTUFBTSxLQUFLLFNBQVMsRUFBRTtZQUN6QyxPQUFPLFFBQVEsQ0FBQztTQUNqQjtRQUNELElBQUksZ0JBQWdCLENBQUMsTUFBTSxLQUFLLE9BQU8sRUFBRTtZQUN2QyxPQUFPLE9BQU8sQ0FBQztTQUNoQjtRQUNELElBQUksZ0JBQWdCLENBQUMsTUFBTSxLQUFLLFVBQVUsRUFBRTtZQUMxQyxPQUFPLFFBQVEsQ0FBQztTQUNqQjtRQUVELElBQUksZ0JBQWdCLENBQUMsTUFBTSxLQUFLLElBQUksRUFBRTtZQUNwQyxtREFBbUQ7WUFDbkQscUJBQXFCO1lBQ3JCLE9BQU8sSUFBSSxDQUFDO1NBQ2I7UUFFRCxNQUFNLElBQUksS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUM7SUFDNUMsQ0FBQztDQUNGO0FBd0JEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FxQkcifQ==