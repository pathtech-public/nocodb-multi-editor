import UITypes from '../UITypes';
import { IDType } from './index';
export declare class MssqlUi {
    static getNewTableColumns(): ({
        column_name: string;
        title: string;
        dt: string;
        dtx: string;
        ct: string;
        nrqd: boolean;
        rqd: boolean;
        ck: boolean;
        pk: boolean;
        un: boolean;
        ai: boolean;
        cdf: any;
        clen: any;
        np: any;
        ns: number;
        dtxp: string;
        dtxs: string;
        altered: number;
        uidt: string;
        uip: string;
        uicn: string;
        au?: undefined;
    } | {
        column_name: string;
        title: string;
        dt: string;
        dtx: string;
        ct: string;
        nrqd: boolean;
        rqd: boolean;
        ck: boolean;
        pk: boolean;
        un: boolean;
        ai: boolean;
        cdf: any;
        clen: number;
        np: any;
        ns: any;
        dtxp: string;
        dtxs: string;
        altered: number;
        uidt: string;
        uip: string;
        uicn: string;
        au?: undefined;
    } | {
        column_name: string;
        title: string;
        dt: string;
        dtx: string;
        ct: string;
        nrqd: boolean;
        rqd: boolean;
        ck: boolean;
        pk: boolean;
        un: boolean;
        ai: boolean;
        cdf: string;
        clen: number;
        np: any;
        ns: any;
        dtxp: string;
        dtxs: string;
        altered: number;
        uidt: UITypes;
        uip: string;
        uicn: string;
        au?: undefined;
    } | {
        column_name: string;
        title: string;
        dt: string;
        dtx: string;
        ct: string;
        nrqd: boolean;
        rqd: boolean;
        ck: boolean;
        pk: boolean;
        un: boolean;
        ai: boolean;
        au: boolean;
        cdf: string;
        clen: number;
        np: any;
        ns: any;
        dtxp: string;
        dtxs: string;
        altered: number;
        uidt: UITypes;
        uip: string;
        uicn: string;
    })[];
    static getNewColumn(suffix: any): {
        column_name: string;
        dt: string;
        dtx: string;
        ct: string;
        nrqd: boolean;
        rqd: boolean;
        ck: boolean;
        pk: boolean;
        un: boolean;
        ai: boolean;
        cdf: any;
        clen: number;
        np: any;
        ns: any;
        dtxp: string;
        dtxs: string;
        altered: number;
        uidt: string;
        uip: string;
        uicn: string;
    };
    static getDefaultLengthForDatatype(type: any): "" | 255;
    static getDefaultLengthIsDisabled(type: any): boolean;
    static getDefaultValueForDatatype(type: any): "" | "eg: ";
    static getDefaultScaleForDatatype(type: any): string;
    static colPropAIDisabled(col: any, columns: any): boolean;
    static colPropUNDisabled(_col: any): boolean;
    static onCheckboxChangeAI(col: any): void;
    static showScale(_columnObj: any): boolean;
    static removeUnsigned(columns: any): void;
    static columnEditable(colObj: any): boolean;
    static extractFunctionName(query: any): any;
    static extractProcedureName(query: any): any;
    static handleRawOutput(result: any, headers: any): any;
    static splitQueries(query: any): any;
    /**
     * if sql statement is SELECT - it limits to a number
     * @param args
     * @returns {string|*}
     */
    sanitiseQuery(args: any): any;
    static getColumnsFromJson(json: any, tn: any): any[];
    static isValidTimestamp(key: any, value: any): boolean;
    static isValidDate(value: any): boolean;
    static onCheckboxChangeAU(col: any): void;
    static colPropAuDisabled(col: any): boolean;
    static getAbstractType(col: any): any;
    static getUIType(col: any): any;
    static getDataTypeForUiType(col: {
        uidt: UITypes;
    }, idType?: IDType): {
        readonly dt: string;
        readonly [key: string]: any;
    };
    static getDataTypeListForUiType(col: any, idType?: IDType): string[];
    static getUnsupportedFnList(): any[];
}
/**
 * @copyright Copyright (c) 2021, Xgene Cloud Ltd
 *
 * @author Naveen MR <oof1lab@gmail.com>
 * @author Pranav C Balan <pranavxc@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
