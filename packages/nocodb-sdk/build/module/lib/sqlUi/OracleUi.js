export class OracleUi {
    static getNewTableColumns() {
        return [
            {
                column_name: 'id',
                title: 'Id',
                dt: 'integer',
                dtx: 'integer',
                ct: 'int(11)',
                nrqd: false,
                rqd: true,
                ck: false,
                pk: true,
                un: false,
                ai: false,
                cdf: null,
                clen: null,
                np: null,
                ns: null,
                dtxp: '',
                dtxs: '',
                altered: 1,
                uidt: 'ID',
                uip: '',
                uicn: '',
            },
            {
                column_name: 'title',
                title: 'Title',
                dt: 'varchar',
                dtx: 'specificType',
                ct: 'varchar(45)',
                nrqd: true,
                rqd: false,
                ck: false,
                pk: false,
                un: false,
                ai: false,
                cdf: null,
                clen: 45,
                np: null,
                ns: null,
                dtxp: '45',
                dtxs: '',
                altered: 1,
                uidt: 'SingleLineText',
                uip: '',
                uicn: '',
            },
            // {
            //  column_name: "created_at",
            //   dt: "timestamp",
            //   dtx: "specificType",
            //   ct: "varchar(45)",
            //   nrqd: true,
            //   rqd: false,
            //   ck: false,
            //   pk: false,
            //   un: false,
            //   ai: false,
            //   cdf: 'CURRENT_TIMESTAMP',
            //   clen: 45,
            //   np: null,
            //   ns: null,
            //   dtxp: '',
            //   dtxs: ''
            // },
            // {
            //  column_name: "updated_at",
            //   dt: "timestamp",
            //   dtx: "specificType",
            //   ct: "varchar(45)",
            //   nrqd: true,
            //   rqd: false,
            //   ck: false,
            //   pk: false,
            //   un: false,
            //   ai: false,
            //   cdf: 'CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP',
            //   clen: 45,
            //   np: null,
            //   ns: null,
            //   dtxp: '',
            //   dtxs: ''
            // }
        ];
    }
    static getNewColumn(suffix) {
        return {
            column_name: 'title' + suffix,
            dt: 'varchar',
            dtx: 'specificType',
            ct: 'varchar(45)',
            nrqd: true,
            rqd: false,
            ck: false,
            pk: false,
            un: false,
            ai: false,
            cdf: null,
            clen: 45,
            np: null,
            ns: null,
            dtxp: '45',
            dtxs: '',
            altered: 1,
            uidt: 'SingleLineText',
            uip: '',
            uicn: '',
        };
    }
    // static getDefaultLengthForDatatype(type) {
    //   switch (type) {
    //     case "int":
    //       return 11;
    //       break;
    //     case "tinyint":
    //       return 1;
    //       break;
    //     case "smallint":
    //       return 5;
    //       break;
    //
    //     case "mediumint":
    //       return 9;
    //       break;
    //     case "bigint":
    //       return 20;
    //       break;
    //     case "bit":
    //       return 64;
    //       break;
    //     case "boolean":
    //       return '';
    //       break;
    //     case "float":
    //       return 12;
    //       break;
    //     case "decimal":
    //       return 10;
    //       break;
    //     case "double":
    //       return 22;
    //       break;
    //     case "serial":
    //       return 20;
    //       break;
    //     case "date":
    //       return '';
    //       break;
    //     case "datetime":
    //     case "timestamp":
    //       return 6;
    //       break;
    //     case "time":
    //       return '';
    //       break;
    //     case "year":
    //       return '';
    //       break;
    //     case "char":
    //       return 255;
    //       break;
    //     case "varchar":
    //       return 45;
    //       break;
    //     case "nchar":
    //       return 255;
    //       break;
    //     case "text":
    //       return '';
    //       break;
    //     case "tinytext":
    //       return '';
    //       break;
    //     case "mediumtext":
    //       return '';
    //       break;
    //     case "longtext":
    //       return ''
    //       break;
    //     case "binary":
    //       return 255;
    //       break;
    //     case "varbinary":
    //       return 65500;
    //       break;
    //     case "blob":
    //       return '';
    //       break;
    //     case "tinyblob":
    //       return '';
    //       break;
    //     case "mediumblob":
    //       return '';
    //       break;
    //     case "longblob":
    //       return '';
    //       break;
    //     case "enum":
    //       return '\'a\',\'b\'';
    //       break;
    //     case "set":
    //       return '\'a\',\'b\'';
    //       break;
    //     case "geometry":
    //       return '';
    //     case "point":
    //       return '';
    //     case "linestring":
    //       return '';
    //     case "polygon":
    //       return '';
    //     case "multipoint":
    //       return '';
    //     case "multilinestring":
    //       return '';
    //     case "multipolygon":
    //       return '';
    //     case "json":
    //       return ''
    //       break;
    //
    //   }
    //
    // }
    static getDefaultLengthForDatatype(type) {
        switch (type) {
            default:
                return '';
        }
    }
    static getDefaultLengthIsDisabled(type) {
        switch (type) {
            case 'integer':
                return true;
            case 'bfile':
            case 'binary rowid':
            case 'binary double':
            case 'binary_float':
            case 'blob':
            case 'canoical':
            case 'cfile':
            case 'char':
            case 'clob':
            case 'content pointer':
            case 'contigous array':
            case 'date':
            case 'decimal':
            case 'double precision':
            case 'float':
            case 'interval day to second':
            case 'interval year to month':
            case 'lob pointer':
            case 'long':
            case 'long raw':
            case 'named collection':
            case 'named object':
            case 'nchar':
            case 'nclob':
            case 'number':
            case 'nvarchar2':
            case 'octet':
            case 'oid':
            case 'pointer':
            case 'raw':
            case 'real':
            case 'ref':
            case 'ref cursor':
            case 'rowid':
            case 'signed binary integer':
            case 'smallint':
            case 'table':
            case 'time':
            case 'time with tz':
            case 'timestamp':
            case 'timestamp with local time zone':
            case 'timestamp with local tz':
            case 'timestamp with timezone':
            case 'timestamp with tz':
            case 'unsigned binary integer':
            case 'urowid':
            case 'varchar':
            case 'varchar2':
            case 'varray':
            case 'varying array':
                return false;
        }
    }
    static getDefaultValueForDatatype(type) {
        switch (type) {
            default:
                return '';
        }
    }
    static getDefaultScaleForDatatype(type) {
        switch (type) {
            case 'integer':
            case 'bfile':
            case 'binary rowid':
            case 'binary double':
            case 'binary_float':
            case 'blob':
            case 'canoical':
            case 'cfile':
            case 'char':
            case 'clob':
            case 'content pointer':
            case 'contigous array':
            case 'date':
            case 'decimal':
            case 'double precision':
            case 'float':
            case 'interval day to second':
            case 'interval year to month':
            case 'lob pointer':
            case 'long':
            case 'long raw':
            case 'named collection':
            case 'named object':
            case 'nchar':
            case 'nclob':
            case 'number':
            case 'nvarchar2':
            case 'octet':
            case 'oid':
            case 'pointer':
            case 'raw':
            case 'real':
            case 'ref':
            case 'ref cursor':
            case 'rowid':
            case 'signed binary integer':
            case 'smallint':
            case 'table':
            case 'time':
            case 'time with tz':
            case 'timestamp':
            case 'timestamp with local time zone':
            case 'timestamp with local tz':
            case 'timestamp with timezone':
            case 'timestamp with tz':
            case 'unsigned binary integer':
            case 'urowid':
            case 'varchar':
            case 'varchar2':
            case 'varray':
            case 'varying array':
                return ' ';
        }
    }
    static colPropAIDisabled(col, columns) {
        // console.log(col);
        if (col.dt === 'int4' ||
            col.dt === 'integer' ||
            col.dt === 'bigint' ||
            col.dt === 'smallint') {
            for (let i = 0; i < columns.length; ++i) {
                if (columns[i].cn !== col.cn && columns[i].ai) {
                    return true;
                }
            }
            return false;
        }
        else {
            return true;
        }
    }
    static colPropUNDisabled(_col) {
        // console.log(col);
        return true;
        // if (col.dt === 'int' ||
        //   col.dt === 'tinyint' ||
        //   col.dt === 'smallint' ||
        //   col.dt === 'mediumint' ||
        //   col.dt === 'bigint') {
        //   return false;
        // } else {
        //   return true;
        // }
    }
    static onCheckboxChangeAI(col) {
        console.log(col);
        if (col.dt === 'int' ||
            col.dt === 'bigint' ||
            col.dt === 'smallint' ||
            col.dt === 'tinyint') {
            col.altered = col.altered || 2;
        }
        // if (!col.ai) {
        //   col.dtx = 'specificType'
        // } else {
        //   col.dtx = ''
        // }
    }
    static showScale(_columnObj) {
        return false;
    }
    static removeUnsigned(columns) {
        for (let i = 0; i < columns.length; ++i) {
            if (columns[i].altered === 1 &&
                !(columns[i].dt === 'int' ||
                    columns[i].dt === 'bigint' ||
                    columns[i].dt === 'tinyint' ||
                    columns[i].dt === 'smallint' ||
                    columns[i].dt === 'mediumint')) {
                columns[i].un = false;
                console.log('>> resetting unsigned value', columns[i].cn);
            }
            console.log(columns[i].cn);
        }
    }
    static columnEditable(colObj) {
        return colObj.tn !== '_evolutions' || colObj.tn !== 'nc_evolutions';
    }
    static extractFunctionName(query) {
        const reg = /^\s*CREATE\s+(?:OR\s+REPLACE\s*)?\s*FUNCTION\s+(?:[\w\d_]+\.)?([\w_\d]+)/i;
        const match = query.match(reg);
        return match && match[1];
    }
    static extractProcedureName(query) {
        const reg = /^\s*CREATE\s+(?:OR\s+REPLACE\s*)?\s*PROCEDURE\s+(?:[\w\d_]+\.)?([\w_\d]+)/i;
        const match = query.match(reg);
        return match && match[1];
    }
    static splitQueries(query) {
        /***
         * we are splitting based on semicolon
         * there are mechanism to escape semicolon within single/double quotes(string)
         */
        return query.match(/\b("[^"]*;[^"]*"|'[^']*;[^']*'|[^;])*;/g);
    }
    static onCheckboxChangeAU(col) {
        console.log(col);
        // if (1) {
        col.altered = col.altered || 2;
        // }
        // if (!col.ai) {
        //   col.dtx = 'specificType'
        // } else {
        //   col.dtx = ''
        // }
    }
    /**
     * if sql statement is SELECT - it limits to a number
     * @param args
     * @returns {string|*}
     */
    sanitiseQuery(args) {
        let q = args.query.trim().split(';');
        if (q[0].startsWith('Select')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else if (q[0].startsWith('select')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else if (q[0].startsWith('SELECT')) {
            q = q[0] + ` LIMIT 0,${args.limit ? args.limit : 100};`;
        }
        else {
            return args.query;
        }
        return q;
    }
    getColumnsFromJson(json, tn) {
        const columns = [];
        try {
            if (typeof json === 'object' && !Array.isArray(json)) {
                const keys = Object.keys(json);
                for (let i = 0; i < keys.length; ++i) {
                    switch (typeof json[keys[i]]) {
                        case 'number':
                            if (Number.isInteger(json[keys[i]])) {
                                columns.push({
                                    dp: null,
                                    tn,
                                    column_name: keys[i],
                                    cno: keys[i],
                                    dt: 'int',
                                    np: 10,
                                    ns: 0,
                                    clen: null,
                                    cop: 1,
                                    pk: false,
                                    nrqd: false,
                                    rqd: false,
                                    un: false,
                                    ct: 'int(11) unsigned',
                                    ai: false,
                                    unique: false,
                                    cdf: null,
                                    cc: '',
                                    csn: null,
                                    dtx: 'specificType',
                                    dtxp: '11',
                                    dtxs: 0,
                                    altered: 1,
                                });
                            }
                            else {
                                columns.push({
                                    dp: null,
                                    tn,
                                    column_name: keys[i],
                                    cno: keys[i],
                                    dt: 'float',
                                    np: 10,
                                    ns: 2,
                                    clen: null,
                                    cop: 1,
                                    pk: false,
                                    nrqd: false,
                                    rqd: false,
                                    un: false,
                                    ct: 'int(11) unsigned',
                                    ai: false,
                                    unique: false,
                                    cdf: null,
                                    cc: '',
                                    csn: null,
                                    dtx: 'specificType',
                                    dtxp: '11',
                                    dtxs: 2,
                                    altered: 1,
                                });
                            }
                            break;
                        case 'string':
                            if (json[keys[i]].length <= 255) {
                                columns.push({
                                    dp: null,
                                    tn,
                                    column_name: keys[i],
                                    cno: keys[i],
                                    dt: 'varchar',
                                    np: 45,
                                    ns: 0,
                                    clen: null,
                                    cop: 1,
                                    pk: false,
                                    nrqd: false,
                                    rqd: false,
                                    un: false,
                                    ct: 'int(11) unsigned',
                                    ai: false,
                                    unique: false,
                                    cdf: null,
                                    cc: '',
                                    csn: null,
                                    dtx: 'specificType',
                                    dtxp: '45',
                                    dtxs: 0,
                                    altered: 1,
                                });
                            }
                            else {
                                columns.push({
                                    dp: null,
                                    tn,
                                    column_name: keys[i],
                                    cno: keys[i],
                                    dt: 'text',
                                    np: null,
                                    ns: 0,
                                    clen: null,
                                    cop: 1,
                                    pk: false,
                                    nrqd: false,
                                    rqd: false,
                                    un: false,
                                    ct: 'int(11) unsigned',
                                    ai: false,
                                    unique: false,
                                    cdf: null,
                                    cc: '',
                                    csn: null,
                                    dtx: 'specificType',
                                    dtxp: null,
                                    dtxs: 0,
                                    altered: 1,
                                });
                            }
                            break;
                        case 'boolean':
                            columns.push({
                                dp: null,
                                tn,
                                column_name: keys[i],
                                cno: keys[i],
                                dt: 'boolean',
                                np: 3,
                                ns: 0,
                                clen: null,
                                cop: 1,
                                pk: false,
                                nrqd: false,
                                rqd: false,
                                un: false,
                                ct: 'int(11) unsigned',
                                ai: false,
                                unique: false,
                                cdf: null,
                                cc: '',
                                csn: null,
                                dtx: 'specificType',
                                dtxp: '1',
                                dtxs: 0,
                                altered: 1,
                            });
                            break;
                        case 'object':
                            columns.push({
                                dp: null,
                                tn,
                                column_name: keys[i],
                                cno: keys[i],
                                dt: 'json',
                                np: 3,
                                ns: 0,
                                clen: null,
                                cop: 1,
                                pk: false,
                                nrqd: false,
                                rqd: false,
                                un: false,
                                ct: 'int(11) unsigned',
                                ai: false,
                                unique: false,
                                cdf: null,
                                cc: '',
                                csn: null,
                                dtx: 'specificType',
                                dtxp: null,
                                dtxs: 0,
                                altered: 1,
                            });
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        catch (e) {
            console.log('Error in getColumnsFromJson', e);
        }
        return columns;
    }
    static colPropAuDisabled(_col) {
        return true;
    }
    static getAbstractType(col) {
        switch ((col.dt || col.dt).toLowerCase()) {
            case 'integer':
                return 'integer';
            case 'bfile':
            case 'binary rowid':
            case 'binary double':
            case 'binary_float':
                return 'string';
            case 'blob':
                return 'blob';
            case 'canoical':
            case 'cfile':
            case 'char':
            case 'clob':
            case 'content pointer':
            case 'contigous array':
                return 'string';
            case 'date':
                return 'date';
            case 'decimal':
            case 'double precision':
            case 'float':
                return 'float';
            case 'interval day to second':
            case 'interval year to month':
                return 'string';
            case 'lob pointer':
                return 'string';
            case 'long':
                return 'integer';
            case 'long raw':
                return 'string';
            case 'named collection':
            case 'named object':
            case 'nchar':
            case 'nclob':
                return 'string';
            case 'nvarchar2':
            case 'octet':
            case 'oid':
            case 'pointer':
            case 'raw':
                return 'string';
            case 'real':
            case 'number':
                return 'float';
            case 'ref':
            case 'ref cursor':
            case 'rowid':
            case 'signed binary integer':
                return 'string';
            case 'smallint':
                return 'integer';
            case 'table':
                return 'string';
            case 'time':
            case 'time with tz':
                return 'time';
            case 'timestamp':
            case 'timestamp with local time zone':
            case 'timestamp with local tz':
            case 'timestamp with timezone':
            case 'timestamp with tz':
                return 'datetime';
            case 'unsigned binary integer':
            case 'urowid':
            case 'varchar':
            case 'varchar2':
                return 'string';
            case 'varray':
            case 'varying array':
                return 'string';
        }
    }
    static getUIType(col) {
        switch (this.getAbstractType(col)) {
            case 'integer':
                return 'Number';
            case 'boolean':
                return 'Checkbox';
            case 'float':
                return 'Decimal';
            case 'date':
                return 'Date';
            case 'datetime':
                return 'CreateTime';
            case 'time':
                return 'Time';
            case 'year':
                return 'Year';
            case 'string':
                return 'SingleLineText';
            case 'text':
                return 'LongText';
            case 'blob':
                return 'Attachment';
            case 'enum':
                return 'SingleSelect';
            case 'set':
                return 'MultiSelect';
            case 'json':
                return 'LongText';
        }
    }
    static getDataTypeForUiType(col, idType) {
        const colProp = {};
        switch (col.uidt) {
            case 'ID':
                {
                    const isAutoIncId = idType === 'AI';
                    const isAutoGenId = idType === 'AG';
                    colProp.dt = isAutoGenId ? 'varchar' : 'integer';
                    colProp.pk = true;
                    colProp.un = isAutoIncId;
                    colProp.ai = isAutoIncId;
                    colProp.rqd = true;
                    colProp.meta = isAutoGenId ? { ag: 'nc' } : undefined;
                }
                break;
            case 'ForeignKey':
                colProp.dt = 'varchar';
                break;
            case 'SingleLineText':
                colProp.dt = 'varchar';
                break;
            case 'LongText':
                colProp.dt = 'clob';
                break;
            case 'Attachment':
                colProp.dt = 'clob';
                break;
            case 'Checkbox':
                colProp.dt = 'tinyint';
                colProp.dtxp = 1;
                break;
            case 'MultiSelect':
                colProp.dt = 'varchar2';
                break;
            case 'SingleSelect':
                colProp.dt = 'varchar2';
                break;
            case 'Collaborator':
                colProp.dt = 'varchar';
                break;
            case 'Date':
                colProp.dt = 'varchar';
                break;
            case 'Year':
                colProp.dt = 'year';
                break;
            case 'Time':
                colProp.dt = 'time';
                break;
            case 'PhoneNumber':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isMobilePhone'],
                    args: [''],
                    msg: ['Validation failed : isMobilePhone'],
                };
                break;
            case 'Email':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isEmail'],
                    args: [''],
                    msg: ['Validation failed : isEmail'],
                };
                break;
            case 'URL':
                colProp.dt = 'varchar';
                colProp.validate = {
                    func: ['isURL'],
                    args: [''],
                    msg: ['Validation failed : isURL'],
                };
                break;
            case 'Number':
                colProp.dt = 'integer';
                break;
            case 'Decimal':
                colProp.dt = 'decimal';
                break;
            case 'Currency':
                colProp.dt = 'decimal';
                colProp.validate = {
                    func: ['isCurrency'],
                    args: [''],
                    msg: ['Validation failed : isCurrency'],
                };
                break;
            case 'Percent':
                colProp.dt = 'double';
                break;
            case 'Duration':
                colProp.dt = 'integer';
                break;
            case 'Rating':
                colProp.dt = 'integer';
                break;
            case 'Formula':
                colProp.dt = 'varchar';
                break;
            case 'Rollup':
                colProp.dt = 'varchar';
                break;
            case 'Count':
                colProp.dt = 'integer';
                break;
            case 'Lookup':
                colProp.dt = 'varchar';
                break;
            case 'DateTime':
                colProp.dt = 'timestamp';
                break;
            case 'CreateTime':
                colProp.dt = 'timestamp';
                break;
            case 'LastModifiedTime':
                colProp.dt = 'timestamp';
                break;
            case 'AutoNumber':
                colProp.dt = 'integer';
                break;
            case 'Barcode':
                colProp.dt = 'varchar';
                break;
            case 'Button':
                colProp.dt = 'varchar';
                break;
            default:
                colProp.dt = 'varchar';
                break;
        }
        return colProp;
    }
    static getUnsupportedFnList() {
        return [];
    }
}
// module.exports = PgUiHelp;
/**
 * @copyright Copyright (c) 2021, Xgene Cloud Ltd
 *
 * @author Naveen MR <oof1lab@gmail.com>
 * @author Pranav C Balan <pranavxc@gmail.com>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiT3JhY2xlVWkuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvbGliL3NxbFVpL09yYWNsZVVpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBLE1BQU0sT0FBTyxRQUFRO0lBQ25CLE1BQU0sQ0FBQyxrQkFBa0I7UUFDdkIsT0FBTztZQUNMO2dCQUNFLFdBQVcsRUFBRSxJQUFJO2dCQUNqQixLQUFLLEVBQUUsSUFBSTtnQkFDWCxFQUFFLEVBQUUsU0FBUztnQkFDYixHQUFHLEVBQUUsU0FBUztnQkFDZCxFQUFFLEVBQUUsU0FBUztnQkFDYixJQUFJLEVBQUUsS0FBSztnQkFDWCxHQUFHLEVBQUUsSUFBSTtnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsSUFBSTtnQkFDUixFQUFFLEVBQUUsS0FBSztnQkFDVCxFQUFFLEVBQUUsS0FBSztnQkFDVCxHQUFHLEVBQUUsSUFBSTtnQkFDVCxJQUFJLEVBQUUsSUFBSTtnQkFDVixFQUFFLEVBQUUsSUFBSTtnQkFDUixFQUFFLEVBQUUsSUFBSTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixJQUFJLEVBQUUsRUFBRTtnQkFDUixPQUFPLEVBQUUsQ0FBQztnQkFDVixJQUFJLEVBQUUsSUFBSTtnQkFDVixHQUFHLEVBQUUsRUFBRTtnQkFDUCxJQUFJLEVBQUUsRUFBRTthQUNUO1lBQ0Q7Z0JBQ0UsV0FBVyxFQUFFLE9BQU87Z0JBQ3BCLEtBQUssRUFBRSxPQUFPO2dCQUNkLEVBQUUsRUFBRSxTQUFTO2dCQUNiLEdBQUcsRUFBRSxjQUFjO2dCQUNuQixFQUFFLEVBQUUsYUFBYTtnQkFDakIsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsR0FBRyxFQUFFLEtBQUs7Z0JBQ1YsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsRUFBRSxFQUFFLEtBQUs7Z0JBQ1QsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsRUFBRSxFQUFFLElBQUk7Z0JBQ1IsRUFBRSxFQUFFLElBQUk7Z0JBQ1IsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsT0FBTyxFQUFFLENBQUM7Z0JBQ1YsSUFBSSxFQUFFLGdCQUFnQjtnQkFDdEIsR0FBRyxFQUFFLEVBQUU7Z0JBQ1AsSUFBSSxFQUFFLEVBQUU7YUFDVDtZQUNELElBQUk7WUFDSiw4QkFBOEI7WUFDOUIscUJBQXFCO1lBQ3JCLHlCQUF5QjtZQUN6Qix1QkFBdUI7WUFDdkIsZ0JBQWdCO1lBQ2hCLGdCQUFnQjtZQUNoQixlQUFlO1lBQ2YsZUFBZTtZQUNmLGVBQWU7WUFDZixlQUFlO1lBQ2YsOEJBQThCO1lBQzlCLGNBQWM7WUFDZCxjQUFjO1lBQ2QsY0FBYztZQUNkLGNBQWM7WUFDZCxhQUFhO1lBQ2IsS0FBSztZQUNMLElBQUk7WUFDSiw4QkFBOEI7WUFDOUIscUJBQXFCO1lBQ3JCLHlCQUF5QjtZQUN6Qix1QkFBdUI7WUFDdkIsZ0JBQWdCO1lBQ2hCLGdCQUFnQjtZQUNoQixlQUFlO1lBQ2YsZUFBZTtZQUNmLGVBQWU7WUFDZixlQUFlO1lBQ2YsMERBQTBEO1lBQzFELGNBQWM7WUFDZCxjQUFjO1lBQ2QsY0FBYztZQUNkLGNBQWM7WUFDZCxhQUFhO1lBQ2IsSUFBSTtTQUNMLENBQUM7SUFDSixDQUFDO0lBRUQsTUFBTSxDQUFDLFlBQVksQ0FBQyxNQUFNO1FBQ3hCLE9BQU87WUFDTCxXQUFXLEVBQUUsT0FBTyxHQUFHLE1BQU07WUFDN0IsRUFBRSxFQUFFLFNBQVM7WUFDYixHQUFHLEVBQUUsY0FBYztZQUNuQixFQUFFLEVBQUUsYUFBYTtZQUNqQixJQUFJLEVBQUUsSUFBSTtZQUNWLEdBQUcsRUFBRSxLQUFLO1lBQ1YsRUFBRSxFQUFFLEtBQUs7WUFDVCxFQUFFLEVBQUUsS0FBSztZQUNULEVBQUUsRUFBRSxLQUFLO1lBQ1QsRUFBRSxFQUFFLEtBQUs7WUFDVCxHQUFHLEVBQUUsSUFBSTtZQUNULElBQUksRUFBRSxFQUFFO1lBQ1IsRUFBRSxFQUFFLElBQUk7WUFDUixFQUFFLEVBQUUsSUFBSTtZQUNSLElBQUksRUFBRSxJQUFJO1lBQ1YsSUFBSSxFQUFFLEVBQUU7WUFDUixPQUFPLEVBQUUsQ0FBQztZQUNWLElBQUksRUFBRSxnQkFBZ0I7WUFDdEIsR0FBRyxFQUFFLEVBQUU7WUFDUCxJQUFJLEVBQUUsRUFBRTtTQUNULENBQUM7SUFDSixDQUFDO0lBRUQsNkNBQTZDO0lBQzdDLG9CQUFvQjtJQUNwQixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixzQkFBc0I7SUFDdEIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZix1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixFQUFFO0lBQ0Ysd0JBQXdCO0lBQ3hCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysb0JBQW9CO0lBQ3BCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2Ysc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YscUJBQXFCO0lBQ3JCLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLHdCQUF3QjtJQUN4QixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixvQkFBb0I7SUFDcEIsZUFBZTtJQUNmLHNCQUFzQjtJQUN0QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLG9CQUFvQjtJQUNwQixvQkFBb0I7SUFDcEIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLHFCQUFxQjtJQUNyQixvQkFBb0I7SUFDcEIsZUFBZTtJQUNmLHdCQUF3QjtJQUN4QixzQkFBc0I7SUFDdEIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQiw4QkFBOEI7SUFDOUIsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsb0JBQW9CO0lBQ3BCLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLHNCQUFzQjtJQUN0QixtQkFBbUI7SUFDbkIseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsbUJBQW1CO0lBQ25CLDJCQUEyQjtJQUMzQixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsRUFBRTtJQUNGLE1BQU07SUFDTixFQUFFO0lBQ0YsSUFBSTtJQUVKLE1BQU0sQ0FBQywyQkFBMkIsQ0FBQyxJQUFJO1FBQ3JDLFFBQVEsSUFBSSxFQUFFO1lBQ1o7Z0JBQ0UsT0FBTyxFQUFFLENBQUM7U0FDYjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsMEJBQTBCLENBQUMsSUFBSTtRQUNwQyxRQUFRLElBQUksRUFBRTtZQUNaLEtBQUssU0FBUztnQkFDWixPQUFPLElBQUksQ0FBQztZQUNkLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxjQUFjLENBQUM7WUFDcEIsS0FBSyxlQUFlLENBQUM7WUFDckIsS0FBSyxjQUFjLENBQUM7WUFDcEIsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLGlCQUFpQixDQUFDO1lBQ3ZCLEtBQUssaUJBQWlCLENBQUM7WUFDdkIsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssa0JBQWtCLENBQUM7WUFDeEIsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLHdCQUF3QixDQUFDO1lBQzlCLEtBQUssd0JBQXdCLENBQUM7WUFDOUIsS0FBSyxhQUFhLENBQUM7WUFDbkIsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLGtCQUFrQixDQUFDO1lBQ3hCLEtBQUssY0FBYyxDQUFDO1lBQ3BCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssS0FBSyxDQUFDO1lBQ1gsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLEtBQUssQ0FBQztZQUNYLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyx1QkFBdUIsQ0FBQztZQUM3QixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxjQUFjLENBQUM7WUFDcEIsS0FBSyxXQUFXLENBQUM7WUFDakIsS0FBSyxnQ0FBZ0MsQ0FBQztZQUN0QyxLQUFLLHlCQUF5QixDQUFDO1lBQy9CLEtBQUsseUJBQXlCLENBQUM7WUFDL0IsS0FBSyxtQkFBbUIsQ0FBQztZQUN6QixLQUFLLHlCQUF5QixDQUFDO1lBQy9CLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLFVBQVUsQ0FBQztZQUNoQixLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUssZUFBZTtnQkFDbEIsT0FBTyxLQUFLLENBQUM7U0FDaEI7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLDBCQUEwQixDQUFDLElBQUk7UUFDcEMsUUFBUSxJQUFJLEVBQUU7WUFDWjtnQkFDRSxPQUFPLEVBQUUsQ0FBQztTQUNiO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQywwQkFBMEIsQ0FBQyxJQUFJO1FBQ3BDLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssY0FBYyxDQUFDO1lBQ3BCLEtBQUssZUFBZSxDQUFDO1lBQ3JCLEtBQUssY0FBYyxDQUFDO1lBQ3BCLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxpQkFBaUIsQ0FBQztZQUN2QixLQUFLLGlCQUFpQixDQUFDO1lBQ3ZCLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLGtCQUFrQixDQUFDO1lBQ3hCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyx3QkFBd0IsQ0FBQztZQUM5QixLQUFLLHdCQUF3QixDQUFDO1lBQzlCLEtBQUssYUFBYSxDQUFDO1lBQ25CLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxrQkFBa0IsQ0FBQztZQUN4QixLQUFLLGNBQWMsQ0FBQztZQUNwQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLFdBQVcsQ0FBQztZQUNqQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssS0FBSyxDQUFDO1lBQ1gsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLEtBQUssQ0FBQztZQUNYLEtBQUssTUFBTSxDQUFDO1lBQ1osS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLFlBQVksQ0FBQztZQUNsQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssdUJBQXVCLENBQUM7WUFDN0IsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxPQUFPLENBQUM7WUFDYixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssY0FBYyxDQUFDO1lBQ3BCLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssZ0NBQWdDLENBQUM7WUFDdEMsS0FBSyx5QkFBeUIsQ0FBQztZQUMvQixLQUFLLHlCQUF5QixDQUFDO1lBQy9CLEtBQUssbUJBQW1CLENBQUM7WUFDekIsS0FBSyx5QkFBeUIsQ0FBQztZQUMvQixLQUFLLFFBQVEsQ0FBQztZQUNkLEtBQUssU0FBUyxDQUFDO1lBQ2YsS0FBSyxVQUFVLENBQUM7WUFDaEIsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLGVBQWU7Z0JBQ2xCLE9BQU8sR0FBRyxDQUFDO1NBQ2Q7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsRUFBRSxPQUFPO1FBQ25DLG9CQUFvQjtRQUNwQixJQUNFLEdBQUcsQ0FBQyxFQUFFLEtBQUssTUFBTTtZQUNqQixHQUFHLENBQUMsRUFBRSxLQUFLLFNBQVM7WUFDcEIsR0FBRyxDQUFDLEVBQUUsS0FBSyxRQUFRO1lBQ25CLEdBQUcsQ0FBQyxFQUFFLEtBQUssVUFBVSxFQUNyQjtZQUNBLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxFQUFFO2dCQUN2QyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssR0FBRyxDQUFDLEVBQUUsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFO29CQUM3QyxPQUFPLElBQUksQ0FBQztpQkFDYjthQUNGO1lBQ0QsT0FBTyxLQUFLLENBQUM7U0FDZDthQUFNO1lBQ0wsT0FBTyxJQUFJLENBQUM7U0FDYjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsaUJBQWlCLENBQUMsSUFBSTtRQUMzQixvQkFBb0I7UUFDcEIsT0FBTyxJQUFJLENBQUM7UUFDWiwwQkFBMEI7UUFDMUIsNEJBQTRCO1FBQzVCLDZCQUE2QjtRQUM3Qiw4QkFBOEI7UUFDOUIsMkJBQTJCO1FBQzNCLGtCQUFrQjtRQUNsQixXQUFXO1FBQ1gsaUJBQWlCO1FBQ2pCLElBQUk7SUFDTixDQUFDO0lBRUQsTUFBTSxDQUFDLGtCQUFrQixDQUFDLEdBQUc7UUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNqQixJQUNFLEdBQUcsQ0FBQyxFQUFFLEtBQUssS0FBSztZQUNoQixHQUFHLENBQUMsRUFBRSxLQUFLLFFBQVE7WUFDbkIsR0FBRyxDQUFDLEVBQUUsS0FBSyxVQUFVO1lBQ3JCLEdBQUcsQ0FBQyxFQUFFLEtBQUssU0FBUyxFQUNwQjtZQUNBLEdBQUcsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUM7U0FDaEM7UUFFRCxpQkFBaUI7UUFDakIsNkJBQTZCO1FBQzdCLFdBQVc7UUFDWCxpQkFBaUI7UUFDakIsSUFBSTtJQUNOLENBQUM7SUFFRCxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQVU7UUFDekIsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQsTUFBTSxDQUFDLGNBQWMsQ0FBQyxPQUFPO1FBQzNCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxFQUFFO1lBQ3ZDLElBQ0UsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sS0FBSyxDQUFDO2dCQUN4QixDQUFDLENBQ0MsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxLQUFLO29CQUN2QixPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLFFBQVE7b0JBQzFCLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssU0FBUztvQkFDM0IsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxVQUFVO29CQUM1QixPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLFdBQVcsQ0FDOUIsRUFDRDtnQkFDQSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQztnQkFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDM0Q7WUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUM1QjtJQUNILENBQUM7SUFFRCxNQUFNLENBQUMsY0FBYyxDQUFDLE1BQU07UUFDMUIsT0FBTyxNQUFNLENBQUMsRUFBRSxLQUFLLGFBQWEsSUFBSSxNQUFNLENBQUMsRUFBRSxLQUFLLGVBQWUsQ0FBQztJQUN0RSxDQUFDO0lBRUQsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEtBQUs7UUFDOUIsTUFBTSxHQUFHLEdBQ1AsMkVBQTJFLENBQUM7UUFDOUUsTUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMvQixPQUFPLEtBQUssSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUVELE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLO1FBQy9CLE1BQU0sR0FBRyxHQUNQLDRFQUE0RSxDQUFDO1FBQy9FLE1BQU0sS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDL0IsT0FBTyxLQUFLLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUs7UUFDdkI7OztXQUdHO1FBQ0gsT0FBTyxLQUFLLENBQUMsS0FBSyxDQUFDLHlDQUF5QyxDQUFDLENBQUM7SUFDaEUsQ0FBQztJQUVELE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxHQUFHO1FBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDakIsV0FBVztRQUNYLEdBQUcsQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLE9BQU8sSUFBSSxDQUFDLENBQUM7UUFDL0IsSUFBSTtRQUVKLGlCQUFpQjtRQUNqQiw2QkFBNkI7UUFDN0IsV0FBVztRQUNYLGlCQUFpQjtRQUNqQixJQUFJO0lBQ04sQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxhQUFhLENBQUMsSUFBSTtRQUNoQixJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVyQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDN0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxZQUFZLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDO1NBQ3pEO2FBQU0sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3BDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsWUFBWSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQztTQUN6RDthQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNwQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLFlBQVksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUM7U0FDekQ7YUFBTTtZQUNMLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztTQUNuQjtRQUVELE9BQU8sQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVELGtCQUFrQixDQUFDLElBQUksRUFBRSxFQUFFO1FBQ3pCLE1BQU0sT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUVuQixJQUFJO1lBQ0YsSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNwRCxNQUFNLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUUvQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsRUFBRTtvQkFDcEMsUUFBUSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDNUIsS0FBSyxRQUFROzRCQUNYLElBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtnQ0FDbkMsT0FBTyxDQUFDLElBQUksQ0FBQztvQ0FDWCxFQUFFLEVBQUUsSUFBSTtvQ0FDUixFQUFFO29DQUNGLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO29DQUNwQixHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztvQ0FDWixFQUFFLEVBQUUsS0FBSztvQ0FDVCxFQUFFLEVBQUUsRUFBRTtvQ0FDTixFQUFFLEVBQUUsQ0FBQztvQ0FDTCxJQUFJLEVBQUUsSUFBSTtvQ0FDVixHQUFHLEVBQUUsQ0FBQztvQ0FDTixFQUFFLEVBQUUsS0FBSztvQ0FDVCxJQUFJLEVBQUUsS0FBSztvQ0FDWCxHQUFHLEVBQUUsS0FBSztvQ0FDVixFQUFFLEVBQUUsS0FBSztvQ0FDVCxFQUFFLEVBQUUsa0JBQWtCO29DQUN0QixFQUFFLEVBQUUsS0FBSztvQ0FDVCxNQUFNLEVBQUUsS0FBSztvQ0FDYixHQUFHLEVBQUUsSUFBSTtvQ0FDVCxFQUFFLEVBQUUsRUFBRTtvQ0FDTixHQUFHLEVBQUUsSUFBSTtvQ0FDVCxHQUFHLEVBQUUsY0FBYztvQ0FDbkIsSUFBSSxFQUFFLElBQUk7b0NBQ1YsSUFBSSxFQUFFLENBQUM7b0NBQ1AsT0FBTyxFQUFFLENBQUM7aUNBQ1gsQ0FBQyxDQUFDOzZCQUNKO2lDQUFNO2dDQUNMLE9BQU8sQ0FBQyxJQUFJLENBQUM7b0NBQ1gsRUFBRSxFQUFFLElBQUk7b0NBQ1IsRUFBRTtvQ0FDRixXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztvQ0FDcEIsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7b0NBQ1osRUFBRSxFQUFFLE9BQU87b0NBQ1gsRUFBRSxFQUFFLEVBQUU7b0NBQ04sRUFBRSxFQUFFLENBQUM7b0NBQ0wsSUFBSSxFQUFFLElBQUk7b0NBQ1YsR0FBRyxFQUFFLENBQUM7b0NBQ04sRUFBRSxFQUFFLEtBQUs7b0NBQ1QsSUFBSSxFQUFFLEtBQUs7b0NBQ1gsR0FBRyxFQUFFLEtBQUs7b0NBQ1YsRUFBRSxFQUFFLEtBQUs7b0NBQ1QsRUFBRSxFQUFFLGtCQUFrQjtvQ0FDdEIsRUFBRSxFQUFFLEtBQUs7b0NBQ1QsTUFBTSxFQUFFLEtBQUs7b0NBQ2IsR0FBRyxFQUFFLElBQUk7b0NBQ1QsRUFBRSxFQUFFLEVBQUU7b0NBQ04sR0FBRyxFQUFFLElBQUk7b0NBQ1QsR0FBRyxFQUFFLGNBQWM7b0NBQ25CLElBQUksRUFBRSxJQUFJO29DQUNWLElBQUksRUFBRSxDQUFDO29DQUNQLE9BQU8sRUFBRSxDQUFDO2lDQUNYLENBQUMsQ0FBQzs2QkFDSjs0QkFFRCxNQUFNO3dCQUVSLEtBQUssUUFBUTs0QkFDWCxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFFO2dDQUMvQixPQUFPLENBQUMsSUFBSSxDQUFDO29DQUNYLEVBQUUsRUFBRSxJQUFJO29DQUNSLEVBQUU7b0NBQ0YsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7b0NBQ3BCLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO29DQUNaLEVBQUUsRUFBRSxTQUFTO29DQUNiLEVBQUUsRUFBRSxFQUFFO29DQUNOLEVBQUUsRUFBRSxDQUFDO29DQUNMLElBQUksRUFBRSxJQUFJO29DQUNWLEdBQUcsRUFBRSxDQUFDO29DQUNOLEVBQUUsRUFBRSxLQUFLO29DQUNULElBQUksRUFBRSxLQUFLO29DQUNYLEdBQUcsRUFBRSxLQUFLO29DQUNWLEVBQUUsRUFBRSxLQUFLO29DQUNULEVBQUUsRUFBRSxrQkFBa0I7b0NBQ3RCLEVBQUUsRUFBRSxLQUFLO29DQUNULE1BQU0sRUFBRSxLQUFLO29DQUNiLEdBQUcsRUFBRSxJQUFJO29DQUNULEVBQUUsRUFBRSxFQUFFO29DQUNOLEdBQUcsRUFBRSxJQUFJO29DQUNULEdBQUcsRUFBRSxjQUFjO29DQUNuQixJQUFJLEVBQUUsSUFBSTtvQ0FDVixJQUFJLEVBQUUsQ0FBQztvQ0FDUCxPQUFPLEVBQUUsQ0FBQztpQ0FDWCxDQUFDLENBQUM7NkJBQ0o7aUNBQU07Z0NBQ0wsT0FBTyxDQUFDLElBQUksQ0FBQztvQ0FDWCxFQUFFLEVBQUUsSUFBSTtvQ0FDUixFQUFFO29DQUNGLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO29DQUNwQixHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztvQ0FDWixFQUFFLEVBQUUsTUFBTTtvQ0FDVixFQUFFLEVBQUUsSUFBSTtvQ0FDUixFQUFFLEVBQUUsQ0FBQztvQ0FDTCxJQUFJLEVBQUUsSUFBSTtvQ0FDVixHQUFHLEVBQUUsQ0FBQztvQ0FDTixFQUFFLEVBQUUsS0FBSztvQ0FDVCxJQUFJLEVBQUUsS0FBSztvQ0FDWCxHQUFHLEVBQUUsS0FBSztvQ0FDVixFQUFFLEVBQUUsS0FBSztvQ0FDVCxFQUFFLEVBQUUsa0JBQWtCO29DQUN0QixFQUFFLEVBQUUsS0FBSztvQ0FDVCxNQUFNLEVBQUUsS0FBSztvQ0FDYixHQUFHLEVBQUUsSUFBSTtvQ0FDVCxFQUFFLEVBQUUsRUFBRTtvQ0FDTixHQUFHLEVBQUUsSUFBSTtvQ0FDVCxHQUFHLEVBQUUsY0FBYztvQ0FDbkIsSUFBSSxFQUFFLElBQUk7b0NBQ1YsSUFBSSxFQUFFLENBQUM7b0NBQ1AsT0FBTyxFQUFFLENBQUM7aUNBQ1gsQ0FBQyxDQUFDOzZCQUNKOzRCQUVELE1BQU07d0JBRVIsS0FBSyxTQUFTOzRCQUNaLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0NBQ1gsRUFBRSxFQUFFLElBQUk7Z0NBQ1IsRUFBRTtnQ0FDRixXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztnQ0FDcEIsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7Z0NBQ1osRUFBRSxFQUFFLFNBQVM7Z0NBQ2IsRUFBRSxFQUFFLENBQUM7Z0NBQ0wsRUFBRSxFQUFFLENBQUM7Z0NBQ0wsSUFBSSxFQUFFLElBQUk7Z0NBQ1YsR0FBRyxFQUFFLENBQUM7Z0NBQ04sRUFBRSxFQUFFLEtBQUs7Z0NBQ1QsSUFBSSxFQUFFLEtBQUs7Z0NBQ1gsR0FBRyxFQUFFLEtBQUs7Z0NBQ1YsRUFBRSxFQUFFLEtBQUs7Z0NBQ1QsRUFBRSxFQUFFLGtCQUFrQjtnQ0FDdEIsRUFBRSxFQUFFLEtBQUs7Z0NBQ1QsTUFBTSxFQUFFLEtBQUs7Z0NBQ2IsR0FBRyxFQUFFLElBQUk7Z0NBQ1QsRUFBRSxFQUFFLEVBQUU7Z0NBQ04sR0FBRyxFQUFFLElBQUk7Z0NBQ1QsR0FBRyxFQUFFLGNBQWM7Z0NBQ25CLElBQUksRUFBRSxHQUFHO2dDQUNULElBQUksRUFBRSxDQUFDO2dDQUNQLE9BQU8sRUFBRSxDQUFDOzZCQUNYLENBQUMsQ0FBQzs0QkFDSCxNQUFNO3dCQUVSLEtBQUssUUFBUTs0QkFDWCxPQUFPLENBQUMsSUFBSSxDQUFDO2dDQUNYLEVBQUUsRUFBRSxJQUFJO2dDQUNSLEVBQUU7Z0NBQ0YsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7Z0NBQ3BCLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO2dDQUNaLEVBQUUsRUFBRSxNQUFNO2dDQUNWLEVBQUUsRUFBRSxDQUFDO2dDQUNMLEVBQUUsRUFBRSxDQUFDO2dDQUNMLElBQUksRUFBRSxJQUFJO2dDQUNWLEdBQUcsRUFBRSxDQUFDO2dDQUNOLEVBQUUsRUFBRSxLQUFLO2dDQUNULElBQUksRUFBRSxLQUFLO2dDQUNYLEdBQUcsRUFBRSxLQUFLO2dDQUNWLEVBQUUsRUFBRSxLQUFLO2dDQUNULEVBQUUsRUFBRSxrQkFBa0I7Z0NBQ3RCLEVBQUUsRUFBRSxLQUFLO2dDQUNULE1BQU0sRUFBRSxLQUFLO2dDQUNiLEdBQUcsRUFBRSxJQUFJO2dDQUNULEVBQUUsRUFBRSxFQUFFO2dDQUNOLEdBQUcsRUFBRSxJQUFJO2dDQUNULEdBQUcsRUFBRSxjQUFjO2dDQUNuQixJQUFJLEVBQUUsSUFBSTtnQ0FDVixJQUFJLEVBQUUsQ0FBQztnQ0FDUCxPQUFPLEVBQUUsQ0FBQzs2QkFDWCxDQUFDLENBQUM7NEJBQ0gsTUFBTTt3QkFFUjs0QkFDRSxNQUFNO3FCQUNUO2lCQUNGO2FBQ0Y7U0FDRjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1YsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUMvQztRQUNELE9BQU8sT0FBTyxDQUFDO0lBQ2pCLENBQUM7SUFFRCxNQUFNLENBQUMsaUJBQWlCLENBQUMsSUFBSTtRQUMzQixPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFRCxNQUFNLENBQUMsZUFBZSxDQUFDLEdBQUc7UUFDeEIsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFdBQVcsRUFBRSxFQUFFO1lBQ3hDLEtBQUssU0FBUztnQkFDWixPQUFPLFNBQVMsQ0FBQztZQUNuQixLQUFLLE9BQU8sQ0FBQztZQUNiLEtBQUssY0FBYyxDQUFDO1lBQ3BCLEtBQUssZUFBZSxDQUFDO1lBQ3JCLEtBQUssY0FBYztnQkFDakIsT0FBTyxRQUFRLENBQUM7WUFDbEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBQ2hCLEtBQUssVUFBVSxDQUFDO1lBQ2hCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssaUJBQWlCLENBQUM7WUFDdkIsS0FBSyxpQkFBaUI7Z0JBQ3BCLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssTUFBTTtnQkFDVCxPQUFPLE1BQU0sQ0FBQztZQUNoQixLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssa0JBQWtCLENBQUM7WUFDeEIsS0FBSyxPQUFPO2dCQUNWLE9BQU8sT0FBTyxDQUFDO1lBQ2pCLEtBQUssd0JBQXdCLENBQUM7WUFDOUIsS0FBSyx3QkFBd0I7Z0JBQzNCLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssYUFBYTtnQkFDaEIsT0FBTyxRQUFRLENBQUM7WUFDbEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sU0FBUyxDQUFDO1lBQ25CLEtBQUssVUFBVTtnQkFDYixPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLGtCQUFrQixDQUFDO1lBQ3hCLEtBQUssY0FBYyxDQUFDO1lBQ3BCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxPQUFPO2dCQUNWLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyxLQUFLLENBQUM7WUFDWCxLQUFLLFNBQVMsQ0FBQztZQUNmLEtBQUssS0FBSztnQkFDUixPQUFPLFFBQVEsQ0FBQztZQUNsQixLQUFLLE1BQU0sQ0FBQztZQUNaLEtBQUssUUFBUTtnQkFDWCxPQUFPLE9BQU8sQ0FBQztZQUNqQixLQUFLLEtBQUssQ0FBQztZQUNYLEtBQUssWUFBWSxDQUFDO1lBQ2xCLEtBQUssT0FBTyxDQUFDO1lBQ2IsS0FBSyx1QkFBdUI7Z0JBQzFCLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssVUFBVTtnQkFDYixPQUFPLFNBQVMsQ0FBQztZQUNuQixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxRQUFRLENBQUM7WUFDbEIsS0FBSyxNQUFNLENBQUM7WUFDWixLQUFLLGNBQWM7Z0JBQ2pCLE9BQU8sTUFBTSxDQUFDO1lBQ2hCLEtBQUssV0FBVyxDQUFDO1lBQ2pCLEtBQUssZ0NBQWdDLENBQUM7WUFDdEMsS0FBSyx5QkFBeUIsQ0FBQztZQUMvQixLQUFLLHlCQUF5QixDQUFDO1lBQy9CLEtBQUssbUJBQW1CO2dCQUN0QixPQUFPLFVBQVUsQ0FBQztZQUNwQixLQUFLLHlCQUF5QixDQUFDO1lBQy9CLEtBQUssUUFBUSxDQUFDO1lBQ2QsS0FBSyxTQUFTLENBQUM7WUFDZixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxRQUFRLENBQUM7WUFDbEIsS0FBSyxRQUFRLENBQUM7WUFDZCxLQUFLLGVBQWU7Z0JBQ2xCLE9BQU8sUUFBUSxDQUFDO1NBQ25CO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBRztRQUNsQixRQUFRLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDakMsS0FBSyxTQUFTO2dCQUNaLE9BQU8sUUFBUSxDQUFDO1lBQ2xCLEtBQUssU0FBUztnQkFDWixPQUFPLFVBQVUsQ0FBQztZQUNwQixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxTQUFTLENBQUM7WUFDbkIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBQ2hCLEtBQUssVUFBVTtnQkFDYixPQUFPLFlBQVksQ0FBQztZQUN0QixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxNQUFNLENBQUM7WUFDaEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sTUFBTSxDQUFDO1lBQ2hCLEtBQUssUUFBUTtnQkFDWCxPQUFPLGdCQUFnQixDQUFDO1lBQzFCLEtBQUssTUFBTTtnQkFDVCxPQUFPLFVBQVUsQ0FBQztZQUNwQixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxZQUFZLENBQUM7WUFDdEIsS0FBSyxNQUFNO2dCQUNULE9BQU8sY0FBYyxDQUFDO1lBQ3hCLEtBQUssS0FBSztnQkFDUixPQUFPLGFBQWEsQ0FBQztZQUN2QixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxVQUFVLENBQUM7U0FDckI7SUFDSCxDQUFDO0lBRUQsTUFBTSxDQUFDLG9CQUFvQixDQUFDLEdBQXNCLEVBQUUsTUFBZTtRQUNqRSxNQUFNLE9BQU8sR0FBUSxFQUFFLENBQUM7UUFDeEIsUUFBUSxHQUFHLENBQUMsSUFBSSxFQUFFO1lBQ2hCLEtBQUssSUFBSTtnQkFDUDtvQkFDRSxNQUFNLFdBQVcsR0FBRyxNQUFNLEtBQUssSUFBSSxDQUFDO29CQUNwQyxNQUFNLFdBQVcsR0FBRyxNQUFNLEtBQUssSUFBSSxDQUFDO29CQUNwQyxPQUFPLENBQUMsRUFBRSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7b0JBQ2pELE9BQU8sQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDO29CQUNsQixPQUFPLENBQUMsRUFBRSxHQUFHLFdBQVcsQ0FBQztvQkFDekIsT0FBTyxDQUFDLEVBQUUsR0FBRyxXQUFXLENBQUM7b0JBQ3pCLE9BQU8sQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDO29CQUNuQixPQUFPLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztpQkFDdkQ7Z0JBQ0QsTUFBTTtZQUNSLEtBQUssWUFBWTtnQkFDZixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsTUFBTTtZQUNSLEtBQUssZ0JBQWdCO2dCQUNuQixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsTUFBTTtZQUNSLEtBQUssVUFBVTtnQkFDYixPQUFPLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQztnQkFDcEIsTUFBTTtZQUNSLEtBQUssWUFBWTtnQkFDZixPQUFPLENBQUMsRUFBRSxHQUFHLE1BQU0sQ0FBQztnQkFDcEIsTUFBTTtZQUNSLEtBQUssVUFBVTtnQkFDYixPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsT0FBTyxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7Z0JBQ2pCLE1BQU07WUFDUixLQUFLLGFBQWE7Z0JBQ2hCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsVUFBVSxDQUFDO2dCQUN4QixNQUFNO1lBQ1IsS0FBSyxjQUFjO2dCQUNqQixPQUFPLENBQUMsRUFBRSxHQUFHLFVBQVUsQ0FBQztnQkFDeEIsTUFBTTtZQUNSLEtBQUssY0FBYztnQkFDakIsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBRXZCLE1BQU07WUFDUixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUixLQUFLLE1BQU07Z0JBQ1QsT0FBTyxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUM7Z0JBQ3BCLE1BQU07WUFDUixLQUFLLGFBQWE7Z0JBQ2hCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixPQUFPLENBQUMsUUFBUSxHQUFHO29CQUNqQixJQUFJLEVBQUUsQ0FBQyxlQUFlLENBQUM7b0JBQ3ZCLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQztvQkFDVixHQUFHLEVBQUUsQ0FBQyxtQ0FBbUMsQ0FBQztpQkFDM0MsQ0FBQztnQkFDRixNQUFNO1lBQ1IsS0FBSyxPQUFPO2dCQUNWLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixPQUFPLENBQUMsUUFBUSxHQUFHO29CQUNqQixJQUFJLEVBQUUsQ0FBQyxTQUFTLENBQUM7b0JBQ2pCLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQztvQkFDVixHQUFHLEVBQUUsQ0FBQyw2QkFBNkIsQ0FBQztpQkFDckMsQ0FBQztnQkFDRixNQUFNO1lBQ1IsS0FBSyxLQUFLO2dCQUNSLE9BQU8sQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFDO2dCQUN2QixPQUFPLENBQUMsUUFBUSxHQUFHO29CQUNqQixJQUFJLEVBQUUsQ0FBQyxPQUFPLENBQUM7b0JBQ2YsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDO29CQUNWLEdBQUcsRUFBRSxDQUFDLDJCQUEyQixDQUFDO2lCQUNuQyxDQUFDO2dCQUNGLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE9BQU8sQ0FBQyxRQUFRLEdBQUc7b0JBQ2pCLElBQUksRUFBRSxDQUFDLFlBQVksQ0FBQztvQkFDcEIsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDO29CQUNWLEdBQUcsRUFBRSxDQUFDLGdDQUFnQyxDQUFDO2lCQUN4QyxDQUFDO2dCQUNGLE1BQU07WUFDUixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxDQUFDLEVBQUUsR0FBRyxRQUFRLENBQUM7Z0JBQ3RCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLE9BQU87Z0JBQ1YsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsT0FBTyxDQUFDLEVBQUUsR0FBRyxXQUFXLENBQUM7Z0JBQ3pCLE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxDQUFDLEVBQUUsR0FBRyxXQUFXLENBQUM7Z0JBQ3pCLE1BQU07WUFDUixLQUFLLGtCQUFrQjtnQkFDckIsT0FBTyxDQUFDLEVBQUUsR0FBRyxXQUFXLENBQUM7Z0JBQ3pCLE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFNBQVM7Z0JBQ1osT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsT0FBTyxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUM7Z0JBQ3ZCLE1BQU07WUFDUjtnQkFDRSxPQUFPLENBQUMsRUFBRSxHQUFHLFNBQVMsQ0FBQztnQkFDdkIsTUFBTTtTQUNUO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDakIsQ0FBQztJQUVELE1BQU0sQ0FBQyxvQkFBb0I7UUFDekIsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDO0NBQ0Y7QUFFRCw2QkFBNkI7QUFDN0I7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQXFCRyJ9