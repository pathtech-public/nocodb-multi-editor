var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import jsep from 'jsep';
export const jsepCurlyHook = {
    name: 'curly',
    init(jsep) {
        jsep.hooks.add('gobble-token', function gobbleCurlyLiteral(env) {
            const OCURLY_CODE = 123; // {
            const CCURLY_CODE = 125; // }
            const { context } = env;
            if (!jsep.isIdentifierStart(context.code) &&
                context.code === OCURLY_CODE) {
                context.index += 1;
                const nodes = context.gobbleExpressions(CCURLY_CODE);
                if (context.code === CCURLY_CODE) {
                    context.index += 1;
                    env.node = {
                        type: jsep.IDENTIFIER,
                        name: nodes.map((node) => node.name).join(' '),
                    };
                    return env.node;
                }
                else {
                    context.throwError('Unclosed }');
                }
            }
        });
    },
};
export function substituteColumnAliasWithIdInFormula(formula, columns) {
    return __awaiter(this, void 0, void 0, function* () {
        const substituteId = (pt) => __awaiter(this, void 0, void 0, function* () {
            if (pt.type === 'CallExpression') {
                for (const arg of pt.arguments || []) {
                    yield substituteId(arg);
                }
            }
            else if (pt.type === 'Literal') {
                return;
            }
            else if (pt.type === 'Identifier') {
                const colNameOrId = pt.name;
                const column = columns.find((c) => c.id === colNameOrId ||
                    c.column_name === colNameOrId ||
                    c.title === colNameOrId);
                pt.name = '{' + column.id + '}';
            }
            else if (pt.type === 'BinaryExpression') {
                yield substituteId(pt.left);
                yield substituteId(pt.right);
            }
        });
        // register jsep curly hook
        jsep.plugins.register(jsepCurlyHook);
        const parsedFormula = jsep(formula);
        yield substituteId(parsedFormula);
        return jsepTreeToFormula(parsedFormula);
    });
}
export function substituteColumnIdWithAliasInFormula(formula, columns, rawFormula) {
    const substituteId = (pt, ptRaw) => {
        var _a;
        if (pt.type === 'CallExpression') {
            let i = 0;
            for (const arg of pt.arguments || []) {
                substituteId(arg, (_a = ptRaw === null || ptRaw === void 0 ? void 0 : ptRaw.arguments) === null || _a === void 0 ? void 0 : _a[i++]);
            }
        }
        else if (pt.type === 'Literal') {
            return;
        }
        else if (pt.type === 'Identifier') {
            const colNameOrId = pt === null || pt === void 0 ? void 0 : pt.name;
            const column = columns.find((c) => c.id === colNameOrId ||
                c.column_name === colNameOrId ||
                c.title === colNameOrId);
            pt.name = (column === null || column === void 0 ? void 0 : column.title) || (ptRaw === null || ptRaw === void 0 ? void 0 : ptRaw.name) || (pt === null || pt === void 0 ? void 0 : pt.name);
        }
        else if (pt.type === 'BinaryExpression') {
            substituteId(pt.left, ptRaw === null || ptRaw === void 0 ? void 0 : ptRaw.left);
            substituteId(pt.right, ptRaw === null || ptRaw === void 0 ? void 0 : ptRaw.right);
        }
    };
    // register jsep curly hook
    jsep.plugins.register(jsepCurlyHook);
    const parsedFormula = jsep(formula);
    const parsedRawFormula = rawFormula && jsep(rawFormula);
    substituteId(parsedFormula, parsedRawFormula);
    return jsepTreeToFormula(parsedFormula);
}
export function jsepTreeToFormula(node) {
    if (node.type === 'BinaryExpression' || node.type === 'LogicalExpression') {
        return ('(' +
            jsepTreeToFormula(node.left) +
            ' ' +
            node.operator +
            ' ' +
            jsepTreeToFormula(node.right) +
            ')');
    }
    if (node.type === 'UnaryExpression') {
        return node.operator + jsepTreeToFormula(node.argument);
    }
    if (node.type === 'MemberExpression') {
        return (jsepTreeToFormula(node.object) +
            '[' +
            jsepTreeToFormula(node.property) +
            ']');
    }
    if (node.type === 'Identifier') {
        const formulas = [
            'AVG',
            'ADD',
            'DATEADD',
            'WEEKDAY',
            'AND',
            'OR',
            'CONCAT',
            'TRIM',
            'UPPER',
            'LOWER',
            'LEN',
            'MIN',
            'MAX',
            'CEILING',
            'FLOOR',
            'ROUND',
            'MOD',
            'REPEAT',
            'LOG',
            'EXP',
            'POWER',
            'SQRT',
            'SQRT',
            'ABS',
            'NOW',
            'REPLACE',
            'SEARCH',
            'INT',
            'RIGHT',
            'LEFT',
            'SUBSTR',
            'MID',
            'IF',
            'SWITCH',
            'URL',
        ];
        if (!formulas.includes(node.name))
            return '{' + node.name + '}';
        return node.name;
    }
    if (node.type === 'Literal') {
        if (typeof node.value === 'string') {
            return '"' + node.value + '"';
        }
        return '' + node.value;
    }
    if (node.type === 'CallExpression') {
        return (jsepTreeToFormula(node.callee) +
            '(' +
            node.arguments.map(jsepTreeToFormula).join(', ') +
            ')');
    }
    if (node.type === 'ArrayExpression') {
        return '[' + node.elements.map(jsepTreeToFormula).join(', ') + ']';
    }
    if (node.type === 'Compound') {
        return node.body.map((e) => jsepTreeToFormula(e)).join(' ');
    }
    if (node.type === 'ConditionalExpression') {
        return (jsepTreeToFormula(node.test) +
            ' ? ' +
            jsepTreeToFormula(node.consequent) +
            ' : ' +
            jsepTreeToFormula(node.alternate));
    }
    return '';
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybXVsYUhlbHBlcnMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvbGliL2Zvcm11bGFIZWxwZXJzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLE9BQU8sSUFBSSxNQUFNLE1BQU0sQ0FBQztBQUl4QixNQUFNLENBQUMsTUFBTSxhQUFhLEdBQUc7SUFDM0IsSUFBSSxFQUFFLE9BQU87SUFDYixJQUFJLENBQUMsSUFBSTtRQUNQLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxTQUFTLGtCQUFrQixDQUFDLEdBQUc7WUFDNUQsTUFBTSxXQUFXLEdBQUcsR0FBRyxDQUFDLENBQUMsSUFBSTtZQUM3QixNQUFNLFdBQVcsR0FBRyxHQUFHLENBQUMsQ0FBQyxJQUFJO1lBQzdCLE1BQU0sRUFBRSxPQUFPLEVBQUUsR0FBRyxHQUFHLENBQUM7WUFDeEIsSUFDRSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO2dCQUNyQyxPQUFPLENBQUMsSUFBSSxLQUFLLFdBQVcsRUFDNUI7Z0JBQ0EsT0FBTyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUM7Z0JBQ25CLE1BQU0sS0FBSyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDckQsSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLFdBQVcsRUFBRTtvQkFDaEMsT0FBTyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUM7b0JBQ25CLEdBQUcsQ0FBQyxJQUFJLEdBQUc7d0JBQ1QsSUFBSSxFQUFFLElBQUksQ0FBQyxVQUFVO3dCQUNyQixJQUFJLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7cUJBQy9DLENBQUM7b0JBQ0YsT0FBTyxHQUFHLENBQUMsSUFBSSxDQUFDO2lCQUNqQjtxQkFBTTtvQkFDTCxPQUFPLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO2lCQUNsQzthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0NBQ2MsQ0FBQztBQUVsQixNQUFNLFVBQWdCLG9DQUFvQyxDQUN4RCxPQUFPLEVBQ1AsT0FBcUI7O1FBRXJCLE1BQU0sWUFBWSxHQUFHLENBQU8sRUFBTyxFQUFFLEVBQUU7WUFDckMsSUFBSSxFQUFFLENBQUMsSUFBSSxLQUFLLGdCQUFnQixFQUFFO2dCQUNoQyxLQUFLLE1BQU0sR0FBRyxJQUFJLEVBQUUsQ0FBQyxTQUFTLElBQUksRUFBRSxFQUFFO29CQUNwQyxNQUFNLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDekI7YUFDRjtpQkFBTSxJQUFJLEVBQUUsQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO2dCQUNoQyxPQUFPO2FBQ1I7aUJBQU0sSUFBSSxFQUFFLENBQUMsSUFBSSxLQUFLLFlBQVksRUFBRTtnQkFDbkMsTUFBTSxXQUFXLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQztnQkFDNUIsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FDekIsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUNKLENBQUMsQ0FBQyxFQUFFLEtBQUssV0FBVztvQkFDcEIsQ0FBQyxDQUFDLFdBQVcsS0FBSyxXQUFXO29CQUM3QixDQUFDLENBQUMsS0FBSyxLQUFLLFdBQVcsQ0FDMUIsQ0FBQztnQkFDRixFQUFFLENBQUMsSUFBSSxHQUFHLEdBQUcsR0FBRyxNQUFNLENBQUMsRUFBRSxHQUFHLEdBQUcsQ0FBQzthQUNqQztpQkFBTSxJQUFJLEVBQUUsQ0FBQyxJQUFJLEtBQUssa0JBQWtCLEVBQUU7Z0JBQ3pDLE1BQU0sWUFBWSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDNUIsTUFBTSxZQUFZLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzlCO1FBQ0gsQ0FBQyxDQUFBLENBQUM7UUFDRiwyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDckMsTUFBTSxhQUFhLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3BDLE1BQU0sWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2xDLE9BQU8saUJBQWlCLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDMUMsQ0FBQztDQUFBO0FBRUQsTUFBTSxVQUFVLG9DQUFvQyxDQUNsRCxPQUFPLEVBQ1AsT0FBcUIsRUFDckIsVUFBVztJQUVYLE1BQU0sWUFBWSxHQUFHLENBQUMsRUFBTyxFQUFFLEtBQVcsRUFBRSxFQUFFOztRQUM1QyxJQUFJLEVBQUUsQ0FBQyxJQUFJLEtBQUssZ0JBQWdCLEVBQUU7WUFDaEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ1YsS0FBSyxNQUFNLEdBQUcsSUFBSSxFQUFFLENBQUMsU0FBUyxJQUFJLEVBQUUsRUFBRTtnQkFDcEMsWUFBWSxDQUFDLEdBQUcsRUFBRSxNQUFBLEtBQUssYUFBTCxLQUFLLHVCQUFMLEtBQUssQ0FBRSxTQUFTLDBDQUFHLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUM1QztTQUNGO2FBQU0sSUFBSSxFQUFFLENBQUMsSUFBSSxLQUFLLFNBQVMsRUFBRTtZQUNoQyxPQUFPO1NBQ1I7YUFBTSxJQUFJLEVBQUUsQ0FBQyxJQUFJLEtBQUssWUFBWSxFQUFFO1lBQ25DLE1BQU0sV0FBVyxHQUFHLEVBQUUsYUFBRixFQUFFLHVCQUFGLEVBQUUsQ0FBRSxJQUFJLENBQUM7WUFDN0IsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FDekIsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUNKLENBQUMsQ0FBQyxFQUFFLEtBQUssV0FBVztnQkFDcEIsQ0FBQyxDQUFDLFdBQVcsS0FBSyxXQUFXO2dCQUM3QixDQUFDLENBQUMsS0FBSyxLQUFLLFdBQVcsQ0FDMUIsQ0FBQztZQUNGLEVBQUUsQ0FBQyxJQUFJLEdBQUcsQ0FBQSxNQUFNLGFBQU4sTUFBTSx1QkFBTixNQUFNLENBQUUsS0FBSyxNQUFJLEtBQUssYUFBTCxLQUFLLHVCQUFMLEtBQUssQ0FBRSxJQUFJLENBQUEsS0FBSSxFQUFFLGFBQUYsRUFBRSx1QkFBRixFQUFFLENBQUUsSUFBSSxDQUFBLENBQUM7U0FDcEQ7YUFBTSxJQUFJLEVBQUUsQ0FBQyxJQUFJLEtBQUssa0JBQWtCLEVBQUU7WUFDekMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsS0FBSyxhQUFMLEtBQUssdUJBQUwsS0FBSyxDQUFFLElBQUksQ0FBQyxDQUFDO1lBQ25DLFlBQVksQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssYUFBTCxLQUFLLHVCQUFMLEtBQUssQ0FBRSxLQUFLLENBQUMsQ0FBQztTQUN0QztJQUNILENBQUMsQ0FBQztJQUVGLDJCQUEyQjtJQUMzQixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNyQyxNQUFNLGFBQWEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDcEMsTUFBTSxnQkFBZ0IsR0FBRyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3hELFlBQVksQ0FBQyxhQUFhLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztJQUM5QyxPQUFPLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxDQUFDO0FBQzFDLENBQUM7QUFFRCxNQUFNLFVBQVUsaUJBQWlCLENBQUMsSUFBSTtJQUNwQyxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssa0JBQWtCLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxtQkFBbUIsRUFBRTtRQUN6RSxPQUFPLENBQ0wsR0FBRztZQUNILGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDNUIsR0FBRztZQUNILElBQUksQ0FBQyxRQUFRO1lBQ2IsR0FBRztZQUNILGlCQUFpQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDN0IsR0FBRyxDQUNKLENBQUM7S0FDSDtJQUVELElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxpQkFBaUIsRUFBRTtRQUNuQyxPQUFPLElBQUksQ0FBQyxRQUFRLEdBQUcsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0tBQ3pEO0lBRUQsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLGtCQUFrQixFQUFFO1FBQ3BDLE9BQU8sQ0FDTCxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQzlCLEdBQUc7WUFDSCxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ2hDLEdBQUcsQ0FDSixDQUFDO0tBQ0g7SUFFRCxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssWUFBWSxFQUFFO1FBQzlCLE1BQU0sUUFBUSxHQUFHO1lBQ2YsS0FBSztZQUNMLEtBQUs7WUFDTCxTQUFTO1lBQ1QsU0FBUztZQUNULEtBQUs7WUFDTCxJQUFJO1lBQ0osUUFBUTtZQUNSLE1BQU07WUFDTixPQUFPO1lBQ1AsT0FBTztZQUNQLEtBQUs7WUFDTCxLQUFLO1lBQ0wsS0FBSztZQUNMLFNBQVM7WUFDVCxPQUFPO1lBQ1AsT0FBTztZQUNQLEtBQUs7WUFDTCxRQUFRO1lBQ1IsS0FBSztZQUNMLEtBQUs7WUFDTCxPQUFPO1lBQ1AsTUFBTTtZQUNOLE1BQU07WUFDTixLQUFLO1lBQ0wsS0FBSztZQUNMLFNBQVM7WUFDVCxRQUFRO1lBQ1IsS0FBSztZQUNMLE9BQU87WUFDUCxNQUFNO1lBQ04sUUFBUTtZQUNSLEtBQUs7WUFDTCxJQUFJO1lBQ0osUUFBUTtZQUNSLEtBQUs7U0FDTixDQUFDO1FBQ0YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUFFLE9BQU8sR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO1FBQ2hFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQztLQUNsQjtJQUVELElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxTQUFTLEVBQUU7UUFDM0IsSUFBSSxPQUFPLElBQUksQ0FBQyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQ2xDLE9BQU8sR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO1NBQy9CO1FBRUQsT0FBTyxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztLQUN4QjtJQUVELElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxnQkFBZ0IsRUFBRTtRQUNsQyxPQUFPLENBQ0wsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUM5QixHQUFHO1lBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2hELEdBQUcsQ0FDSixDQUFDO0tBQ0g7SUFFRCxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssaUJBQWlCLEVBQUU7UUFDbkMsT0FBTyxHQUFHLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDO0tBQ3BFO0lBRUQsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLFVBQVUsRUFBRTtRQUM1QixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztLQUM3RDtJQUVELElBQUksSUFBSSxDQUFDLElBQUksS0FBSyx1QkFBdUIsRUFBRTtRQUN6QyxPQUFPLENBQ0wsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUM1QixLQUFLO1lBQ0wsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUNsQyxLQUFLO1lBQ0wsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUNsQyxDQUFDO0tBQ0g7SUFFRCxPQUFPLEVBQUUsQ0FBQztBQUNaLENBQUMifQ==