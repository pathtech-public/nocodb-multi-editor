export function validatePassword(p) {
    let error = '';
    let progress = 0;
    let hint = null;
    let valid = true;
    if (!p) {
        error =
            'At least 8 letters with one Uppercase, one number and one special letter';
        valid = false;
    }
    else {
        if (!(p.length >= 8)) {
            error += 'Atleast 8 letters. ';
            valid = false;
        }
        else {
            progress = Math.min(100, progress + 25);
        }
        if (!p.match(/.*[A-Z].*/)) {
            error += 'One Uppercase Letter. ';
            valid = false;
        }
        else {
            progress = Math.min(100, progress + 25);
        }
        if (!p.match(/.*[0-9].*/)) {
            error += 'One Number. ';
            valid = false;
        }
        else {
            progress = Math.min(100, progress + 25);
        }
        if (!p.match(/[$&+,:;=?@#|'<>.^*()%!_-]/)) {
            error += 'One special letter. ';
            hint = "Allowed special character list :  $&+,:;=?@#|'<>.^*()%!_-";
            valid = false;
        }
        else {
            progress = Math.min(100, progress + 25);
        }
    }
    return { error, valid, progress, hint };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFzc3dvcmRIZWxwZXJzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9wYXNzd29yZEhlbHBlcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsTUFBTSxVQUFVLGdCQUFnQixDQUFDLENBQUM7SUFDaEMsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO0lBQ2YsSUFBSSxRQUFRLEdBQUcsQ0FBQyxDQUFDO0lBQ2pCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztJQUNoQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUM7SUFDakIsSUFBSSxDQUFDLENBQUMsRUFBRTtRQUNOLEtBQUs7WUFDSCwwRUFBMEUsQ0FBQztRQUM3RSxLQUFLLEdBQUcsS0FBSyxDQUFDO0tBQ2Y7U0FBTTtRQUNMLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLEVBQUU7WUFDcEIsS0FBSyxJQUFJLHFCQUFxQixDQUFDO1lBQy9CLEtBQUssR0FBRyxLQUFLLENBQUM7U0FDZjthQUFNO1lBQ0wsUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLFFBQVEsR0FBRyxFQUFFLENBQUMsQ0FBQztTQUN6QztRQUVELElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQ3pCLEtBQUssSUFBSSx3QkFBd0IsQ0FBQztZQUNsQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1NBQ2Y7YUFBTTtZQUNMLFFBQVEsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxRQUFRLEdBQUcsRUFBRSxDQUFDLENBQUM7U0FDekM7UUFFRCxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUN6QixLQUFLLElBQUksY0FBYyxDQUFDO1lBQ3hCLEtBQUssR0FBRyxLQUFLLENBQUM7U0FDZjthQUFNO1lBQ0wsUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLFFBQVEsR0FBRyxFQUFFLENBQUMsQ0FBQztTQUN6QztRQUVELElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLDJCQUEyQixDQUFDLEVBQUU7WUFDekMsS0FBSyxJQUFJLHNCQUFzQixDQUFDO1lBQ2hDLElBQUksR0FBRywyREFBMkQsQ0FBQztZQUNuRSxLQUFLLEdBQUcsS0FBSyxDQUFDO1NBQ2Y7YUFBTTtZQUNMLFFBQVEsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxRQUFRLEdBQUcsRUFBRSxDQUFDLENBQUM7U0FDekM7S0FDRjtJQUNELE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQztBQUMxQyxDQUFDIn0=