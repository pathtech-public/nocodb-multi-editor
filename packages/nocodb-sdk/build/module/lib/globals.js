export var ViewTypes;
(function (ViewTypes) {
    ViewTypes[ViewTypes["FORM"] = 1] = "FORM";
    ViewTypes[ViewTypes["GALLERY"] = 2] = "GALLERY";
    ViewTypes[ViewTypes["GRID"] = 3] = "GRID";
    ViewTypes[ViewTypes["KANBAN"] = 4] = "KANBAN";
})(ViewTypes || (ViewTypes = {}));
export var RelationTypes;
(function (RelationTypes) {
    RelationTypes["HAS_MANY"] = "hm";
    RelationTypes["BELONGS_TO"] = "bt";
    RelationTypes["MANY_TO_MANY"] = "mm";
})(RelationTypes || (RelationTypes = {}));
export var ExportTypes;
(function (ExportTypes) {
    ExportTypes["EXCEL"] = "excel";
    ExportTypes["CSV"] = "csv";
})(ExportTypes || (ExportTypes = {}));
export var ErrorMessages;
(function (ErrorMessages) {
    ErrorMessages["INVALID_SHARED_VIEW_PASSWORD"] = "INVALID_SHARED_VIEW_PASSWORD";
    ErrorMessages["NOT_IMPLEMENTED"] = "NOT_IMPLEMENTED";
})(ErrorMessages || (ErrorMessages = {}));
export var AuditOperationTypes;
(function (AuditOperationTypes) {
    AuditOperationTypes["COMMENT"] = "COMMENT";
    AuditOperationTypes["DATA"] = "DATA";
    AuditOperationTypes["PROJECT"] = "PROJECT";
    AuditOperationTypes["VIRTUAL_RELATION"] = "VIRTUAL_RELATION";
    AuditOperationTypes["RELATION"] = "RELATION";
    AuditOperationTypes["TABLE_VIEW"] = "TABLE_VIEW";
    AuditOperationTypes["TABLE"] = "TABLE";
    AuditOperationTypes["VIEW"] = "VIEW";
    AuditOperationTypes["META"] = "META";
    AuditOperationTypes["WEBHOOKS"] = "WEBHOOKS";
    AuditOperationTypes["AUTHENTICATION"] = "AUTHENTICATION";
    AuditOperationTypes["TABLE_COLUMN"] = "TABLE_COLUMN";
})(AuditOperationTypes || (AuditOperationTypes = {}));
export var AuditOperationSubTypes;
(function (AuditOperationSubTypes) {
    AuditOperationSubTypes["UPDATE"] = "UPDATE";
    AuditOperationSubTypes["INSERT"] = "INSERT";
    AuditOperationSubTypes["DELETE"] = "DELETE";
    AuditOperationSubTypes["CREATED"] = "CREATED";
    AuditOperationSubTypes["DELETED"] = "DELETED";
    AuditOperationSubTypes["RENAMED"] = "RENAMED";
    AuditOperationSubTypes["IMPORT_FROM_ZIP"] = "IMPORT_FROM_ZIP";
    AuditOperationSubTypes["EXPORT_TO_FS"] = "EXPORT_TO_FS";
    AuditOperationSubTypes["EXPORT_TO_ZIP"] = "EXPORT_TO_ZIP";
    AuditOperationSubTypes["UPDATED"] = "UPDATED";
    AuditOperationSubTypes["SIGNIN"] = "SIGNIN";
    AuditOperationSubTypes["SIGN"] = "SIGN";
    AuditOperationSubTypes["PASSWORD_RESET"] = "PASSWORD_RESET";
    AuditOperationSubTypes["PASSWORD_FORGOT"] = "PASSWORD_FORGOT";
    AuditOperationSubTypes["PASSWORD_CHANGE"] = "PASSWORD_CHANGE";
    AuditOperationSubTypes["EMAIL_VERIFICATION"] = "EMAIL_VERIFICATION";
    AuditOperationSubTypes["ROLES_MANAGEMENT"] = "ROLES_MANAGEMENT";
    AuditOperationSubTypes["INVITE"] = "INVITE";
    AuditOperationSubTypes["RESEND_INVITE"] = "RESEND_INVITE";
})(AuditOperationSubTypes || (AuditOperationSubTypes = {}));
export var PluginCategory;
(function (PluginCategory) {
    PluginCategory["STORAGE"] = "Storage";
    PluginCategory["EMAIL"] = "Email";
})(PluginCategory || (PluginCategory = {}));
export var ModelTypes;
(function (ModelTypes) {
    ModelTypes["TABLE"] = "table";
    ModelTypes["VIEW"] = "view";
})(ModelTypes || (ModelTypes = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2xvYmFscy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvZ2xvYmFscy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxNQUFNLENBQU4sSUFBWSxTQUtYO0FBTEQsV0FBWSxTQUFTO0lBQ25CLHlDQUFRLENBQUE7SUFDUiwrQ0FBVyxDQUFBO0lBQ1gseUNBQVEsQ0FBQTtJQUNSLDZDQUFVLENBQUE7QUFDWixDQUFDLEVBTFcsU0FBUyxLQUFULFNBQVMsUUFLcEI7QUFFRCxNQUFNLENBQU4sSUFBWSxhQUlYO0FBSkQsV0FBWSxhQUFhO0lBQ3ZCLGdDQUFlLENBQUE7SUFDZixrQ0FBaUIsQ0FBQTtJQUNqQixvQ0FBbUIsQ0FBQTtBQUNyQixDQUFDLEVBSlcsYUFBYSxLQUFiLGFBQWEsUUFJeEI7QUFFRCxNQUFNLENBQU4sSUFBWSxXQUdYO0FBSEQsV0FBWSxXQUFXO0lBQ3JCLDhCQUFlLENBQUE7SUFDZiwwQkFBVyxDQUFBO0FBQ2IsQ0FBQyxFQUhXLFdBQVcsS0FBWCxXQUFXLFFBR3RCO0FBRUQsTUFBTSxDQUFOLElBQVksYUFHWDtBQUhELFdBQVksYUFBYTtJQUN2Qiw4RUFBNkQsQ0FBQTtJQUM3RCxvREFBbUMsQ0FBQTtBQUNyQyxDQUFDLEVBSFcsYUFBYSxLQUFiLGFBQWEsUUFHeEI7QUFFRCxNQUFNLENBQU4sSUFBWSxtQkFhWDtBQWJELFdBQVksbUJBQW1CO0lBQzdCLDBDQUFtQixDQUFBO0lBQ25CLG9DQUFhLENBQUE7SUFDYiwwQ0FBbUIsQ0FBQTtJQUNuQiw0REFBcUMsQ0FBQTtJQUNyQyw0Q0FBcUIsQ0FBQTtJQUNyQixnREFBeUIsQ0FBQTtJQUN6QixzQ0FBZSxDQUFBO0lBQ2Ysb0NBQWEsQ0FBQTtJQUNiLG9DQUFhLENBQUE7SUFDYiw0Q0FBcUIsQ0FBQTtJQUNyQix3REFBaUMsQ0FBQTtJQUNqQyxvREFBNkIsQ0FBQTtBQUMvQixDQUFDLEVBYlcsbUJBQW1CLEtBQW5CLG1CQUFtQixRQWE5QjtBQUVELE1BQU0sQ0FBTixJQUFZLHNCQW9CWDtBQXBCRCxXQUFZLHNCQUFzQjtJQUNoQywyQ0FBaUIsQ0FBQTtJQUNqQiwyQ0FBaUIsQ0FBQTtJQUNqQiwyQ0FBaUIsQ0FBQTtJQUNqQiw2Q0FBbUIsQ0FBQTtJQUNuQiw2Q0FBbUIsQ0FBQTtJQUNuQiw2Q0FBbUIsQ0FBQTtJQUNuQiw2REFBbUMsQ0FBQTtJQUNuQyx1REFBNkIsQ0FBQTtJQUM3Qix5REFBK0IsQ0FBQTtJQUMvQiw2Q0FBbUIsQ0FBQTtJQUNuQiwyQ0FBaUIsQ0FBQTtJQUNqQix1Q0FBYSxDQUFBO0lBQ2IsMkRBQWlDLENBQUE7SUFDakMsNkRBQW1DLENBQUE7SUFDbkMsNkRBQW1DLENBQUE7SUFDbkMsbUVBQXlDLENBQUE7SUFDekMsK0RBQXFDLENBQUE7SUFDckMsMkNBQWlCLENBQUE7SUFDakIseURBQStCLENBQUE7QUFDakMsQ0FBQyxFQXBCVyxzQkFBc0IsS0FBdEIsc0JBQXNCLFFBb0JqQztBQUVELE1BQU0sQ0FBTixJQUFZLGNBR1g7QUFIRCxXQUFZLGNBQWM7SUFDeEIscUNBQW1CLENBQUE7SUFDbkIsaUNBQWUsQ0FBQTtBQUNqQixDQUFDLEVBSFcsY0FBYyxLQUFkLGNBQWMsUUFHekI7QUFFRCxNQUFNLENBQU4sSUFBWSxVQUdYO0FBSEQsV0FBWSxVQUFVO0lBQ3BCLDZCQUFlLENBQUE7SUFDZiwyQkFBYSxDQUFBO0FBQ2YsQ0FBQyxFQUhXLFVBQVUsS0FBVixVQUFVLFFBR3JCIn0=