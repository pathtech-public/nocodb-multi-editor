export var XcNotificationType;
(function (XcNotificationType) {
    XcNotificationType["EMAIL"] = "Email";
    XcNotificationType["URL"] = "URL";
    XcNotificationType["DISCORD"] = "Discord";
    XcNotificationType["TELEGRAM"] = "Telegram";
    XcNotificationType["SLACK"] = "Slack";
    XcNotificationType["WHATSAPP"] = "Whatsapp";
    XcNotificationType["TWILIO"] = "Twilio";
})(XcNotificationType || (XcNotificationType = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiWGNOb3RpZmljYXRpb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvbGliL1hjTm90aWZpY2F0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtBLE1BQU0sQ0FBTixJQUFZLGtCQVFYO0FBUkQsV0FBWSxrQkFBa0I7SUFDNUIscUNBQWUsQ0FBQTtJQUNmLGlDQUFXLENBQUE7SUFDWCx5Q0FBbUIsQ0FBQTtJQUNuQiwyQ0FBcUIsQ0FBQTtJQUNyQixxQ0FBZSxDQUFBO0lBQ2YsMkNBQXFCLENBQUE7SUFDckIsdUNBQWlCLENBQUE7QUFDbkIsQ0FBQyxFQVJXLGtCQUFrQixLQUFsQixrQkFBa0IsUUFRN0IifQ==